% Designed by: Johannes R�hm (FTM, Technical University of Munich)
%-------------
%Created on: 25.09.2018
% ------------
% Version: Matlab2017b
%-------------
% Description: Initialization of the BMW650i vehicle model, lateral
% dynamics calculated by a DOUBLE TRACK MODEL
% ------------

%% Set parameters
open_system('Querdynamikmodell_BMWF06_650i_ZSM');
Parameterfile_Querdynamikmodell_BMWF06_650i_ZSM;

% simulation parameters
stepsize = 5e-5;
savestep = 1e-3;
savedec = savestep / stepsize;

% clear model workspace
load_system('Querdynamikmodell_BMWF06_650i_ZSM');
mod_workspace = get_param('Querdynamikmodell_BMWF06_650i_ZSM', 'modelworkspace');
mod_workspace.clear;

%% INPUT DATA

% initialization of driving test data file
% if there is no driving test data file, comment out
[fname,pname] = uigetfile('*.mat');
addpath(pname);                                 
load(fname);

% simulation parameters
%simtime = 156; % for maneuver 3 
simtime = round((V_VEH_COG(end,1)-V_VEH_COG(1,1)).*10)/10;  % duration of the simulation / duration of driving test data [s]
input_time = (0 : stepsize: simtime)';                      % time step vector [s]

% initialization of driving test data
% if there is no driving test data, comment out variables and set corresponding simin blocks in Simulink to any constant value
current_gear= [ST_GRSEL_DRV(:,1)-ST_GRSEL_DRV(1,1),ST_GRSEL_DRV(:,2)];                          % gear
delta_maneuver = [STEA_FTAX_EFFV(:,1)-STEA_FTAX_EFFV(1,1),STEA_FTAX_EFFV(:,2)];                 % steering angle wheel
v_maneuver = [V_VEH_COG(:,1)-V_VEH_COG(1,1),V_VEH_COG(:,2)/3.6];                                % velocity
input_r = 30;   %input_r = [V_VEH_COG(:,1)-V_VEH_COG(1,1), 30 * ones(length(V_VEH_COG(:,1)), 1)];                % input radius


%% INITIAL CONDITIONS FOR SIMULATION
MuRoad=1;

v0=V_VEH_COG(1,2)/3.6;
set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','v0','v0');

psi0 = AngleHeading(1,2)*pi/180;                            % psi0 from driving test data [rad]
set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','INIT_psi','psi0');

psip0 = VYAW_VEH(1,2)*pi/180;                               % psip0 from driving test data [rad/s]
set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','psip0','psip0');

%phi0 = AngleRoll(1,2)*pi/180;                               % phi0 from driving test data [rad]
%theta0 = AnglePitch(1,2)*pi/180;                            % theta0 from driving test data [rad]
%beta0 = -AngleSlip(1,2)*pi/180;                             % beta0 from driving test data [rad]
%M_brems_maneuver = [AVL_BRTORQ_SUM(:,1)-AVL_BRTORQ_SUM(1,1),AVL_BRTORQ_SUM(:,2)];

%% Select maneuver
% 1: quasi-steady-state skidpad // CANWIN2_70_370
% 2: quasi-steady-state skidpad with loadcycle braking // CANWIN7_272_275
% 3: quasi-steady-state skidpad with loadcycle accelerating // CANWIN4_unpack (ausgewertet t=153-156s)
% 4: step steer // CANWIN9_44_48
% 5: waevetest // CANWIN11_90_115
SelectManeuver=1;

%%

% select between damper current:
% 1: fixed current 2: SPORT-regulation 3: COMFORT-regulation
SelectStrom = 2;
% posibility to stop simulation at a certain lateral acceleration during quasi-steady-state skidpad
activate_stop = 0;                                          % set 1 to stop at acceleration ay_stop
ay_stop = 10;                                               % [m/s^2] lateral acceleration to stop the simulation
par_VEH.isteer = [0.06015 0];