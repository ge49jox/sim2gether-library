% Designed by: Johannes R�hm (FTM, Technical University of Munich)
%-------------
% Created on: 25.09.2018
% ------------
% Version: Matlab2017b
%-------------
% Description: This script calculates the lateral stiffness of the tires from the simulation data of the double track model
% use steady state skidpad for double track simulation 
% inideces are dependent on the measurement data. Must be within a region of less than 4 m/s^2
% lateral acceleration and NOT within regions of of high oscillations (that are caused at low velocities due to the magic formula) 
% ------------

%% This script calculates the lateral stiffness of the tires from the simulation data of the double track model
%use steady state skidpad for double track simulation 
%inideces are dependent on the measurement data. Must be within a region of less than 4 m/s^2
%lateral acceleration and NOT within regions of of high oscillations (that are caused at low velocities due to the magic formula) 
start_index=120/0.001;
end_index=160/0.001;

%Get the relevant values of the lateral stiffness
c_alpha_LF_relevant=c_alpha_LF(start_index:end_index);
c_alpha_LR_relevant=c_alpha_LR(start_index:end_index);
c_alpha_RF_relevant=c_alpha_RF(start_index:end_index);
c_alpha_RR_relevant=c_alpha_RR(start_index:end_index);
%calculate the mean values for each tire and the mean of both front and rear tires
c_alpha_F=-(mean(c_alpha_LF_relevant)+mean(c_alpha_RF_relevant))/2;
c_alpha_R=-(mean(c_alpha_LR_relevant)+mean(c_alpha_RR_relevant))/2;


%% Calculation by delta_y/delta_x (between start_index and end_index)-->this method was used for semester thesis "Implementierung und Vergleich von Querdynamikmodellen in Gesamtfahrzeugsimulationen"
c_alpha_lf_new=(FsLF(end_index)-FsLF(start_index))/(alpha_LF(end_index)-alpha_LF(start_index));
c_alpha_rf_new=(FsRF(end_index)-FsRF(start_index))/(alpha_RF(end_index)-alpha_RF(start_index));
c_alpha_f_new=(c_alpha_lf_new+c_alpha_rf_new)/2;

c_alpha_lr_new=(FsLR(end_index)-FsLR(start_index))/(alpha_LR(end_index)-alpha_LR(start_index));
c_alpha_rr_new=(FsRR(end_index)-FsRR(start_index))/(alpha_RR(end_index)-alpha_RR(start_index));
c_alpha_r_new=(c_alpha_lr_new+c_alpha_rr_new)/2;