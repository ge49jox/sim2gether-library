%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameter File for Querdynamikmodell_BMWF06_650i_ZSM		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Querdynamikmodell_BMWF06_650i_ZSM/Control Unit/Control Unit Simple

	%Brake Distribution Front
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Control Unit/Control Unit Simple','Brake_Distribution_Front','0.62');

	%%%Maximal Braking Torque
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Control Unit/Control Unit Simple','MaxBrkTrq','4200');
    MaxBrkTrq=4200;

	%Maximal Motor Torque
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Control Unit/Control Unit Simple','MaxMotorTrq','650');  %Quelle: TD_BMW_6er_Coupe_650i_xDrive_07_2012


%% Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r

	%P-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r','P_S','45');

	%I-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r','I_S','45');

	%Activate Maximal lateral Acceleration
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r','activate_stop','off');

	%Maximal lateral Acceleration (simulation stops) [m/s^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r','ay_stop','15');


%% Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung

	%P-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung','P_S','45');

	%I-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung','I_S','45');

	%Activate Maximal lateral Acceleration
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung','activate_stop','off');

	%Maximal lateral Acceleration (simulation stops) [m/s^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung','ay_stop','15');

%% Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung

	%P-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung','P_S','45');

	%I-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung','I_S','45');

	%Activate Maximal lateral Acceleration
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung','activate_stop','off');

	%Maximal lateral Acceleration (simulation stops) [m/s^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung','ay_stop','15');
%% Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/ICE_scaling_simple

	%Engine Type
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/ICE_scaling_simple','engine_type','TurboGasoline');

	%Peak Power [kW]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/ICE_scaling_simple','peak_power','330');


%% Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission

	%Maximal Motor Torque [Nm]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','Max_M_Torque','650');  %TD_BMW_6er_Coupe_650i_xDrive_07_2012

	%%%Ratio Gears [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','iGears','[4.714 3.143 2.106 1.667 1.285 1.000 0.839 0.667]'); %[4.714 3.143 2.106 1.667 1.285 1.000 0.839 0.667] , [12 8]

	%%%Nominal Speed [1/min]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','n_nominal_Trans','5000');

	%Eingriffswinkel [rad]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','alpha','20*pi/180');

	%Grundkreisschrägungswinkel [rad]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','beta_b','18.7*pi/180');

	%Schrägungswinkel [rad]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','beta','20*pi/180');

	%Profilüberdeckung [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','eps_alpha','1.4');

	%Kopfüberdeckung Ritzel
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','eps_1','0.7');

	%Kopfüberdeckung Rad
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','eps_2','0.7');

	%mu
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','mu','0.05');

	%Maximale Übersetzung Stufe [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','imax_Stufe','6');

	%E-Modul [GPa]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','E','210');

	%K-Faktor, [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','K','7');

	%Mindestgrübchensicherheit nach DIN 3990
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','S_H_min','1.2');

	%Mindestzahnbruchsicherheit nach DIN 3990
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Multi Stage Multi Gear Transmission','S_F_min','1.5');


%% Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential

	%Maximal Efficiency of Bevel Gear [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','MaxEffBevelGear','0.98');

	%%%Maximal Torque [Nm]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','MaxTrq','1000');

	%Ratio Bevel Gear [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','iBevelGear','1/2.81');

	%Standübersetzung
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','i_null','-1');

	%Dichte Stahl [kg / m^3]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','rho','7800');

	%Zugfestigkeit des Werkstoffs [MPa]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','R_mN','900');

	%Berechnungsfaktor
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','f_W','0.40');

	%Schubfaktor hinterfragen ob sinnvoll !!!!!
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','r_tau','0.58');

	%E-Modul [Pa]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','E','210000E+6');

	%Poisson Ratio
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','v_poisson','0.3');

	%Safety Factor Shaft 1
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','S','5');

	%maximal allowable stress
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','sun_p_zul','60');

	%pressure angle of bevel gear
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','sun_alpha','45');

	%thickness of sun bevel gear to total r_sun
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','sun_f_d_1_sun','0.25');

	%radius 1 of sun bevel gear to r_sun
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','sun_f_r_sun_ritz_1','1.4');

	%radius 2 of sun bevel gear to r_sun
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','sun_f_r_sun_ritz_2','2.5');

	%Annahme: thickness of planet bevel gear
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','planet_d_1_planet','0.003');

	%radius 1 of planet bevel gear to r_sun
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','planet_f_r_planet_ritz_1','0.4');

	%gap between cage and bevel gears
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','cage_d_spalt_diff_korb','0.002');

	%Annahme : thickness of cage
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','cage_t_diff_korb','0.004');

	%gap between cage and crown wheel
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','crown_wheel_d_spalt_crown_wheel','0.015');

	%length of crown wheel
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','crown_wheel_l_crown_wheel','0.022');

	%Annahme: thickness of crown wheel
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','crown_wheel_d_crown_wheel_ritz','0.005');

	%thickness of crown_wheel only teeth
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','crown_wheel_t_diff_korb_crown_wheel','0.005');

	%thickness of box
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','box_t_geh','0.006');

	%angle of conic part of box zur Antriebswelle
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','box_beta_geh_keg_aw','30');

	%angle of conic part of box zu den Seitenwellen
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','box_beta_geh_keg_sw','30');

	%fudge factor
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Drivetrain/Offenes Differential','f_fudge','1.07');


%% Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model

	%Air Density [kg/m^3]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','RohAir','1.204');

	%Mass Vehicle [kg]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_m','2.16e+03');

	%Unsprung mass Front [kg]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_m_usF','1.37415e+02');

	%Unsprung mass Rear [kg]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_m_usR','1.35825e+02');

	%Inertia of Vehicle about x [kg m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_Ix','1100');

	%Inertia of Vehicle about y [kg m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_Iy','4500');

	%Inertia of Vehicle about z [kg m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_Iz','5100');

	%Inertia of Front Wheel (single) [kg m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_Iwf','1.902');

	%Inertia of Rear Wheel (single) [kg m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_Iwr','1.989');

	%Center of Gravity of Vehicle in x [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_xSP','1.43');

	%Center of Gravity of Vehicle in y [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ySP','0');

	%Center of Gravity of Vehicle in z [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_zSP','0.2');

	%Center of Gravity of unsprung masses in the front in z [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ztSPf','0');

	%Center of Gravity of unsprung masses in the rear in z [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ztSPr','0');

	%Gravity [N/kg]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_g','9.81');

	%cw-Value in z [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_cz','0');

	%cw-Value in x [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_cx','0.29');

	%cw-Value in y [-]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_cy','0');

	%Front Surface area [m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_Afront','2.24');

	%Pressure point position in x-direction [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_xDP','1.43');

	%Pressure point position in y-direction [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_yDP','0');

	%Initial Speed [m/s]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','v0','0');

	%Wheel Base [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_l','2.968');

	%Track Front [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_sF','1.6');

	%Track Rear [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_sR','1.635');

    xSP=1.43;   %Center of Gravity of Vehicle in x [m]
    l = 2.968;  %Wheel Base [m]
    lR = l-xSP; 
    
    slopeRA = -0.010;               %Steigung der Wankachse [%]
    heightRC= -0.200;               %Höhe des Wankzentrums [m] (Konstruktions-KOS)
    zRCf = heightRC-(slopeRA*xSP);   %Roll Center Height Front [m]
    zRCr = zRCf+(slopeRA*l);        %Roll Center Height Rear [m]
    
    
	%Roll Center Height Front [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_zRCf','zRCf');

	%Roll Center Height Rear [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_zRCr','zRCr');

	%Pitch Center Height [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_zPC','-0.125');

    csF = 32992;            % Radbezogene Federrate vorne [N/m]
    csR = 41332;            % Radbezogene Federrate hinten [N/m]
    ts = [-0.10:0.01:0.03];
    cF_front = [ts;csF*ts+4475]'; 
    cF_rear = [ts;csR*ts+4098]';

	%Spring Characteristic Front (first column travel distance in m second column Force in N) [N/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_cF_front','cF_front');

	%Spring Characteristic Rear (first column travel distance in m second column Force in N) [N/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_cF_rear','cF_rear');

	%Spring Track Front [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ssF','1.6');

	%Spring Track Rear [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ssR','1.635');

	
    
    % Dämpferkennlinien bei verschiedenen Bestromungen  !!!!!! IN MASK INITIALISATION NOW !!!!!!!!
% Dämpferkennlinien: [Eingang: Anregungsgeschwindigkeit in mm/s; Ausgang: Dämpferkraft in N]
% load ('Kennlinien_Daempfer.mat');
% par_VEH.ddF0mA = VA0mA;         % Dämpferkennlinie vorne bei 0mA
% par_VEH.ddF200mA = VA200mA;     % Dämpferkennlinie vorne bei 200mA
% par_VEH.ddF400mA = VA400mA;     % Dämpferkennlinie vorne bei 400mA
% par_VEH.ddF600mA = VA600mA;     % Dämpferkennlinie vorne bei 600mA
% par_VEH.ddF800mA = VA800mA;     % Dämpferkennlinie vorne bei 800mA
% par_VEH.ddF1000mA = VA1000mA;   % Dämpferkennlinie vorne bei 1000mA
% par_VEH.ddF1200mA = VA1200mA;   % Dämpferkennlinie vorne bei 1200mA
% par_VEH.ddF1400mA = VA1400mA;   % Dämpferkennlinie vorne bei 1400mA
% par_VEH.ddF1600mA = VA1600mA;   % Dämpferkennlinie vorne bei 1600mA
% par_VEH.ddR0mA = HA0mA;         % Dämpferkennlinie hinten bei 0mA
% par_VEH.ddR200mA = HA200mA;     % Dämpferkennlinie hinten bei 200mA
% par_VEH.ddR400mA = HA400mA;     % Dämpferkennlinie hinten bei 400mA
% par_VEH.ddR600mA = HA600mA;     % Dämpferkennlinie hinten bei 600mA
% par_VEH.ddR800mA = HA800mA;     % Dämpferkennlinie hinten bei 800mA
% par_VEH.ddR1000mA = HA1000mA;   % Dämpferkennlinie hinten bei 1000mA
% par_VEH.ddR1200mA = HA1200mA;   % Dämpferkennlinie hinten bei 1200mA
% par_VEH.ddR1400mA = HA1400mA;   % Dämpferkennlinie hinten bei 1400mA
% par_VEH.ddR1600mA = HA1600mA;   % Dämpferkennlinie hinten bei 1600mA
% clear VA0mA VA200mA VA400mA VA600mA VA800mA VA1000mA VA1200mA VA1400mA VA1600mA HA0mA HA200mA HA400mA HA600mA HA800mA HA1000mA HA1200mA HA1400mA HA1600mA
% par_VEH.ddFtable_compression = [par_VEH.ddF0mA(1:13,2) par_VEH.ddF200mA(1:13,2) par_VEH.ddF400mA(1:13,2) par_VEH.ddF600mA(1:13,2) par_VEH.ddF800mA(1:13,2) par_VEH.ddF1000mA(1:13,2) par_VEH.ddF1200mA(1:13,2) par_VEH.ddF1400mA(1:13,2) par_VEH.ddF1600mA(1:13,2)];
% par_VEH.ddFtable_rebound = [par_VEH.ddF0mA(13:25,2) par_VEH.ddF200mA(13:25,2) par_VEH.ddF400mA(13:25,2) par_VEH.ddF600mA(13:25,2) par_VEH.ddF800mA(13:25,2) par_VEH.ddF1000mA(13:25,2) par_VEH.ddF1200mA(13:25,2) par_VEH.ddF1400mA(13:25,2) par_VEH.ddF1600mA(13:25,2)];
% par_VEH.ddRtable_compression = [par_VEH.ddR0mA(1:13,2) par_VEH.ddR200mA(1:13,2) par_VEH.ddR400mA(1:13,2) par_VEH.ddR600mA(1:13,2) par_VEH.ddR800mA(1:13,2) par_VEH.ddR1000mA(1:13,2) par_VEH.ddR1200mA(1:13,2) par_VEH.ddR1400mA(1:13,2) par_VEH.ddR1600mA(1:13,2)];
% par_VEH.ddRtable_rebound = [par_VEH.ddR0mA(13:25,2) par_VEH.ddR200mA(13:25,2) par_VEH.ddR400mA(13:25,2) par_VEH.ddR600mA(13:25,2) par_VEH.ddR800mA(13:25,2) par_VEH.ddR1000mA(13:25,2) par_VEH.ddR1200mA(13:25,2) par_VEH.ddR1400mA(13:25,2) par_VEH.ddR1600mA(13:25,2)];

    % Dämpferstromregelung (vorerst nur Abhängigkeit von Querbeschleunigung berücksichtigt)
% Polynomkoeffizienten Abh. von Querbeschleunigung für VA-Dämpfer im SPORT Modus: [-0.0094 -6.3543e-04 1.4060]
% Polynomkoeffizienten Abh. von Querbeschleunigung für HA-Dämpfer im SPORT Modus: [-0.0055 -0.0011 1.3063]
% Polynomkoeffizienten Abh. von Querbeschleunigung für VA-Dämpfer im COMFORT Modus: [-0.0139 -0.0041 1.5623]
% Polynomkoeffizienten Abh. von Querbeschleunigung für HA-Dämpfer im COMFORT Modus: [-0.0100 -0.0085 1.5242]
% par_VEH.ddFStromSPORTtable = [0.4725 0.6505 0.8096 0.9500 1.0715 1.1743 1.2582 1.3234 1.3697 1.3973 1.4060 1.3973 1.3697 1.3234 1.2582 1.1743 1.0715 0.9500 0.8096 0.6505 0.4725];
% par_VEH.ddRStromSPORTtable = [0.7673 0.8707 0.9631 1.0445 1.1149 1.1743 1.2227 1.2601 1.2865 1.3019 1.3063 1.3019 1.2865 1.2601 1.2227 1.1743 1.1149 1.0445 0.9631 0.8707 0.7673];
% par_VEH.ddFStromCOMFORTtable = [0.2133 0.4733 0.7055 0.9099 1.0865 1.2353 1.3563 1.4495 1.5149 1.5525 1.5623 1.5525 1.5149 1.4495 1.3563 1.2353 1.0865 0.9099 0.7055 0.4733 0.2133];
% par_VEH.ddRStromCOMFORTtable = [0.6092 0.7907 0.9522 1.0937 1.2152 1.3167 1.3982 1.4597 1.5012 1.5227 1.5242 1.5227 1.5012 1.4597 1.3982 1.3167 1.2152 1.0937 0.9522 0.7907 0.6092];   
% par_VEH.ddFStrom = 0;           % Dämpferstrom vorne [A] (bei fester Bestromung)
% par_VEH.ddRStrom = 0;           % Dämpferstrom hinten [A] (bei fester Bestromung)

    set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','ddFStromCOMFORTtable','[0.2133 0.4733 0.7055 0.9099 1.0865 1.2353 1.3563 1.4495 1.5149 1.5525 1.5623 1.5525 1.5149 1.4495 1.3563 1.2353 1.0865 0.9099 0.7055 0.4733 0.2133]');
    set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','ddRStromCOMFORTtable','[0.6092 0.7907 0.9522 1.0937 1.2152 1.3167 1.3982 1.4597 1.5012 1.5227 1.5242 1.5227 1.5012 1.4597 1.3982 1.3167 1.2152 1.0937 0.9522 0.7907 0.6092]');
    set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','ddFStromSPORTtable','[0.4725 0.6505 0.8096 0.9500 1.0715 1.1743 1.2582 1.3234 1.3697 1.3973 1.4060 1.3973 1.3697 1.3234 1.2582 1.1743 1.0715 0.9500 0.8096 0.6505 0.4725]');
    set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','ddRStromSPORTtable','[0.7673 0.8707 0.9631 1.0445 1.1149 1.1743 1.2227 1.2601 1.2865 1.3019 1.3063 1.3019 1.2865 1.2601 1.2227 1.1743 1.1149 1.0445 0.9631 0.8707 0.7673]');
    set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','ddFStrom','0');
    set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','ddRStrom','0');

    
    %%%Polynomial Coefficients for Damper Rate Rebound Front [Ns/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ddF_reb','[-3.707824029757397e+03 0]');

	%%%Polynomial Coefficients for Damper Rate Compression Front [Ns/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ddF_comp','[-1.483129611902959e+03 0]');

	%%%Polynomial Coefficients for Damper Rate Rebound Rear [Ns/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ddR_reb','[-7.328177750508903e+03 0]');

	%%%Polynomial Coefficients for Damper Rate Compression Rear [Ns/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_ddR_comp','[-2.931271100203561e+03 0]');

	%Damper Track Front [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_sdF','1.22');

	%Damper Track Rear [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_sdR','1.24');

	%Resulting Spring rate of anti-roll bar Front [N/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_carbF','19341');

	%Resulting Spring rate of anti-roll bar Rear [N/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_carbR','4511');

	%Anti Roll Bar Track Front [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_sarbF','1.6');

	%Anti Roll Bar Track Rear [m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_sarbR','1.635');

	%Consider Kinematics
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_sw_kinematics','on');

	%Change in Toe over steering angle Front ???VEC???
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_kin_lw_toe_f','[25.062667880921907 0.063030823856030 0.004087458845074 -0.000000015381536]');

	%Change in Chamber over steering angle ???VEC???
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_kin_lw_camber_f','[-3.425895576245629 0.040734768557570 0.216146975718826 -0.000002215034190]');

	%Polynomial Coefficients for Kinematic Chamber Front [°/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_kin_camb_f','[-29.0194 -25.9833 -0.2919]');

	%Polynomial Coefficients for Kinematic Toe Front [°/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_kin_toe_f','[-2.0008 -2.3572 0.0399]');

	%Polynomial Coefficients for Kinematic Chamber Rear [°/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_kin_camb_r','[-63.6725 -14.9959 -1.5574]');

	%Polynomial Coefficients for Kinematic Toe Rear [°/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_kin_toe_r','[-0.1010 2.0823 0.0464]');

	%Consider Elastokinematics
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_sw_compliance','on');

	%Polynomial Coefficients for Toe through Twist Beam [°/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_toe_twistbeam','[0 0]');

	%Polynomial Coefficients for Camber through Twist Beam [°/m]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_camb_twistbeam','[0 0]');

	%Polynomial Coefficients for Elastokinematic Toe though lateral Forces Front [°/N]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_y_toe_f','[-7.8097e-09 -6.6338e-05 -0.0069]');

	%Polynomial Coefficients for Elastokinematic Toe though lateral Forces Rear [°/N]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_y_toe_r','[-3.3556e-09 6.7675e-06 5.6300e-04]');

	%Polynomial Coefficients for Elastokinematic Toe though longitudinal Forces Front [°/N]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_x_toe_f','[-8.9916e-09 5.6929e-05 -0.01247]');

	%Polynomial Coefficients for Elastokinematic Toe though longitudinal Forces Rear [°/N]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_x_toe_r','[2.1151e-08 1.4997e-05 -9.8142e-05]');

	%Polynomial Coefficients for Elastokinematic Camber though lateral Forces Front [°/N]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_y_camb_f','[-5.9098e-09 8.5629e-05 0.01607]');

	%Polynomial Coefficients for Elastokinematic Camber though lateral Forces Rear [°/N]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_y_camb_r','[-2.6402e-08 8.3907e-05 0.01550]');

	%Polynomial Coefficients for Elastokinematic Camber though longitudinal Forces Front [°/N]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_x_camb_f','[-2.9061e-08 1.6354e-05 5.2094e-04]');

	%Polynomial Coefficients for Elastokinematic Camber though longitudinal Forces Rear [°/N]
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','VEH_elkin_x_camb_r','[-1.8002e-08 -3.8770e-06 0.0049]');

	%Initial Position x
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','INIT_x','0');

	%Initial Position y
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','INIT_y','0');

	%Initial Position psi
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','INIT_psi','0');

	%Initial psip
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','psip0','0');

	%Initial Road Position z Right Front
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','INIT_zroad_RF0','0');

	%Initial Road Position z Left Front
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','INIT_zroad_LF0','0');

	%Initial Road Position z Right Rear
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','INIT_zroad_RR0','0');

	%Initial Road Position z Left Rear
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','INIT_zroad_LR0','0');

    
    
    %%nicht gemessen
	%Tire Options Front (Druckfunktion deaktiviert)
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','SelectedTireFront','245_35_R20_b24__F10_DU_S_RSC_SPSportMaxxGT_7950_Serie_p24_VQLX0ZM_DS101130_MF52_R.tir'); %Edit_245_35_R20_b24__F10_DU_S_RSC_SPSportMaxxGT_7950_Serie_p24_VQLX0ZM_DS101130_MF52_R.tir

	%PIO
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PIOF','200'); 

	%PPX1
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPX1F','0'); %-0.7333

	%PPX2
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPX2F','0');

	%PPX3
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPX3F','0'); %0.0525

	%PPX4
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPX4F','0');

	%PPY1
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPY1F','0'); %0.5101

	%PPY2
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPY2F','0'); %1.5804

	%PPY3
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPY3F','0'); %-0.1092

	%PPY4
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPY4F','0');

	%QPZ1
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','QPZ1F','0'); %0.5213

	%Loadindex
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','LoadindexF','95');

	%Tire Options Rear
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','SelectedTireRear','275_30_R20_b24__F10_DU_S_RSC_SPSportMaxxGT_7950_Serie_p24_VQLX0ZM_DS101130_MF52_R.tir'); %Edit_275_30_R20_b24__F10_DU_S_RSC_SPSportMaxxGT_7950_Serie_p24_VQLX0ZM_DS101130_MF52_R.tir

	%PIO
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PIOR','200');

	%PPX1
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPX1R','0'); %0.2667

	%PPX2
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPX2R','0');

	%PPX3
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPX3R','0'); %0.0525

	%PPX4
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPX4R','0');

	%PPY1
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPY1R','0'); %0.5101

	%PPY2
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPY2R','0'); %1.5804

	%PPY3
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPY3R','0'); %-0.1092

	%PPY4
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','PPY4R','0');

	%QPZ1
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','QPZ1R','0'); %0.5213

	%Loadindex
	set_param('Querdynamikmodell_BMWF06_650i_ZSM/Dynamics/Double Track Model','LoadindexR','97');


