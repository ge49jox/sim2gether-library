%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameter File for Querdynamikmodell_BMWF06_650i_ESM_nonlinear		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Control Unit/Control Unit Simple

	%Brake Distribution Front
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Control Unit/Control Unit Simple','Brake_Distribution_Front','0.62');

	%Maximal Braking Torque
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Control Unit/Control Unit Simple','MaxBrkTrq','4200');
    MaxBrkTrq=4200;

	%Maximal Motor Torque
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Control Unit/Control Unit Simple','MaxMotorTrq','650');


%% Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r

	%P-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r','P_S','45');

	%I-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r','I_S','45');

	%Activate Maximal lateral Acceleration
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r','activate_stop','off');

	%Maximal lateral Acceleration (simulation stops) [m/s^2]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt/PID Driver Steering r','ay_stop','15');


%% Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung

	%P-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung','P_S','45');

	%I-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung','I_S','45');

	%Activate Maximal lateral Acceleration
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung','activate_stop','off');

	%Maximal lateral Acceleration (simulation stops) [m/s^2]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Verzögerung/PID Driver Steering r_mit Lastwechsel Verzögerung','ay_stop','15');

%% Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung

	%P-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung','P_S','45');

	%I-Value [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung','I_S','45');

	%Activate Maximal lateral Acceleration
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung','activate_stop','off');

	%Maximal lateral Acceleration (simulation stops) [m/s^2]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Driver and Environment/quasistationäre Kreisfahrt mit Lastwechsel Beschleunigung/PID Driver Steering r_mit Lastwechsel Beschleunigung','ay_stop','15');

%% Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Axle Drive

	%Ratio [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Axle Drive','ratio','2.81'); % https://www.auto-motor-und-sport.de/bmw/6er/f06-f12-f13/technische-daten/

	%Efficiency [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Axle Drive','trq_eff','0.98');


%% Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/ICE_scaling_simple

	%Engine Type
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/ICE_scaling_simple','engine_type','TurboGasoline');

	%Peak Power [kW]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/ICE_scaling_simple','peak_power','330');


%% Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission

	%Maximal Motor Torque [Nm]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','Max_M_Torque','650');

	%Ratio Gears [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','iGears','[4.714 3.143 2.106 1.667 1.285 1.000 0.839 0.667]');

	%Nominal Speed [1/min]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','n_nominal_Trans','5000');

	%Eingriffswinkel [rad]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','alpha','20*pi/180');

	%Grundkreisschrägungswinkel [rad]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','beta_b','18.7*pi/180');

	%Schrägungswinkel [rad]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','beta','20*pi/180');

	%Profilüberdeckung [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','eps_alpha','1.4');

	%Kopfüberdeckung Ritzel
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','eps_1','0.7');

	%Kopfüberdeckung Rad
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','eps_2','0.7');

	%mu
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','mu','0.05');

	%Maximale Übersetzung Stufe [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','imax_Stufe','6');

	%E-Modul [GPa]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','E','210');

	%K-Faktor, [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','K','7');

	%Mindestgrübchensicherheit nach DIN 3990
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','S_H_min','1.2');

	%Mindestzahnbruchsicherheit nach DIN 3990
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Drivetrain/Multi Stage Multi Gear Transmission','S_F_min','1.5');


%% Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear

	%Air Density [kg/m^3]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','RohAir','1.204');

	%Mass Vehicle [kg]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_m','2.16e+03');

	%Inertia of Vehicle about z [kg m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_Iz','5100');

	%Inertia of Front Wheel (single) [kg m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_Iwf','1.902');

	%Inertia of Rear Wheel (single) [kg m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_Iwr','1.989');

	%Center of Gravity of Vehicle in x [m]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_xSP','1.43');

	%Gravity [N/kg]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_g','9.81');

	%cw-Value in z [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_cz','0');

	%cw-Value in x [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_cx','0.29');

	%cw-Value in y [-]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_cy','0');

	%Front Surface area [m^2]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_Afront','2.24');

	%Pressure point position in x-direction [m]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_xDP','1.43');

	%Pressure point position in y-direction [m]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_yDP','0');

	%Wheel Base [m]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','VEH_l','2.968');

	%Initial Position x
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','INIT_x','0');

	%Initial Position y
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','INIT_y','0');

	%Initial Position psi
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','INIT_psi','0');

	%Initial psip
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','psip0','0');

	%Initial Road Position z Right Front
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','INIT_zroad_RF0','0');

	%Initial Road Position z Left Front
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','INIT_zroad_LF0','0');

	%Initial Road Position z Right Rear
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','INIT_zroad_RR0','0');

	%Initial Road Position z Left Rear
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','INIT_zroad_LR0','0');

	%Initial Speed [m/s]
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','v0','0');

	%Tire Options Front
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','SelectedTireFront','245_35_R20_b24__F10_DU_S_RSC_SPSportMaxxGT_7950_Serie_p24_VQLX0ZM_DS101130_MF52_R.tir');

	%PIO
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PIOF','200');

	%PPX1
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPX1F','0');

	%PPX2
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPX2F','0');

	%PPX3
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPX3F','0');

	%PPX4
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPX4F','0');

	%PPY1
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPY1F','0');

	%PPY2
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPY2F','0');

	%PPY3
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPY3F','0');

	%PPY4
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPY4F','0');

	%QPZ1
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','QPZ1F','0');

	%Loadindex
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','LoadindexF','95');

	%Tire Options Rear
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','SelectedTireRear','275_30_R20_b24__F10_DU_S_RSC_SPSportMaxxGT_7950_Serie_p24_VQLX0ZM_DS101130_MF52_R.tir');

	%PIO
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PIOR','200');

	%PPX1
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPX1R','0');

	%PPX2
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPX2R','0');

	%PPX3
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPX3R','0');

	%PPX4
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPX4R','0');

	%PPY1
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPY1R','0');

	%PPY2
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPY2R','0');

	%PPY3
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPY3R','0');

	%PPY4
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','PPY4R','0');

	%QPZ1
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','QPZ1R','0');

	%Loadindex
	set_param('Querdynamikmodell_BMWF06_650i_ESM_nonlinear/Dynamics/SingleTrackModel_nonlinear','LoadindexR','97');

