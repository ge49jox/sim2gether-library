function [ input_data] = Kreisfahrt(v0, v_final, r0,r_final, stepsize, simtime)

% Time Vector
input_time = (0 : stepsize: simtime)';

% Radius Vector
input_r = interp1([0,simtime], [r0, r_final], input_time);
% Radius over Time
input_r=[input_time input_r];
% Velocity Vector
input_v = interp1([0,simtime], [v0, v_final], input_time);
% Velocity over Time
input_v=[input_time input_v];

% zRoad over Time (zeros)
input_zroad_RF = [input_time zeros(length(input_time), 1)];                              
input_zroad_LF = [input_time zeros(length(input_time), 1)];                            
input_zroad_RR = [input_time zeros(length(input_time), 1)];                             
input_zroad_LR = [input_time zeros(length(input_time), 1)];

% Empty fields (zero)
input_delta_h= [input_time zeros(length(input_time), 1)];
input_psip= [input_time zeros(length(input_time), 1)];



% Wrap in Struct
input_data.input_time=input_time;
input_data.input_r=input_r;
input_data.input_v=input_v;
input_data.input_zroad_RF=input_zroad_RF;
input_data.input_zroad_LF=input_zroad_LF;
input_data.input_zroad_RR=input_zroad_RR;
input_data.input_zroad_LR=input_zroad_LR;

input_data.input_delta_h=input_delta_h;
input_data.input_psip= input_psip;

end

