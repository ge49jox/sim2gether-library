function [input_data, simtime]= Sinus_lenken(deltah,delta_Periodendauer, v0_sin, v_final_sin,stepsize)

% First 30 seconds no steering angle
input_delta_h = zeros(6001,1); 

% Add the sinus waves to the steering angle with some breakes inbetween
for i=1:length(delta_Periodendauer)
einzel_sin_laenge = 0:stepsize:delta_Periodendauer(i);
einzel_sin = deltah*sin(linspace(0,2*pi,length(einzel_sin_laenge))); 
multiple_sin = repmat(einzel_sin,1,5)';
input_delta_h(end+1:end+length(multiple_sin)+6001)...
    = [multiple_sin;zeros(6001,1)];
end

% Calculation of simulation time
simtime=length(input_delta_h)*stepsize;

%Time Vector
input_time = (0 : stepsize: simtime-stepsize)'; 

% Steering angle over time
input_delta_h=[input_time input_delta_h];

% Velocity Vector
input_v = interp1([0,simtime], [v0_sin, v_final_sin], input_time);
% Velocity over Time
input_v=[input_time input_v];

% zRoad over Time (zeros)
input_zroad_RF = [input_time zeros(length(input_time), 1)];
input_zroad_LF = [input_time zeros(length(input_time), 1)];
input_zroad_RR = [input_time zeros(length(input_time), 1)];
input_zroad_LR = [input_time zeros(length(input_time), 1)]; 

% Empty fields (zero)
% Radius over Time
input_r = [input_time zeros(length(input_time), 1)];
% psip over time
input_psip= [input_time zeros(length(input_time), 1)];

% Wrap in Struct    
input_data.input_time=input_time;
input_data.input_v=input_v;
input_data.input_delta_h=input_delta_h;
input_data.input_zroad_RF=input_zroad_RF;
input_data.input_zroad_LF=input_zroad_LF;
input_data.input_zroad_RR=input_zroad_RR;
input_data.input_zroad_LR=input_zroad_LR;

input_data.input_psip=input_psip;
input_data.input_r=input_r;

end

