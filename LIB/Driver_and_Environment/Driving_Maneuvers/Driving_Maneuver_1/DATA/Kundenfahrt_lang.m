function [input_data, simtime] = Kundenfahrt_lang()


load ('Environment_and_Driving_Cycle\DATA\Zyklen\cyc_test_eup_timestep_cyc0.1_t600_Qkombi_1784.mat')

% Duration of Simulation
simtime = max(time);

% Time Vector
input_time = time;                                                     

% Velocity over Time
input_v = [input_time FZG_v_xSP];

% psip over Time
input_psip = [input_time FZG_psip];                                                     % Nutzung im Fahrerregler

% zRoad over Time (zeros)
input_zroad_RF = [input_time zeros(length(input_time), 1)];                              % Strassenanregung
input_zroad_LF = [input_time zeros(length(input_time), 1)];                            % Strassenanregung
input_zroad_RR = [input_time zeros(length(input_time), 1)];                             % Strassenanregung
input_zroad_LR = [input_time zeros(length(input_time), 1)];    

%Empty fields (zero)
%Radius
input_r= [input_time zeros(length(input_time), 1)];
%Steering Wheel Angle
input_delta_h= [input_time zeros(length(input_time), 1)];

clear ('time', 'fahrzyklen', 'FZG_v_xSP', 'FZG_psip')

% Wrap in Struct
input_data.input_psip=input_psip;
input_data.input_time=input_time;
input_data.input_v=input_v;
input_data.input_zroad_RF=input_zroad_RF;
input_data.input_zroad_LF=input_zroad_LF;
input_data.input_zroad_RR=input_zroad_RR;
input_data.input_zroad_LR=input_zroad_LR;

input_data.input_r=input_r;
input_data.input_delta_h=input_delta_h;
end
