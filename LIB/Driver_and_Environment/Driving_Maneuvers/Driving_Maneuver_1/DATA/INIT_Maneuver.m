function [input_data] = INIT_Maneuver(Maneuver,stepsize_KF,simtime_KF,v0_KF,v_final_KF,r0_KF,r_final_KF,stepsize_LWS,simtime_LWS,deltah_LWS,deltah_LWS_Time,deltah_LWS_Start_time,v0_LWS,v_final_LWS,deltah_sin,delta_Periodendauer_sin, v0_sin, v_final_sin,stepsize_sin,stepsize_LKF,stepsize_KKF,SetSimTime,SetStepSize)
%% Kreisfahrt
if strcmp(Maneuver, 'Circular Drive (Kreisfahrt)')
[input_data] = Kreisfahrt(v0_KF, v_final_KF, r0_KF,r_final_KF, stepsize_KF, simtime_KF)

    SimTime=simtime_KF;
    StepSize=stepsize_KF;
end

%% Lenkwinkelsprung
if strcmp(Maneuver, 'Step Steer (Lenkwinkelsprung)')
    [input_data] = Lenkwinkelsprung(deltah_LWS,deltah_LWS_Time, deltah_LWS_Start_time, v0_LWS, v_final_LWS, stepsize_LWS, simtime_LWS)
    
    SimTime=simtime_LWS;
    StepSize=stepsize_LWS;
end

%% Sinuslenken
if strcmp(Maneuver, 'Sinus Steering (Sinuslenken)')
    [input_data, simtime_sin]= Sinus_lenken(deltah_sin,delta_Periodendauer_sin, v0_sin, v_final_sin,stepsize_sin)
        
    SimTime=simtime_sin;
    StepSize=stepsize_sin;
end

%% Lange Kundenfahrt
if strcmp(Maneuver, 'Long Custom Ride (Lange Kundenfahrt)')
    [input_data, simtime_LKF]= Kundenfahrt_lang()
    
    SimTime=simtime_LKF;
    StepSize=stepsize_LKF;
end

%% Kurze Kundenfahrt
if strcmp(Maneuver, 'Short Custom Ride (Kurze Kundenfahrt)')
    [input_data, simtime_KKF]= Kundenfahrt_kurz()
    
    SimTime=simtime_KKF;
    StepSize=stepsize_KKF;
end

%% Set Simtime und Stepsize
if SetSimTime==1
   set_param(char(bdroot(gcb)),'StopTime',num2str(SimTime));
end

if SetStepSize==1
   set_param(char(bdroot(gcb)),'FixedStep',num2str(StepSize));
end

end