function [ input_data] = Lenkwinkelsprung(deltah_LWS,deltah_LWS_Time, deltah_LWS_Start_time, v0_SAS, v_final_SAS, stepsize, simtime)

% Time Vector
input_time = (0 : stepsize: simtime)';                                       

% Amount of Steps of the actual steering angle changing
LWS_Steps=round(deltah_LWS_Time/stepsize);

% Amount of Steps before the start of steering angle change
LWS_Start_Steps=round(deltah_LWS_Start_time/stepsize);

% Remaining Steps
LWS_Rest_steps= length(input_time)-LWS_Steps-LWS_Start_Steps;

% Steering Angle Vector
input_delta_h=[zeros(1,LWS_Start_Steps) linspace(0,deltah_LWS,LWS_Steps) deltah_LWS*ones(1, LWS_Rest_steps)]';
%Steering Angle over Time
input_delta_h=[input_time input_delta_h];

%Velocity over Time
input_v = [input_time interp1([0,simtime], [v0_SAS, v_final_SAS], input_time)];                  

% zRoad over Time (zeros)
input_zroad_RF = [input_time zeros(length(input_time), 1)];
input_zroad_LF = [input_time zeros(length(input_time), 1)];
input_zroad_RR = [input_time zeros(length(input_time), 1)];
input_zroad_LR = [input_time zeros(length(input_time), 1)];

% Empty fields (zero)
input_psip= [input_time zeros(length(input_time), 1)];
input_r = [input_time zeros(length(input_time), 1)];

% Wrap in Struct
input_data.input_time=input_time;
input_data.input_delta_h=input_delta_h;
input_data.input_v=input_v;
input_data.input_zroad_RF=input_zroad_RF;
input_data.input_zroad_LF=input_zroad_LF;
input_data.input_zroad_RR=input_zroad_RR;
input_data.input_zroad_LR=input_zroad_LR;

input_data.input_psip=input_psip;
input_data.input_r=input_r;

end

