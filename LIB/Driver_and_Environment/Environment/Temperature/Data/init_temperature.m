function [Temperature, Elevation, switch_temperature] = init_temperature(temperature_mode,temperature_constant,temperature_values,temperature_time,elevation_values,elevation_time)

% Designed by: Benedikt Taiber (FTM, Technical University of Munich)
% ------------------------------------------------------------------------
% Description: Function initializes Temperature block
% ------------------------------------------------------------------------
% Input:    - temperature_mode: User selection for temperature-mode
%           - temperature_constant: Constant temperature value
%           - temperature_values: [1xN] vector containing custom 
%                                 temperature values
%           - temperature_time: [1xN] vector containing time stamps for
%                               temperature values
%           - elevation_values: [1xN] vector containing custom elevation
%                               profile values
%           - elevation_time:[1xN] vector containing time stamps for custom
%                            elevation values
% ------------------------------------------------------------------------
% Output:   - Temperature: Struct containing constant and custom
%                          temperature values
%           - Elevation: Struct containing custom elevation profile values
%           - switch_temperature: Decision variable for switch within model 
%                                 block
% ------------------------------------------------------------------------

Temperature = struct('Constant',0,'Time',[],'Values',[]);
Elevation = struct('Time',[],'Values',[]);
    % Pre-allocation of result-structs

if strcmp(temperature_mode,'Constant Temperature')
    
    switch_temperature = 3;
    Temperature.Constant = temperature_constant;
    Temperature.Time = [0];
    Temperature.Values = [0];
    Elevation.Time = [0];
    Elevation.Values = [0];
    
elseif strcmp(temperature_mode,'Custom Temperature')
    
    switch_temperature = 2;
    Temperature.Constant = 0;
    Temperature.Time = temperature_time;
    Temperature.Values = temperature_values;
    Elevation.Time = [0];
    Elevation.Values = [0];
    
elseif strcmp(temperature_mode,'Temperature via Elevation Profile')

    switch_temperature = 1;
    Temperature.Constant = 0;
    Temperature.Time = [0];
    Temperature.Values = [0];
    Elevation.Time = elevation_time;
    Elevation.Values = elevation_values;

end

