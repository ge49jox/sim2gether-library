function [GroundSlope, Elevation, switch_slope] = init_ground_slope(slope_mode,slope_constant,slope_values,slope_time,elevation_values,elevation_time)

% Designed by: Benedikt Taiber (FTM, Technical University of Munich)
% ------------------------------------------------------------------------
% Description: Function initializes GroundSlope block
% ------------------------------------------------------------------------
% Input:    - slope_mode: User selection for slope-mode
%           - slope_constant: Constant ground slope value
%           - slope_values: [1xN] vector containing custom ground slope 
%                           values
%           - slope_time: [1xN] vector containing time stamps for custom
%                         ground slope values
%           - elevation_values: [1xN] vector containing custom elevation
%                               profile values
%           - elevation_time:[1xN] vector containing time stamps for custom
%                           elevation values
% ------------------------------------------------------------------------
% Output:   - GroundSlope: Struct containing constant and custom ground
%                          slope values
%           - Elevation: Struct containing custom elevation profile values
%           - switch_slope: Decision variable for switch within model block
% ------------------------------------------------------------------------

GroundSlope = struct('Constant',0,'Time',[],'Values',[]);
Elevation = struct('Time',[],'Values',[]);
    % Pre-allocation of result-structs

if strcmp(slope_mode,'Constant Ground Slope')
    
    switch_slope = 3;
    GroundSlope.Constant = slope_constant;
    GroundSlope.Time = [0];
    GroundSlope.Values = [0];
    Elevation.Time = [0];
    Elevation.Values = [0];
    
elseif strcmp(slope_mode,'Custom Ground Slope')
    
    switch_slope = 2;
    GroundSlope.Constant = 0;
    GroundSlope.Time = slope_time;
    GroundSlope.Values = slope_values;
    Elevation.Time = [0];
    Elevation.Values = [0];
    
elseif strcmp(slope_mode,'Ground Slope via Elevation Profile')

    switch_slope = 1;
    GroundSlope.Constant = 0;
    GroundSlope.Time = [0];
    GroundSlope.Values = [0];
    Elevation.Time = elevation_time;
    Elevation.Values = elevation_values;
    
end
