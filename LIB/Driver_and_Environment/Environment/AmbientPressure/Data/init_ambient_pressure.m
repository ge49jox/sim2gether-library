function [AmbientPressure, Elevation, switch_slope] = init_ambient_pressure(pressure_mode,pressure_constant,pressure_values,pressure_time,elevation_values,elevation_time)

% Designed by: Benedikt Taiber (FTM, Technical University of Munich)
% ------------------------------------------------------------------------
% Description: Function initializes AmbientPressure block
% ------------------------------------------------------------------------
% Input:    - pressure_mode: User selection for pressure-mode
%           - pressure_constant: Constant ambient pressure value
%           - pressure_values: [1xN] vector containing custom ambient 
%                              pressure values
%           - pressure_time: [1xN] vector containing time stamps for custom
%                            ambient pressure values
%           - elevation_values: [1xN] vector containing custom elevation
%                               profile values
%           - elevation_time:[1xN] vector containing time stamps for custom
%                           elevation values
% ------------------------------------------------------------------------
% Output:   - Pressure: Struct containing constant and custom ambient
%                       pressure values
%           - Elevation: Struct containing custom elevation profile values
%           - switch_pressure: Decision variable for switch within model 
%                              block
% ------------------------------------------------------------------------

AmbientPressure = struct('Constant',0,'Time',[],'Values',[]);
Elevation = struct('Time',[],'Values',[]);
    % Pre-allocation of result-structs

if strcmp(pressure_mode,'Constant Ambient Pressure')
    
    switch_slope = 3;
    AmbientPressure.Constant = pressure_constant;
    AmbientPressure.Time = [0];
    AmbientPressure.Values = [0];
    Elevation.Time = [0];
    Elevation.Values = [0];
    
elseif strcmp(pressure_mode,'Custom Ambient Pressure')
    
    switch_slope = 2;
    AmbientPressure.Constant = 0;
    AmbientPressure.Time = pressure_time;
    AmbientPressure.Values = pressure_values;
    Elevation.Time = [0];
    Elevation.Values = [0];
    
elseif strcmp(pressure_mode,'Ambient Pressure via Elevation Profile')

    switch_slope = 1;
    AmbientPressure.Constant = 0;
    AmbientPressure.Time = [0];
    AmbientPressure.Values = [0];
    Elevation.Time = elevation_time;
    Elevation.Values = elevation_values;
    
end

