function [LongWind, switch_wind] = init_longitudinal_wind(wind_mode,long_wind_velocity,long_wind_time,long_wind_speed)

% Designed by: Benedikt Taiber (FTM, Technical University of Munich)
% ------------------------------------------------------------------------
% Description: Function initializes Longitudinal_Wind block
% ------------------------------------------------------------------------
% Input:    - wind_mode: User selection for wind-mode
%           - long_wind_velocity: [1xN] vector containing wind velocity
%                                 values
%           - long_wind_time: [1xN] vector containing time stamps for wind
%                             velocity values
%           - long_wind_speed: Constant wind velocity value
% ------------------------------------------------------------------------
% Output:   - LongWind: Struct containing custom longitudinal wind velocity
%                       profile
%           - switch_wind: Decision variable for switch within model block
% ------------------------------------------------------------------------

LongWind = struct('Time',[],'Values',[],'Constant',0);
    % Pre-allocation of result-struct

if strcmp(wind_mode,'Constant Wind Velocity Value')
    
    switch_wind = 1;
    LongWind.Constant = long_wind_speed;
    LongWind.Time = [0];
    LongWind.Values = [0];
    
elseif strcmp(wind_mode,'Custom Wind Velocity Value')
    
    switch_wind = 0;
    LongWind.Constant = 0;
    LongWind.Time = long_wind_time;
    LongWind.Values = long_wind_velocity;

end