function [matrix_eta, vektor_M, vektor_n_rad,J,m,Jx,Jy,Jz] = Init_ElectricDriveEffMap (M_EM_max,M_EM_nenn,n_EM_max,n_EM_nenn, typ_EM, eta_mit_LE, U_Bat, cos_phi)

persistent matrix_eta_Old;
persistent vektor_M_Old;
persistent vektor_n_rad_Old;

[NewBuild] = CheckIfNewEffMapisrequired (M_EM_max,M_EM_nenn,n_EM_max,n_EM_nenn, typ_EM, eta_mit_LE, U_Bat, cos_phi)

if NewBuild==1
[matrix_eta, vektor_M, vektor_n_rad] = ElectricalEngineEfficiencyMap(M_EM_max,M_EM_nenn,n_EM_max,n_EM_nenn, typ_EM, eta_mit_LE, U_Bat, cos_phi)
matrix_eta_Old=matrix_eta;
vektor_M_Old=vektor_M;
vektor_n_rad_Old=vektor_n_rad;
else
matrix_eta=matrix_eta_Old;
vektor_M=vektor_M_Old;
vektor_n_rad=vektor_n_rad_Old;
end

%% Calculation of Mass and J_red und Inertia
[J,m] = Traegheit_EM( M_EM_nenn, n_EM_nenn, typ_EM)
[Jx,Jy,Jz] = TraegheitAchsen_EM( typ_EM, M_EM_nenn, n_EM_nenn )
end
