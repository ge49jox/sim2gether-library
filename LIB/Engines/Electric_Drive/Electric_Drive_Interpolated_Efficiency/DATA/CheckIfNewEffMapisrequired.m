function [NewBuild] = CheckIfNewEffMapisrequired (M_EM_max,M_EM_nenn,n_EM_max,n_EM_nenn, typ_EM, eta_mit_LE, U_Bat, cos_phi)


persistent M_EM_max_Old;
persistent M_EM_nenn_Old;
persistent n_EM_max_Old;
persistent n_EM_nenn_Old;
persistent typ_EM_Old;
persistent eta_mit_LE_Old;
persistent U_Bat_Old;
persistent cos_phi_Old;

EqualType = strcmp(typ_EM_Old, typ_EM);

if (M_EM_max_Old==M_EM_max) & (M_EM_nenn_Old==M_EM_nenn) & (n_EM_max_Old == n_EM_max) & (n_EM_nenn_Old==n_EM_nenn) & (EqualType==1) & (eta_mit_LE_Old == eta_mit_LE) & (U_Bat_Old==U_Bat) & (cos_phi_Old == cos_phi)    
    NewBuild=0;
else
    NewBuild=1;
    
    M_EM_max_Old=M_EM_max;
    M_EM_nenn_Old=M_EM_nenn;
    n_EM_max_Old=n_EM_max;
    n_EM_nenn_Old=n_EM_nenn;
    typ_EM_Old=typ_EM;
    eta_mit_LE_Old=eta_mit_LE;
    U_Bat_Old=U_Bat;
    cos_phi_Old=cos_phi;  
    
end



end