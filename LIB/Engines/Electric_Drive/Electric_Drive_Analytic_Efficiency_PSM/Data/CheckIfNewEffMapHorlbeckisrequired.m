function [NewBuild] = CheckIfNewEffMapHorlbeckisrequired (P_n, n_n, n_max, U_n, Uberlastfaktor, cphi, m, Schaltung, Erregung_Rotor, Kuehlung, Material_Stator, tics_omega, tics_M)

persistent P_n_Old;
persistent n_n_Old;
persistent n_max_Old;
persistent U_n_Old;
persistent Uberlastfaktor_Old;
persistent cphi_Old;
persistent m_Old;
persistent Schaltung_Old;
persistent Erregung_Rotor_Old;
persistent Kuehlung_Old;
persistent Material_Stator_Old;
persistent tics_omega_Old;
persistent tics_M_Old;

EqualMaterial = strcmp(Material_Stator_Old, Material_Stator);
EqualKuehlung = strcmp(Kuehlung_Old,Kuehlung);
EqualErregung = strcmp(Erregung_Rotor_Old,Erregung_Rotor);
EqualSchaltung = strcmp (Schaltung_Old,Schaltung);


if (P_n_Old==P_n) & (n_n_Old==n_n) & (n_max_Old == n_max) & (U_n_Old==U_n) & (Uberlastfaktor_Old==Uberlastfaktor) & (cphi_Old == cphi) & (m_Old==m) & (EqualMaterial==1) &  (EqualKuehlung==1) & (EqualErregung==1) & ( EqualSchaltung==1) & (tics_omega_Old==tics_omega) & (tics_M_Old==tics_M) 
    NewBuild=0;
else
    NewBuild=1;
    
P_n_Old = P_n;
n_n_Old = n_n;
n_max_Old = n_max;
U_n_Old = U_n;
Uberlastfaktor_Old = Uberlastfaktor;
cphi_Old = cphi;
m_Old = m;
Schaltung_Old = Schaltung;
Erregung_Rotor_Old = Erregung_Rotor;
Kuehlung_Old = Kuehlung;
Material_Stator_Old = Material_Stator;
tics_omega_Old = tics_omega;
tics_M_Old = tics_M;
    
end



end