function [omega_asfs1, omega_fs1fs2, omega_max] = g02_omega_lim(prim, varargin)
% Berechnet die Grenzdrehzahlen zwischen Ankerstellbereich,
% Feldschwächebereich I, Feldschwächebereich II und die maximale Drehzahl
% (in rad/s) aus elektrischer Sicht.
om_max = prim.n_max*2*pi/60;
%% Ankerstellbereich - Feldschwächebereich 1
% Der Ankerstellbereich geht bei der Drehzahl in den Feldschwächebereich 1 
% über, bei der gerade noch der Maximalstrom mit der maximalen Spannung 
% erreicht werden kann.
[idmax, iqmax] = g01_iMTPA(prim, prim.i_max);
as_fs1 = @(omega)(g01_us(prim, omega, idmax, iqmax, varargin{:})-prim.u_max);
try
    omega_asfs1 = fzero(as_fs1, [0 om_max]);
catch
    omega_asfs1 = om_max;
    omega_fs1fs2 = om_max;
    omega_max = om_max;
    return;
end
%% Feldschwächebereich 1 - Feldschwächebereich 2
% Der Feldschwächebereich 1 endet bei der Drehzahl, bei der auch mit der 
% maximalen Spannung nicht mehr der maximale Strom gestellt werden kann, 
% dort geht er in den Feldschwächebereich 2 über. 
fs1_fs2 = @(omega)(i_MMPV_s(prim, omega, varargin{:})-prim.i_max);
try
    omega_fs1fs2 = fzero(fs1_fs2, [omega_asfs1 om_max]);
catch
    omega_fs1fs2 = inf;
%     omega_max = om_max;
end
%% Elektrische Maximaldrehzahl
% Hier soll die Drehzahl angegeben werden, ab der kein Moment mehr
% produziert werden kann, also die maximale Leerlaufdrehzahl.
fs2_max = @(omega)M_MMPV(prim, omega, varargin{:});
try
    omega_max = fzero(fs2_max, [0 om_max]);
catch
    fs1_max = @(omega)M_FW(prim, omega, varargin{:});
    try
        omega_max = fzero(fs1_max, [omega_asfs1 om_max]);
        omega_max = real(omega_max)-imag(omega_max); % liegt sehr sicher in sinnvollem Bereich
    catch
        omega_max = om_max;
    end
end

end

function i = i_MMPV_s(prim, omega, varargin)
% Gibt den maximalen Strombetrag in Abhaengigkeit von omega an, der auf der MTPV-Kurve erreicht wird.
    [i_d, i_q] = g01_iMTPV_ro(prim, omega, varargin{:});
    i = sqrt(i_d.^2 + i_q.^2); % Realteil prüfen, um Fehler bei Nullstellensuche zu unterdrücken?
end

function M = M_MMPV(prim, omega, varargin)
% Gibt das Moment an, das bei Betrieb auf der MTPV-Kurve erreicht wird.
    [i_d, i_q] = g01_iMTPV_ro(prim, omega, varargin{:});
    M = g01_Mi(prim, i_d, i_q);
end

function M = M_FW(prim, omega, varargin)
% Gibt das Moment an, das bei Betrieb auf der FW-Kurve erreicht wird.
    [i_d, i_q] = g01_iFW_ro(prim, omega, prim.i_max, varargin{:});
    M = g01_Mi(prim, i_d, i_q);
    M(abs(imag(M))>1e-3) = -1;
end