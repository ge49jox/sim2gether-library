function sec = g02_secondary_parameters(prim, varargin)
% Berechnet sekundaere Parameter der Synchronmaschine aus primaeren.
    % Charakteristische Drehzahlen, z.B. Eckdrehzahl
    [sec.omega_nom, sec.omega_fs1fs2, sec.omega_max] = g02_omega_lim(prim, varargin{:});
    % Ansteuerung im Nennpunkt
    [i_d, i_q] = g01_iMTPA(prim, prim.i_max);
    [u_d, u_q] = g01_udq(prim, sec.omega_nom, i_d, i_q, varargin{:});
    % Nennmoment
    sec.M_nom = g01_Mi(prim, i_d, i_q);
    % Effizienz im Nennpunkt
    sec.eta_nom = (sec.M_nom*sec.omega_nom)/g01_P(prim, i_d, i_q, u_d, u_q);
end