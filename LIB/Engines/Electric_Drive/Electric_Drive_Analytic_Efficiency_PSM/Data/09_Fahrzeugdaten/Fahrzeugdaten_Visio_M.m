%% Dieses Skript legt die Fahrzeugdaten f�r die Simulation fest:


%% Fahrzeug-Parameter initialisieren:

       g = 9.81; % Erdbeschleunigung (in m/s^2 = N/kg);
rho_Luft = 1.2; % Luftdichte bei 20�C, (in kg/m^3);
   r_Rad = 0.3; % Radradius (in m);
   
     c_w = 0.24; % Luftwiderstandskoeffizient (dimensionslos);  
  c_Roll = 0.01; % Rollwiderstandskoeffizient (dimensionslos);
lambda_m = 1.05; % Drehmassenzuschlagsfaktor (dimensionslos);

i_ges = 9.2;   % Gesamtgetriebe�bersetzung;
m_Fzg = 650;   % Fahrzeugmasse, inkl. Fahrer;
A_Stirn = 1.2; % Stirnfl�che;
    
m_Fzg_eff = m_Fzg*lambda_m; % effektive Fahrzeugmasse; Sie ber�cksichtigt
    % auch die Tr�gheit aller rotierenden Massen.
    
t_Schrittweite = 0.1; % Schrittweite f�r die Simulation in Simulink;

