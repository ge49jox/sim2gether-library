% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fahrzyklen werden initialisiert.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NEDC;       % New European Driving Cycle (auf deutsch: NEFZ);
UDC;        % Urban Driving Cycle, Stadtteil von NEDC;
EUDC;       % Extra Urban Driving Cycle, Überlandteil von NEDC;
CADC_rural; % Common Artemis Driving Cycle, rural = auf dem Land;
CADC_urban; % Common Artemis Driving Cycle, urban = in der Stadt;
FTP72;      % Federal Test Procedure 1972;



