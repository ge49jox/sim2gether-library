% Dieses Skript berechnet die maximale Leistung, welche die Maschine
% abliefern kann. Dieser Wert wird f�r die Plotoptionen und f�r das
% Kennfeld ben�tigt.



P_max_1=max(Pelmotgen_Matrix_neu);

P_max=abs(max(P_max_1));

clearvars P_max_1