function [ Pelmotgen_Matrix_neu ] = ...
    no_NaN_in_mot_aus_Matrix_SM( Pelmotgen_Matrix_alt )

% Input dieser Funktion ist eine beliebige Kennfeldmatrix, in der
% die elektrische Wirkleistung P_el in Abh�ngigkeit der
% Winkelgeschwindigkeit W und des Drehmoments M hinterlegt ist.
% Solche Matrizen erstrecken sich von - M_N <= M <= +M_N,
% d. h. sowohl f�r den Motorbetrieb, als auch f�r den Generatorbetrieb,
% daher die Bezeichnung Pelmotgen.

% Nicht erreichbare Punkte in der M-W-Ebene sind stets mit NaN belegt.
% Angenommen man verlangt einen Punkt, welcher �ber der motorischen
% Grenzlurve liegt, so w�rde der Rechner versuchen, auf ein NaN
% zuzugreifen. Dies liefert in Simulink eine Fehlermeldung, was auch
% erw�nscht ist. So kann man schnell erkennnen, dass die ASM nicht in der
% Lage ist, den Zyklus mit den gegebenen Parametern zu bew�ltigen.
% Die Simulation wird abgebrochen.

% Anders verh�lt es sich f�r NaN-Punkte im negativem M-Bereich:
% Angenommen, man verlangt einen Punkt, welcher unter der generatorischen
% Grenzkurve liegt, so w�rde der Rechner erneut versuchen, auf ein NaN
% zuzugreifen. Dies bedeutet lediglich, dass die gew�nschte Verz�gerung
% durch den Generatorbetrieb allein nicht abgedeckt werden kann.
% In der Realit�t w�rde der Rest durch die mechanischen Bremsen
% abgedeckt werden. D. h. f�r alle Punkte unterhalb der generatorischen
% Grenzkurve kann der Wert der Grenzkurve selbst angenommen werden.
% Es wird dann eben nur soviel rekuperiert, wie m�glich ist.
% Die mechanischen Bremsen m�ssen somit in der Simulation nicht explizit
% ber�cksichtigt werden, da nur der Energieverbrauch der ASM und damit nur
% die elektrische Leistung P_el von Interesse ist.

% Diese Funktion liefert somit eine Matrx, die im Generator-Bereich (M < 0)
% keinerlei NaN-Wert mehr enth�lt, daher der Name no_NaN_in_gen.
% Angenommen man betrachtet eine feste Winkelgeschwindigkeit W und bewegt
% sich von M = 0 immer weiter nach unten. Irgendwann wird man an die 
% generatorische Grenzkurve sto�en. Alle P_el-Werte weiter unten haben
% zun�chst den Wert NaN. All diese Werte werden ersetzt durch den
% M-Wert der Grenzkurve. Die so entstandene neue Matrix wird sp�ter in
% Simulink eingebunden werden, in Form einer 2D Lookup Table.

%% Berechnung von Pelmotgen_Matrix_neu:

Pelmotgen_Matrix_neu = Pelmotgen_Matrix_alt; % nur Initialisierung!
% Sp�ter werden in Pelmotgen_Matrix_neu f�r negative M alle NaN-Eintr�ge
% entfernt und durch den generatorischen Grenzkurvenwert ersetzt!

[zmax, smax] = size(Pelmotgen_Matrix_alt);

hilfsindex_s = (smax )/2; % muss eine nat�rliche Zahl sein!
% Bsp: [-40,-30,-20,-10,0,+10,+20,+30,+40]
% => zmax = 9 => hilfsindex_z = 4, ;


for z = 1 : zmax    
    for s =  hilfsindex_s : smax% Gehe im Kennfeld mitte nach rechts:
        Eintrag = Pelmotgen_Matrix_alt(z,s); 
        if isnan(Eintrag) == 1 % erster Eintrag, der Ein NaN ist:   
            
                Pelmotgen_Matrix_neu(z,s) = Pelmotgen_Matrix_neu(z,s-1);
         
        end
    end
end


end % Hier endet die Funktion

