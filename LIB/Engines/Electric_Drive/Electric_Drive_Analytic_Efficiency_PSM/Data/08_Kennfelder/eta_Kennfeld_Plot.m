gcf = figure( ...
    'PaperUnits','centimeters', ...
    'Units','centimeters', ...
    'Position',[ ...
        Plot_figure_x, ...
        Plot_figure_y, ...
        Plot_figure_Breite, ...
        Plot_figure_Hoehe ...
    ], ...
    'Resize',Plot_Resize_String ...
);

gca = axes( ...
    'XLim',[0, 1250], ...
    'YLim',[0, Plot_y_max] ...
);


hold on;
grid on;
set(gca,'layer',Plot_Layer_String);
box on;

%title('Wirkungsgrad-Kennfeld');
xlabel('Winkelgeschwindigkeit \Omega/(rad/s)');
ylabel('Drehmoment M/(Nm)');





[C,h] = contourf(OM, M, ...
    om_M_eta,Plot_Contour_Levels_eta);
% "(1/1000)", damit P_el in kW angezeigt wird.

clabel(C,h,Plot_Label_Vektor_eta);
%clabel(C,'manual'); % manuelles Setzen von Zahlenwerten;
% clabel(C,h,'manual'); % manuelles Setzen von Zahlenwerten; zus�tzlich
    % werden die Labels rotiert.


h2= plot(om,m, 'red', 'linewidth', 2);
h4=plot(om_min,m_min, 'black', 'linewidth', 2);

% Fahrzyklus-Kurve:
%h1 = plot( ...
%        Simulation_Ergebnis.signals.values(:,1), ... % W
%        Simulation_Ergebnis.signals.values(:,2), ... % M
%        'ro', ...
%        'LineWidth',Plot_Fahrzyklus_LineWidth ...
%);

hcb = colorbar(Plot_Colorbar_Position_String);
colorTitleHandle = get(hcb,'Title');
titleString = ('Wirkungsgrad \eta');
set(colorTitleHandle ,'String',titleString);



%colormap jet

legend( ...
    [h2 h4],{ ...
       Plot_GK_mot_ue_Legend_String, ...             
       'motorische Grenzkurve'
    } ...
);




clearvars gca gcf X Y C h ... % werden nicht mehr ben�tigt
    hcb colorTitleHandle titleString 
%    h1 h2 h3 h4
