% In diesem Skript werden alle Momente, Winkelgeschwindigkeiten und
% dazugeh�rigen elektrischen Leistung so getrimmt, gespiegelt und
% erweitert, dass damit ein Fahrzyklussimulation durchgef�hrt werden kann.

M_umgedreht=-fliplr(M);
M_addiert=[M_umgedreht M];
M_gesamt=[M_addiert;M_addiert]; 

om_M_Pel_umgedreht=-flipud(om_M_Pel);
om_M_Pel_addiert=[om_M_Pel_umgedreht;om_M_Pel]; %Matrix f�r die Simulation

om_M_Pel_umgedreht_2=-fliplr(om_M_Pel);  %zum Kennfeldplotten ben�tigt
om_M_Pel_addiert_2=[om_M_Pel_umgedreht_2 om_M_Pel];
om_M_Pel_gesamt=[om_M_Pel_addiert_2;om_M_Pel_addiert_2]; 

OM_umgedreht=fliplr(OM);
OM_addiert=[OM_umgedreht OM];
OM_gesamt=[OM_addiert;OM_addiert]; 


%om_M_eta_umgedreht=fliplr(om_M_Pel);
%om_M_eta_addiert=[om_M_eta_umgedreht om_M_eta];
%om_M_eta_gesamt=[om_M_eta_addiert;om_M_eta_addiert]; 


OM_Vektor_lang=OM_gesamt(:,1)';

OM_Vektor=OM(:,1)';

M_Vektor_lang=M_gesamt(1,:);


z=tics_M+1;
M_Vektor_lang(1,z)=0.00001;

clear vars z
