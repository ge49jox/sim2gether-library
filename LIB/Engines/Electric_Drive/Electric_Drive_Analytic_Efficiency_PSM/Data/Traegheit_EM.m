function [J,m] = Traegheit_EM( M_nenn, n_nenn, typ_EM)



if strcmp('ASM', typ_EM)
m_ASM = Masse_EM(M_nenn,n_nenn,'ASM'); 
m_PSM = Masse_EM(M_nenn,n_nenn,'PSM');%[kg]

J_PSM = 0.0002*M_nenn-0.0029;   %[kg m^2] Pesce S.49, Datenbasis bis ca. 330Nm;
J_ASM = (1+(m_ASM - m_PSM)/(2*m_PSM))*J_PSM;%[kg m^2]

%Untergrenzen, Pesce S.49
if J_ASM < 0.001
    J_ASM = 0.001;
end

J=J_ASM;
m=m_ASM;

elseif strcmp('PSM', typ_EM)
m_PSM = Masse_EM(M_nenn,n_nenn,'PSM');%[kg]    
J_PSM = 0.0002*M_nenn-0.0029;   %[kg m^2] Pesce S.49, Datenbasis bis ca. 330Nm;

%Untergrenzen, Pesce S.49
if J_PSM < 0.001
    J_PSM = 0.001;
end

J=J_PSM;
m=m_PSM;

end

end
