function [OM, M, om_M_eta_1, om_M_Pel_1, om_P_out] = g05_efficiency_map(par, tics_omega, tics_M, varargin)

%% Vorbereiten des Kennfeldbereichs
% Achsenwerte
M_val = linspace(0,par.sec.M_nom, tics_M);
omega_val = linspace(0,par.prim.n_max*2*pi/60, tics_omega);
[M, OM] = meshgrid(M_val, omega_val);

if(nargin>3)
    consider_losses = varargin{1}.consider_losses;
else
    consider_losses = false;
end
if(consider_losses)
    M_brutto = M+g05_power_loss(OM,M)./OM;
    M_brutto(isnan(M_brutto)) = 0;
else
    M_brutto = M;
end

%% Berechnen der Stellgroessen
[om_M_id, om_M_iq, om_M_ud, om_M_uq] = g03_control_momentbased(par, M_brutto, OM, varargin{:});


%% Berechnung der Effizienzwerte in jedem Betriebspunkt
om_M_Pmech = omega_val'*M_val;                                      % Mechanische Leistung
om_M_Pel_1 = g01_P(par.prim, om_M_id, om_M_iq, om_M_ud, om_M_uq);     % Elektrische Leistung


if(consider_losses)
    om_P_out = (om_M_Pmech-g05_power_loss(OM,M));
else
    om_P_out = om_M_Pmech;
end

om_M_eta_1 = om_P_out./om_M_Pel_1;                                    % Wirkungsgradkennfeld

end