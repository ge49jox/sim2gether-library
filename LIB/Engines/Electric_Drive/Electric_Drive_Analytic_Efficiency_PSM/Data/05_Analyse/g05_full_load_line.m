function [omega, M, eta] = g05_full_load_line(par, tics_omega, varargin)
% Berechnet die Volllastkennline und die zugehoerigen Effizienzwerte

%% Vorbereiten des Kennfeldbereichs
% Achsenwerte
omega = linspace(0,par.sec.omega_max, tics_omega);

%% Berechnen der Stellgroessen
FS1 = find(omega<par.sec.omega_fs1fs2);
FS2 = find(omega>=par.sec.omega_fs1fs2);
if(~isempty(FS1))
[M(FS1), om_id(FS1), om_iq(FS1)] = g03_control_currentbased(par.prim, omega(FS1), ones(size(FS1))*par.prim.i_max, varargin{:});
end
if(~isempty(FS2))
    [om_id(FS2), om_iq(FS2)] = g01_iMTPV_ro(par.prim, omega(FS2), varargin{:});
    M(FS2) = g01_Mi(par.prim, om_id(FS2), om_iq(FS2));
end

[om_ud, om_uq] = g01_udq(par.prim, omega, om_id, om_iq, varargin{:});
if(nargin>2)
if(varargin{1}.consider_losses)
    M = M-g05_power_loss(omega, M)./omega;
end
end
%% Berechnen der Effizienzwerte
om_Pel = g01_P(par.prim, om_id, om_iq, om_ud, om_uq);
om_Pmech = M.*omega;
eta = om_Pmech./om_Pel;


end