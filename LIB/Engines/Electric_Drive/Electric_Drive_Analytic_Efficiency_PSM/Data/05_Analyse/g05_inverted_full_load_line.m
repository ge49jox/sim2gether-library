function omega = g05_inverted_full_load_line(par, M, varargin)
% Berechnet die Volllastkennlinie mit vertauschten Achsen
omega = zeros(size(M));
for i = 1:1:length(M)
    delta_m = @(omega)(g03_max_moment(par, omega, varargin{:})-M(i));
    try
        omega(i) = fzero(delta_m, [0 par.sec.omega_max]);
    catch
        omega(i) = NaN;
    end
end
end