function [i_d_red, i_q_red] = g05_downscaled_currvect(par, omega, i_d, i_q)
% Skaliert einen Vektor so, dass er die Maximalspannung nicht
% überschreitet
i_d_red = zeros(size(omega));
i_q_red = zeros(size(omega));
for i = 1:1:length(omega)
    overvoltage = @(alpha)(g01_us(par.prim, omega(i), alpha*i_d(i), alpha*i_q(i))-par.prim.u_max);
    if(overvoltage(1)<=0)
        i_d_red(i) = i_d(i);
        i_q_red(i) = i_q(i);
    else
        try
            a = fzero(overvoltage, [0 1]);
            i_d_red(i) = a*i_d(i);
            i_q_red(i) = a*i_q(i);
        catch
            i_q_red(i) = 0;
            i_d_red(i) = -sqrt(i_d(i)^2+i_q(i)^2);
        end
    end
end
end