function overview_par(par, elementnumber)

T = struct2table(par{elementnumber}.sec);
[omega, M, ~] = g05_full_load_line(par{elementnumber}, 50);
plot(omega*30/pi, M);
grid on;
a = gca;
a.Position([2 4]) = [0.2 0.7];
t = uitable('Data', T{:,:}, 'ColumnName', fieldnames(par{elementnumber}.sec));
t.ColumnName{1} = 'n_nom';
t.Data(1) = t.Data(1)*30/pi;
t.Position(3:4) = t.Extent(3:4);
t.Units = 'normalized';
t.Position(1) = a.Position(1);
title(num2str(elementnumber));

end