function P_loss = g05_power_loss(om, m)
% Berechnet pauschale Verluste
  % Hohe Verluste verschieben Bestpunkt in Richtung hoher Leistung
magn_loss = 1e-4*om.^2+1e0*om;  % Siehe Dissertation Peters, Faktoren unbekannt
mech_loss = 5e-4*m.*om;         % von SKF.com
P_loss = magn_loss+mech_loss;
end