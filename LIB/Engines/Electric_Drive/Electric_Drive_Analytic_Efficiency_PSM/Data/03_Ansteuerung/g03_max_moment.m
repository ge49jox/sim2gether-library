function Mm = g03_max_moment(par, omega, varargin)
% Berechnet das maximal mögliche Moment bei omega.
    id = zeros(size(omega));
    iq = zeros(size(omega));
    
    for j = 1:1:numel(omega)
        if(omega(j)<par.sec.omega_nom) % Ankerstellbereich
            [id(j), iq(j)] = g01_iMTPA(par.prim, par.prim.i_max);
        else if(omega(j)<par.sec.omega_fs1fs2) % Feldschwächebereich 1
                [id(j), iq(j)] = g01_iFW_ro(par.prim, omega(j), par.prim.i_max, varargin{:});
            else    % Feldschwächebereich 2
                [id(j), iq(j)] = g01_iMTPV_ro(par.prim, omega(j), varargin{:});
            end
        end
    end
    Mm = g01_Mi(par.prim, id, iq);
end