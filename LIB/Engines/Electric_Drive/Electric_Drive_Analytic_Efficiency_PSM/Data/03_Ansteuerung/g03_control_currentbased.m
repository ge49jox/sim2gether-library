function [M, id, iq] = g03_control_currentbased(prim, omega, i_s, varargin)
% Berechnet das Moment und die Stromkomponenten in Abh�ngigkeit des 
% Strombetrags

% Versuch, den Betriebszustand mit minimalem Strombetrag zu erreichen
    [id, iq] = g01_iMTPA(prim, i_s);
% Pruefung, ob Feld zum Einhalten der Spannungsgrenze geschwaecht werden muss
    overvoltage = find(g01_us(prim, omega, id, iq, varargin{:})>prim.u_max);
%     overvoltage = overvoltage(:)';
    if(~isempty(overvoltage))
        [id(overvoltage), iq(overvoltage)] = g01_iFW_ro(prim, omega(overvoltage), i_s(overvoltage), varargin{:});
    end
% Pruefung, ob Betriebszustand moeglich
    imaginary = (imag(id)~=0)|(imag(iq)~=0);
    impossible = imaginary|(g01_us(prim, omega, id, iq, varargin{:})>prim.u_max+1e-2);
    possible = ~impossible;

% Berechnen des Moments
    M = zeros(size(omega));
    % Nicht fahrbare Werte liefern Moment -1 (Fehler)
    M(impossible) = -1;
    % Moment f�r fahrbare Betriebspunkte
    M(possible) = g01_Mi(prim, id(possible), iq(possible));
end