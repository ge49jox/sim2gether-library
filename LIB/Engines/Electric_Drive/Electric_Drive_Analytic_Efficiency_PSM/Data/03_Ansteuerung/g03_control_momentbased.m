function [i_d, i_q, u_d, u_q] = g03_control_momentbased(par, M, omega, varargin)
% Berechnet die erforderlichen Steuergr��en, um das gew�nschte Moment bei
% vorgegebener Drehzahl zu erhalten.

na = NaN;
Mm = g03_max_moment(par, omega, varargin{:});
    
    i_d = zeros(size(omega));
    i_q = i_d;
    u_d = i_d;
    u_q = i_d;
    imax = i_d;
    I = i_d;
    % Unmoegliche Betriebsbereiche
    impossible = find(M>Mm|imag(Mm))';
    possible = find(~(M>Mm|imag(Mm)))';%find(M<=Mm);
    i_d(impossible) = na;
    i_q(impossible) = na;
    u_d(impossible) = na;
    u_q(impossible) = na;

    % Festlegen der Obergrenze f�r den Strombetrag
    fs2 = find(omega>=par.sec.omega_fs1fs2);
    if(~isempty(fs2))
        [idMMPV, iqMMPV] = g01_iMTPV_ro(par.prim, omega(fs2), varargin{:});
        imax(fs2) = sqrt(idMMPV.^2+iqMMPV.^2);
    end
    imax(imax>par.prim.i_max) = par.prim.i_max;
    imax(omega<par.sec.omega_fs1fs2) = par.prim.i_max;
    
    % Bestimmen des erforderlichen Strombetrags
    loop = find(~(M>Mm|imag(Mm)));
    loop = loop(:)';
    for j = loop %find(M<=Mm)
        % Gleichung: M(idq(omega, i_s))-M = 0
        MI = @(i_s)(g03_control_currentbased(par.prim, omega(j), i_s, varargin{:})-M(j));
        try
            I(j) = fzero(MI, [0 imax(j)]);
        catch
            error(strcat('Fehler in der Nullstellensuche: Gewuenschtes Moment ', num2str(M(j)), ' mit maximalem Moment ', num2str(Mm(j)), ' nicht erreichbar.'));
        end
    end
    % Berechnen der Stromkomponenten
    [~, i_d(possible), i_q(possible)] = g03_control_currentbased(par.prim, omega(possible), I(possible), varargin{:});
    % Spannungskomponenten
    [u_d(possible), u_q(possible)] = g01_udq(par.prim, omega(possible), i_d(possible), i_q(possible), varargin{:});

end