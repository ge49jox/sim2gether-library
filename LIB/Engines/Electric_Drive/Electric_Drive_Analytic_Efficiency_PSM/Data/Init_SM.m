function [ om_M_Pel_addiert_2, om_M_Pel_addiert_Sim, M_Vektor_lang, OM_Vektor, omega_Grenz,m_Grenz,m_Grenz_neg,J,mass,Jx,Jy,Jz, init] =...
    Init_SM( P_n, n_n, n_max, U_n, Uberlastfaktor, cphi, m, Schaltung, Erregung_Rotor, Kuehlung, Material_Stator, tics )

%rmpath(genpath(strcat(erase(which('LIB_Electric_Drive'),'\LIB_Electric_Drive.slx'),'\Electric_Drive_Analytic_Efficiency_ASM')))

tics_omega=tics;
tics_M=tics;
persistent om_M_Pel_addiert_Old;
persistent om_M_Pel_addiert_Sim_Old;
persistent M_Vektor_lang_Old;
persistent OM_Vektor_Old;
persistent omega_Grenz_Old;
persistent m_Grenz_Old;
persistent m_Grenz_neg_Old;
persistent init_Old;

P_n=P_n*1000;%Nennleistung


[NewBuild] = CheckIfNewEffMapHorlbeckisrequired (P_n, n_n, n_max, U_n, Uberlastfaktor, cphi, m, Schaltung, Erregung_Rotor, Kuehlung, Material_Stator, tics_omega, tics_M)


if NewBuild == 1


%P_n=P_n*1000;%Nennleistung
%% Hauptskript zur Auslegung der Synchronmaschine
%clear all
%% Definition der Auslegungsparameter
%% Quantitative Parameter 

% P_n=30*1e3;                             % in W          - Nennleistung
% n_n=3000;                               % in 1/min      - Nenndrehzahl
% n_max = 10000;                          % in 1/min      - Maximaldrehzahl
% U_n=250;                             	% in V          - Nennspannung
% Uberlastfaktor=1;                     % Werte zwischen 1.0-4.0 w�hlen
% %�berlastfaktor 1 w�rde die Kennfelder im Nennbereich berechnen
% 
% %% Qualitative Parameter 
% cphi=0.9;                       
% % in 1 - Leistungsfaktor
% m=3;                                    % Zahl der Phasen des Wechselstroms (Strangzahl)
%Schaltung='Stern';                      % "Dreieck" oder "Stern"
%Erregung_Rotor='permanent';         	% hier nur "permanent" (=Vollpol)                           
% Kuehlung='Wasser';                      % "Luft" oder "Fluessig"
% Material_Stator='Kupfer';               % "Kupfer" oder "Aluminium"
%% Aufl�sung der Kennfelder einstellen:

% tics_omega = 62;
% tics_M     = 60;
% Diese Einstellungen definieren die Aufloesung des Kennfelds. Vorsicht,
% die Werte bestimmen ma�geblich die Rechenzeit! Immer die selbe Anzahl angeben!

%% Optionen Kennfeldberechnung 
    configuration.consider_resistance = true;
    configuration.motor_temp = 75;
    configuration.consider_losses = false;  %immer false; Verluste werden an anderer Stelle ber�cksichtigt
   % -------------------------------------------------------------------------
%% Matlabpfad festlegen:
%    addpath(genpath(pwd));
% -------------------------------------------------------------------------
%% Aufruf der Funktion zur iterativen Auslegung
    par = g04_iterative_design_motor(P_n, n_n, U_n, cphi, m, Schaltung, ...
    Erregung_Rotor, Kuehlung, Material_Stator, configuration, n_max);

%% Nennmoment 
    M_nenn=par.sec.M_nom;
    par.sec.omega_max=n_max/60*pi*2;
%% Nennlastkennlinie:
    [om_min, m_min, eta_min] = g05_full_load_line(par, tics_omega, configuration);
 
%% �berlast einstellen:
    Ueberlast_SM;
   
%% Eisenverluste berechnen:
    Parameter_Eisenverluste;
    %%
    Hauptskript_Eisenverluste_SM;

%% Kennfeldberechnung:
    [OM, M, om_M_eta_1, om_M_Pel_1, om_P_out] = g05_efficiency_map(par, tics_omega, tics_M, configuration);

%% Leistungskennfeld berechnen (inkl. Eisenverluste):
    om_M_Pel=om_M_Pel_1+P_Fe_Matrix; 
  
%% Wirkungsgradkennfeld berechnen (inkl. Eisenverluste):
    om_M_eta = om_P_out./om_M_Pel;
  

%% Fullloadline berechnen:
    [om, m, eta] = g05_full_load_line(par, tics_omega, configuration);

%% Kennfelder erweitern
    Matrizenswap;
  
%% Leistungskennfeld f�r Simulation anpassen:
   om_M_Pel_addiert_Sim= no_NaN_in_gen_aus_Matrix_SM(om_M_Pel_addiert_2);
      Pelmotgen_Matrix_neu=om_M_Pel_addiert_Sim;
   om_M_Pel_addiert_Sim= no_NaN_in_mot_aus_Matrix_SM(om_M_Pel_addiert_Sim);   
%% Maximale Leistung berechnen:
    P_max_berechnen;
    
%% Plotoptionen initialisieren
    Plot_Optionen_SM;
      
%% Wirkungsgrad-Kennfeld plotten
    eta_Kennfeld_Plot;

%% Leistungs-Kennfeld plotten
    Pel_Kennfeld_Plot;
%% Grenzkurve Moment kombiniert aus �berlastgrenzkurve und regul�re Lastgrenzkurve

[r,c]=find(om_min<max(om));
omega_Grenz=[om om_min(length(c)+1:end)];
m_Grenz=[m m_min(length(c)+1:end)];
m_Grenz_neg=-m_Grenz;

clearvars r c 
%% Fahrzeug initialisieren:
    %Fahrzeugdaten_Visio_M;

%% Fahrzyklen initialisieren:
    %Fahrzyklen;
 
%% Simulationsmodell_SM in Simulink �ffnen:
  %  Simulationsmodell_SM;
  
%% Das Ausf�hren dieser Zelle startet die Simulation:
    %set_param('Simulationsmodell_SM','SimulationCommand','start')
    
%% Speichert Workspace in struct

save_workspace;  
    
%% Variables   
om_M_Pel_addiert_Old = om_M_Pel_addiert_2;
om_M_Pel_addiert_Sim_Old = om_M_Pel_addiert_Sim;
M_Vektor_lang_Old = M_Vektor_lang;
OM_Vektor_Old = OM_Vektor;
omega_Grenz_Old = omega_Grenz;
m_Grenz_Old = m_Grenz;
m_Grenz_neg_Old= m_Grenz_neg;
init_Old= init;

else

om_M_Pel_addiert_2=om_M_Pel_addiert_Old;
om_M_Pel_addiert_Sim=om_M_Pel_addiert_Sim_Old;
M_Vektor_lang=M_Vektor_lang_Old;
OM_Vektor=OM_Vektor_Old;
omega_Grenz=omega_Grenz_Old;
m_Grenz=m_Grenz_Old;
m_Grenz_neg=m_Grenz_neg_Old;
init=init_Old;
    
end


%% Calculation of Mass and J_red und Inertia

if strcmp(Erregung_Rotor,'permanent')
typ_EM = 'PSM'
end
M_n=P_n/(2*pi*n_n/60);

[J,mass] = Traegheit_EM( M_n, n_n, typ_EM);
[Jx,Jy,Jz] = TraegheitAchsen_EM( typ_EM, M_n, n_n );

%addpath(genpath(strcat(erase(which('LIB_Electric_Drive'),'\LIB_Electric_Drive.slx'),'\Electric_Drive_Analytic_Efficiency_ASM')))


end

