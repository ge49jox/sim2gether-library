classdef machine_parameters
    % Parameter einer elektrischen Maschine
    properties (GetAccess = public, SetAccess = public)
        prim;       % Primaere elektrische Parameter
        mech;       % Mechanische Parameter
        T = 75;     % Aktuelle Betriebstemperatur
        sec;        % Sekundaere Elektrische Parameter
        X;          % Abweichung von der Grobauslegung
        config      % Konfiguration (fuer die Berechnung sekundaerer Parameter)
    end
    
    methods
        % ---------- CONSTRUCT ----------
        function obj = machine_parameters(prim, T, mech, config)
            obj.prim = prim;
            if(nargin>1)
                obj.T = T;
                if(nargin>2)
                    obj.mech = mech;
                    if(nargin>3)
                        obj.config = config;
                    end
                end
            end
        end
        % ---------- SET ----------
        function obj = set.prim(obj, prim)
            obj.prim = prim;
            obj.prim.R_s = resistance(obj);
            obj = secondary_par(obj);
        end
        function obj = set.sec(obj, sec)
            obj.sec = sec;
        end
        function obj = set.mech(obj, mech)
            obj.mech = mech;
        end
        function obj = set.T(obj, T)
            obj.T = T;
            obj.prim.R_s = resistance(obj);
        end
        function obj = set.X(obj, x)
            obj.X = x;
        end
        function obj = set.config(obj, config)
            obj.config = config;
            obj = secondary_par(obj);
        end
        % ---------- GET ----------
        function prim = get.prim(obj)
            prim = obj.prim;
        end
        function sec = get.sec(obj)
            sec = obj.sec;
        end
        function mech = get.mech(obj)
            mech = obj.mech;
        end
        % ---------- UPDATE ----------
        function obj = secondary_par(obj)
%             addpath('02_Nominalwerte');
            if(isempty(obj.config))
                obj.sec = g02_secondary_parameters(obj.prim);
            else
                obj.sec = g02_secondary_parameters(obj.prim, obj.config);
            end
            % rmpath('02_Nominalwerte');
        end
        function R = resistance(obj)
            if(isfield(obj.prim, 'res')) % Widerstand wird nur temperaturabhaengig angepasst, wenn die Daten dazu hinterlegt sind.
                switch obj.T
                    case 20
                        R = obj.prim.res.R_20;
                    case 75
                        R = obj.prim.res.R_75;
                    case 95
                        R = obj.prim.res.R_95;
                    case 115
                        R = obj.prim.res.R_115;
                    otherwise
                        R = obj.prim.res.R_20*(1+obj.prim.res.beta*(obj.T-20));  % siehe Skript von P. Kugelmann Z. 566
                end
            else
                R = obj.prim.R_s;
            end
        end
        
    end
end