%% Verwendete Strukturen und Variablen
% Dieses File soll als Index fuer zentrale verwendete Variablen dienen

%% Qualitative Auslegungsgroessen
cphi                % Leistungsfaktor (Verhaeltnis Blindleistung-Realleistung)
m                   % Strangzahl: Zahl der Stromkomponenten, ueblicherweise 3
Schaltung           % Schaltung der Phasen: Dreieck oder Stern (String)
Erregung_Rotor      % Fremd- oder Permanenterregt (String)
Kuehlung            % Art der Kuehlung: Wasser oder Luft (String)
Material_Stator     % Material der Spulen: Kupfer oder Aluminium (String)

%% Quantitative Auslegungsgroessen
P_n                 % Nennleistung in W
n_n                 % Nenndrehzahl in U/min
U_n                 % Nennspannung in V
p                   % Polpaarzahl

%% Primaere Parameter
par.prim.psi_PM     % Permanenter Fluss
par.prim.L_d        % Induktivitaet in Hauptrichtung
par.prim.L_q        % Induktivitaet in Querrichtung
par.prim.i_max      % Maximaler Strangstrom
par.prim.u_max      % Maximale Spannung
par.prim.p          % Polpaarzahl
par.prim.R_s        % Ohm'scher Widerstand je Strang
par.prim.n_max      % Zulaessige Hoechstdrehzahl
% Informationen zur Temperaturabhaengigkeit des Widerstandes
par.prim.res.R_20   % Widerstandswert bei 20�C
par.prim.res.R_75   % Widerstandswert bei 75�C
par.prim.res.R_95   % Widerstandswert bei 95�C
par.prim.res.R_115  % Widerstandswert bei 115�C
par.prim.res.beta   % Faktor Temperaturabhaengigkeit

%% Sekundaere Parameter
par.sec.omega_nom   % Winkelgeschwindigkeit im Nennpunkt
par.sec.omega_fs1fs2% Winkelgeschwindigkeit am Uebergang zwischen Feldschwaechebereich I und Feldschwaechebereich II auf der Volllastkennlinie
par.sec.omega_max   % Elektrische oder mechanische Hoechstwinkelgeschwindigkeit
par.sec.M_nom       % Nennmoment
par.sec.eta_nom     % Effizienz im Nennpunkt

%% Konfigurationsparameter
config.consider_resistance  % Ein- oder Ausschalten des Widerstandes in der Kennfeldberechnung
config.motor_temp   % Motortemperatur (temperaturabhaengiger Widerstand)