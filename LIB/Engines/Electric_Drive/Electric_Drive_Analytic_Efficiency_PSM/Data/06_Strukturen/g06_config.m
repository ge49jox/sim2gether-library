function config_param = g06_config( conf_input )
% Erstellt ein vollstaendiges Struct, das die Konfigurationsparameter fuer
% die Berechnung beinhaltet. Dabei wird das Struct immer dann aktualisiert,
% wenn die Funktion mit einem Input bzw. zum ersten Mal aufgerufen wird.

persistent config

if(isempty(config)||nargin==1)                          % Erstaufruf oder Konfigurationsaenderung
    %--------------- Standardkonfiguration hier erstellen -----------------
    default.consider_resistance = true;
    default.motor_temp = 75;
    % Weitere Parameter hier ergaenzen
    %----------------------------------------------------------------------
    if nargin==1
        config = conf_input;
        names = fieldnames(default);
        for i=1:1:length(names)                         % Pruefen aller Felder
            if(~isfield(config, names(i)))              % Wenn Feld unbesetzt
                config.(names(i)) = default.(names(i)); % Einsetzen des Standardwertes
            end
        end
    else                                                % Wenn keine Konfiguration spezifiziert
        config = default;                               % Zuweisen der Standardwerte
    end
end
%% Ausgabe
config_param = config;
end

