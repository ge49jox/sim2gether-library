function q = g04_fig_of_merit(par, P_n, n_n, varargin)

% Guetemass, das die Abweichung zwischen vorgegebenem und berechneten
% Nennpunkt der ausgelegten elektrischen Maschine bewertet.
par.config = varargin{1};
q = ((P_n./(2*pi.*n_n/60)-par.sec.M_nom)./(P_n./(2*pi.*n_n/60))).^2 ...
        +( ((2*pi.*n_n/60)-par.sec.omega_nom) ./ (2*pi.*n_n/60) ).^2;
end