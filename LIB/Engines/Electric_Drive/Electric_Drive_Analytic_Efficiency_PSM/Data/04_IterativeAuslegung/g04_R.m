function R = g04_R(res, temp_deg)
% Bestimmt den Widerstandswert in Abhaengigkeit der Temperatur. Die
% Informationen dazu sind in der Struktur res gespeichert.
switch temp_deg
    case 20
        R = res.R_20;
    case 75
        R = res.R_75;
    case 95
        R = res.R_95;
    case 115
        R = res.R_115;
    otherwise
        R=res.R_20*(1+res.beta*(temp_deg-20));  % siehe Skript von P. Kugelmann Z. 566
end
end