function par = g04_iterative_design_motor(P_n, n_n, U_n, cphi, m, Schaltung, Erregung_Rotor, Kuehlung, Material_Stator, optional_config, n_max)
% Aufteilung der Konstruktionsparameter in qualitative und quantitative.
% Hier werden nur quantitative Parameter variiert: P_n, n_n, U_n,
% zusammengefasst im Vektor x = [P_n, n_n, U_n]
eta = 0;
for p = 1:1:6
    x0 = ones(1,3);
    if(exist('optional_config', 'var'))
        temp_deg = optional_config.motor_temp;
        Q = @(x)g04_fig_of_merit(g04_Grobauslegung(x(1)*P_n, x(2)*n_n, x(3)*U_n, p, cphi, m, Schaltung, Erregung_Rotor, Kuehlung, Material_Stator, temp_deg), P_n, n_n, optional_config);
    else
        Q = @(x)g04_fig_of_merit(g04_Grobauslegung(x(1)*P_n, x(2)*n_n, x(3)*U_n, p, cphi, m, Schaltung, Erregung_Rotor, Kuehlung, Material_Stator), P_n, n_n);
    end
    [x_opt, q_val] = fminsearch(Q, x0);
    % Diese Berechnung nach der Optimierung ist eigentlich doppelt - man
    % koennte etwas Zeit sparen:
    par_cycle = g04_Grobauslegung(x_opt(1)*P_n, x_opt(2)*n_n, x_opt(3)*U_n, p, cphi, m, Schaltung, Erregung_Rotor, Kuehlung, Material_Stator);
    par_cycle.X = x_opt;
    % --------
    if(par_cycle.sec.eta_nom>eta&&q_val<1e-8)
        par = par_cycle;
        eta = par_cycle.sec.eta_nom;
    end
end
t=2;
assignin('base', 'x', t);
if(isempty(par))
    printf('Mit diesen Parametern wurde keine Loesung gefunden.\n');
end
end