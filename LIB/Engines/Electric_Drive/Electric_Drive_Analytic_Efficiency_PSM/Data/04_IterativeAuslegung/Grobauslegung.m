% P_n=16000
% n_n=5250
% U_n=310
% p=6
% Synchronmotorgrobauslegung nach Philipp Kugelmann.
% switch_plots = false;
mu_0=4e-7*pi;
P_n = P_n*1e-3; % Routine rechnet mit P in kW, wird aber der Uebersicht halber in W uebergeben.
% Loeschen der temporaeren Daten der uebrigen Funktionen
clear motorinputs M_i_s M_m;
%% ------------------------------------------------------------------------------------------------------------------------------- 
%     ERMITTLUNG DER HAUPTABMESSUNGEN 

%% Berechnung der Frequenz f der Wechselspannung und der Synchrondrehzahl n_s
f = p*n_n/60;
n_s=f/p*60;
%% Berechnung des Nenndrehmoments M_n
M=P_n*1000*60/(2*pi*n_s);

%% Auswahl der Bauweise des Rotors
if strcmp(Erregung_Rotor,'fremderregt')==1
    Bauweise_Rotor='Schenkelpol';
elseif strcmp(Erregung_Rotor,'permanent')==1
    Bauweise_Rotor='Vollpol';
else
    disp('ungueltige Erregung des Rotors')
    return
end

%% Ermittlung der Strangspannung U_str in V
if strcmp(Schaltung,'Dreieck')==1
    U_Str=U_n;
elseif strcmp(Schaltung,'Stern')==1
    U_Str=U_n/sqrt(3);
else
    disp('ungueltige Schaltung')
    return
end
%% Ermittlung der induzierten Spannung E_h in V
E_h=U_Str;      % Gilt nur f�r cphi=1, aber Abweichung bei cphi=0.8 nur +5% (�bererregt) oder -5% (untererregt), daher vernachl�ssigt

%% Absch�tzen des Nennwirkungsgrad eta_ang (anhand Vogt 2, S. 531)
if P_n<0.4
    eta_ang=93;
elseif P_n>=0.4 && P_n<=0.8
    eta_ang=94;
elseif P_n>0.8 && P_n<=2
    eta_ang=95;
elseif P_n>2 && P_n<=7
    eta_ang=96;
elseif P_n>7 && P_n<=20
    eta_ang=97;
elseif P_n>20 && P_n<=100
    eta_ang=98;
elseif P_n>100 && P_n<=2000
    eta_ang=99;
else
    disp('Nennleistung zu hoch')
    return
end

%% Berechnung der elektrischen Leistung P_el in kW
P_el=P_n/(eta_ang/100);
%% Berechnung der Scheinleistung P_s in kW oder kVA
P_S=P_el/cphi;
%% Berechnung des Strangstroms I_str in A
I_Str=P_S*1000/(m*U_Str);
%% Berechnung des Nennstroms I_n in A
if strcmp(Schaltung,'Dreieck')==1
    I_n=I_Str*sqrt(3);
elseif strcmp(Schaltung,'Stern')==1
    I_n=I_Str;
else
    disp('ungueltige Schaltung')
    return
end
%% Diagramm Nennwirkungsgrad eta in % (Vogt 6 - Bild 9.1.7 - S. 571)
% f1 = @(x) (-1.459e-07*x^4 + 0.0003945*x^3 + 98.49*x^2 + 1784*x + 430.9)/(x^2 + 18.49*x + 5.043);  % Funktion aus Curve Fitting Tool
% eta=f1(P_S);
% if(switch_plots)
% figure('Name','Nennwirkungsgrad eta','NumberTitle','off');
% hold on
% fplot(f1,[0.1 1000],'b');
%     xlabel('P_s in kW')
%     ylabel('\eta in %')
%     axis([0,2*P_S,90,100])                              % Berechenbar bis 1000 P_s, aber Anzeige nur im relevanten Bereich bis 2*P_s
%     grid on
% plot(P_S,eta,'rx');
% end


%% Diagramm Ausnutzungszahl C_s in kWmin/m3 (Vogt 6 - Bild 9.1.8 - S.573)
f2 = @(x) 5.559*exp(0.0002019*x) - 1.945*exp(-0.01073*x); 	% Funktion aus Curve Fitting Tool        
% if(switch_plots)
% figure('Name','Ausnutzungszahl C_s','NumberTitle','off');
% hold on
% fplot(f2,[10 1000],'b');
%     xlabel('P_s/2*p in kW')
%     ylabel('C_s in kWmin/m3')
%     grid on
% end
P_hilf=P_S/(2*p);
C_s1=f2(P_hilf);
if cphi == 0.8
    C_s=C_s1;                                          	% Diagramm gilt f�r cphi=0.8
else
    C_s=C_s1*(0.8+0.25*cphi);                           % bei cphi!=0.8: lineare Korrektur um +5% bei cphi=1 
end
% if(switch_plots)
% plot(P_hilf,C_s,'rx');
% end
clear P_hilf C_s1

%% Berechnung der Ausnutzungszahl C und C_mech in kWmin/m3
C=C_s*E_h/U_Str;
% C_mech=C*eta*cphi;

%% Ermittlung der relativen Ankerl�nge lambda (Vogt 2 - Tafel 14.3 - S.534)
if p==1
    lambda=1.4;                                         % soll zwischen 1 und 4 liegen
elseif p>1
    lambda=1.15*sqrt(p);                                % soll zwischen 0.5 und 2.5 liegen
    if lambda>4
        lambda=4;
    end
end
%% Berechnung der inneren Scheinleistung P_i in kW
P_iS=P_S*E_h/U_Str;

%% Berechnung des Durchmessers D in m
D=nthroot((P_iS*2*p)/(n_s*C*lambda*pi),3);              % nach 13.20 a) Vogt 5 - Seite 430

%% Kontrolle �ber maximale Umfangsgeschwindigkeit v_max (Vogt 6 - Seite 587f)
v=D*pi*n_s/60;
if strcmp(Bauweise_Rotor,'Schenkelpol')==1 && v>140 || strcmp(Bauweise_Rotor,'Vollpol')==1 && v>190
    disp('maximale Umfangsgeschwindigkeit v_max zu gro�')
    return
end
%% Berechnung der Polteilung T_p in m
T_p=(D*pi)/(2*p);

%% Berechnung des Luftspalts in mm
B_p_ang=0.80;                                           % laut Vogt 6 - S. 582	
if strcmp(Kuehlung,'Luft')==1
    A_ang=30;                                           % Strombelag in A/mm laut Vogt 6 - S. 580 zwischen 30 und 120 bei indirekter K�hlung mit Luft 
elseif strcmp(Kuehlung,'Wasser')==1
    A_ang=160;                                          % Strombelag in A/mm laut Vogt 6 - S. 580 zwischen 160 und 300 bei direkter K�hlung mit Wasser 
else
    disp('ungueltige K�hlart')
    return
end

if strcmp(Bauweise_Rotor,'Schenkelpol')==1
    delta_0_ang=0.45*T_p*A_ang/B_p_ang;                 % Gilt f�r Sinusfeldpole oder kreisbogenf�rmiger Polkontur
elseif strcmp(Bauweise_Rotor,'Vollpol')==1
    delta_0_ang=0.25*T_p*A_ang/B_p_ang;
else
    disp('ungueltige Bauweise des Rotors')
    return
end
% Komma durch Punkt ersetzt von Luca Puccetti
if delta_0_ang<0.2;
    delta_0_ang=0.2;
end

%% Berechnung der L�ngen in m
l_i=P_S/(C_s*D^2*n_s);                                  % ideellen Ankerl�nge l_i in m
l_v=0.008;                                              % L�nge der K�hlkan�le l_v in m: Laut Vogt 6 - S. 585 zwischen 6 und 10mm
gamma_v_ang=1/(1+5*delta_0_ang/(l_v*1000));

if strcmp(Kuehlung,'Wasser')==1 || (strcmp(Kuehlung,'Luft')==1 && l_i<=0.2)
    n_v=0;
    l=l_i-2*delta_0_ang/1000;                           % reale Stator- und Ankerl�nge l in m (inkl. K�hlkan�le)
elseif strcmp(Kuehlung,'Luft')==1 && l_i>0.2
    n_v=floor((l_i*100)/8);                             % Anzahl der K�hlkan�le (entstehendes Teilpaket max. 8 cm lang)
    l=l_i+n_v*l_v*gamma_v_ang-2*delta_0_ang/1000;       % reale Stator- und Ankerl�nge l in m (inkl. K�hlkan�le)
end

l_Fe=l-n_v*l_v;                                         % Blechpaketl�nge l_fe in m
% l_P=2*l_i-l;                                            % Poll�nge l_p in m



%% ------------------------------------------------------------------------------------------------------------------------------- 
%     WICKLUNG UND DIMENSIONIERUNG DES ST�NDERS

%% Auswahl der Wicklungsart abh�ngig von der Leistung (Vogt. S.96)
if P_n>10
    Wicklungsart='gesehnte Zweischichtwicklung';
else
    Wicklungsart='Einschichtwicklung';
end
%% Winkelgeschwindigkeit der Versorgung W in rad/s
W_fn=2*pi*f;

%% Vorgabe der Luftspaltinduktion B_p_max in T, des Wicklungsfaktor xi_p sowie der Anzahl paralleler Wicklungszweige
xi_p=0.92;                                              % laut Vogt 6 - S. 603
a=1;                                                    % Anzahl der parallelen Zweige a                      

%% magnetischer Luftspaltfluss Phi_h in Wb
Phi_h_ang=2/pi*B_p_ang*l_i*T_p;
%% optimale Strangwindungszahl w_s_opt
w_Str_opt=sqrt(2)*E_h/(W_fn*xi_p*Phi_h_ang);
        
%% Optimierung der Strangwicklungszahl w_s  bei m�glichst kleiner Lochzahl q_s (Herstellungskosten)                  
w_fehlergrenze=0.08;                                    % Grenze der Wicklungszahlabweichung
w_fehler_tat=w_fehlergrenze+1;
        
T_N_min=0.014;                                          % minimale Nutteilung in m
T_N_max=0.070;                                          % maximale Nutteilung in m laut Vogt 6 - S. 585 (30-70mm, bei kleinen Maschinen 10-20)
        
if strcmp(Wicklungsart,'Einschichtwicklung')==1
    q_min=max(2,ceil(D*pi/(2*m*p*T_N_max)));            % minimale Lochzahl
    z_N_min=1;                                          % minimale Leiteranzahl je Nut
    z_N_schrittweite=1;
elseif strcmp(Wicklungsart,'gesehnte Zweischichtwicklung')==1
    q_min=max(3,ceil(D*pi/(2*T_N_max*p*m)));            % minimale Lochzahl
    z_N_min=2;                                          % minimale Leiteranzahl je Nut
    z_N_schrittweite=2;
end
q_max=floor(D*pi/(2*m*p*T_N_min));                      % maximale Lochzahl
z_N_max=floor(w_Str_opt*a/(p*q_min));                   % maximale Leiteranzahl je Nut


if z_N_max < z_N_min 
    z_N_max = z_N_min;
end
if q_max < q_min 
    q_max = q_min;
end
q_alt=q_min;
        
%% Variation der Lochzahl q und der Nutleiteranzahl z_n
for q_schleife=q_min:1:q_max
    for z_N_schleife=z_N_min:z_N_schrittweite:z_N_max
        w_schleife=p*q_schleife*z_N_schleife/a;
        w_fehler=abs(w_schleife-w_Str_opt)/w_Str_opt;
        if (w_fehler<=w_fehlergrenze && q_schleife>q_alt) || (w_fehler<w_fehler_tat && q_schleife==q_alt)
            q=q_schleife;                               % Polzahl je Nut und Strang (Lochzahl) q in 1
            z_N=z_N_schleife;                           % Leiteranzahl je Nut z_N in 1
            w_Str=w_schleife;                           % Strangwicklungszahl w_Str in 1
            N=2*p*m*q;                                  % St�ndernutzahl N in 1
            T_N=D*pi/N;                                 % Nutteilung T_N in m
            w_a=w_Str*m*a;                              % Gesamtwindungszahl w_ges in 1
%             w_P=w_a/(2*p);                              % Windungszahl pro Pol in 1;
            z_a=w_a*2;                                  % Gesamtleiteranzahl z_a in 1
            q_alt=q_schleife;
            w_fehler_tat=w_fehler;
        end
    end
end
clear w_schleife z_n_schleife q_schleife
% Eingefuegt von Luca Puccetti
if(~exist('w_Str', 'var'))
%     q = 3;
    z_N = 2;
    w_Str = 24;
    N = 72;
    T_N = 0.0087;
%     w_a = 72; 
%     w_P = 9;
    z_a = 144;
end
% Ende der Einfuegung;
%% Tats�chlicher Hauptwellenfluss Phi_h_tat in Wb
Phi_h_tat=E_h*sqrt(2)/(w_Str*W_fn*xi_p);
%% Tats�chliche Luftspaltinduktion B_p_tat in T
B_p_tat=pi/2*Phi_h_tat/(l_i*T_p);

%% Ermittlung der Spulenweite W in 1
if strcmp(Wicklungsart,'Einschichtwicklung')==1
%     W=T_p;                                              % Spulenweite in m
%     u=1/2;                                              % Zahl der auf eine Nut entfallende Spulen
elseif strcmp(Wicklungsart,'gesehnte Zweischichtwicklung')==1
%     W=5/6*T_p;                                          % Spulenweite in m (gilt f�r 5/6 Sehnung)
%     u=1;                                                % Zahl der auf eine Nut entfallende Spulen
end
%% Ermittlung der Gesamtspulenanzahl k_ges in 1
% k_a=N*u;
%% Berechnung des Nutenschrittes y_n in 1
% y_d=N/(2*p);                                            % Durchmesserschritt in 1
% X_v=T_p-W;                                              % Spulenverk�rzung X_v in m
% y_v=N/(2*p)-W/T_N;                                      % Nutenschrittverk�rzung in 1
% y=y_d-y_v;                                              % Nutenschritt in 1
%% Berechnung der Spulenweite et und des Nuten- und Sehnungswinkels alpha_nS und et_v
% alpha_N=2*p/N*180;                                      % Nutenwinkel in Grad    
% et_v=y_v*alpha_N;                                       % Sehnungswinkel in Grad
% et=180-et_v;                                            % Spulenweite in bezogenen Koordinaten in Grad



%% Berechnung des Zweigstroms in A
I_Zw=I_Str/a;
%% Berechnung des effektiven Strombelags am Statorumfang A in A/mm
A=2*w_Str*m*I_Zw/(pi*D*1000);

%% maximalen Stromdichte im St�nder in A/mm^2  laut Vogt 6 - S. 580
if strcmp(Kuehlung,'Luft')==1 
    if strcmp(Bauweise_Rotor,'Vollpol')==1
        S_max=7;
    elseif strcmp(Bauweise_Rotor,'Schenkelpol')==1
        if strcmp(Wicklungsart,'Einschichtwicklung')==1
            S_max=4;
        elseif strcmp(Wicklungsart,'gesehnte Zweischichtwicklung')==1
            S_max=3.5;
        end
    end
elseif strcmp(Kuehlung,'Wasser')==1
    if strcmp(Bauweise_Rotor,'Vollpol')==1
        S_max=18;
    elseif strcmp(Bauweise_Rotor,'Schenkelpol')==1
        if strcmp(Wicklungsart,'Einschichtwicklung')==1
            S_max=10.5;
        elseif strcmp(Wicklungsart,'gesehnte Zweischichtwicklung')==1
            S_max=9;
        end
    end
end

%% Korrektur des Luftspaltes delta_0
if strcmp(Bauweise_Rotor,'Schenkelpol')==1
    delta_0=0.45*T_p*A/B_p_tat;                       	% Gilt f�r Sinusfeldpole oder kreisbogenf�rmiger Polkontur
elseif strcmp(Bauweise_Rotor,'Vollpol')==1
    delta_0=0.25*T_p*A/B_p_tat;
end
% Komma durch Punkt ersetzt von Luca Puccetti
if delta_0<0.2
    delta_0=0.2;
end

%% tats�chliche ideelle L�nge:
gamma_v_tat=1/(1+5*delta_0/(l_v*1000));                 % gamma_v_tat=(10/65)*(l_v*1000/delta_0)
l_i_tat=l-n_v*l_v*gamma_v_tat+2*delta_0/1000;

%% Luftspaltfluss:
Phi_delta=Phi_h_tat;                                    % Absch�tzung des Luftspaltfluss nach Vogt 6 - S. 604

%% Berechnung der H�he des St�nderr�ckens h_r in m
phi_Fe=0.95;
if strcmp(Bauweise_Rotor,'Schenkelpol')==1
    B_R_zul=1.45;                                    	% laut Vogt 6 - S 582 zwischen 1.0 und 1.45
elseif strcmp(Bauweise_Rotor,'Vollpol')==1
    B_R_zul=1.5;                                       	% laut Vogt 6 - S 582 zwischen 1.1 und 1.5
end

h_R=0.5*Phi_delta/(l_Fe*phi_Fe*B_R_zul);                % R�ckenh�he
%h_r=(B_p_tat*2/pi*T_p*l_i/2)/(l_fe*phi_fe*B_r_zul);  	% Vogt 6 S. 581 mit Annahmen: l_i/l_fe=1.1, B_m=B_p*2/pi (Vogt 6 - S. 604) 

if h_R>0.4*T_p                                       	% soll laut S. 586 0.2-0.4*T_p betragen
%     disp(['H�he des Ankerr�ckens h_r =',num2str(h_R,3),' wurde zu gro� berechnet, Korrektur zu 0.4*T_p'])
    h_R=0.4*T_p;
elseif h_R<0.2*T_p                                  	% soll laut S. 586 0.2-0.4*T_p betragen
%  	disp(['H�he des Ankerr�ckens h_r =',num2str(h_R,3),' wurde zu klein berechnet, Korrektur zu 0.2*T_p'])
    h_R=0.2*T_p;
end

%% Iterative Generierung der Nut- und Zahnform im Stator
phi_N=0.7;                                              % Nutf�llfaktor be. auf den reinen Leiterquerschnitt (Vogt 6 - S. 586)  0.35-0.60 - Quelle f�r 0.7: zytec Cofat 2015 (Messe Lorenz)
A_Leiter=z_N*I_Zw/S_max;
l_Leiter=2*(l+1.3*T_p+(0.03+0.02*U_Str/1000));          % mittlere Windungsl�nge nach Vogt 6 - 9.1.39 - S. 586
% A_Teilleiter=A_Leiter/z_N;                              % Leiterquerschnitt eines Leiters pro Nut

% Nutma�optimierung:
b_NS=8.5*D+1;                                           % Nutschlitzbreite
% h_NS=4*D+0.2;                                           % Nutschlitzh�he

if 0.2*T_N*1000 < b_NS
    b_N_start=b_NS;                                    % Vorgabe einer minimalen Nutbreite
else
    b_N_start=0.2*T_N*1000;
end
h_N_start=1;                                            % Vorgabe einer minimalen Nuth�he

A_N=b_N_start*(h_N_start-0.5*b_N_start);


b_N_a=b_N_start;
b_N_i=b_N_start;
h_N=h_N_start;

zaehler_nut_opti=0;
zaehler_case_1=0;
zaehler_case_2=0;
zaehler_case_3=0;
zaehler_case_4=0;
zaehler_case_5=0;
% Anmerkung von Luca Puccetti: Hier kann sich das Programm ewig drehen,
% daher wurde eine Obergrenze eingefuegt.
while (A_N*phi_N)<A_Leiter && zaehler_nut_opti<1e3                             % Rezept nach Skript von Lorenz S. 99
    
    b_Z_a=(D*1000+2*h_N-b_N_a)*pi/N-b_N_a;              % Zahnbreite au�en
    b_Z_i=(D*1000+b_N_i)*pi/N-b_N_i;                    % Zahnbreite innen
%     b_Z_m=(b_Z_a+b_Z_i)/2;                              % Zahnbreite in der Mitte
    
    B_R=(2/pi*B_p_tat*T_p*l_i)/(2*h_R*phi_Fe*l_Fe);     % R�ckeninduktion
    B_Z_a=(B_p_tat*T_N*l_i)/(b_Z_a/1000*phi_Fe*l_Fe);   % Zahninduktion au�en
    B_Z_i=(B_p_tat*T_N*l_i)/(b_Z_i/1000*phi_Fe*l_Fe);   % Zahninduktion innen
    
    if B_Z_a<B_R && B_Z_i<B_R && B_Z_a<B_Z_i            % Case 1
        Fall=1;
    elseif B_Z_a<B_R && B_Z_i<B_R && B_Z_a>=B_Z_i       % Case 2
        Fall=2;
    elseif B_R<B_Z_a && B_R<B_Z_i                       % Case 3
        Fall=3;
    elseif B_Z_a<B_Z_i                                  % Case 4
        Fall=4;
    else                                                % Case 5
        Fall=5;
    end
        
    switch Fall
    case 1
        zaehler_case_1=zaehler_case_1+1;
        b_N_a=b_N_a+0.1;
        
%         A_N=0.5*(b_N_i+b_N_a)*(h_N-0.5*b_N_a-0.5*b_N_i)+0.5*(0.5*b_N_a)^2*pi;
        b_Z_a=(D*1000+2*h_N-b_N_a)*pi/N-b_N_a;
        b_Z_i=(D*1000+b_N_i)*pi/N-b_N_i;
        b_Z_m=(b_Z_a+b_Z_i)/2;
        if b_N_a<=0 || b_N_i<=0 || b_Z_a<=0 || b_Z_i<=0 || b_Z_m<=0 || h_N<=0
%             Fall=2;
        end
        
    case 2
        zaehler_case_2=zaehler_case_2+1;
        b_N_i=b_N_i+0.1;
        
%         A_N=0.5*(b_N_i+b_N_a)*(h_N-0.5*b_N_a-0.5*b_N_i)+0.5*(0.5*b_N_a)^2*pi;
        b_Z_a=(D*1000+2*h_N-b_N_a)*pi/N-b_N_a;
        b_Z_i=(D*1000+b_N_i)*pi/N-b_N_i;
        b_Z_m=(b_Z_a+b_Z_i)/2;
        if b_N_a<=0 || b_N_i<=0 || b_Z_a<=0 || b_Z_i<=0 || b_Z_m<=0 || h_N<=0
%             Fall=3;
        end
        
    case 3
        zaehler_case_3=zaehler_case_3+1;
        h_N=h_N+0.1;
        
%         A_N=0.5*(b_N_i+b_N_a)*(h_N-0.5*b_N_a-0.5*b_N_i)+0.5*(0.5*b_N_a)^2*pi;
        b_Z_a=(D*1000+2*h_N-b_N_a)*pi/N-b_N_a;
        b_Z_i=(D*1000+b_N_i)*pi/N-b_N_i;
        b_Z_m=(b_Z_a+b_Z_i)/2;
        if b_N_a<=0 || b_N_i<=0 || b_Z_a<=0 || b_Z_i<=0 || b_Z_m<=0 || h_N<=0
%             Fall=4;
        end
        
	case 4
        zaehler_case_4=zaehler_case_4+1;
        b_N_a=b_N_a+0.1;
        
%         A_N=0.5*(b_N_i+b_N_a)*(h_N-0.5*b_N_a-0.5*b_N_i)+0.5*(0.5*b_N_a)^2*pi;
        b_Z_a=(D*1000+2*h_N-b_N_a)*pi/N-b_N_a;
        b_Z_i=(D*1000+b_N_i)*pi/N-b_N_i;
        b_Z_m=(b_Z_a+b_Z_i)/2;
        while b_N_a<=0 || b_N_i<=0 || b_Z_a<=0 || b_Z_i<=0 || b_Z_m<=0 || h_N<=0
%             Fall=5;
        end
        
    case 5
        zaehler_case_5=zaehler_case_5+1;
        b_N_i=b_N_i+0.1;
        
%         A_N=0.5*(b_N_i+b_N_a)*(h_N-0.5*b_N_a-0.5*b_N_i)+0.5*(0.5*b_N_a)^2*pi;
        b_Z_a=(D*1000+2*h_N-b_N_a)*pi/N-b_N_a;
        b_Z_i=(D*1000+b_N_i)*pi/N-b_N_i;
        b_Z_m=(b_Z_a+b_Z_i)/2;
        if b_N_a<=0 || b_N_i<=0 || b_Z_a<=0 || b_Z_i<=0 || b_Z_m<=0 || h_N<=0
            disp('Bei der Optimierung der Nut ist Case 5 entartet (Programmfehler!)')
            return
        end
        
    otherwise
        disp('Bei der Optimierung der Nut trifft kein Case zu (Programmfehler!)')
    end % ende von switch case
    
    A_N=0.5*(b_N_i+b_N_a)*(h_N-0.5*b_N_a-0.5*b_N_i)+0.5*(0.5*b_N_a)^2*pi;
%     b_Z_a=(D*1000+2*h_N-b_N_a)*pi/N-b_N_a;
%     b_Z_i=(D*1000+b_N_i)*pi/N-b_N_i;
%     b_Z_m=(b_Z_a+b_Z_i)/2;
    
    zaehler_nut_opti=zaehler_nut_opti+1;
end % nun ist die Nut gro� genug

% Zum nachvollziehen, welche Ma�e wie oft erh�ht wurden:
% zaehler_case_1        % b_n_a
% zaehler_case_2        % b_n_i
% zaehler_case_3        % h_n
% zaehler_case_4        % b_n_a
% zaehler_case_5        % b_n_i

% R�ckenteilung
% T_R=(D+2*h_N/1000)*pi/(2*p);
 D_a=D+2*(h_R+h_N/1000);                                 % Au�endurchmesser des kompletten St�nders



%% ------------------------------------------------------------------------------------------------------------------------------- 
%     ENTWURF DES L�UFERS

%% Berechnung des Innendurchmessers D_i in m
% D_L=D-2*delta_0/1000;                                   % Au�endurchmesser des Rotors

%% Berechnung der Polabmessungen nach Lorenz Skript (ab S. 100)
% Polbedeckungsfaktor nach Lorenz Skript (Tabelle S. 102)
% if p<2
%     alpha_i_ang=0.40;                                                   % Angenommen!! Mit 0.55 sehr hohe Werte f�r b_ps
% elseif p==2
%     alpha_i_ang=0.55;
% elseif p==3
%     alpha_i_ang=0.60;
% elseif p==4
%     alpha_i_ang=0.65;
% elseif p==5
%     alpha_i_ang=0.70;
% elseif p>=6
%     alpha_i_ang=0.75;
% end
% Polschuhform (S. 103)
% b_PS=alpha_i_ang*T_p*1000;                                              % Polschuhbreite
% b_P=0.55*b_PS;                                                          % Bisher nur angenommene Polbreite!!
% h_P=0.2*T_p*1000;                                                       % Polh�he nach Vogt 6 - Tab. 9.1.7 - S. 568 (alternativ Lorenz Skrip)
% h_PS=0.25*h_P;                                                          % Bisher nur angenommene Polschuhh�he!!   
% h_J=0.2*T_p*1000;                                                       % 0.2-0.4 T_p laut Vogt 5 - Tab. 13.10 - S. 439

% Phi_PK=1.2*Phi_delta;                                                   % Polstreufluss laut Vogt 6 - S. 604: 15-25% gr��er als der Luftspaltfluss
% Phi_J=Phi_PK/2;                                                         % Jochfluss Vogt 6 - Bild S. 179
% if strcmp(Bauweise_Rotor,'Schenkelpol')==1
%     B_pk_zul=1.8;                                                       % laut Vogt 6 - S. 582 zwischen 1.3 und 1.8
% elseif strcmp(Bauweise_Rotor,'Vollpol')==1
%     B_pk_zul=1.7;                                                       % laut Vogt 6 - S. 582 zwischen 1.1 und 1.7
% end
% A_PK=Phi_PK/B_pk_zul;                                                   % Querschnitt des Polkerns



    
%% Selbstinduktivit�t des Luftspaltfeldes / Hauptinduktivit�t der Erregerwicklung in H
% L_delta=mu_0*w_P^2*l_i_tat*2*p*T_p/(delta_0/1000)*alpha_i_ang;              % Vogt 6 - 8.1.18 - S. 515

%% Selbstinduktivit�t eines Stranges
L_h_Str=mu_0/(delta_0/1000)*2/pi*T_p*l_i_tat*4/pi*(w_Str*xi_p)^2/(2*p); % Vogt 6 - 8.1.29 b) - S. 519

%% Hauptinduktivit�t
% L_h=m/2*mu_0/(delta_0/1000)*2/pi*T_p*l_i_tat*4/pi*(w_Str*xi_p)^2/(2*p); % gilt f�r m=3   Vogt 6 - 8.1.42 b) - S. 524
   


%% Wicklungswiderst�nde nach Vogt 6 - S. 435
if strcmp(Material_Stator,'Kupfer')==1                  % Leitf�higkeit von Kupfer bei unterschiedlichen Temperaturen
    kappa_20=58;
    kappa_75=47.7;
    kappa_95=44.8;
    kappa_115=42.2;
    beta=0.00392;
elseif strcmp(Material_Stator,'Aluminium')==1           % Leitf�higkeit von Aluminium bei unterschiedlichen Temperaturen
    kappa_20=37;
    kappa_75=30.6;
    kappa_95=28.8;
    kappa_115=27.2;
    beta=0.004;
end

R_20=z_a*l_Leiter/(kappa_20*A_Leiter/z_N*a);            % Widerstand der Statorwicklung bei 20�C
R_75=z_a*l_Leiter/(kappa_75*A_Leiter/z_N*a);            % Widerstand der Statorwicklung bei 75�C
R_95=z_a*l_Leiter/(kappa_95*A_Leiter/z_N*a);            % Widerstand der Statorwicklung bei 95�C
R_115=z_a*l_Leiter/(kappa_115*A_Leiter/z_N*a);          % Widerstand der Statorwicklung bei 115�C
% T_bel=130;
% R_T=R_20*(1+beta*(T_bel-20));                           % Widerstand der Statorwicklung bei einer beliebigen Temperatur T_bel

%% Ausgabe der Ergebnisse
% if(switch_plots)
% Ergebnisvektor={ P_n ; n_n ; U_n ; f ; cphi ; m ; mu_0 ; Material_Stator ; Schaltung ; Erregung_Rotor ; Kuehlung ; Bauweise_Rotor ; 
%      M ; p ; n_s ; U_Str ; E_h ; eta_ang ; P_el ; P_S ; I_Str ; I_n ; eta ; C_s ; C ; C_mech ; lambda ; P_iS ; D ; v ; T_p ; B_p_ang ;
%      A_ang ; delta_0_ang ; l_i ; l_v ; gamma_v_ang ; n_v ; l ; l_Fe ; l_P ; 
%      Wicklungsart ; W_fn ; xi_p ; a ; Phi_h_ang ; w_Str_opt ; T_N_min ; T_N_max ; q_min ; q_max ; z_N_min ; z_N_max ; z_N_schrittweite ; 
%      q ; z_N ; w_Str ; N ; T_N ; w_a ; w_P ; z_a ; Phi_h_tat ; B_p_tat ; W ; u ; k_a ; y_d ; X_v ; y_v ; y ; alpha_N ; et_v ; et ; 
%      I_Zw ; A ; S_max ; delta_0 ; gamma_v_tat ; l_i_tat ; Phi_delta ; phi_Fe ; B_R_zul ; h_R ; phi_N ; A_Leiter ; l_Leiter ; A_Teilleiter ; 
%      b_Z_a ; b_Z_i ; b_NS ; h_NS ; b_N_a ; b_N_i ; h_N ; A_N ; zaehler_nut_opti ; T_R ; D_a ; 
%      D_L ; alpha_i_ang ; b_PS ; b_P ; h_PS ; h_P ; h_J ; Phi_PK ; Phi_J ; B_pk_zul ; A_PK ;
%      L_delta ; L_h_Str ; L_h ; kappa_20 ; kappa_95 ; kappa_115 ; beta ; R_20 ; R_75 ; R_95 ; R_115 ; beta; T_bel ; R_T };
%  
% rnames =       {'P_n','n_n','U_n','f','cphi','m','mu_0','Material_Stator','Schaltung','Erregung_Rotor','Kuehlung','Bauweise_Rotor',...
%     'M','p','n_s','U_Str','E_h','eta_ang','P_el','P_S','I_Str','I_n','eta','C_s','C','C_mech','lambda','P_iS','D','v','T_p','B_p_ang',...
%     'A_ang','delta_0_ang','l_i','l_v','gamma_v_ang','n_v','l','l_Fe','l_P',...
%     'Wicklungsart','W_fn','xi_p','a','Phi_h_ang','w_Str_opt','T_N_min','T_N_max','q_min','q_max','z_N_min','z_N_max','z_N_schrittweite',...
%     'q','z_N','w_Str','N','T_N','w_a','w_P','z_a','Phi_h_tat','B_p_tat','W','u','k_a','y_d','X_v','y_v','y','alpha_N','et_v','et',...
%     'I_Zw','A','S_max','delta_0','gamma_v_tat','l_i_tat','Phi_delta','phi_Fe','B_R_zul','h_R','phi_N','A_Leiter','l_Leiter','A_Teilleiter',...
%     'b_Z_a','b_Z_i','b_NS','h_NS','b_N_a','b_N_i','h_N','A_N','zaehler_nut_opti','T_R','D_a',...
%     'D_L','alpha_i_ang','b_PS','b_P','h_PS','h_P','h_J','Phi_PK','Phi_J','B_pk_zul','A_PK',...
%     'L_delta','L_h_Str','L_h','kappa_20','kappa_95','kappa_115','beta','R_20','R_75','R_95','R_115','beta','T_bel','R_T'};
% cnames = {'Werte'};
% end
% if(switch_plots)
% fig_erg=figure('Name','Ergebnistabelle','NumberTitle','off');
% uitable(fig_erg,'Data',Ergebnisvektor,'RowName',rnames,'ColumnName',cnames);
% end
%% Zuweisung der Ausgabeparameter
% eingefuegt von Luca Puccetti
% M = 3/2 * p * psi_PM * i_q;
% -> psi_PM = 2*M/(3*p*i_q);
prim.psi_PM = 2*M/(3*I_n*p);
prim.L_d = L_h_Str;
prim.L_q = L_h_Str;
prim.i_max = I_n;    % 180
prim.u_max = U_n;
prim.p = p;
prim.res = struct('R_20', R_20, 'R_75', R_75, 'R_95', R_95, 'R_115', R_115, 'beta', beta);
prim.n_max = n_n*3.5; % ... waehlt man das Verhaeltnis von Nenndrehzahl zu Maximaldrehzahl im Bereich von 1:2 bis 1:5. AUS: Energieeffiziente elektrische Antriebe: Grundlagen, Leistungselektronik - Johannes Teiglk�tter
prim.Abmasse=struct('D', D, 'D_a', D_a, 'l', l) ; 
par = machine_parameters(prim);
if(exist('temp_deg', 'var'))
    par.T = temp_deg;
end
% Hier kann auch ein Satz an mechanischen Parametern uebergeben werden.
% Dazu definiert man zun�chst eine Struktur, etwa:
% mech = struct('l_v', l_v, 'l_i', l_i);
% Die Struktur kann dann an das Parameterobjekt uebergeben:
% par.mech = mech;
 % Ende des Programms / der Funktion Synchronmotorauslegung





%% Durchmesser alternativ
% if (P_i/(2*p))<=600 && p>=2 && p<=4
%     k1=3*p
%     k2=4.7
% elseif (P_s/(2*p))>600 && p>=3
%     k1=10*p                                             % Werte gelten nur f�r labmda=1 und k_red=1, deswegen Umrechnung unten
%     k2=4.0
% else
%     disp('ungueltige Leistung und Polpaarzahl f�r Durchmesserermittlung')
%     return
% end
% D1=k1/100+k2/100*nthroot((P_i*1000*p)/(n_s*lambda),3)   % nach 13.20 b)
% D=D1/(nthroot(lambda,3))                                % Umrechnung nach Vogt 5 - Seite 430: normal kommt hier k_red vor, evtl nur bei Asynchron?
% clear k1 k2

%% Nutabmessungen alternativ
% %% Berechung des Leiterquerschnitts A_l_ang und des Nutquerschnitts A_n im St�nder in mm^2
% A_l_ang=I_zw/S_max;
% phi_n=0.7;                                              % Nutf�llfaktor be. auf den reinen Leiterquerschnitt (Vogt 6 - S. 586)  0.35-0.60 - Quelle f�r 0.7: zytec Cofat 2015 (Messe Lorenz)
% A_n=z_n*A_l_ang/phi_n;
% 
% %% Berechnung der Nutabmessungen im Stator
% B_z_max=2.0;                                            % maximale Zahninduktion laut Vogt 6 - S. 582 zwischen 1.6 und 2.0
% alpha_p_ang=1.4;                                        % Abplattungsfaktor laut Vogt 6 - S. 258 / Vogt 5 - S. 489
% phi_fe=0.95;                                            % Eisenf�llfaktor: laut Vogt 5 - S. 501: 0.93, laut Vogt 6 - S. 604: 0.95
% 
% B_max_ang=alpha_p_ang*B_p_tat*2/pi;                     % Vogt 6 - S. 244
% b_z_min=B_max_ang*T_n*l_i/(l_fe*phi_fe*B_z_max)*1000;   % minimal m�gliche Zahnbreite in mm
% b_n_ang=T_n*1000-b_z_min;                               % Nutbreite �ber Nutteilung in mm
% b_n_max=sqrt(A_n/3);                                    % maximale Nutbreite �ber H�hen/Breiten-Verh�ltnis (3-5.5) in mm, gew�hlt 3
% b_n_min=sqrt(A_n/5.5);                                  % minimale Nutbreite �ber H�hen/Breiten-Verh�ltnis (3-5.5) in mm zur Kontrolle
% 
% % Festlegung der entg�ltigen Werte:
% if b_n_ang<b_n_min
%     disp('Nutbreite kleiner als h/b = 5.5!')
%     b_n=sqrt(A_n/5.5);
% elseif b_n_ang>b_n_max
%     disp('Nutbreite gr��er als h/b = 3!')
%     b_n=sqrt(A_n/3);
% else
%     b_n=b_n_ang;                                        % Nutbreite in mm                                 
% end
% h_n=A_n/b_n;                                            % Nuth�he in mm
% b_z_i=T_n*1000-b_n;                                     % minimale Zahnbreite am inneren Umfang des Stators in mm
% b_z_a=((D*1000+2*h_n)*pi/N)-b_n;                        % maximale Zahnbreite am Boden der Z�hne des Stators in mm

%% Aufteilung in Teilleiter
% b_tl=2;                                                 % Vorgabe der Breite eines Teilleiters in mm mit Isolierung
% h_tl=1;                                                 % Vorgabe der H�he eines Teilleiters in mm
% A_tl=b_tl*h_tl;                                         % Querschnitt eines Teilleiters in mm^2
% tl_neben=floor(b_n_m*0.7/b_tl);                         % Anzahl der Teilleiter nebeneinander (min. 30% bleiben zur Isolierung �ber) 
% d_iso=(b_n-tl_neben*b_tl)/2;                            % Dicke der Isolation pro Seite in mm
% n_tl=ceil(A_l_ang/A_tl);                                % Mindestanzahl der Teilleiter gesamt
% A_l=n_tl*A_tl;                                          % Leiterquerschnitt aus n_tl Teilleitern in mm^2
% tl_ueber=n_tl/tl_neben;                                 % Anzahl der Teilleiter �bereinander

%% Berechnung der Stromverdr�ngung in den Teilleitern
% alpha=sqrt(f_n/50*(b_tl*tl_neben)/b_n);                 % Reduktionsfaktor zur Ermittlung der reduzierten Leiterh�he massiver Leiter in 1/cm
% beta=alpha*h_tl/10;                                     % reduzierte Leiterh�he massiver Leiter in 1
% k_rn=1+n_tl^2/9*beta^4;                                 % Widerstandsverh�ltnis in 1
% 
% l_s=l+1.3*T_p+(0.03+0.02*U_n/1000);                     % L�nge eines Ankerleiters l_s in m
% l_m=2*(l+1.3*T_p+(0.03+0.02*U_n/1000));                 % mittlere Windungsl�nge der Wicklungen l_m in m
% l_w=l_s-l;                                              % Wicklungskopfl�nge l_w in m
% k_r=(k_rn+l_w/l_i)/(1+l_w/l_i);                         % mittleres Widerstandsverh�ltnis des gesamten Leiters

%% Bisherige Bestimmung der Polkernabmessungen
% Phi_pk=1.25*Phi_delta;                                  % Polstreufluss laut Vogt 6 - S. 604: 15-25% gr��er als der Luftspaltfluss
% Phi_j=Phi_pk/2;                                         % Jochfluss Vogt 6 - Bild S. 179
% if strcmp(Bauweise_Rotor,'Schenkelpol')==1
%     B_pk_zul=1.8;                                       % laut Vogt 6 - S. 582 zwischen 1.3 und 1.8
% elseif strcmp(Bauweise_Rotor,'Vollpol')==1
%     B_pk_zul=1.7;                                       % laut Vogt 6 - S. 582 zwischen 1.1 und 1.7
% end
% A_pk=Phi_pk/B_pk_zul;
% %A_fe=sqrt(A_pk^2)
% h_pk=0.3*T_p*1000;                                      % laut Vogt 6 - 9.1.7 - S. 586: 0.20-0.55*T_p
% b_pk=0.7*T_p*1000;