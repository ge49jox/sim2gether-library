function [ Jx, Jy, Jz ] = TraegheitAchsen_EM( typ_EM, M_EM_nenn, n_EM_nenn )
%Trägheiten des EM um beide Hauptachsen, Koeffizienten aus
%Regression_TraegheitUmAchsen_EM

load('Traegheitskoeffizienten_EM.mat');
if (strcmp(typ_EM,'PSM') == 1)
    Jy = p_PSM_Jy(1)*M_EM_nenn*n_EM_nenn^(2/3) + p_PSM_Jy(2); %[kg m^2]
    Jx = p_PSM_Jxz(1)*M_EM_nenn^(5/6)*n_EM_nenn^(7/6) + p_PSM_Jxz(2);
    Jz = Jx;
else
    Jy = p_ASM_Jy(1)*M_EM_nenn^2 + p_ASM_Jy(2)*M_EM_nenn + p_ASM_Jy(3);
    Jx = p_ASM_Jxz(1)*M_EM_nenn^2 + p_ASM_Jxz(2)*M_EM_nenn + p_ASM_Jxz(3);
    Jz = Jx;
end

end

 