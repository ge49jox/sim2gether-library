function [i_dMMPA, i_qMMPA] = g01_iMTPA(prim, i_s)
% Bestimmt die Stromkomponenten so, dass sich als Gesamstrom i_s ergibt und
% maximales Moment bei gegebenem Gesamtstrom ergibt.
if(prim.L_d == prim.L_q)
    i_dMMPA = zeros(size(i_s));
    i_qMMPA = i_s;
else
    i_dMMPA = (-prim.psi_PM + sqrt(8*i_s.^2*(prim.L_d-prim.L_q)^2 + prim.psi_PM^2))/(4*(prim.L_d-prim.L_q));
    i_qMMPA = sqrt(i_s.^2-i_dMMPA.^2);
end
end