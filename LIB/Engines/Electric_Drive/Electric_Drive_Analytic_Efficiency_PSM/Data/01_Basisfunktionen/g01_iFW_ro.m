function [i_dFS, i_qFS] = g01_iFW_ro(prim, omega, i_s, optional_config)
% Field-weakening current, resistance optional
% Berechnet den Strom im Feldschwaechebetrieb unter Beruecksichtigung des
% Statorwiderstands. Die eingesetzte Loesung ist analytisch hergeleitet von
% Luca Puccetti

if (nargin==4)
    consider_resistance = optional_config.consider_resistance;
else
    consider_resistance = true;
end
if(~consider_resistance)
    [i_dFS, i_qFS] = g01_iFW_rn(prim, omega, i_s);
else
%% Koeffizienten der Spannungsbetragsgleichung
omega_el = prim.p*omega;
A11 = prim.R_s.^2+omega_el.^2*prim.L_d^2;
A12 = prim.R_s*omega_el*(prim.L_d-prim.L_q);
A22 = prim.R_s.^2+omega_el.^2*prim.L_q^2;
a1 = omega_el.^2*prim.L_d*prim.psi_PM;
a2 = prim.R_s*omega_el*prim.psi_PM;
alpha = omega_el.^2*prim.psi_PM.^2-prim.u_max.^2;

%% Koeffizienten der Gleichung mit transformierter, eingesetzter Strombetragsgleichung
A = A11.*i_s.^2 + 2*a1.*i_s + alpha;
B = - 4*A12.*i_s.^2 - 4*a2.*i_s;
C = 2*alpha - 2*A11.*i_s.^2 + 4*A22.*i_s.^2;
D = 4*A12.*i_s.^2 - 4*a2.*i_s;
E = A11.*i_s.^2 - 2*a1.*i_s + alpha;

%% Loesung der quartischen Gleichung
t = h01_root4thdeg(A,B,C,D,E); % roots vllt besser

%% Ruecktransformation
id = -(ones(4,1)*i_s)./(1+t.^2).*(1-t.^2);
iq = -(ones(4,1)*i_s)./(1+t.^2)*2.*t;

%% Auswaehlen der reellen Loesung mit maximalem Moment
M = g01_Mi(prim, id, iq);
% Falls keine reelle Loesung existiert, wird eine beliebige imaginaere
% Loesung zurueckgegeben
% Erg�nzung: Aufgrund numerischer Ungenauigkeit muss ein kleiner
% Imagin�rteil ignoriert werden. Das ist keine sch�ne L�sung und w�rde sich
% ggf. durch Anwendung der Matlab-integrierten L�sungsfunktion f�r
% quartische Gleichungen umgehen lassen.
i_dFS = zeros(size(A));
i_qFS = zeros(size(A));
for i=1:1:length(A)
    m = M(:,i);
    m(isnan(m)) = -1;
    if(isempty( max(m(abs(imag(m))<2e-2)) ))
        i_dFS(i) = id(1,i); % Indizes vertauscht
        i_qFS(i) = iq(1,i);
    else
        index = real(m)==max(real(m(abs(imag(m))<2e-2)));
        if(sum(index)>1)
            index = find(index, 1, 'first');
        end
        i_dFS(i) = real(id( index,i ));
        i_qFS(i) = real(iq( index,i ));
    end
    % Beseitigen des Fehlers, dass bei lastfreiem Drehen die
    % Spannungsgrenze �berschritten werden kann. Numerische Ungenauigkeit
    % erzwingt Offset von etwa 1e-7
%     if(isreal(i_dFS(i)+i_qFS(i))&&(g01_us(prim, omega(i), i_dFS(i), i_qFS(i), optional_config)-1e-6>prim.u_max))
%         i_qFS(i) = i_qFS(i)+1j;
%     end
        
end
end
end