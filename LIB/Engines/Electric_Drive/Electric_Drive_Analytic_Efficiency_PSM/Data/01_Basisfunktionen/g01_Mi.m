function M = g01_Mi(prim, i_d, i_q)
% Drehmoment einer permanenterregten Synchronmaschine bei
% Winkelgeschwindigkeit omega und Stromkomponenten i_d und i_q.
M = 3/2 *prim.p* (prim.psi_PM*i_q + (prim.L_d-prim.L_q).*i_d.*i_q);
% M = prim.p* (prim.psi_PM*i_q + (prim.L_d-prim.L_q).*i_d.*i_q);
end
