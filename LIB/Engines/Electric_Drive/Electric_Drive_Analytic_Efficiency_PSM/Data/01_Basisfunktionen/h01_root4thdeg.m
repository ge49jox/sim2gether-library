function x = h01_root4thdeg(A,B,C,D,E)
% Berechnet die Nullstellen eines Polynoms der Form 0 = Ax^4+Bx^3+Cx^2+Dx+E
%% Definition der Koeffizienten alpha, beta und gamma
% Auf diese Weise wird das Polynom auf ein Polynom dritten Grades
% transformiert: u^4+alpha*u^3+beta*u^2+gamma = 0
alpha = -3*B.^2./(8*A.^2)+C./A;
beta = B.^3./(8*A.^3)-B.*C./(2*A.^2)+D./A;
gamma = -3*B.^4./(256*A.^4)+B.^2.*C./(16.*A.^3)-B.*D./(4*A.^2)+E./A;

%% Definition der Hilfsgroessen P,Q,U
P = -alpha.^2/12-gamma;
Q = -alpha.^3/108+alpha.*gamma/3-beta.^2/8;
U = (-Q/2+sqrt(Q.^2/4+P.^3/27)).^(1/3);

%% Definition der Hilfsgroessen y,w,z
if(P==0)
    y = -5/6*alpha-Q.^(1/3);
else
    y = -5/6*alpha+U-P./(3*U);
end
w = sqrt(alpha+2*y);
% z = beta./(2*w);

%% Loesen der Gleichung
x = zeros(4, length(A));
index = 1;
for r = [-1, 1]
    for s = [-1, 1]
        x(index,:) = -B./(4*A)+1/2*(s*w+r*sqrt(-(alpha+2*y)-2*(alpha+s*beta./w)));
        index = index+1;
    end
end
end