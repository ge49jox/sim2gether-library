function [i_dFS, i_qFS] = g01_iFW_rn(prim, omega, i_s)
% Field-weakening current, resistance neglected
% Berechnet die Stromkomponenten i_d und i_q für den Felschwächebereich.
omega_el = prim.p*omega;
    if (prim.L_d == prim.L_q)
        i_dFS = (prim.u_max^2 - omega_el.^2.*(prim.L_q^2*i_s.^2 + prim.psi_PM.^2) )./ (2*prim.L_d*prim.psi_PM*omega_el.^2);
    else
        i_dFS = ( sqrt( (prim.L_d^2-prim.L_q^2)*prim.u_max^2 + prim.L_q^2*((prim.L_q^2-prim.L_d^2)*omega_el.^2.*i_s.^2+omega_el.^2*prim.psi_PM^2) )- omega_el*prim.L_d*prim.psi_PM ) ./ (omega_el*(prim.L_d^2-prim.L_q^2));
		% Fehlt hier ein Quadrat bei -omega_el*prim.L_d*prim.p?
    end
    i_qFS = sqrt(i_s.^2-i_dFS.^2);
end