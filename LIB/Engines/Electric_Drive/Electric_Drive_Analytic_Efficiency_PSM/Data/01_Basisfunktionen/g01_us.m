function [u] = g01_us(prim, omega, i_d, i_q, varargin)

if (nargin==5)
    consider_resistance = varargin{1}.consider_resistance;
else
    consider_resistance = true;
end

if(consider_resistance)
    [u_d, u_q] = g01_udq(prim, omega, i_d, i_q, varargin{:});
    u = sqrt(u_d.^2+u_q.^2);
else
    u = prim.p*omega.*sqrt( (prim.L_q*i_q).^2 + (prim.L_d*i_d+prim.psi_PM).^2 );
end

end