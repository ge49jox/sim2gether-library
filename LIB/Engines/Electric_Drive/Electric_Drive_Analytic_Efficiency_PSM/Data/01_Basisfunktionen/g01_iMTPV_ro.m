function [i_dMTPV, i_qMTPV] = g01_iMTPV_ro(prim, omega, optional_config)
% Maximum torque per volt current, resistance optional
% Berechnet die Stromkomponenten i_d und i_q derart, dass sich maximales
% Moment bei zul�ssigem Spannungsbetrag einstellt.
% In dieser Version wird die Loesung von Lorenz Horlbeck (FTM) eingesetzt,
% die den Strangwiderstand beruecksichtigt.

% Im Falle der Vollpolmaschine tritt jedoch eine Singularitaet auf, weshalb
% der Loesungsweg in diesem Fall ein anderer ist. Statt iq = f(id) wird
% hier id direkt bestimmt, um dann iq so zu w�hlen, dass die maximale
% Spannung erreicht wird.

%% Input pruefen
if (nargin==3)
    consider_resistance = optional_config.consider_resistance;
else
    consider_resistance = true;
end

if(consider_resistance)
    % --------------------- mit Widerstand --------------------------------
    %% Vorbereiten der Konstanten in der Gleichung
    omega_k = prim.p*omega;
    A11 = 0.5*(prim.L_d - prim.L_q)*(prim.R_s^2+omega_k.^2*prim.L_d^2);
    A22 = 0.5*(prim.L_d - prim.L_q)*(-prim.R_s^2-omega_k.^2*prim.L_q^2);
    a1 = 0.25*prim.psi_PM*(prim.R_s^2+omega_k.^2*prim.L_d*(2*prim.L_d-prim.L_q));
    alpha = 0.5*omega_k.^2*prim.L_d*prim.psi_PM^2;
%     i_dMTPV = zeros(size(omega));
    i_qMTPV = zeros(size(omega));
    
    %% Loesen der Gleichung
    % Fall 1: Induktivitaeten ungleich (Schenkelpolmaschine)
    if(prim.L_d~=prim.L_q)
        % Als Startwert fuer die Nullstellensuche wird der Wert ohne Widerstand
        % benutzt:
        [idstart, ~] = g01_iMTPV_rn(prim, omega);
        idstart(idstart>prim.i_max) = prim.i_max;
        i_dmax = zeros(size(omega));
        
        for j = 1:1:numel(omega)
            % Finden des Stroms bei dem die Spannungsgrenze erreicht wird
            u = @(i_d)real(u_MTPVid(prim, omega(j), A11(j), A22(j), a1(j), alpha(j), i_d)-prim.u_max);
            if(omega(j)==0)
                i_dmax(j) = inf;
            else
                i_dmax(j) = fzero(u, idstart(j));
            end
        end
        % Berechnen der Stromkomponenten fuer diesen Strom
        i_qMTPV = i_QMTPV(A11, A22, a1, alpha, i_dmax);
        i_dMTPV = i_dmax;

    % Fall 2: Induktivitaeten gleich (Vollpolmaschine)
    else
        i_dMTPV = -0.5*alpha./a1;
        % Als Startwert fuer die Nullstellensuche wird der Wert ohne Widerstand
        % benutzt:
        [ ~, iqstart] = g01_iMTPV_rn(prim, omega);
        iqstart(iqstart>prim.i_max) = prim.i_max;
        for j = 1:1:numel(omega)
            % Finden des Stroms bei dem die Spannungsgrenze erreicht wird
            u = @(i_q)(g01_us(prim, omega(j), i_dMTPV(j), i_q)-prim.u_max);
            i_qMTPV(j) = fzero(u, iqstart(j));
        end
    end
    % ---------------------------------------------------------------------
else
    % ---------------------- ohne Widerstand ------------------------------
    [i_dMTPV, i_qMTPV] = g01_iMTPV_rn(prim, omega);
    % ---------------------------------------------------------------------
end
end

function i_q = i_QMTPV(A11, A22, a1, alpha, i_d)
try
    i_q = sqrt((-A11.*i_d.^2 - 2*a1.*i_d - alpha)./A22); % VORZEICHEN UNKLAR
catch
    error('Sizes not equal!');
end
end

function u_s = u_MTPVid(prim, omega, A11, A22, a1, alpha, i_d)
    i_q = i_QMTPV(A11, A22, a1, alpha, i_d);
    u_s = g01_us(prim, omega, i_d, i_q);
end