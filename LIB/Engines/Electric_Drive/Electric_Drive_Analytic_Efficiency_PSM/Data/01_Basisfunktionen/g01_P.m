function P = g01_P(prim, id, iq, ud, uq)
% Berechnet die Leistung aus den Strom- und Spannungskomponenten
% P = 1.5*prim.p*(id.*ud + iq.*uq);
P = 1.5*(id.*ud + iq.*uq);
end