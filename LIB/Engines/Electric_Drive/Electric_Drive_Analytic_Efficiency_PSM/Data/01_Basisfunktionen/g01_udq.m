function [u_d, u_q] = g01_udq(prim, omega, i_d, i_q, optional_config)
% Berechnet die Spannungskomponenten in Abhängigkeit der Drehzahl und der
% Stromkomponenten.
omega_el = prim.p*omega;
u_d = prim.R_s*i_d - omega_el.*prim.L_q.*i_q;
u_q = prim.R_s*i_q + omega_el.*(prim.L_d*i_d + prim.psi_PM);

end