function [i_dMMPV, i_qMMPV] = g01_iMTPV_rn(prim, omega)
% Maximum torque per volt current, resistance neglected
% Berechnet die Stromkomponenten i_d und i_q derart, dass sich maximales
% Moment bei zulässigem Spannungsbetrag einstellt.
omega_el = prim.p*omega;
    if(prim.L_d == prim.L_q)
        i_dMMPV = -prim.psi_PM/prim.L_d*ones(size(omega));
    else
        VZ = -1;
        i_dMMPV = -(prim.psi_PM*omega_el*(4*prim.L_d - 3*prim.L_q)+VZ*sqrt((prim.L_q*prim.psi_PM*omega_el).^2 + 8*prim.u_max^2*(prim.L_d-prim.L_q)^2))./(4*(prim.L_d-prim.L_q)*omega_el*prim.L_d);
    end
    i_qMMPV = sqrt(prim.u_max^2-omega_el.^2.*(prim.L_d*i_dMMPV+prim.psi_PM).^2)./(omega_el*prim.L_q);
    if(~isreal(i_dMMPV)||~isreal(i_qMMPV))
        error('Mit diesen Parametern ist die Drehzahl nicht erreichbar. Der Permanentmagnet ist zu stark bzw. Induktivität/Maximalstrom zu gering.');
    end
end