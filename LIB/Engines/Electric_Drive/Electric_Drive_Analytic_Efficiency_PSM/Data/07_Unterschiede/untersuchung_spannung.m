% Untersuchung der Spannung in Abh�ngigkeit von i_d und i_q
% id = -350:10:30;
% iq = -20:10:250;
id = -10000:100:10000;
iq = -10000:100:10000;
[ID, IQ] = meshgrid(id, iq);
r_off.consider_resistance = false;
r_on.consider_resistance = true;
U_100off = g01_us(par.prim, 100, ID, IQ, r_off);
U_100on = g01_us(par.prim, 100, ID, IQ, r_on);
% figure('Name', 'ohne Statorwiderstand')
% surf(ID, IQ, U_100off)
% xlabel('i_d')
% ylabel('i_q')
% figure('Name', 'mit Statorwiderstand')
% surf(ID, IQ, U_100on)
% xlabel('i_d')
% ylabel('i_q')
figure('Name', 'Differenz')
contourf(ID, IQ, U_100on-U_100off)
xlabel('i_d in A')
ylabel('i_q in A')
% zlabel('\Delta u in V')
% Die Differenz ist Drehzahlunabh�ngig!

% u_on = g01_us(par.prim, 100, id, zeros(size(id)), r_on);
% u_off = g01_us(par.prim, 100, id, zeros(size(id)), r_off);
% figure('Name', 'i_q = 0')
% plot(id, u_on-u_off)