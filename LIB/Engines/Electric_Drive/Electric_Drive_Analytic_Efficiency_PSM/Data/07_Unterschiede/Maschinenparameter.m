% Beispiele fuer elektrische Parameter aus Tobias Finken:
% Fahrzyklusorientierte Auslegung ...

%Spannung ist entweder 280 oder 600V. (S. 116)
triu = sqrt(2);
trps = sqrt(2/3);
u_smax = 280;
% Maximaldrehzahl ist fast immer 6000 U/min;
n_max = 6000; 

% par.prim.psi_PM     % Permanenter Fluss
% par.prim.L_d        % Induktivitaet in Hauptrichtung
% par.prim.L_q        % Induktivitaet in Querrichtung
% par.prim.i_max      % Maximaler Strangstrom
% par.prim.u_max      % Maximale Spannung
% par.prim.p          % Polpaarzahl
% par.prim.R_s        % Ohm'scher Widerstand je Strang
% par.prim.n_max      % Zulaessige Hoechstdrehzahl

addpath('01_Basisfunktionen', ...
        '02_Nominalwerte', ...
        '03_Ansteuerung', ...
        '04_IterativeAuslegung', ...
        '05_Analyse', ...
        '06_Strukturen', ...
        '07_Unterschiede', ...
        '08_Abbildungen');

%prim.psi_PM = 2*M/(3*I_n*p);
%% S. 117: 16 Maschinen, 
% Maximaldrehzahl teilweise unklar
% Nenndrehzahl 8000 U/min Maximaldrehzahl unklar!
prim6_9(1).p = 4;
prim6_9(1).i_max = 258;
prim6_9(1).u_max = u_smax;
prim6_9(1).L_d = 0.18e-3;
prim6_9(1).L_q = 0.36e-3;
prim6_9(1).R_s = 4e-3;
prim6_9(1).psi_PM = 93e-3;
prim6_9(1).n_max = 10e3;

prim6_9(2).p = 4;
prim6_9(2).i_max = 232;
prim6_9(2).u_max = u_smax;
prim6_9(2).L_d = 0.16e-3;
prim6_9(2).L_q = 0.36e-3;
prim6_9(2).R_s = 9e-3;
prim6_9(2).psi_PM = 80e-3;
prim6_9(2).n_max = 10e3;

prim6_9(3).p = 8;
prim6_9(3).i_max = 269;
prim6_9(3).u_max = u_smax;
prim6_9(3).L_d = 0.09e-3;
prim6_9(3).L_q = 0.15e-3;
prim6_9(3).R_s = 4e-3;
prim6_9(3).psi_PM = 67e-3;
prim6_9(3).n_max = 10e3;

prim6_9(4).p = 8;
prim6_9(4).i_max = 249;
prim6_9(4).u_max = u_smax;
prim6_9(4).L_d = 0.05e-3;
prim6_9(4).L_q = 0.12e-3;
prim6_9(4).R_s = 6e-3;
prim6_9(4).psi_PM = 66e-3;
prim6_9(4).n_max = 10e3;

% Nenndrehzahl 4000 U/min; Maximaldrehzahl unklar!
prim6_9(5).p = 4;
prim6_9(5).i_max = 141;
prim6_9(5).u_max = u_smax;
prim6_9(5).L_d = 0.62e-3;
prim6_9(5).L_q = 1.19e-3;
prim6_9(5).R_s = 13e-3;
prim6_9(5).psi_PM = 171e-3;
prim6_9(5).n_max = n_max;

prim6_9(6).p = 4;
prim6_9(6).i_max = 108;
prim6_9(6).u_max = u_smax;
prim6_9(6).L_d = 0.73e-3;
prim6_9(6).L_q = 1.67e-3;
prim6_9(6).R_s = 42e-3;
prim6_9(6).psi_PM = 171e-3;
prim6_9(6).n_max = n_max;

prim6_9(7).p = 8;
prim6_9(7).i_max = 162;
prim6_9(7).u_max = u_smax;
prim6_9(7).L_d = 0.24e-3;
prim6_9(7).L_q = 0.40e-3;
prim6_9(7).R_s = 11e-3;
prim6_9(7).psi_PM = 111e-3;
prim6_9(7).n_max = n_max;

prim6_9(8).p = 8;
prim6_9(8).i_max = 124;
prim6_9(8).u_max = u_smax;
prim6_9(8).L_d = 0.21e-3;
prim6_9(8).L_q = 0.49e-3;
prim6_9(8).R_s = 24e-3;
prim6_9(8).psi_PM = 133e-3;
prim6_9(8).n_max = n_max;

% Nenndrehzahl 2000 U/min
prim6_9(9).p = 4;       % Wirkungsgradkennfeld in Abb. 6.23c auf S. 108
prim6_9(9).i_max = 67;
prim6_9(9).u_max = u_smax;
prim6_9(9).L_d = 2.71e-3;
prim6_9(9).L_q = 5.22e-3;
prim6_9(9).R_s = 57e-3;
prim6_9(9).psi_PM = 357e-3;
prim6_9(9).n_max = 6000;

prim6_9(10).p = 4;       % Wirkungsgradkennfeld in Abb. 6.23g auf S. 108
prim6_9(10).i_max = 56;
prim6_9(10).u_max = u_smax;
prim6_9(10).L_d = 2.73e-3;
prim6_9(10).L_q = 6.25e-3;
prim6_9(10).R_s = 156e-3;
prim6_9(10).psi_PM = 332e-3;
prim6_9(10).n_max = 6000;

prim6_9(11).p = 8;  %CWp8     % Wirkungsgradkennfeld in Abb. 6.23d auf S. 108
prim6_9(11).i_max = 81;
prim6_9(11).u_max = u_smax;
prim6_9(11).L_d = 0.98e-3;
prim6_9(11).L_q = 1.61e-3;
prim6_9(11).R_s = 43e-3;
prim6_9(11).psi_PM = 222e-3;
prim6_9(11).n_max = 6000;

prim6_9(12).p = 8;       % Wirkungsgradkennfeld in Abb. 6.23h auf S. 108
prim6_9(12).i_max = 68;
prim6_9(12).u_max = u_smax;
prim6_9(12).L_d = 0.70e-3;
prim6_9(12).L_q = 1.64e-3;
prim6_9(12).R_s = 82e-3;
prim6_9(12).psi_PM = 244e-3;
prim6_9(12).n_max = 6000;

% Nenndrehzahl 1000 U/min; Maximaldrehzahl unklar!
prim6_9(13).p = 4;
prim6_9(13).i_max = 34;
prim6_9(13).u_max = u_smax;
prim6_9(13).L_d = 10.38e-3;
prim6_9(13).L_q = 19.98e-3;
prim6_9(13).R_s = 220e-3;
prim6_9(13).psi_PM = 699e-3;
prim6_9(13).n_max = n_max;

prim6_9(14).p = 4;
prim6_9(14).i_max = 29;
prim6_9(14).u_max = u_smax;
prim6_9(14).L_d = 10.16e-3;
prim6_9(14).L_q = 23.20e-3;
prim6_9(14).R_s = 581e-3;
prim6_9(14).psi_PM = 640e-3;
prim6_9(14).n_max = n_max;

prim6_9(15).p = 8;
prim6_9(15).i_max = 40;
prim6_9(15).u_max = u_smax;
prim6_9(15).L_d = 3.92e-3;
prim6_9(15).L_q = 6.45e-3;
prim6_9(15).R_s = 171e-3;
prim6_9(15).psi_PM = 445e-3;
prim6_9(15).n_max = n_max;

prim6_9(16).p = 8;
prim6_9(16).i_max = 34;
prim6_9(16).u_max = u_smax;
prim6_9(16).L_d = 2.80e-3;
prim6_9(16).L_q = 6.56e-3;
prim6_9(16).R_s = 329e-3;
prim6_9(16).psi_PM = 488e-3;
prim6_9(16).n_max = n_max;

%% S. 127: 8 Maschinen
% Polpaarzahl unklar, max n 6000
% Eckdrehzal 2000 U/min

prim6_11(1).p = 8;   %gleich wie 6_9_11
prim6_11(1).i_max = 81; % Etwa Faktor 1.266;
prim6_11(1).u_max = u_smax; % Etwa Faktor 1.54
prim6_11(1).L_d = 0.98e-3;
prim6_11(1).L_q = 1.61e-3;
prim6_11(1).R_s = 43e-3;
prim6_11(1).psi_PM = 222e-3;
prim6_11(1).n_max = 8000;

prim6_11(2).p = 8; % Abb 6.36
prim6_11(2).i_max = 67;
prim6_11(2).u_max = u_smax;
prim6_11(2).L_d = 1.41e-3;
prim6_11(2).L_q = 2.32e-3;
prim6_11(2).R_s = 61e-3;
prim6_11(2).psi_PM = 214e-3;
prim6_11(2).n_max = 8000;

prim6_11(3).p = 8; % Abb. 6.36
prim6_11(3).i_max = 90;
prim6_11(3).u_max = u_smax;
prim6_11(3).L_d = 0.79e-3;
prim6_11(3).L_q = 1.31e-3;
prim6_11(3).R_s = 35e-3;
prim6_11(3).psi_PM = 240e-3;
prim6_11(3).n_max = 8000;

prim6_11(4).p = 8; % Abb 6.37
prim6_11(4).i_max = 67;
prim6_11(4).u_max = u_smax;
prim6_11(4).L_d = 0.71e-3;
prim6_11(4).L_q = 1.61e-3;
prim6_11(4).R_s = 61e-3;
prim6_11(4).psi_PM = 267e-3;
prim6_11(4).n_max = 8000;

prim6_11(5).p = 8; % Abb 6.37
prim6_11(5).i_max = 101;
prim6_11(5).u_max = u_smax;
prim6_11(5).L_d = 0.94e-3;
prim6_11(5).L_q = 1.55e-3;
prim6_11(5).R_s = 27e-3;
prim6_11(5).psi_PM = 178e-3;
prim6_11(5).n_max = 8000;

prim6_11(6).p = 8; % Abb 6.38
prim6_11(6).i_max = 73;
prim6_11(6).u_max = u_smax;
prim6_11(6).L_d = 1.2e-3;
prim6_11(6).L_q = 1.2e-3;
prim6_11(6).R_s = 52e-3;
prim6_11(6).psi_PM = 245e-3;
prim6_11(6).n_max = 8000;

prim6_11(7).p = 8; % Abb 6.38
prim6_11(7).i_max = 81;
prim6_11(7).u_max = u_smax;
prim6_11(7).L_d = 0.98e-3;
prim6_11(7).L_q = 1.96e-3;
prim6_11(7).R_s = 43e-3;
prim6_11(7).psi_PM = 222e-3;
prim6_11(7).n_max = 8000;

prim6_11(8).p = 8; % Abb 6.38
prim6_11(8).i_max = 101;
prim6_11(8).u_max = u_smax;
prim6_11(8).L_d = 0.63e-3;
prim6_11(8).L_q = 1.88e-3;
prim6_11(8).R_s = 27e-3;
prim6_11(8).psi_PM = 178e-3;
prim6_11(8).n_max = 8000;

% S. 140: 10
% Hier sind die Nenn-/Eckdrehzahlen 2000 U/min
% Die Maximaldrehzahlen markieren das Ende des FS1.
% oben
prim7_7(1).p = 4;
prim7_7(1).i_max = 48;
prim7_7(1).u_max = u_smax;
prim7_7(1).L_d = 3.07e-3;
prim7_7(1).L_q = 3.46e-3;
prim7_7(1).R_s = 214e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(1).psi_PM = 332e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(1).n_max = 4700;

prim7_7(2).p = 8;
prim7_7(2).i_max = 75;
prim7_7(2).u_max = u_smax;
prim7_7(2).L_d = 0.62e-3;
prim7_7(2).L_q = 0.71e-3;
prim7_7(2).R_s = 82e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(2).psi_PM = 244e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(2).n_max = 3000;

% eingelassen
prim7_7(3).p = 4;
prim7_7(3).i_max = 53;
prim7_7(3).u_max = u_smax;
prim7_7(3).L_d = 2.49e-3;
prim7_7(3).L_q = 5.19e-3;
prim7_7(3).R_s = 178e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(3).psi_PM = 267e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(3).n_max = 5400;

prim7_7(4).p = 8;
prim7_7(4).i_max = 77;
prim7_7(4).u_max = u_smax;
prim7_7(4).L_d = 0.58e-3;
prim7_7(4).L_q = 1.22e-3;
prim7_7(4).R_s = 82e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(4).psi_PM = 332e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(4).n_max = 3200;

% vergraben
prim7_7(5).p = 4;
prim7_7(5).i_max = 56;
prim7_7(5).u_max = u_smax;
prim7_7(5).L_d = 2.74e-3;
prim7_7(5).L_q = 6.28e-3;
prim7_7(5).R_s = 156e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(5).psi_PM = 332e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(5).n_max = 6000;

prim7_7(6).p = 8;
prim7_7(6).i_max = 67;
prim7_7(6).u_max = u_smax;
prim7_7(6).L_d = 0.72e-3;
prim7_7(6).L_q = 1.67e-3;
prim7_7(6).R_s = 82e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(6).psi_PM = 244e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(6).n_max = 3500;

% vergrabenV
prim7_7(7).p = 4;
prim7_7(7).i_max = 55;
prim7_7(7).u_max = u_smax;
prim7_7(7).L_d = 2.51e-3;
prim7_7(7).L_q = 6.58e-3;
prim7_7(7).R_s = 135e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(7).psi_PM = 332e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(7).n_max = 6000;

prim7_7(8).p = 8;
prim7_7(8).i_max = 65;
prim7_7(8).u_max = u_smax;
prim7_7(8).L_d = 0.90e-3;
prim7_7(8).L_q = 2.21e-3;
prim7_7(8).R_s = 98e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(8).psi_PM = 244e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(8).n_max = 5000;

% sammler
prim7_7(9).p = 4;
prim7_7(9).i_max = 45;
prim7_7(9).u_max = u_smax;
prim7_7(9).L_d = 4.34e-3;
prim7_7(9).L_q = 7.06e-3;
prim7_7(9).R_s = 156e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(9).psi_PM = 332e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(9).n_max = 6000;

prim7_7(10).p = 8;
prim7_7(10).i_max = 82;
prim7_7(10).u_max = u_smax;
prim7_7(10).L_d = 0.41e-3;
prim7_7(10).L_q = 0.62e-3;
prim7_7(10).R_s = 54e-3;     % Diese Angabe fehlt in der Tabelle
prim7_7(10).psi_PM = 244e-3; % Diese Angabe fehlt in der Tabelle
prim7_7(10).n_max = 2700;

%% Ueberfuehren in Parameter-Objekte
par6_9 = cell(1, numel(prim6_9));
for i = 1:1:numel(prim6_9)
    prim6_9(i).i_max = prim6_9(i).i_max*triu;
    prim6_9(i).u_max = prim6_9(i).u_max*triu;
    prim6_9(i).psi_PM = prim6_9(i).psi_PM*trps;
    par6_9{i} = machine_parameters(prim6_9(i));
end
par6_11 = cell(1, numel(prim6_11));
for i = 1:1:numel(prim6_11)
    prim6_11(i).i_max = prim6_11(i).i_max*triu;
    prim6_11(i).u_max = prim6_11(i).u_max*triu;
    prim6_11(i).psi_PM = prim6_11(i).psi_PM*trps;
    par6_11{i} = machine_parameters(prim6_11(i));
end
par7_7 = cell(1, numel(prim7_7));
for i = 1:1:numel(prim7_7)
    prim7_7(i).i_max = prim7_7(i).i_max*triu;
    prim7_7(i).u_max = prim7_7(i).u_max*triu;
    prim7_7(i).psi_PM = prim7_7(i).psi_PM*trps;
    par7_7{i} = machine_parameters(prim7_7(i));
end
%% Ausgabe der Sekundaeren Parameter
% figure;
% iterator = counter(1);
% overview_par(par6_9,1);
% updatefunc9 = @()overview_par( par6_9, mod(iterator()-1, numel(par6_9))+1 );
% u = uicontrol(gcf, 'Style', 'Pushbutton', 'Callback', 'updatefunc9()', 'String', 'Next');
% u.Units = 'normalized';
% u.Position(1) = 1-u.Position(3)-u.Position(1);
% 
% figure;
% iterator = counter(1);
% overview_par(par6_11,1);
% updatefunc11 = @()overview_par( par6_11, mod(iterator()-1, numel(par6_11))+1 );
% u = uicontrol(gcf, 'Style', 'Pushbutton', 'Callback', 'updatefunc11()', 'String', 'Next');
% u.Units = 'normalized';
% u.Position(1) = 1-u.Position(3)-u.Position(1);