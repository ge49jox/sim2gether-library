function [OM_fw, M_fw] = g07_area_fw(par, varargin)
% Bestimmt den Kennfeldbereich, in dem mit Feldschwächestrom gefahren wird.
pts_line = 100;
%% Finden des letzten Wertes 
% (Kriterien: vorgegebene Grenze, Erreichbarkeit)
M_FWmax = @(omega)(g03_control_currentbased(par.prim, omega, par.prim.i_max, varargin{:}));
if(isinf(par.sec.omega_fs1fs2))
    try
        om_max = fzero(M_FWmax, [par.sec.omega_nom, par.sec.omega_max]);
    catch
        om_max = inf;
    end
end
om_limit = min(om_max, par.sec.omega_max);
% Initialisieren, unterschiedliche Zahl, falls Kennfeld rechts durch
% vertikale Linie oder einzelnen Punkt beschrieben wird.
if(isinf(om_max))
    OM_fw = zeros(1, pts_line*2+1);
    M_fw  = zeros(1, pts_line*2+1);
    OM_fw(pts_line*2+1) = om_limit;
else
    OM_fw = zeros(1, pts_line*2);
    M_fw  = zeros(1, pts_line*2);
end
%% Grenze zum Ankerstellbereich
M_fw(1:pts_line) = linspace(0, par.sec.M_nom);
for i = 1:1:pts_line
    [id, iq, ~, ~] = g03_control_momentbased(par, M_fw(i), 0, varargin{:});
    voltage = @(omega)(g01_us(par.prim, omega, id, iq)-par.prim.u_max);
    OM_fw(i) = fzero(voltage, par.sec.omega_nom);
end
OM_fw(OM_fw>om_limit) = om_limit;
%% Grenze entlang Volllast
step = (om_limit-par.sec.omega_nom)/(pts_line);
OM_fw(pts_line+1:(pts_line*2)) = (par.sec.omega_nom+step):step:om_limit;
for i = 1:1:pts_line
    M_fw(pts_line+i) = g03_max_moment(par, OM_fw(pts_line+i), varargin{:});
end
end