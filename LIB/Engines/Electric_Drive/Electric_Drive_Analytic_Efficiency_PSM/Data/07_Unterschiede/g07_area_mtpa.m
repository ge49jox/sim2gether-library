function [OM_mtpa, M_mtpa] = g07_area_mtpa(par, varargin)
% Bestimmt Kennfeldbereich, in dem mit MTPA-Steuerung gefahren wird.
pts_line = 100;
M_mtpa = zeros(1, 2*pts_line+1);
OM_mtpa = zeros(1, 2*pts_line+1);
M_mtpa(1, 2:(pts_line+1)) = ones(1, pts_line)*par.sec.M_nom;
OM_mtpa(1, 2:(pts_line+1)) = linspace(0, par.sec.omega_nom, pts_line);
M_mtpa((pts_line+2):end) = linspace(par.sec.M_nom*(pts_line-1)/pts_line, 0, pts_line);
for i = 1:1:pts_line
    [id, iq, ~, ~] = g03_control_momentbased(par, M_mtpa(pts_line+1+i), 0, varargin{:});
    voltage = @(omega)(g01_us(par.prim, omega, id, iq, varargin{:})-par.prim.u_max);
    OM_mtpa(pts_line+1+i) = fzero(voltage, par.sec.omega_nom-1);
end
OM_mtpa(OM_mtpa>par.sec.omega_max) = par.sec.omega_max;
end