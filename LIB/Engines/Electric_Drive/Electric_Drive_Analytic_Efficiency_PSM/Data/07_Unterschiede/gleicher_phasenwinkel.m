% U-I-Kennlinie bei bestimmter Drehzahl und bestimmtem Phasenwinkel.

% Diese Kennlinie verl�uft n�mlich gar nicht �hnlich einem ohm'schen
% Widerstand, eher invers dazu.
M = 100;
OM = 400;

[om_M_id, om_M_iq, om_M_ud, om_M_uq] = g03_control_momentbased(par, M, OM, r_off);
alpha = 0:1e-1:1;
om_M_id_red = alpha*om_M_id;
om_M_iq_red = alpha*om_M_iq;
u = g01_us(par.prim, OM, om_M_id_red, om_M_iq_red);
plot(alpha, u)