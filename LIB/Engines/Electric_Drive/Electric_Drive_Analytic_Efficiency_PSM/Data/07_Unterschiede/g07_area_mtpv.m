function [OM_mtpv, M_mtpv] = g07_area_mtpv(par, varargin)
pts_line = 100;
if(~isinf(par.sec.omega_fs1fs2))
    OM_mtpv = linspace(par.sec.omega_fs1fs2, par.sec.omega_max, pts_line);
    [id, iq] = g01_iMTPV_ro(par.prim, OM_mtpv, varargin{:});
    M_mtpv  = g01_Mi(par.prim, id, iq);
else
    OM_mtpv = NaN;
    M_mtpv = NaN;
end
end
    
    