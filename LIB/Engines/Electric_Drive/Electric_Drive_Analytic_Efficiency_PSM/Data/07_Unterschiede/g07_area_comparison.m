function [handle] = g07_area_comparison(par)
% Erzeug einen Plot, der die Berechnung mit und ohne Widerstand vergleicht.
%% Ohne Widerstand
r_off.consider_resistance = false;
r_off.consider_losses = false;
par_off = par;
par_off.config = r_off;
[OM_mtpa_off, M_mtpa_off] = g07_area_mtpa(par_off, r_off);
[OM_fw_off  , M_fw_off  ] = g07_area_fw(par_off, r_off);
[OM_mtpv_off, M_mtpv_off] = g07_area_mtpv(par_off, r_off);
%% Mit Widerstand
r_on.consider_resistance = true;
r_on.consider_losses = false;
par_on = par;
par_on.config = r_on;
[OM_mtpa_on, M_mtpa_on] = g07_area_mtpa(par_on, r_on);
[OM_fw_on  , M_fw_on  ] = g07_area_fw(par_on, r_on);
[OM_mtpv_on, M_mtpv_on] = g07_area_mtpv(par_on, r_on);
%% Plot
color_off = 'b';
color_on  = 'r';
opacity = 0.2;
handle = figure;
hold on
% Ohne Widerstand
mtpa_off = fill(OM_mtpa_off*30/pi, M_mtpa_off, color_off);
fw_off   = fill(OM_fw_off*30/pi  , M_fw_off  , color_off);
mtpv_off = fill(OM_mtpv_off*30/pi, M_mtpv_off, color_off);
set(mtpa_off, 'facealpha', opacity);
set(fw_off  , 'facealpha', opacity);
set(mtpv_off, 'facealpha', opacity);
% Mit Widerstand
mtpa_on = fill(OM_mtpa_on*30/pi, M_mtpa_on, color_on);
fw_on   = fill(OM_fw_on*30/pi  , M_fw_on  , color_on);
mtpv_on = fill(OM_mtpv_on*30/pi, M_mtpv_on, color_on);
set(mtpa_on, 'facealpha', opacity);
set(fw_on  , 'facealpha', opacity);
set(mtpv_on, 'facealpha', opacity);
hold off
end