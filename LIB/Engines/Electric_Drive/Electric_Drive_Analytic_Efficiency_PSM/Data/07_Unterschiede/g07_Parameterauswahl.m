% Dieses Skript wählt Parametersätze mit besonderen Charakteristiken aus.
%% Maximaler Widerstand
R_max_index = zeros(1,3);
R_max = zeros(1,3);
for i = 1:1:numel(par6_9)
    if(par6_9{i}.prim.R_s>R_max(1))
        R_max(1) = par6_9{i}.prim.R_s;
        R_max_index(1) = i;
    end
end
for i = 1:1:numel(par6_11)
    if(par6_11{i}.prim.R_s>R_max(2))
        R_max(2) = par6_11{i}.prim.R_s;
        R_max_index(2) = i;
    end
end
for i = 1:1:numel(par7_7)
    if(par7_7{i}.prim.R_s>R_max(3))
        R_max(3) = par7_7{i}.prim.R_s;
        R_max_index(3) = i;
    end
end
%% Erreichen des MTPV-Bereiches
num_of_engines_mtpv = 0;
index6_9 = zeros(1, numel(par6_9));
for i = 1:1:numel(par6_9)
    if(~isinf(par6_9{i}.sec.omega_fs1fs2))
        num_of_engines_mtpv = num_of_engines_mtpv+1;
        index6_9(num_of_engines_mtpv) = i;
    end
end
index6_9 = index6_9(1:num_of_engines_mtpv);
num_of_engines_mtpv = 0;
index6_11 = zeros(1, numel(par6_11));
for i = 1:1:numel(par6_11)
    if(~isinf(par6_11{i}.sec.omega_fs1fs2))
        num_of_engines_mtpv = num_of_engines_mtpv+1;
        index6_11(num_of_engines_mtpv) = i;
    end
end
index6_11 = index6_11(1:num_of_engines_mtpv);
num_of_engines_mtpv = 0;
index7_7 = zeros(1, numel(par7_7));
for i = 1:1:numel(par7_7)
    if(~isinf(par7_7{i}.sec.omega_fs1fs2))
        num_of_engines_mtpv = num_of_engines_mtpv+1;
        index7_7(num_of_engines_mtpv) = i;
    end
end
index7_7 = index7_7(1:num_of_engines_mtpv);
%% Maximales Verhältnis von L_d und L_q
% L_q/L_d
L_max_index = zeros(1,3);
L_max = zeros(1,3);
for i = 1:1:numel(par6_9)
    if(par6_9{i}.prim.L_q/par6_9{i}.prim.L_d>L_max(1))
        L_max(1) = par6_9{i}.prim.L_q/par6_9{i}.prim.L_d;
        L_max_index(1) = i;
    end
end
for i = 1:1:numel(par6_11)
    if(par6_11{i}.prim.L_q/par6_11{i}.prim.L_d>L_max(2))
        L_max(2) = par6_11{i}.prim.L_q/par6_11{i}.prim.L_d;
        L_max_index(2) = i;
    end
end
for i = 1:1:numel(par7_7)
    if(par7_7{i}.prim.L_q/par7_7{i}.prim.L_d>L_max(3))
        L_max(3) = par7_7{i}.prim.L_q/par7_7{i}.prim.L_d;
        L_max_index(3) = i;
    end
end