function [OM, M, delta_off, delta_on] = g07_area_overvoltage(par, tics_omega, tics_M)
%% Vorbereiten des Kennfeldbereichs
% Achsenwerte
M_val = linspace(0,par.sec.M_nom, tics_M);
omega_val = linspace(0,par.prim.n_max*2*pi/60, tics_omega);
[M, OM] = meshgrid(M_val, omega_val);

%% Berechnen der Stellgroessen
par_off = par;
r_off.consider_resistance = false;
r_off.consider_losses = false;
par_off.config = r_off;
[~, ~, ud_off, uq_off] = g03_control_momentbased(par_off, M, OM, r_off);

par_on = par;
r_on.consider_resistance = true;
r_on.consider_losses = true;
par_on.config = r_on;
[~, ~, ud_on, uq_on] = g03_control_momentbased(par_on, M, OM, r_on);

%% Berechnung der Überspannungswerte
U_off = sqrt(ud_off.^2+uq_off.^2);
overvoltage_index_off = find(U_off>par.prim.u_max);
delta_off = zeros(size(OM));
delta_off(overvoltage_index_off) = U_off(overvoltage_index_off)-par.prim.u_max;
U_on = sqrt(ud_on.^2+uq_on.^2);
overvoltage_index_on = find(U_on>par.prim.u_max);
delta_on = zeros(size(OM));
delta_on(overvoltage_index_on) = U_on(overvoltage_index_on)-par.prim.u_max;

% %% Plot
% subplot(1,2,1)
% surf(OM, M, delta_on)
% zlim([0 ceil(max(max(delta_off)))]);
% title('R beruecksichtigt')
% subplot(1,2,2)
% surf(OM, M, delta_off)
% title('R vernachlaessigt')
end