% Wie verh�lt sich die Spannung in der omega-M-Ebene?

%% Betrachtung in omega-Richtung

% % omega = linspace(0, par.sec.omega_max, 100);
% omega = linspace(260, 270, 100);
% M = 90.*ones(size(omega));
r_on.consider_resistance = true;
r_off.consider_resistance = false;
% [id_on, iq_on, ud_on, uq_on] = g03_control_momentbased(par, M, omega, r_on);
% [id_off, iq_off, ud_off, uq_off] = g03_control_momentbased(par, M, omega, r_off);
% figure
% plot(omega, sqrt(ud_on.^2+uq_on.^2)-sqrt(ud_off.^2+uq_off.^2))
% xlabel('Drehzahl \omega_{mech}')
% ylabel('Spannungsdifferenz')
% figure
% plot(omega, sqrt(ud_on.^2+uq_on.^2))
% hold on
% plot(omega, sqrt(ud_off.^2+uq_off.^2))

%% Betrachtung in M-Richtung

omega = 265;
M = linspace(0, g03_max_moment(par, omega));
omega = omega.*ones(size(M));

[id_on, iq_on, ud_on, uq_on] = g03_control_momentbased(par, M, omega, r_on);
[id_off, iq_off, ud_off, uq_off] = g03_control_momentbased(par, M, omega, r_off);
figure
plot(M, sqrt(ud_on.^2+uq_on.^2)-sqrt(ud_off.^2+uq_off.^2))
xlabel('Drehmoment')
ylabel('Spannungsdifferenz')
figure
plot(M, sqrt(ud_on.^2+uq_on.^2))
hold on
plot(M, sqrt(ud_off.^2+uq_off.^2))