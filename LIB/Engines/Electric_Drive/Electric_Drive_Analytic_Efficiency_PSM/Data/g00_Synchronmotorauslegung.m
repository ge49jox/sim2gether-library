%% Hauptskript zur Auslegung der Synchronmaschine
clear all
%% Definition der Auslegungsparameter
%% Quantitative Parameter 
P_n=30*1e3;                             % in W          - Nennleistung
n_n=3000;                               % in 1/min      - Nenndrehzahl
n_max = 10000;                          % in 1/min      - Maximaldrehzahl
U_n=250;                             	% in V          - Nennspannung
Uberlastfaktor=1;                     % Werte zwischen 1.0-4.0 w�hlen
%�berlastfaktor 1 w�rde die Kennfelder im Nennbereich berechnen

%% Qualitative Parameter 
cphi=0.9;                       
% in 1 - Leistungsfaktor
m=3;                                    % Zahl der Phasen des Wechselstroms (Strangzahl)
Schaltung='Stern';                      % "Dreieck" oder "Stern"
Erregung_Rotor='permanent';         	% hier nur "permanent" (=Vollpol)                           
Kuehlung='Wasser';                      % "Luft" oder "Fluessig"
Material_Stator='Kupfer';               % "Kupfer" oder "Aluminium"
%% Aufl�sung der Kennfelder einstellen:

tics_omega = 62;
tics_M     = 60;
% Diese Einstellungen definieren die Aufloesung des Kennfelds. Vorsicht,
% die Werte bestimmen ma�geblich die Rechenzeit! Immer die selbe Anzahl angeben!


%% Optionen Kennfeldberechnung 
    configuration.consider_resistance = true;
    configuration.motor_temp = 75;
    configuration.consider_losses = false;  %immer false; Verluste werden an anderer Stelle ber�cksichtigt
   % -------------------------------------------------------------------------
%% Matlabpfad festlegen:
%    addpath(genpath(pwd));
% -------------------------------------------------------------------------
%% Aufruf der Funktion zur iterativen Auslegung
    par = g04_iterative_design_motor(P_n, n_n, U_n, cphi, m, Schaltung, ...
    Erregung_Rotor, Kuehlung, Material_Stator, configuration, n_max);

%% Nennmoment 
    M_nenn=par.sec.M_nom;
    par.sec.omega_max=n_max/60*pi*2;
%% Nennlastkennlinie:
    [om_min, m_min, eta_min] = g05_full_load_line(par, tics_omega, configuration);
    
%% �berlast einstellen:
    Ueberlast_SM;
   
%% Eisenverluste berechnen:
    Parameter_Eisenverluste;
    %%
    Hauptskript_Eisenverluste_SM;

%% Kennfeldberechnung:
    [OM, M, om_M_eta_1, om_M_Pel_1, om_P_out] = g05_efficiency_map(par, tics_omega, tics_M, configuration);

%% Leistungskennfeld berechnen (inkl. Eisenverluste):
    om_M_Pel=om_M_Pel_1+P_Fe_Matrix; 
    
%% Wirkungsgradkennfeld berechnen (inkl. Eisenverluste):
    om_M_eta = om_P_out./om_M_Pel;
  

%% Fullloadline berechnen:
    [om, m, eta] = g05_full_load_line(par, tics_omega, configuration);

%% Kennfelder erweitern
    Matrizenswap;
     
%% Leistungskennfeld f�r Simulation anpassen:
   om_M_Pel_addiert_Sim= no_NaN_in_gen_aus_Matrix_SM(om_M_Pel_addiert);
   Pelmotgen_Matrix_neu=om_M_Pel_addiert_Sim;
   om_M_Pel_addiert_Sim= no_NaN_in_mot_aus_Matrix_SM(om_M_Pel_addiert_Sim);
    
%% Maximale Leistung berechnen:
    P_max_berechnen;
    
%% Plotoptionen initialisieren
    Plot_Optionen_SM;
      
%% Wirkungsgrad-Kennfeld plotten
    eta_Kennfeld_Plot;

%% Leistungs-Kennfeld plotten
    Pel_Kennfeld_Plot;
     
%% Fahrzeug initialisieren:
    %Fahrzeugdaten_Visio_M;

%% Fahrzyklen initialisieren:
    %Fahrzyklen;
 
%% Simulationsmodell_SM in Simulink �ffnen:
  %  Simulationsmodell_SM;
  
%% Das Ausf�hren dieser Zelle startet die Simulation:
    %set_param('Simulationsmodell_SM','SimulationCommand','start')
     
