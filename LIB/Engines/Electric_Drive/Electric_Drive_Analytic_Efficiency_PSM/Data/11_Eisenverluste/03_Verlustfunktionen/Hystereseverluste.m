%% Berechnung der spezifischen Hystereseverluste
function p_hyst = Hystereseverluste(k,k_bh,H_c,B_max,f,rho)

p_hyst = k * k_bh * 4 * H_c * B_max * f / rho;