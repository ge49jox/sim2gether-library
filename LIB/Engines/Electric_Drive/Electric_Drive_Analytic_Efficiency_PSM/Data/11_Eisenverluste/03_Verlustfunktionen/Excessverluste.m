%% Berechnung der spezifischen Excessverluste
function p_exc = Excessverluste(C,B_max,f,rho)
p_exc = C * B_max^1.5 * f.^1.5 / rho;