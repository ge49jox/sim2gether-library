%% Hauptskript zur Berechnung der Eisenverluste

%% alle Unterordner zum Suchpfad hinzuf�gen:
%addpath(genpath(pwd));

%% Initialisierung der Inputparameter:
 primaere_Parameter;


%% Wahl des Blechs und Initialisierung der zugeh�rigen Materialparameter:
M530_50A;

%% �bergabe des Rotor- und Statorvolumens:
sekundaere_Parameter;

%% Interpolation von mu_r
tics_f_nenn=(500-50)/(tics_omega-1); %dimensioniert die Winkelgeschwindigkeit in den Skripten gleich wenn die Werte in Zeile 17 ge�ndert werden, m�ssen sie auch hier angepasst werden.
f_nenn = [50:tics_f_nenn:500];
n_nenn = 2*pi*f_nenn/p;
mu_rS = interp2(B,f,M_mu,B_rS_max,f_nenn); % Interpolieren von mu_r aus Datenbl�ttern
mu_zS = interp2(B,f,M_mu,B_zS_tat,f_nenn);
%mu_rL = interp2(B,f,M_mu,B_rL_max,f_nenn);
%mu_zL = interp2(B,f,M_mu,B_zL_tat,f_nenn);

%% Bestimmung der Fitparameter
% Die Fitparameter k und C werden in dem eigenen Dokument 'Fitparameter.m'
% im Ordner '04_Berechnung_Fitparameter' bestimmt. In der Funktion
% 'createFit.m' im selben Ordner k�nnen in Zeile 30 die gew�nschten unteren
% Grenzen f�r die zu fittenden Koeffizienten bestimmt werden. Hierf�r
% m�ssen die Werte des Vektors 'opts.Lower = [C k];' entsprechend angepasst
% werden. Standardm��ig stehen die Werte f�r [C k] auf [0 0]. Dies ergibt
% den mathematisch besten Fit. M�chte man einen physikalisch sinnvollen Fit
% erzielen, so sollte der Wert von k ungef�hr auf 1 gesetzt werden (der
% Wert f�r C ergibt sich dann automatisch). Hierf�r ergeben sich in 
% einzelnen F�llen niedriger Induktionen jedoch schlechte Fit-Ergebnisse 
% mit gro�en Abweichungen bei hohen Frequenzen. Die Parameter stehen daher 
% stadardm��ig auf Null und k�nnen vom Benutzer des Tools manuell angepasst
% werden.
Fitparameter;
%% Berechnung der Eisenverluste:
% Es werden die folgenden Bezeichnungen verwendet:
    % p - spezifische Eisenverluste
    % P - tats�chliche auftretende Eisenverluste
% 1. Stator
p_rS = Eisenverluste(B_rS_max,f_nenn,rho,H_c,sigma,mu_rS,d,k_rS,C_rS,k_bh,k_bw); 
P_rS = p_rS * rho * V_rS;
p_zS = Eisenverluste(B_zS_tat,f_nenn,rho,H_c,sigma,mu_zS,d,k_zS,C_zS,k_bh,k_bw);
P_zS = p_zS * rho * V_zS;

P_S = P_rS + P_zS;       % Gesamtverluste im Stator in W
%%
% 2. L�ufer
% p_rL = Eisenverluste(B_rL_max,f_nenn,rho,H_c,sigma,mu_rL,d,k,C,k_bh,k_bw);
% P_rL = p_rL * rho * V_rL;
% p_zL = Eisenverluste(B_zL_tat,f_nenn,rho,H_c,sigma,mu_zL,d,k,C,k_bh,k_bw);
% P_zL = p_zL * rho * V_zL;
% 
% P_L = P_rL + P_zL   ;    % Gesamtverluste im Rotor in W
%% 
% 3. Gesamte Eisenverluste in W
%P_Fe = P_S + P_L;   
P_Fe=P_S;

%% Transformiert den P_Fe Vektor in eine Matrix kompatibel zu den Kennfeldern
Eisenverluste_in_Kennfeld;
%%
% grafische Ausgabe der Ergebnisse 
%figure('NumberTitle','off','Name','Eisenverluste P in Abh�ngigkeit der Frequenz f');
%plot([0,n_nenn],[0,P_Fe]);
%xlabel('Drehzahl n in 1/min')
%ylabel('Verlustleistung P in W')
%%
%M=[0,200]; % sp�ter ausblenden bzw. in Prim�re Inputparameter �bernehmen
%figure('NumberTitle','off','Name','Verlustkennfeld');
%contourf([0,n_nenn],M,[0,P_Fe;0,P_Fe],length(f_nenn)+1)
%colorbar
%xlabel('Winkelgeschwindigkeit \Omega/(rad/s)')
%ylabel('Drehmoment M in Nm')
%hold on
%%
%X = [0,5250];
%Y = [145.5,145.5];
%X2 = [5250:15000];
%Y2 = @(x) (145.5*5250)./X2;
%plot(X,Y,'k-',X2,Y2(X2),'k-','LineWidth',2)
