function [ R_S_20,R_S_75,R_S_95,R_S_115,R_S_T,R_L_20_s,R_L_75_s,R_L_95_s,R_L_115_s,R_L_T_s,l_m1,l_w1]...
    = wicklungswiderstaende( D,Material_Stator,Material_Laeufer,l,T_p,U_n,A_1S,w_s,A_nL,delta,h_nL,q_L,N_L,p,A_R,xi_1_tat)
%% Verlustrechnung - evtl. sp�ter berechnen
% Wicklungswiederst�nde in ohm bei der Temperatur T in �C (g�ltig bis T_max=50�C; liefert aber auch bis 115�C sehr gute Werte)
% Stator
T_S=60;
if strcmp(Material_Stator,'Kupfer')==1          % Leitf�higkeit des Statormaterials bei unterschiedlichen Temperaturen
    kappa_20=58;
    kappa_75=47.7;
    kappa_95=44.8;
    kappa_115=42.2;
    alpha_to=0.0039;
elseif strcmp(Material_Stator,'Aluminium')==1
    kappa_20=37;
    kappa_75=30.6;
    kappa_95=28.8;
    kappa_115=27.2;
    alpha_to=0.004;
end
l_m1 = 2*(l+1.3*T_p+(0.03+0.02*U_n/1000)); % mittlere Leiterl�nge zur n�herungsweisen 
%Berechung des Wicklungswiderstands
l_w1 = 1.3*T_p+(0.03+0.02*U_n/1000); % Leiterl�nge im Wicklungskopf
R_S_20 = w_s * l_m1 / (kappa_20*A_1S);                 % Widerstand der Statorwicklung bei 20�C
R_S_75 = w_s * l_m1 / (kappa_75*A_1S);                 % Widerstand der Statorwicklung bei 75�C
R_S_95 = w_s * l_m1 / (kappa_95*A_1S);                 % Widerstand der Statorwicklung bei 95�C
R_S_115 = w_s * l_m1 / (kappa_115*A_1S);             % Widerstand der Statorwicklung bei 115�C
R_S_T = R_S_20 * (1+alpha_to*(T_S-20));            % Widerstand der Statorwicklung bei T�C
% Laeufer
T_L=110;
if strcmp(Material_Laeufer,'Kupfer')==1       % Leitf�higkeit des Statormaterials bei unterschiedlichen Temperaturen
    kappa_20=58;
    kappa_75=47.7;
    kappa_95=44.8;
    kappa_115=42.2;
    alpha_to=0.0039;
elseif strcmp(Material_Laeufer,'Aluminium')==1
    % werte f�r Aluminiumguss
    kappa_20=30;
    kappa_75=24.8;
    kappa_95=23.4;
    kappa_115=22.1;
    alpha_to=0.004;
end
l_sL=l;
R_sL_20=l_sL/(kappa_20*A_nL);                                        % Widerstand eines Rotorstabs bei 20�C
R_sL_75=l_sL/(kappa_75*A_nL);                                        % Widerstand eines Rotorstabs bei 75�C
R_sL_95=l_sL/(kappa_95*A_nL);                                       % Widerstand eines Rotorstabs bei 95�C
R_sL_115=l_sL/(kappa_115*A_nL);                                    % Widerstand eines Rotorstabs bei 115�C
R_sL_T=R_sL_20*(1+alpha_to*(T_L-20));                           % Widerstand eines Rotorstabs bei T�C

D_r = D-h_nL;
l_rm = pi*D_r/N_L;

R_r_20 = l_rm/(kappa_20*A_R); 
R_r_75 = l_rm/(kappa_75*A_R);
R_r_95 = l_rm/(kappa_95*A_R);
R_r_115 = l_rm/(kappa_115*A_R);
% Auf die St�nderseite transformierte Statorwiderst�nde
R_L_20_s = ((w_s*xi_1_tat)/(N_L/6))^2 * N_L/3 *(R_sL_20 + (R_r_20/(2*sin(pi*p/N_L)^2)));    % Widerstand der Laeuferwicklung bei 20�C
R_L_75_s = ((w_s*xi_1_tat)/(N_L/6))^2 * N_L/3 *(R_sL_75 + (R_r_75/(2*sin(pi*p/N_L)^2)));      % Widerstand der Laeuferwicklung bei 75�C
R_L_95_s = ((w_s*xi_1_tat)/(N_L/6))^2 * N_L/3 *(R_sL_95 + (R_r_95/(2*sin(pi*p/N_L)^2)));      % Widerstand der Laeuferwicklung bei 95�C
R_L_115_s = ((w_s*xi_1_tat)/(N_L/6))^2 * N_L/3 *(R_sL_115 + (R_r_115/(2*sin(pi*p/N_L)^2)));  % Widerstand der Laeuferwicklung bei 115�C
R_L_T_s = R_L_20_s * (1+alpha_to*(T_L-20));                              % Widerstand der Laeuferwicklung bei T�C


end

