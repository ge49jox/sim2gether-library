function [ L_sigma_1,L_sigma_2,L_sigma_2_s,L_1,sigma,sigma_1,sigma_2,L_sigma_schr,ue_h ] = ...
    streuinduktivitaeten(b_sS,delta,h_nS,b_nS,W,T_p,mu_0,l_i_tat,w_s,xi_1_tat,p,q_S,l_w1,L_hges1,...
    b_sL,h_nL,b_nL,D,N_L,N_S,Wicklungsart)
%% Nut- Zahnkopfstreuung Stator
% Zahnkopfstreuleitwert Stator
bs_delta = b_sS/(delta/1000);
if bs_delta < 3
    l_z = [1.15,1,0.8,0.62,0.5,0.4,0.3,0.25,0.18   ,0.14,0.1,  0.06,0.03,  -0.08];
    bsdelta = [0,0.1,0.25,0.5,0.75,1,1.25,1.5,1.75 ,2   ,2.25,    2.5,2.75,    3];
    polylz = polyfit(bsdelta,l_z,2);
    lz = polyval(polylz,bsdelta);
    lambda_z1 = polyval(polylz,bs_delta);
else
    l_z =     [0, -0.03,-0.05,-0.075,-0.1,-0.11,-0.12,-0.13,-0.14,-0.15,-0.16,-0.17,-0.18];
    bsdelta = [3, 4    ,5    , 6    ,7   ,8    ,9    ,10   ,11   ,12   ,13   ,14   ,16];
    polylz = polyfit(bsdelta,l_z,2);
    lz = polyval(polylz,bsdelta);
    lambda_z1 = polyval(polylz,bs_delta);
end
lambda_l1 = h_nS*0.9 / (3 * b_nS) + 0.1*h_nS/b_sS; % Streuleitwert Stator | Berechnung mit h_nS ist nur eine grobe Abschätzung
% für eine genauere Berechnung des Streuleitwertes müsste die Nutgeometrie
% auskonstruiert sein. Annahme ist hierbei, dass 90% der Nuthöhe mit
% Leitern gefüllt sind und 10% darüber Luft darstellt
% Berechnung der Hilfsfaktoren der Streuung mehrsträngiger gesehnter
% Wicklungen
%% Berücksichtigung der Sehnung
W_tp = W/T_p;  % Sehnung
Wtp = [0.666,0.777,0.888,1];
k1 =  [0.82,0.87,0.94,1];
polyk1 = polyfit(Wtp,k1,1);
k_1 = polyval(polyk1,W_tp);
k2 =  [0.75,0.84,0.91,1];
polyk2 = polyfit(Wtp,k2,1);
k_2 = polyval(polyk2,W_tp);
% Nut Zahnkopf Streuleitwert | d ist der Abstand der beiden Wicklungen
% bei Zweischichtwicklungen
lambda_nz1 = k_1 * lambda_l1 + k_2 * lambda_z1; % +d/(4*b_nS)
L_sigma_nz1 = 2*mu_0*l_i_tat* lambda_nz1 * w_s^2 / (p*q_S);
%% Wicklungskopfstreuung Stator
lambda_ws = 0.3; % muller08 S.335
if strcmp(Wicklungsart,'Einschichtwicklung')==1
    lambda_w = lambda_ws * l_w1 / (2 * l_i_tat); % Einschichtwicklung
elseif strcmp(Wicklungsart,'gesehnte Zweischichtwicklung')==1
    lambda_w = lambda_ws * l_w1 / (2 * l_i_tat); % Zweischichtwicklung mit einfacher Zonenbreite
end
L_sigma_w = 2 * mu_0 * l_i_tat * w_s^2 * lambda_w / p ;
%% Oberwellenstreuung Stator
sigma_o1 = 0.011; % Abgelesen aus Müller08 S.340 für q = 3 und 5/6-Sehnung;
L_sigma_o1 = sigma_o1 * L_hges1;
%% Schrägungsstreuung 
sigma_schr = 1- ((sin(p*pi/N_S))/(p*pi/N_S))^2;
L_sigma_schr = sigma_schr * L_hges1;

%% Streuinduktivität Stator L_sigma_1
L_sigma_1 = L_sigma_nz1 + L_sigma_w + L_sigma_o1;



%% Zahnkopfstreuleitwert Läufer
bs2_delta = b_sL/(delta/1000);
if bs2_delta < 3
    l_z = [1.15,1,0.8,0.62,0.5,0.4,0.3,0.25,0.18   ,0.14,0.1,  0.06,0.03,  -0.08];
    bsdelta = [0,0.1,0.25,0.5,0.75,1,1.25,1.5,1.75 ,2   ,2.25,    2.5,2.75,    3];
    polylz = polyfit(bsdelta,l_z,2);
    lz = polyval(polylz,bsdelta);
    lambda_z2 = polyval(polylz,bs2_delta);
else
    l_z =     [0, -0.03,-0.05,-0.075,-0.1,-0.11,-0.12,-0.13,-0.14,-0.15,-0.16,-0.17,-0.18];
    bsdelta = [3, 4    ,5    , 6    ,7   ,8    ,9    ,10   ,11   ,12   ,13   ,14   ,16];
    polylz = polyfit(bsdelta,l_z,2);
    lz = polyval(polylz,bsdelta);
    lambda_z2 = polyval(polylz,bs2_delta);
end
lambda_l2 = h_nL*0.9 / (3 * b_nL)+ 0.1*h_nL/b_sL;% Streuleitwert Läufer | Berechnung mit h_nL ist nur eine grobe Abschätzung
% für eine genauere Berechnung des Streuleitwertes müsste die Nutgeometrie
% auskonstruiert sein.
lambda_nz2 = lambda_l2 + lambda_z2;
lambda_r = 0.35; % nach Müller08 S.548 0.25...0.5
L_sigma_s = mu_0 * l_i_tat * lambda_nz2; %Streuinduktivität eines Stabes
D_r = D - h_nL; % ungefährer mittlerer Ringdurchmesser
L_sigma_r = mu_0 * pi * D_r * lambda_r / N_L; % Streuinduktivität eines Ringsegmentes
%% Oberwellenstreuung Rotor
ue_h = (w_s*xi_1_tat)/(N_L/6); % reeles Übersetzungsverhältnis
sigma_o2 = ((pi*p/N_L)^2 / (sin(pi*p/N_L)^2)) - 1 ; % Müller Theorie elek. Masch. S.240 (1.8.57b)
L_sigma_o2 = sigma_o2 * L_hges1/ue_h^2;
%% Streudinduktivität Rotor L_sigma_2
L_sigma_2 = N_L/3 * (L_sigma_s + (1/(2*sin(pi*p/N_L)^2)) * L_sigma_r) + L_sigma_o2;
L_sigma_2_s = ue_h^2 * L_sigma_2; % Rotorstreuinduktivität auf Ständerseite transformiert

%% Statorstreuziffer sigma_1
sigma_1 = L_sigma_1/L_hges1;
%% Rotorstreuziffer sigma_2
sigma_2 = L_sigma_2_s/L_hges1;
%% Bondelsche Streuziffer sigma
sigma = 1 - 1/((1 + sigma_1) * (1 + sigma_2));
%% Induktivität L_1
L_1 = L_hges1 + L_sigma_1;


end

