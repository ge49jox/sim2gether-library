function [ V_zS_abg,V_zL_abg,k,B_werte,fit_int ] = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang )
%% Magnetisierungdiagramm und Bestimmung der Zahnfeldstärke H aus Feldinduktion B
B_werte=[0.5:0.1:2.4];
H_50=[105,114,124,135,147,163,184,216,272,395,759,1933,4618,9007,15504,20500,26000,31500,37000,42500,48000];      % aus Datentabelle von Blechhersteller //Anmerkung JoDi: welcher?
%H_50=[105,114,123,133,145,158,174,200,243,333,500,1250,3000,6250,12000,18500,23000,28500,34000,39500,45000];      %aus Berechnungsbeispiel Müller
H_400=[163, 187, 214, 245, 280, 319, 361, 407, 458, 531, 891.3, 2065, 4750, 9139, 15636,20500,26000,31500,37000,42500,48000];
for i=1:length(B_werte)
    H_int(i)=H_50(i)+(H_400(i)-H_50(i))/(400-50)*(f_n-50);  % Interpolation zwischen H_400 und H_50
end

fit_int=spline(B_werte,H_int);                                                                                            % Bestimmung abschnittsweiser Annäherungsfunktion (kubische Polynome) 
for i=1:length(B_werte)-1                                                                                                  % Bestimmung des relevanten Teilabschnitts
    if and(B_zS_abg>=B_werte(i),B_zS_abg<=B_werte(i+1))
        abschn_S=i;  
    end
end

for i=1:length(B_werte)-1                                                                                                  % Bestimmung des relevanten Teilabschnitts
    if and(B_zL_abg>=B_werte(i),B_zL_abg<=B_werte(i+1))
        abschn_L=i;  
    end
end
% Definition der Teilfunktion und Berechnung der Feldstärke
xWerte_S=linspace(fit_int.breaks(abschn_S),fit_int.breaks(abschn_S+1),100);
cf_S=fit_int.coefs(abschn_S,:);
teilfkt_S=polyval(cf_S,xWerte_S-fit_int.breaks(abschn_S));
H_zS_abg=polyval(cf_S,B_zS_abg-fit_int.breaks(abschn_S));
xWerte_L=linspace(fit_int.breaks(abschn_L),fit_int.breaks(abschn_L+1),100);
cf_L=fit_int.coefs(abschn_L,:);
teilfkt_L=polyval(cf_L,xWerte_L-fit_int.breaks(abschn_L));
H_zL_abg=polyval(cf_L,B_zL_abg-fit_int.breaks(abschn_L));
% Diagramm der Magnetisierungskurve
    if strcmp(plothandle,'JA') == 1 
    figure(3)
    hold on
    plot(linspace(fit_int.breaks(abschn_S),fit_int.breaks(abschn_S+1),100),teilfkt_S,'b-')
    plot(B_werte,H_int,'go')
    plot(B_zS_abg,H_zS_abg,'bx')
    plot(linspace(fit_int.breaks(abschn_L),fit_int.breaks(abschn_L+1),100),teilfkt_L,'b-')
    % plot(B50,H50,'ro')
    plot(B_zL_abg,H_zL_abg,'bx')
    axis([0,2,0,17000])
    title('Magnetisierungskurve')
    xlabel('B in T')
    ylabel('H in A/m')
    grid on
    end
clear xWerte_S xWerte_L abschn_L abschn_S teilfkt_L teilfkt_S cf_L cf_S
%% abgeschätzter Spannungsabfall in den Zähnen in A
V_zS_abg=H_zS_abg*h_nS;
V_zL_abg=H_zL_abg*h_nL;
%% Sättigungsfaktor k in 1
k=(V_zS_abg+V_zL_abg)/V_delta_ang;

end

