function [ h_nS ] = statorzahnkorrektur(A_nS,b_zS,N_S,D )
%% Korrektur des Rotornutgeometrie
% x enstpricht der gesuchten Zahn- Nuth�he h_nS

syms x
alpha = 360/N_S;
f = @(x) pi*(((D+2*x)/2).^2 - (D/2)^2)*(alpha/360) - b_zS*x - A_nS/(1000*1000);
sol = vpa(solve(f),2);
h_nS = double(sol(1)); 



end

