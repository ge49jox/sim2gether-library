function [ alpha_p_tat,B_max_tat,V_delta_tat ] = kor_alpha_p(plothandle, k,B_m_tat,k_c,delta,mu_0 )
%% korrigierter Abplattungsfaktor alpha_P_tat in 1
k_werte=[0:0.2:1.8];
alpha_werte=[1.57,1.435,1.385,1.36,1.335,1.32,1.31,1.295,1.28,1.25];
aprox_alpha_p=spline(k_werte,alpha_werte);
for i=1:length(k_werte)-1                                                                                                  % Bestimmung des relevanten Teilabschnitts
    if and(k>=k_werte(i),k<=k_werte(i+1))
        abschn_k=i;  
    end
end
xWerte_k=linspace(aprox_alpha_p.breaks(abschn_k),aprox_alpha_p.breaks(abschn_k+1),100);
koeff=aprox_alpha_p.coefs(abschn_k,:);
funkt=polyval(koeff,xWerte_k-aprox_alpha_p.breaks(abschn_k));
alpha_p_tat=polyval(koeff,k-aprox_alpha_p.breaks(abschn_k));
% Diagramm des Abplattungsfaktors
    if strcmp(plothandle,'JA') == 1 
    figure(4)
    hold on
    plot(linspace(aprox_alpha_p.breaks(abschn_k),aprox_alpha_p.breaks(abschn_k+1),100),funkt,'b-')
    plot(k_werte,alpha_werte,'go')
    plot(k,alpha_p_tat,'bx')
    axis([0,1.6,1.2,1.6])
    title('Abplattungsfaktor')
    xlabel('k in 1')
    ylabel('\alpha_p in A/m')
    grid on
    end
clear xWerte_k abschn_k teilfkt_L funkt koeff aprox_alpha_p
%% Tatsächliche maximale Luftspaltinduktion in T
B_max_tat=alpha_p_tat*B_m_tat;
%% tatsächlicher Spannungsabfall im Luftspalt
V_delta_tat=B_max_tat*k_c*(delta/1000)/mu_0;             % in A
end

