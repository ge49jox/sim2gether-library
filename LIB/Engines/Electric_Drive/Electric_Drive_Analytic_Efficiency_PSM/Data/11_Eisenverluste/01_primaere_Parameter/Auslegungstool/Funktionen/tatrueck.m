function [ H_rS_max,H_rL_max,T_rS,T_rL,h_rS_d_T_rS,h_rL_d_T_rL,sred_1,sred_2 ]...
    = tatrueck(plothandle,B_werte,B_rS_max,B_rL_max,fit_int,D,h_nS,p,delta,...
    h_nL,h_rS,h_rL,B_rS_zul,T_p)
%% Magnetisierungdiagramm und Bestimmung der tats�chlichen R�ckenfeldst�rke H aus Feldinduktion B
for i=1:length(B_werte)-1                                                                                                  % Bestimmung des relevanten Teilabschnitts
    if and(B_rS_max+0.00001>=B_werte(i),B_rS_zul<=B_werte(i+1))
        abschn_S=i;
    end
end
for i=1:length(B_werte)-1                                                                                                  % Bestimmung des relevanten Teilabschnitts
    if and(B_rL_max+0.00001>=B_werte(i),B_rL_max<=B_werte(i+1))
        abschn_L=i;
    end
end
% Definition der Teilfunktion und Berechnung der Feldst�rke
xWerte_S=linspace(fit_int.breaks(abschn_S),fit_int.breaks(abschn_S+1),100);
cf_S=fit_int.coefs(abschn_S,:);
teilfkt_S=polyval(cf_S,xWerte_S-fit_int.breaks(abschn_S));
H_rS_max=polyval(cf_S,B_rS_max-fit_int.breaks(abschn_S));
xWerte_L=linspace(fit_int.breaks(abschn_L),fit_int.breaks(abschn_L+1),100);
cf_L=fit_int.coefs(abschn_L,:);
teilfkt_L=polyval(cf_L,xWerte_L-fit_int.breaks(abschn_L));
H_rL_max=polyval(cf_L,B_rL_max-fit_int.breaks(abschn_L));
% Diagramm der Magnetisierungskurve
if strcmp(plothandle,'JA') == 1
    figure(3)
    plot(linspace(fit_int.breaks(abschn_S),fit_int.breaks(abschn_S+1),100),teilfkt_S,'k-')
    plot(B_rS_max,H_rS_max,'kx')
    plot(linspace(fit_int.breaks(abschn_L),fit_int.breaks(abschn_L+1),100),teilfkt_L,'k-')
    plot(B_rL_max,H_rL_max,'kx')
end
clear i xWerte_S xWerte_L abschn_L abschn_S teilfkt_L teilfkt_S cf_L cf_S B50 H50 fit50Hz
%%
T_rS=(D+2*h_nS)*pi/(2*p);
T_rL=(D-2*delta/1000-2*h_nL)*pi/(2*p);
h_rS_d_T_rS=h_rS/T_rS;
h_rL_d_T_rL=h_rL/T_rL;
xWerte=[0:0.02:0.32];
if h_rS_d_T_rS > 0.2199
    sred_1 = T_p/3;             % N�herung nach Punga (entnommen M�ller2008 S.228)
else
    %% Eingabe des C_r Diagramms
    % Werteeingabe
    hT_S_08=[0:0.02:0.26];
    Cr_S_08=[0.7,0.76,0.82,0.86,0.9,0.935,0.96,0.99,1.02,1.05,1.08,1.11,1.15,1.185];
    hT_S_10=[0:0.02:0.32];
    Cr_S_10=[0.6,0.66,0.7,0.735,0.77,0.8,0.83,0.855,0.88,0.9,0.93,0.96,0.99,1.03,1.075,1.13,1.19];
    hT_S_12=[0:0.02:0.32];
    Cr_S_12=[0.5,0.55,0.58,0.62,0.65,0.675,0.7,0.72,0.745,0.76,0.78,0.805,0.845,0.87,0.91,0.955,1.025];
    hT_S_14=[0:0.02:0.32];
    Cr_S_14=[0.4,0.435,0.45,0.47,0.485,0.5,0.51,0.53,0.55,0.575,0.605,0.64,0.675,0.72,0.755,0.8,0.85];
    hT_S_16=[0:0.02:0.22];
    Cr_S_16=[0.27,0.285,0.3,0.305,0.315,0.325,0.335,0.355,0.37,0.385,0.415,0.44];
    hT_S_18=[0:0.02:0.22];
    Cr_S_18=Cr_S_16-0.14;                                                       % zus�tzliche Funktion B_r_max=1.8 T erstellt
    %
    % Interpolation der Zwischenfunktion
    if and(B_rS_max>=0.8,B_rS_max<=1.0)
        hT_S_tat=(0:0.02:min(hT_S_08(length(hT_S_08)),hT_S_10(length(hT_S_10))));
        for i=1:min(length(hT_S_08),length(hT_S_10))
            Cr_S_tat(i)=Cr_S_10(i)+(Cr_S_08(i)-Cr_S_10(i))/(1.0-0.8)*(B_rS_max-0.8);
        end
    elseif and(B_rS_max>=1.0,B_rS_max<=1.2)
        hT_S_tat=(0:0.02:min(hT_S_10(length(hT_S_10)),hT_S_12(length(hT_S_12))));
        for i=1:min(length(hT_S_10),length(hT_S_12))
            Cr_S_tat(i)=Cr_S_12(i)+(Cr_S_10(i)-Cr_S_12(i))/(1.2-1.0)*(B_rS_max-1.0);
        end
    elseif and(B_rS_max>=1.2,B_rS_max<=1.4)
        hT_S_tat=(0:0.02:min(hT_S_12(length(hT_S_12)),hT_S_14(length(hT_S_14))));
        for i=1:min(length(hT_S_12),length(hT_S_14))
            Cr_S_tat(i)=Cr_S_14(i)+(Cr_S_12(i)-Cr_S_14(i))/(1.4-1.2)*(B_rS_max-1.2);
        end
    elseif and(B_rS_max>=1.4,B_rS_max<=1.6)
        hT_S_tat=(0:0.02:min(hT_S_14(length(hT_S_14)),hT_S_16(length(hT_S_16))));
        for i=1:min(length(hT_S_14),length(hT_S_16))
            Cr_S_tat(i)=Cr_S_16(i)+(Cr_S_14(i)-Cr_S_16(i))/(1.6-1.4)*(B_rS_max-1.4);
        end
    elseif and(B_rS_max>=1.6,B_rS_max<=1.8+0.0001)
        hT_S_tat=(0:0.02:min(hT_S_16(length(hT_S_16)),hT_S_18(length(hT_S_18))));
        for i=1:min(length(hT_S_16),length(hT_S_18))
            Cr_S_tat(i)=Cr_S_18(i)+(Cr_S_16(i)-Cr_S_18(i))/(1.8-1.6)*(B_rS_max-1.6);
        end
    end
    
    fit_S=spline(hT_S_tat,Cr_S_tat);
    % Bestimmung des Teilpolynomabschnitts
    for i=1:length(xWerte)-1
        if and(h_rS_d_T_rS>=xWerte(i),h_rS_d_T_rS<=xWerte(i+1))
            abschn_S=i;
        end
    end
    % Definition der Teilfunktion und Berechnung von Cr
    xWerte_S=linspace(fit_S.breaks(abschn_S),fit_S.breaks(abschn_S+1),100);
    cf_S=fit_S.coefs(abschn_S,:);
    teilfkt_S=polyval(cf_S,xWerte_S-fit_S.breaks(abschn_S));
    Cr_S=polyval(cf_S,h_rS_d_T_rS-fit_S.breaks(abschn_S));
    sred_1 = Cr_S*T_rS/2;
end

if h_rL_d_T_rL > 0.3199
    sred_2 = T_p/4;             % N�herung nach Punga (entnommen M�ller2008 S.228)
else
    
    % Berechnung von C_rL
    hT_L_08=[0:0.02:0.32];
    Cr_L_08=[0.7,0.675,0.655,0.645,0.63,0.615,0.6,0.59,0.58,0.57,0.56,0.555,0.56,0.565,0.585,0.615,0.66];
    hT_L_10=[0:0.02:0.32];
    Cr_L_10=[0.6,0.585, 0.575,0.56,0.55,0.54,0.525,0.51,0.495,0.485,0.48,0.48,0.49,0.505,0.545,0.6,0.66];
    hT_L_12=[0:0.02:0.32];
    Cr_L_12=[0.5,0.485,0.475,0.465,0.455,0.445,0.435,0.425,0.41,0.405,0.405,0.41,0.43,0.45,0.5,0.58,0.66];
    hT_L_14=[0:0.02:0.32];
    Cr_L_14=[0.4,0.385,0.375,0.365,0.360,0.355,0.350,0.345,0.34,0.34,0.345,0.355,0.37,0.405,0.46,0.55,0.66];
    hT_L_16=[0:0.02:0.32];
    Cr_L_16=[0.27,0.255,0.245,0.235,0.225,0.21,0.195,0.185,0.175,0.17,0.165,0.175,0.2,0.245,0.305,0.44,0.66];
    
    %
    %Interpolation der Zwischenfunktion
    if and(B_rL_max>=0.8,B_rL_max<=1.0)
        hT_L_tat=(0:0.02:min(hT_L_08(length(hT_L_08)),hT_L_10(length(hT_L_10))));
        for i=1:min(length(hT_L_08),length(hT_L_10))
            Cr_L_tat(i)=Cr_L_10(i)+(Cr_L_08(i)-Cr_L_10(i))/(1.0-0.8)*(B_rL_max-0.8);
        end
    elseif and(B_rL_max>=1.0,B_rL_max<=1.2)
        hT_L_tat=(0:0.02:min(hT_L_10(length(hT_L_10)),hT_L_12(length(hT_L_12))));
        for i=1:min(length(hT_L_10),length(hT_L_12))
            Cr_L_tat(i)=Cr_L_12(i)+(Cr_L_10(i)-Cr_L_12(i))/(1.2-1.0)*(B_rL_max-1.0);
        end
    elseif and(B_rL_max>=1.2,B_rL_max<=1.4)
        hT_L_tat=(0:0.02:min(hT_L_12(length(hT_L_12)),hT_L_14(length(hT_L_14))));
        for i=1:min(length(hT_L_12),length(hT_L_14))
            Cr_L_tat(i)=Cr_L_14(i)+(Cr_L_12(i)-Cr_L_14(i))/(1.4-1.2)*(B_rL_max-1.2);
        end
    elseif and(B_rL_max>=1.4,B_rL_max<=1.6+0.0001)
        hT_L_tat=(0:0.02:min(hT_L_14(length(hT_L_14)),hT_L_16(length(hT_L_16))));
        for i=1:min(length(hT_L_14),length(hT_L_16))
            Cr_L_tat(i)=Cr_L_16(i)+(Cr_L_14(i)-Cr_L_16(i))/(1.6-1.4)*(B_rL_max-1.4);
        end
    end
    
    fit_L=spline(hT_L_tat,Cr_L_tat);
    % Bestimmung des Teilpolynomabschnittes
    for i=1:length(xWerte)-1
        if and(h_rL_d_T_rL>=xWerte(i),h_rL_d_T_rL<=xWerte(i+1))
            abschn_L=i;
        end
    end
    % Definition der Teilfunktion und Berechnung von Cr
    xWerte_L=linspace(fit_L.breaks(abschn_L),fit_L.breaks(abschn_L+1),100);
    cf_L=fit_L.coefs(abschn_L,:);
    teilfkt_L=polyval(cf_L,xWerte_L-fit_L.breaks(abschn_L));
    Cr_L=polyval(cf_L,h_rL_d_T_rL-fit_L.breaks(abschn_L));
    
    sred_2 = Cr_L*T_rL/2;

end 
% if strcmp(plothandle,'JA') == 1
%     % Plotten // Achtung !!! Plotten funktioniert noch nicht. Es muss noch
%     % gekl�rt werden ob diese Diagramme �berhaupt sinnvoll sind...
%     [xData1, yData1] = prepareCurveData( hT_S_08, Cr_S_08 );
%     [xData2, yData2] = prepareCurveData( hT_S_10, Cr_S_10 );
%     [xData3, yData3] = prepareCurveData( hT_S_12, Cr_S_12 );
%     [xData4, yData4] = prepareCurveData( hT_S_14, Cr_S_14 );
%     [xData5, yData5] = prepareCurveData( hT_S_16, Cr_S_16 );
%     [xData6, yData6] = prepareCurveData( hT_S_18, Cr_S_18 );
%     [xData7, yData7] = prepareCurveData( hT_L_08, Cr_L_08 );
%     [xData8, yData8] = prepareCurveData( hT_L_10, Cr_L_10 );
%     [xData9, yData9] = prepareCurveData( hT_L_12, Cr_L_12 );
%     [xData10, yData10] = prepareCurveData( hT_L_14, Cr_L_14 );
%     [xData11, yData11] = prepareCurveData( hT_L_16, Cr_L_16 );
%     [xData12, yData12] = prepareCurveData( hT_S_tat, Cr_S_tat );
%     [xData13, yData13] = prepareCurveData( hT_L_tat, Cr_L_tat );
%     
%         figure(5);
%         hold on
%         p1 = plot( fit( xData1, yData1,'splineinterp', 'Normalize', 'on' ), xData1, yData1 );
%         p2 = plot( fit( xData2, yData2,'splineinterp', 'Normalize', 'on' ), xData2, yData2 );
%         p3 = plot( fit( xData3, yData3,'splineinterp', 'Normalize', 'on' ), xData3, yData3 );
%         p4 = plot( fit( xData4, yData4,'splineinterp', 'Normalize', 'on' ), xData4, yData4 );
%         p5 = plot( fit( xData5, yData5,'splineinterp', 'Normalize', 'on' ), xData5, yData5 );
%         p6 = plot( fit( xData6, yData6,'splineinterp', 'Normalize', 'on' ), xData6, yData6 );
%         p7 = plot( fit( xData7, yData7,'splineinterp', 'Normalize', 'on' ), xData7, yData7 );
%         p8 = plot( fit( xData8, yData8,'splineinterp', 'Normalize', 'on' ), xData8, yData8 );
%         p9 = plot( fit( xData9, yData9,'splineinterp', 'Normalize', 'on' ), xData9, yData9 );
%         p10 = plot( fit( xData10, yData10,'splineinterp', 'Normalize', 'on' ), xData10, yData10 );
%         p11 = plot( fit( xData11, yData11,'splineinterp', 'Normalize', 'on' ), xData11, yData11 );
%         p12 = plot( fit( xData12, yData12,'splineinterp', 'Normalize', 'on' ), xData12, yData12 );
%         p13 = plot( fit( xData13, yData13,'splineinterp', 'Normalize', 'on' ), xData13, yData13 );
%         plot(h_rS_d_T_rS,Cr_S,'kx')
%         plot(h_rL_d_T_rL,Cr_L,'kx')
%         legend( p1, 'Cr_S_08 vs. hT_S_08', 'untitled fit 1', 'Location', 'NorthEast' );
%         xlabel h_r/T_r
%         ylabel C_r
%         grid on
%     end   
    
    


end

