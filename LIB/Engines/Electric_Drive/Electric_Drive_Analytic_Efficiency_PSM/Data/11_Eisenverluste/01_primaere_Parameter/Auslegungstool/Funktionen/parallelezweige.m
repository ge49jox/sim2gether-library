function [ E_h_r,a ] = parallelezweige( P_n,E_h,B_m_max,l_i,T_p,w,p)
%% Wicklunsart und -faktor abh�ngig von der Leistung (Vogt. S.96)

B_m_ang = B_m_max;
Phi_delta = B_m_ang*l_i*T_p;
Phi_h = Phi_delta;

if P_n>10
    %Wicklungsart='gesehnte Zweischichtwicklung';
    xi_1_ang=0.92;                                   % f�r gesehnte Zweischichtwicklung
    q_S_min = 3;
    z_nS_min = 2;
    E_h_r = zeros(2*p,1);
    for i = 1:2*p
        E_h_r(i) = z_nS_min*p*q_S_min*Phi_h*w*xi_1_ang/(i*sqrt(2));
        % E_h_r ist die Mindestspannung die n�tig ist um eine Ein-/Zweischichtwicklung mit mindestens
        % ein/zwei Leitern je Nut realisieren zu k�nnen. i entspricht der
        % Variaton der parallelen Zweige a.
    end
    mo = mod(2*p,1:2*p);
    for n = 1:2*p        
        if E_h >= E_h_r(n) && ~mo(n)
            a = n;
            break;
        else if E_h >= E_h_r(1)
                a = 1;
            end
        end
        
    end
else
    %Wicklungsart='Einschichtwicklung';
    xi_1_ang=0.96;                                   % f�r Einschichtwicklung
    q_S_min = 3;
    z_nS_min = 1;
    E_h_r = zeros(p,1);
    for i = 1:p
        E_h_r(i) = z_nS_min*p*q_S_min*Phi_h*w*xi_1_ang/(i*sqrt(2));
        % E_h_r ist die Mindestspannung die n�tig ist um eine Ein-/Zweischichtwicklung mit mindestens
        % ein/zwei Leitern je Nut realisieren zu k�nnen. i entspricht der
        % Variaton der parallelen Zweige a.
    end
    mo = mod(p,1:p);
    for n = 1:p
        
        if E_h >= E_h_r(n) && ~mo(n)
            a = n;
            break;
        else if E_h >= E_h_r(1)
                a = 1;
            end
        end
        
    end   
       
end

end

