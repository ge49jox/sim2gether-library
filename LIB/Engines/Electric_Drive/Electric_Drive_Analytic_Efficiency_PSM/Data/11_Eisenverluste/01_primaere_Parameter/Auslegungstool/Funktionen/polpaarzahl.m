function [ n_s,p ] = polpaarzahl( f_n,n_n)
f_n=ceil(f_n+1);                            % um Ungenauigkeiten vom Datenblatt aufzurunden
diff=f_n/1-n_n/60;
for k=1:28                                  % 28 ist die maximal zu überprüfende Polpaarzahl
    if and(abs(f_n/k-n_n/60)<=diff,f_n/k-n_n/60>=0)
        p=k;                                % [1]    - Polpaarzahl
        diff=abs(n_n/60-f_n/k);
    end
end
n_s=f_n/p*60;                    % [1/min]       - Synchrondrehzahl

end

