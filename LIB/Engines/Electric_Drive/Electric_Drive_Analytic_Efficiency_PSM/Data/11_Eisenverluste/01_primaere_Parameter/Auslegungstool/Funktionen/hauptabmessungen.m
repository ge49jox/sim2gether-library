function [lambda,D,delta,T_p,l_i,l_fe,l ]...
    = hauptabmessungen(P_n,p,C_m,n_s,Kuehlung,lambda,lambdaschalter )
%% relative Ankerl�nge in 1
if ~lambdaschalter
    if p==1
        lambda=0.650;                            %w�hlbar zwischen 0.6 und 1
    else
        lambda=nthroot(p,3);            % oder w�hlbar zwischen 1 und 3
    end
end
%% Ankerdurchmesser
D = nthroot(P_n*2*p/(C_m*n_s*pi*lambda),3);
%% Mindestluftspalt delta in m
if p==1
    delta=0.4*nthroot(P_n,4);
    delta = round(delta,2)
else
    delta=0.25*nthroot(P_n,4);
    delta = round(delta,2);
end
if delta<0.2
    delta=0.2;
end
%% Polteilung in m
T_p=D*pi/(2*p);
%% ideelle Ankerl�nge und Gesamtl�nge der Blechpakete in m
l_i = lambda*T_p;
l_fe = l_i-(2*delta)/1000;
%% reale Stator- und Ankerl�nge in m (inkl. K�hlkan�le)
if or(strcmp(Kuehlung,'Fluessig')==1,and(strcmp(Kuehlung,'Luft')==1,l_fe<=0.2))
    l=l_fe;
    x=0;
elseif and(strcmp(Kuehlung,'Luft')==1,l_fe>0.2)
    y=l_fe/0.06;
    x=ceil(y)-1;                            % Anzahl der K�hlkan�le
    l=l_fe+0.01*x;
else
    disp('ungueltige K�hlart')
end
end

