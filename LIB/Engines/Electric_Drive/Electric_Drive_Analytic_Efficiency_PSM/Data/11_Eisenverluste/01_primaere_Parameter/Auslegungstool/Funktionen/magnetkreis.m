function [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
    gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
    = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
    N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);

Phi_rS_max=1.05*Phi_delta_tat/2;                             % max. magn. Fluss im Ständerrücke ist 5% höher als im Luftspalt wegen Nutstreufluss
h_rS=Phi_rS_max/(l_fe*phi_fe*B_rS_zul);
Phi_rL_max=Phi_delta_tat/2;                                    % max. magn. Fluss im Läuferrücke
h_rL=Phi_rL_max/(l_fe*phi_fe*B_rL_zul);
%% Außendurchmesser in m
D_a=D+2*(h_nS+h_rS);
%% Rückenquerschnitte A_rS und A_rL in mm^2
A_rS=h_rS*1000*l_fe*1000*phi_fe;
A_rL=h_rL*1000*l_fe*1000*phi_fe;
%% tatsächliche ideelle Ankerlänge und tatsächliche Läufernutteilung in m
l_i_tat=l+2*delta/1000;
T_nL_tat=(D-2*delta/1000)*pi/N_L;
%% Cartersche Faktor
b_sS = b_nS/2;                                           % angenommene Nutschlitzbreite der Statornut in m
b_sL = b_nL/3;                                           % angenommene Nutschlitzbreite der Läufernut in m
gamma_S = 1/(1+5*delta/(b_sS*1000));                          % Hilfsgröße
gamma_L = 1/(1+5*delta/(b_sL*1000));                          % Hilfsgröße
k_cS = T_nS/(T_nS-gamma_S*b_sS);
k_cL = T_nL_tat/(T_nL_tat-gamma_L*b_sL);
k_c = k_cS*k_cL;
%% abgeschätzter Spannungsabfall im Luftspalt
V_delta_ang = B_max*k_c*(delta/1000)/mu_0;             % in A
%% abgeschätzte Zahninduktion in T
% Stator
B_zS_abg=1.03*B_max*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
% Läufer
B_zL_abg=B_max*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);

end

