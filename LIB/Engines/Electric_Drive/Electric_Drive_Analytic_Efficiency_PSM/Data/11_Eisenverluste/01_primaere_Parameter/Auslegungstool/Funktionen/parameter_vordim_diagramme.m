function [k_red,C_m,eta,cphi ]= parameter_vordim_diagramme( plothandle,...
    Material_Stator,Bauweise,Kuehlung,p,P_n,etaschalter,cphischalter,cmschalter,eta,cphi,C_m)
% f�r Polpaarzahl p=1:
x1=[1 10 20 30 40 50 60 70 80 90 100];                             % abgelesene X-Werte
y1=[1.3 1.7 1.79 1.94 2.06 2.17 2.25 2.32 2.39 2.46 2.53];  % abgelesene Y-Werte
n1=length(x1);                                                                % Anzahl der abgelesenen Wertepaare
p1=polyfit(x1,y1,n1-3);                                                    % Ermittlung der Koeffizienten
x1n=1:0.1:100;                                                                 % Aufl�sung des Graphen
f1=polyval(p1,x1n);                                                         % Definition der Polynomfunktion
% f�r Polpaarzahl p=2-6:
x2=[1 10 20 30 40 50 60 70 80 90 100];                            % abgelesene X-Werte
y2=[1.70 2.72 3.07 3.3 3.45 3.6 3.7 3.80 3.87 3.95 4.07];   % abgelesene Y-Werte
n2=length(x2);                                                               % Anzahl der abgelesenen Wertepaare
p2=polyfit(x2,y2,n2-2);                                                   % Ermittlung der Koeffizienten
x2n=1:0.1:100;                                                                % Aufl�sung des Graphen
f2=polyval(p2,x2n);                                                        % Definition der Polynomfunktion
% f�r Polpaarzahl p=8-12:
x3=[10 20 30 40 50 60 70 80 90 100];                              % abgelesene X-Werte
y3=[2.15 2.92 3.38 3.60 3.80 3.90 4.02 4.08 4.13 4.17];    % abgelesene Y-Werte
n3=length(x3);                                                              % Anzahl der abgelesenen Wertepaare
p3=polyfit(x3,y3,n3-2);                                                 % Ermittlung der Koeffizienten
x3n=10:0.1:100;                                                             % Aufl�sung des Graphen
f3=polyval(p3,x3n);                                                       % Definition der Polynomfunktion
% f�r Polpaarzahl p=24-28:
% x4=[5 14 27 41 54 68 81 100];                                    % abgelesene X-Werte
% y4=[1.2 1.95 2.54 2.81 2.95 3.06 3.15 3.27];                % abgelesene Y-Werte
% n4=length(x4);                                                         % Anzahl der abgelesenen Wertepaare
% p4=polyfit(x4,y4,n4-1);                                            % Ermittlung der Koeffizienten
% x4n=1:0.1:100;                                                          % Aufl�sung des Graphen
% f4=polyval(p4,x4n);                                                  % Definition der Polynomfunktion

if strcmp(plothandle,'JA') == 1
    figure(1)
    hold on
    plot(x1n,f1,'r--')
    plot(x1,y1,'r:')
    plot(x1,y1,'ro')
    plot(x2n,f2,'b--')
    plot(x2,y2,'b:')
    plot(x2,y2,'bo')
    plot(x3n,f3,'g--')
    plot(x3,y3,'g:')
    plot(x3,y3,'go')
    %     plot(x4n,f4,'k--')
    %     plot(x4,y4,'k:')
    %     plot(x4,y4,'ko')
    axis([1,100,0,5])
    set(gca,'Xscale','log')
    title('Ausnutzungszahl C_m')
    xlabel('P_m / 2p in kW')
    ylabel('C_m in kWmin / m^3')
    grid on
end
%% Diagramm Nennwirkungsgrad eta (Vogt S.413)
x5=[5 20 40 60 80 100 120 140 160 180 200];                                % abgelesene X-Werte
y5=[82.1 87.8 89.8 90.8 91.5 92 92.33 92.6 92.8 92.933 93];           % abgelesene Y-Werte
n5=length(x5);                                                                            % Anzahl der abgelesenen Wertepaare
p5=polyfit(x5,y5,n5-1);                                                               % Ermittlung der Koeffizienten
x5n=5:0.1:200;                                                                            % Aufl�sung des Graphen
f5=polyval(p5,x5n);                                                                    % Definition der Polynomfunktion
if strcmp(plothandle,'JA') == 1
    figure(2)
    subplot (2,1,1)
    hold on
    plot(x5n,f5,'k-')
    axis([5,200,70,100])
    set(gca,'Xscale','log')
    title('Nennwirkungsgrad \eta_n')
    xlabel('P_m in kW')
    ylabel('\eta_n in %')
    grid on
else
end
clear f5 n5 x5 x5n y5
%% Diagramm cos(phi) (Vogt S.413)
% f�r Polpaarzahl p=1-3
x6=[5 20 40 60 80 100 120 140 160 180 200];                                                  % abgelesene X-Werte
y6=[0.830 0.862 0.871 0.875 0.88 0.882 0.8822 0.8824 0.8826 0.8828 0.883];    % abgelesene Y-Werte
n6=length(x6);                                                                                             % Anzahl der abgelesenen Wertepaare
p6=polyfit(x6,y6,n6-2);                                                                                 % Ermittlung der Koeffizienten
x6n=5:0.1:200;                                                                                              % Aufl�sung des Graphen
f6=polyval(p6,x6n);                                                                                      % Definition der Polynomfunktion
% f�r Polpaarzahl p=4-8
x7=[10 20 40 60 80 100 120 140 160 180 200];                                                % abgelesene X-Werte
y7=[0.742 0.760 0.770 0.775 0.780 0.783 0.785 0.786 0.788 0.789 0.790];         % abgelesene Y-Werte
n7=length(x7);                                                                                             % Anzahl der abgelesenen Wertepaare
p7=polyfit(x7,y7,n7-2);                                                                                 % Ermittlung der Koeffizienten
x7n=10:0.1:200;                                                                                            % Aufl�sung des Graphen
f7=polyval(p7,x7n);                                                                                      % Definition der Polynomfunktion
%
if strcmp(plothandle,'JA') == 1
    subplot (2,1,2)
    hold on
    plot(x6n,f6,'b-')
    plot(x7n,f7,'g-')
    axis([5,200,0.7,1])
    set(gca,'Xscale','log')
    title('Leistungsziffer cos(\phi_n)')
    xlabel('P_m in kW')
    ylabel('cos(\phi_n) in 1')
    grid on
else
end

clear f6 f7 n6 n7 x6 x7 x6n x7n y6 y7
clear f1 f2 f3 f4 n1 n2 n3 n4 x1 x2 x3 x4 x1n x2n x3n x4n y1 y2 y3 y4
%% Ermittlung der Ausnutzungszahl C_m (aus hinterlegtem Diagramm)
% Reduktionsfaktor k_red

if strcmp(Material_Stator,'Aluminium')==1
    k_red=0.75;
elseif strcmp(Material_Stator,'Kupfer')==1
    k_red=1;
else
    disp('ungueltiges Wicklungsmaterial')
end
if strcmp(Bauweise,'offen')==1
    k_red=k_red;
elseif strcmp(Bauweise,'geschlossen')==1
    if strcmp(Kuehlung,'Fluessig')==1
        kred = 1;
    elseif strcmp(Kuehlung,'Luft')==1
        kred = 0.9;
    end
else
    disp('ungueltige Bauweise')
end

if ~cmschalter
    
    if p==1
        C_m=polyval(p1,P_n/(2*p))*k_red;
        if strcmp(plothandle,'JA') == 1
            figure (1)
            plot(P_n/(2*p),C_m,'rx')
        end
    elseif p>=2 && p<=6
        C_m=polyval(p2,P_n/(2*p))*k_red;
        if strcmp(plothandle,'JA') == 1
            plot(P_n/(2*p),C_m,'bx')
        end
    elseif p>=8 && p<=12
        C_m=polyval(p3,P_n/(2*p))*k_red;
        if strcmp(plothandle,'JA') == 1
            plot(P_n/(2*p),C_m,'gx')
        end
    elseif p>=24 && p<=28
        C_m=polyval(p4,P_n/(2*p))*k_red;
        if strcmp(plothandle,'JA') == 1
            plot(P_n/(2*p),C_m,'kx')
        end
    else
        disp('ungueltige Polpaarzahl zur Berechnung der Ausnutzungszahl')
    end
    clear p1 p2 p3 p4

end

%% Ermittlung des Nennwirkungsgrads eta
if ~etaschalter
    eta=polyval(p5,P_n);
    
    if strcmp(plothandle,'JA') == 1
        figure (2)
        subplot (2,1,1)
        plot(P_n,eta,'kx')
    end
    
    clear p5
end

%% Ermittlung des Leistungsfaktors cphi
if ~cphischalter
    if p>=1 && p<=3
        cphi=polyval(p6,P_n);
        if strcmp(plothandle,'JA') == 1
            figure (2)
            subplot (2,1,2)
            plot(P_n,cphi,'bx')
        end
    elseif p>=4 && p<=8
        cphi=polyval(p7,P_n);
        if strcmp(plothandle,'JA') == 1
            plot(P_n,cphi,'gx')
        end
    else
        disp('ungueltige Polpaarzahl zur Berechnung des Leistungsfaktors')
    end
    clear p6 p7
end
end

