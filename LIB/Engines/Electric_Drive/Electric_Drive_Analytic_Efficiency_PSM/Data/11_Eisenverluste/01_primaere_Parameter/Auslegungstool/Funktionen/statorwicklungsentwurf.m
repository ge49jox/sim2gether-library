function [w_s_opt,q_S,z_nS,w_s,N_S,T_nS,w_ges,z_S,q_S_alt,w_s_fehler,W,u,k_ges,y_d,...
    X_v,y_v,y_n,alpha_nS,et_v,et,Z,xi_1_tat,B_max,B_m_tat,B_m_ang,Phi_h,Phi_delta,Phi_delta_tat,Wicklungsart ]...
    = statorwicklungsentwurf(P_n,alpha_p_ang,B_m_max,a,l_i,T_p,E_h,w,D,p,m )
%% Wicklunsart und -faktor abhängig von der Leistung (Vogt. S.96)
if P_n>10
    Wicklungsart='gesehnte Zweischichtwicklung';
    xi_1_ang=0.92;                                   % für gesehnte Zweischichtwicklung
else
    Wicklungsart='Einschichtwicklung';
    xi_1_ang=0.96;                                   % für Einschichtwicklung
end
xi_1_tat=xi_1_ang+0.003;

zaehler_w_s_optimierung=0;
zaehler_B_m_optimierung=0;
zaehler_xi_1_optimierung=0;
while abs(xi_1_ang-xi_1_tat)>0.002
    xi_1_ang=xi_1_tat;
    %% Optimierung auf maximale Luftspaltinduktion in T
    B_m_tat=B_m_max+0.0001;
    for B_m_ang=B_m_max:-0.005:0
        if B_m_tat>B_m_max
            %% magnetischer Luftspaltfluss in Wb
            Phi_delta=B_m_ang*l_i*T_p;
            %% magnetischer Hauptfluss in Wb
            Phi_h=Phi_delta;                            % für Wicklungsentwurf hinreichend genaue Annahme
            %% optimale Strangwindungszahl w_sopt
            w_s_opt=E_h*sqrt(2)/(Phi_h*w*xi_1_ang);
            %% Optimierung der Strangwicklungszahl w_s  bei möglichst kleiner Lochzahl q_s (Herstellungskosten)
            % Grenzen der zu variierenden Größen
            
            w_s_fehlergrenze=0.08;                                          % Grenze der Wicklungszahlabweichung
            w_s_fehler_tat=w_s_fehlergrenze+1;
            T_nS_max=0.045;                                        % maximale Nutteilung in m
            T_nS_min=0.0085;                                       % minimale Nutteilung in m
            if strcmp(Wicklungsart,'Einschichtwicklung')==1
                q_S_min=max(3,ceil(D*pi/(2*T_nS_max*p*m)));          % minimale Lochzahl
                z_nS_min=1;
                z_nS_schrittweite=1;
            elseif strcmp(Wicklungsart,'gesehnte Zweischichtwicklung')==1
                q_S_min=max(3,ceil(D*pi/(2*T_nS_max*p*m)));         % minimale Lochzahl
                z_nS_min=2;
                z_nS_schrittweite=2;
            end
            q_S_max=round(pi*D/(2*m*p*T_nS_min));
            z_nS_max=round(w_s_opt*a/(p*q_S_min));
            if q_S_max < q_S_min
                q_S_max = q_S_min;
            end
            
            q_S_alt=q_S_max;
            
            if z_nS_max < z_nS_min
                z_nS_max = z_nS_min;
            end
            %             q_S_min = 3;
            %             q_S_max = 3;
            % Variation der Lochzahl q_s und der Nutleiterzahl z_n
            for q_S_schleife=q_S_max:-1:q_S_min
                
                for z_n_schleife=z_nS_min:z_nS_schrittweite:z_nS_max
                    w_s_schleife=q_S_schleife*z_n_schleife*p/a;
                    w_s_fehler=abs(w_s_schleife-w_s_opt)/w_s_opt;
                    if or(and(w_s_fehler<=w_s_fehlergrenze,q_S_schleife<q_S_alt),and(w_s_fehler<w_s_fehler_tat,q_S_schleife==q_S_alt))
                        q_S=q_S_schleife;                             % Polzahl je Nut und Strang (Lochzahl)
                        z_nS=z_n_schleife;                            % Leiteranzahl je Nut
                        w_s=w_s_schleife;                             % Strangwindungszahl
                        N_S=2*p*m*q_S;                                % Ständernutzahl
                        T_nS=D*pi/N_S;                                % Nutteilung in m
                        w_ges=w_s*m*a;                                % Gesamtwindungszahl
                        z_S=w_ges*2;                                  % Ständerleiterzahl
                        q_S_alt=q_S_schleife;
                        w_s_fehler_tat=w_s_fehler;
                        zaehler_w_s_optimierung=zaehler_w_s_optimierung+1;
                        
                    end
                    
                end
            end
            
            clear w_s_schleife z_n_schleife
            %% Tatsächlicher Luftspaltfluss und Hauptfluss in Wb
            Phi_h_tat=E_h*sqrt(2)/(w_s*w*xi_1_tat);
            Phi_delta_tat=Phi_h_tat;
            %% Tatsächliche Luftspalltinduktion in T
            B_m_tat=Phi_delta_tat/(l_i*T_p);
            B_max = alpha_p_ang * B_m_tat;                                      % maximale Luftspaltinduktion in T
            zaehler_B_m_optimierung=zaehler_B_m_optimierung+1;
        end
    end
    %%    Wicklungs- oder Nutenschritt y_n in 1
    if strcmp(Wicklungsart,'Einschichtwicklung')==1
        W=T_p;                                 % Spulenweite in m
        u=1/2;                                   % Zahl der auf eine Nut entfallende Spulen
    elseif strcmp(Wicklungsart,'gesehnte Zweischichtwicklung')==1
        W=5/6*T_p;                          % Spulenweite in m (bei oft verwendete 5/6 Sehnung)
        u=1;                                      % Zahl der auf eine Nut entfallende Spulen
    end
    k_ges=N_S*u;                              % Gesamtspulenanzahl
    y_d=N_S/(2*p);                     % Durchmesserschritt in 1
    X_v=T_p-W;                          % Spulenverkürzung X_v in m
    y_v=X_v/T_nS;                        % Nutenschrittverkürzung in 1
    y_n=y_d-y_v;
    alpha_nS=360*p/N_S;              % Nutenwinkel in Grad
    et_v=alpha_nS*y_v;                 % Sehnungswinkel in Grad
    et=180-et_v;                          % Spulenweite in bezogenen Koordinaten in Grad
    %% Wicklungsfaktor xi_1_tat in 1
    Z=q_S;
    xi_1_d=sin(1*pi/2);                                                                                         % Spulenfaktor der Durchmesserwicklung
    xi_1_gr=sin(1*(90*pi/180)/m)/(q_S*sin(1*(90*pi/180)/(m*q_S)));                     % Gruppenfaktor
    if strcmp(Wicklungsart,'Einschichtwicklung')==1
        xi_1_tat=xi_1_d*sin(Z*1*(alpha_nS*pi/180)/2)/(Z*sin(1*(alpha_nS*pi/180)/2));
    elseif strcmp(Wicklungsart,'gesehnte Zweischichtwicklung')==1
        xi_1_tat=xi_1_d*xi_1_gr*cos(1*(et_v*pi/180)/2);
    end
    zaehler_xi_1_optimierung=zaehler_xi_1_optimierung+1;
end


end

