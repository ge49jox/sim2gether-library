% In diesem Skript werden die Parameter zur Berechnung der Eisenverluste
% initialisiert. Das kann entweder manuell geschehen oder automatisiert.
% Für die manuelle Eingabe müssen im Abschnitt "Manuelle Eingabe" sämtliche
% angeführten Faktoren ausgefüllt werden. Falls dies nicht gewünscht ist,
% einfach diese Werte auskommentiert lassen und die Berechnung erfolgt
% automatisch im Abschnitt "Berechnung der Parameter"


%% Berechnung der Parameter

f_n=(n_n*par.prim.p)/60;

hauptprogramm_auslegung_SM;






%% Manuelle Eingabe
%D_a = 0.1588;           % Außendurchmesser des Stators in m
%D = 0.1000;             % Bohrungsdurchmesser des Stators in m
%D_L = 0.1951;           % Außendurchmesser des Läufers in m
%delta = 0.5400;         % Höhe des Luftspalts in mm
%N_S = 36;               % Anzahl der Statornuten in 1
%N_L = 28;               % Anzahl der Läufernuten in 1
%h_nS = 0.0114;          % Statornuthöhe in m
%h_nL = 0.0150;          % Läufernuthöhe in m
%A_nS = 90.1105;         % Nutquerschnittsfläche im Stator in mm^2
%A_nL = 51.1622;         % Nutquerschnittsfläche im Läufer in mm^2
%A_rS = 2.1247e+03;      % Statorrückenfläche in mm^2
%A_rL = 2.6416e+03;      % Läuferrückenfläche in mm^2
%B_rS_max = 1.7;         % maximal auftretende Ständerrückeninduktion in T
%B_rL_max = 1.3;         % maximal auftretende Läuferrückeninduktion in T
%B_zS_tat = 1.8053;      % tatsächliche Zahninduktion im Stator in T
%B_zL_tat = 1.6768;      % tatsächliche Zahninduktion im Läufer in T
%h_rS = 0.0144;          % Ständerrückenhöhe in m
%h_rL = 0.0180;          % Läuferrückenhöhe in m
%l_Fe = 0.1550;          % Länge des Blechpakets in m

%M_n = 40.0161;          % Nennmoment in Nm
%n_n = 3820;             % Nenndrehzahl in 1/min
%p = 2;                  % Polpaarzahl in 1