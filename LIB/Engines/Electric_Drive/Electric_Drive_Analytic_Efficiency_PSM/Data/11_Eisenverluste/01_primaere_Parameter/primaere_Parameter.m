%% Prim�re Parameter
% Dieses Skript dient zur Definition aller Parameter, die f�r die sp�tere
% Berechnung der Eisenverluste notwendig sind.
% Es werden durchgehend die folgenden Indizes verwendet:
    % r - R�cken
    % z - Zahn
    % S - Stator
    % L - L�ufer
% B_rL bezeichnet bspw. die auftretende magn. Flussdichte im Statorr�cken.


%% 1. Bekannte Parameter aus der ASM-Auslegung:
% Im Folgenden werden alle notwendigen Parameter aus dem Tool zur
% Motorauslegung �bernommen. Diese werden ben�tigt, um sp�ter �ber die 
% Flussdichte und das in den sekund�ren Parametern bestimmte Volumen
% aus den spezifischen Eisenverlusten die tats�chlichen Verluste zu 
% berechnen

%D_a = 0.1588;           % Au�endurchmesser des Stators in m
%D = 0.1000;             % Bohrungsdurchmesser des Stators in m
%delta = 0.5400;         % H�he des Luftspalts in mm
%N_S = 36;               % Anzahl der Statornuten in 1
%N_L = 28;               % Anzahl der L�ufernuten in 1
%h_nS = 0.0150;          % Statornuth�he in m
%h_nL = 0.0114;          % L�ufernuth�he in m
%A_nS = 90.1105;         % Nutquerschnittsfl�che im Stator in mm^2
%A_nL = 51.1622;         % Nutquerschnittsfl�che im L�ufer in mm^2
%A_rS = 2.1247e+03;      % Statorr�ckenfl�che in mm^2
%A_rL = 2.6416e+03;      % L�uferr�ckenfl�che in mm^2
%B_rS_max = 1.7;         % maximal auftretende St�nderr�ckeninduktion in T
%B_rL_max = 1.3;         % maximal auftretende L�uferr�ckeninduktion in T
%B_zS_tat = 1.8053;      % tats�chliche Zahninduktion im Stator in T
%B_zL_tat = 1.6768;      % tats�chliche Zahninduktion im L�ufer in T
%h_rS = 0.0144;          % St�nderr�ckenh�he in m
%h_rL = 0.0180;          % L�uferr�ckenh�he in m
%l_Fe = 0.1550;          % L�nge des Blechpakets in m

%M_n = 40.0161;          % Nennmoment in Nm
%n_n = 5250;             % Nenndrehzahl in 1/min
%p = 2;                  % Polpaarzahl in 1


%% 2. Definition von Bearbeitungszuschl�gen (und Fitparametern): 
k_bh = 1.5;     %Bearbeitungszuschlag f�r Hystereseverluste in 1
k_bw = 1.5;     %Bearbeitungszuschlag f�r Wirbelstromverluste in 1

% Die Fitparameter k und C m�ssen f�r die gew�nschte Flussdichte jeweils
% an die gemessenen Verluste angepasst werden. Dies erfolgt automatisiert 
% im Skript "Fitparameter.m"
% k bezeichnet hierbei den Fitfaktor k f�r Hystereseverluste in 1, C den
% Faktor C f�r Excessverluste in 1