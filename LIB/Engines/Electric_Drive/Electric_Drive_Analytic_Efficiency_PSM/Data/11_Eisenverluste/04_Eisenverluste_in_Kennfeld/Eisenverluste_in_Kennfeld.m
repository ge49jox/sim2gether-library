
% Dieses Skript erstellt eine Matrix die genau die selbe Anzahl an Zeilen
% und Spalten hat, wie die dazugehörige P_el Matrix. Dadurch kann diese
% Matrix einfach zu den elektrischen Leistungen addiert werden. 



Laenge_Omega=tics_omega ; 
Laenge_M=tics_M;
n_Ev_kons = n_n;
tics_EV_konst = floor(tics_omega/3.5);

P_Fe_Matrix=zeros(Laenge_Omega,Laenge_M);
%% Uebertrag in Matrix
for n=1:tics_M
    
   P_Fe_Matrix(:,n)=P_Fe;
   
end
%% annähernd konst Eisenverluste plus leichter Abfall in der Feldschwächung
for n=tics_EV_konst+1:tics_omega
P_Fe_Matrix(n,:)=P_Fe_Matrix(tics_EV_konst,1)*0.99^(n-tics_EV_konst);
end




P_Fe_Matrix = 0.4*P_Fe_Matrix;


