%% In diesem Skript erfolgt die Ermittlung der sekund�ren Parameter
% aus den prim�ren Parametern. Es erfolgt die Berechnung des Volumens
% der jeweiligen Bereiche (Joch, Zahn) f�r Stator und L�ufer

D_L = D - 2*delta/1000;             % Au�endurchmesser des L�ufers in m
D_welle = D_L - 2*(h_nL+h_rL);      % Wellendurchmesser in m

%% Berechnung der Zahnfl�che f�r Stator und L�ufer:
A_zS = pi/4*((1000*D_a)^2 - (1000*D)^2) - A_rS - N_S*A_nS;        % Zahnfl�che Stator in mm^2 
A_zL = pi/4*((1000*D_L)^2 - (1000*D_welle)^2) - A_rL - N_L*A_nL;  % Zahnfl�che L�ufer in mm^2

%% Berechnung von R�cken- und Zahnvolumen f�r Stator und L�ufer:
V_zS = A_zS * 1e-06 * l_Fe;     % Zahnvolumen Stator in m^3
V_zL = A_zL * 1e-06 * l_Fe;     % Zahnvolumen L�ufer in m^3
V_rS = A_rS * 1e-06 * l_Fe;     % R�ckenvolumen Stator in m^3
V_rL = A_rL * 1e-06 * l_Fe;     % R�ckenvolumen L�ufer in m^3

n = 2*pi*f/p; % Berechnung der jeweiligen Drehzahl in 1/min aus Frequenz und Polpaarzahl