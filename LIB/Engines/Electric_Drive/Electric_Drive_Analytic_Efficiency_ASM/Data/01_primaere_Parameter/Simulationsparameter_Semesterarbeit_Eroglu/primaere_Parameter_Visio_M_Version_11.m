%% Dieses Skript legt die prim�ren RWTH-Parameter fest, und zwar:
    % prim�re ASM-Parameter
    % Schrittweiten
    % Fahrzeug-Parameter
% "prim�r" bedeutet, dass ALLE weiteren Berechnungen, Simulationsdurchl�ufe
% etc. aus den in diesem Skript initialisierten Parametern abgeleitet
% werden k�nnen.


%% Wichtige Hinweise bzgl. prim�re ASM-Parameter:

% Im Folgenden werden die prim�ren ASM-Parameter initialisiert.
% "prim�r" bedeutet, dass sich ALLE weiteren ASM-Gr��en,
    % wie z. B. Nennrotofrequenz w_2N, Nennmoment M_N,
    % Nennwinkelgeschwindigkeit W_N, Nennleistung P_mechN, Kurven,
    % Kennfelder etc. 
% aus den prim�ren ASM-Parametern berechnen lassen!
    
% Die Berechnung aller weiteren Gr��en wird in separaten Skripten vollzogen
% werden. Es sei darauf hingewiesen, dass durch Modellungenauigkeiten die
% berechneten Werte von den tats�chlich gemessenen Werten abweichen k�nnen.
    
% Alle Gr��en sind in SI-Einheiten anzugeben!
    % (Kreis-) Frequenzen w:    rad/s
        % Ein kleines w steht f�r ein kleines omega.
        % Es gilt w = 2*pi*f mit der Frequenz f.
        % SI-Einheit von f: 1/s = Hz
    % Winkelgeschwindigkeiten W: rad/s (NICHT in rpm bzw. 1/min!)
        % Ein gro�es W steht f�r ein gro�es Omega.
        % Es gilt W = 2*pi*n mit der Drehzahl n.
        % SI-Einheit von n: 1/s, nicht 1/min!
    % Str�me I:                      A
    % Spannungen U:                  V
    % Flussverkettungen Psi:       V*s
    % Widerst�nde R:               V/A = Ohm
    % Induktivit�ten L:          V*s/A = Ohm*s
    
% Alle Gr��en, die von diesem Programm berechnet werden, werden ebenfalls
    % in SI-Einheiten angegeben! Z. B.:
    % Drehmomente M:             V*A*s = N*m
    % Leistungen P:                V*A = W
    
% Index 1: Stator
% Index 2: Rotor
% Index N: Nenn-

% Alle Rotorgr��en sind so anzugeben, dass sie bereits auf die Statorseite
    % umgerechnet worden sind!
% Alle Widerst�nde und Induktivit�ten sind als strangbezogen Werte
    % anzugeben!
% Alle Str�me, Spannungen und Flussverkettungen sind als strangbezogene 
    % Effektivwerte anzugeben!

% Es sei ausdr�cklich darauf hingewiesen, dass s�mtliche Matlab-Skripte,
    % Matlab-Funktionen, Simulink-Modelle etc. auf den Betrieb mit
    % variabler Spannung und variabler Frequenz ausgerichtet sind.
    % Viele gewohnte Gleichungen und Betrachtungsweisen, welche auf dem
    % Betrieb am starren Netz basieren, sind daher hier nicht oder nur
    % bedingt �bertragbar!
    

%% Quelle aller folgenden prim�ren ASM-Parameter (bis auf W_max):
    % http://www1.iem.rwth-aachen.de/uploads/805/1834/ET_Prakt_II_V5.pdf
    % Seite 16   
    % Rheinisch-Westf�lische Technische Hochschule Aachen => RWTH
    
%% 7 Maschinenkonstanten:

       m = 3;                 % Strangzahl, fast immer 3
       p = 2;                 % Polpaarzahl
     R_1 = 0.0926;              % Statorwiderstand
     R_2 = 0.1037;              % Rotorwiderstand
      gamma_s= 1.1;               %S�ttigungsfaktor (zwischen 1-3 w�hlen)
     L_h = 5.8/1000;         % Hauptinduktivit�t
L_sigma1 =  gamma_s*2.1583/10000;         % Statorstreuinduktivit�t
L_sigma2 =  gamma_s*2.3618/10000;         % Rotorstreuinduktivit�t


%% 4 Betriebsgrenzen:

I_1N = 41.4508; % Nennstrom in einem Statorstrang;
    % Er sei der h�chste Strom, welcher in einem Statorstrang thermisch
    % dauerhaft ertragen werden kann.
    % Sei I_1LN der h�chstm�gliche Strom in einem
        % Stator-Anschlussleiter bei Strangzahl m = 3.
    % Bei Stern-Schaltung gilt:   I_1N = I_1LN;
    % Bei Dreieck-Schaltung gilt: I_1N = I_1LN/sqrt(3);
    
U_1N = 217/sqrt(3); % Nennspannung in einem Statorstrang;
    % Sie sei die h�chste Spannung, welche von der Spannungsquelle auf
        % einen (!) Statorstrang geschaltet werden kann.
    % Sei U_1LLN die h�chstm�gliche Spannung zwischen zwei
        % Stator-Anschlussleitern bei Strangzahl m = 3.
    % Bei Stern-Schaltung gilt:   U_1N = U_1LLN/sqrt(3);
    % Bei Dreieck-Schaltung gilt: U_1N = U_1LLN;    
    
Psi_1N = 0.12708; % Nennstatorflussverkettung (wie immer strangbezogen!);
    % Sie sei die h�chstm�gliche Flussverkettung, bevor der magnetische
    % Kreis im Stator der ASM in S�ttigung ger�t.
    % Dieser Wert ist unabh�ngig von der Verschaltungsart
    % (Stern oder Dreieck)!
    % Oft ist Psi_1N nicht direkt angegeben. In diesem Fall
    % l�sst sich Psi_1N berechnen �ber Psi_1N = U_1N/w_1N,
    % wobei w_1N = 2*pi*f_1N die Nennstatorfrequenz ist.
    % Am starren Netz in Deutschland ist f_1N = 50 Hz.

W_max = 1250; % h�chste (z. B. aus mechanischen Gr�nden) zul�ssige
    % Winkelgeschwindigkeit; Sie wird in der Regel durch die Fliekr�fte
    % begrenzt. Achtung: Wie immer in SI-Einheit anzugeben, d. h. in rad/s!    

    
    
%% Wichtige Hinweise bzgl. Schrittweiten:

% Alle Gr��en sind in SI-Einheiten anzugeben!

% Im Folgenden werden die Schrittweiten festgelegt.
% Je kleiner die Schrittweite, desto genauer werden die Kurven, Kennfelder,
% Plots, Simulationsberechnungen etc.
% Allerdings steigt dadurch auch der Rechenaufwand.
% Hier muss ein sinnvoller Kompromiss gefunden werden.

% Die Schrittweiten f�r Winkelgeschwindigkeiten W und f�r Drehmomente M
% sollten sich an der maximalen Winkelgeschwindigkeit W_max und am
% Nennmoment M_N orientieren.
% Die Schrittweite f�r die Rotorfrquenz w_2 sollte sich an der Statorfluss-
% Kipprotorfrequenz w_2Psi1k orientieren.


%% Schrittweiten initialisieren:
    
W_Schrittweite = 2; % in rad/s!
M_Schrittweite = 1; % in Nm!

w2_Schrittweite = 1; % in rad/s! Schrittweite der Rotorfrequenz w_2
    % f�r das Rotorfrequenzdiagramm RFD;

t_Schrittweite = 0.1; % Schrittweite f�r die Simulation in Simulink;
% XY-Graph in Simulink ergab: 0.5 Sekunden sind ein guter Kompromiss.
% Bei 1 Sekunde ist der Graph sehr grob.
% Bei 0.25 Sekunden ist der Graph nicht wesentlich genauer als bei
% 0.5 Sekunden.
% ABER: Auch wenn sich der XY-Graph kaum noch �ndert, muss bedacht werden,
    % dass die numerische Differentiation f�r 0.5 Sekunden immer noch
    % relativ ungenau sein kann. => W�hle sicherheitshalber 0.1 Sekunden!

% NEDC: Die Schrittweiten variieren sehr stark. Viele Zeitwerte liegen
    % nicht auf ganzzahligen Sekundenwerten. Zum Teil gibt es auch
    % Schrittweiten unter 1 Sekunde.   
% CADC: Die Schrittweite betr�gt ausnahmslos 1 Sekunde.
% FTP72: Die Schrittweite betr�gt fast immer 1 Sekunde. Nur an wenigen
    % Stellen ist sie kleiner.
    
% Auch wenn in Simulink die Datenwerte linear interpoliert werden:
% Gr��er als 1 Sekunde sollte t_Schrittweite keinesfalls sein.
% Hier muss ein guter Kompromiss gefunden werden zwischen Genauigkeit
% und Rechenaufwand.



%% Wichtige Hinweise bzgl. Fahrzeug-Parameter:

% Die folgenden Fahrzeug-Parameter sind auf die jeweilige ASM abgestimmt
% worden, sodass diese ASM alle 6 Fahrzyklen
    % 1) NEDC bzw. NEFZ
    % 2) UDC
    % 3) EUDC
    % 4) CADC_urban
    % 5) CADC_rural
    % 6) FTP72
% bew�ltigen kann.  
    
% Alle Gr��en sind in SI-Einheiten anzugeben!


%% Fahrzeug-Parameter initialisieren:

       g = 9.81; % Erdbeschleunigung (in m/s^2 = N/kg);
rho_Luft = 1.2; % Luftdichte bei 20�C, (in kg/m^3);
   r_Rad = 0.3; % Radradius (in m);
   
     c_w = 0.24; % Luftwiderstandskoeffizient (dimensionslos);  
  c_Roll = 0.01; % Rollwiderstandskoeffizient (dimensionslos);
lambda_m = 1.05; % Drehmassenzuschlagsfaktor (dimensionslos);

i_ges = 9.2;   % Gesamtgetriebe�bersetzung;
m_Fzg = 650;   % Fahrzeugmasse, inkl. Fahrer;
A_Stirn = 1.2; % Stirnfl�che;
    
m_Fzg_eff = m_Fzg*lambda_m; % effektive Fahrzeugmasse; Sie ber�cksichtigt
    % auch die Tr�gheit aller rotierenden Massen.
    
%% 1. Bekannte Parameter aus der ASM-Auslegung:
% Im Folgenden werden alle notwendigen Parameter aus dem Tool zur
% Motorauslegung �bernommen. Diese werden ben�tigt, um sp�ter �ber die 
% Flussdichte und das in den sekund�ren Parametern bestimmte Volumen
% aus den spezifischen Eisenverlusten die tats�chlichen Verluste zu 
% berechnen

D_a = 0.1405;           % Au�endurchmesser des Stators in m
D = 0.0883;             % Bohrungsdurchmesser des Stators in m
%D_L = 0.1951;           % Au�endurchmesser des L�ufers in m
delta = 0.46;         % H�he des Luftspalts in mm
N_S = 36;               % Anzahl der Statornuten in 1
N_L = 28;               % Anzahl der L�ufernuten in 1
h_nS = 0.013;          % Statornuth�he in m
h_nL = 0.0103;          % L�ufernuth�he in m
A_nS = 67.5829;         % Nutquerschnittsfl�che im Stator in mm^2
A_nL = 38.3716;         % Nutquerschnittsfl�che im L�ufer in mm^2
A_rS = 1.9527e+03;      % Statorr�ckenfl�che in mm^2
A_rL = 2.432e+03;      % L�uferr�ckenfl�che in mm^2
B_rS_max = 1.7;         % maximal auftretende St�nderr�ckeninduktion in T
B_rL_max = 1.3;         % maximal auftretende L�uferr�ckeninduktion in T
B_zS_tat = 1.8125;      % tats�chliche Zahninduktion im Stator in T
B_zL_tat = 1.6515;      % tats�chliche Zahninduktion im L�ufer in T
h_rS = 0.0131;          % St�nderr�ckenh�he in m
h_rL = 0.0163;          % L�uferr�ckenh�he in m
l_Fe = 0.1568;          % L�nge des Blechpakets in m

M_n = 27.498;          % Nennmoment in Nm
n_n = 3820;             % Nenndrehzahl in 1/min
p = 2;                  % Polpaarzahl in 1


%% 2. Definition von Bearbeitungszuschl�gen und Fitparametern:
k = 1;          %Faktor k f�r Hystereseverluste in 1
C = 1;          %Faktor C f�r Excessverluste in 1
k_bh = 1.5;     %Bearbeitungszuschlag f�r Hystereseverluste in 1
k_bw = 1.5;     %Bearbeitungszuschlag f�r Wirbelstromverluste in 1

% Die Fitparameter k und C m�ssen jeweils manuell f�r die gew�nschte
% Flussdichte an die gemessenen Verluste angepasst werden. Die jeweiligen
% Fits sind im Ordner 04_Fitparameter zu finden.
% [...]

% %% 4. Induzieren der Vektoren f�r Flussdichte und Frequenz (Interpolieren)
% B = [0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9];
% f = [50,60,100,200,400,500];

% p = 2;      %Polpaarzahl
% n = 60/p*f; %Berechnung der Drehzahl in 1/min aus Frequenz und Polpaarzahl


