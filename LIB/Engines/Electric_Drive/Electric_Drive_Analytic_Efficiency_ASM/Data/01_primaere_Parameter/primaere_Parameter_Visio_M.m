%% Dieses Skript legt die prim�ren Parameter des Fahrzeugs fest.
    
% Neben den Fahrzeugparametern wird in diesem Skript auch die Schrittweite
% der Vektoren festgelegt. Die Schrittweite legt ma�geblich die Rechenzeit
% fest.

%% Fahrzeug-Parameter initialisieren:

       g = 9.81; % Erdbeschleunigung (in m/s^2 = N/kg);
rho_Luft = 1.2;  % Luftdichte bei 20�C, (in kg/m^3);
   r_Rad = 0.3;  % Radradius (in m);
   
     c_w = 0.24; % Luftwiderstandskoeffizient (dimensionslos);  
  c_Roll = 0.01; % Rollwiderstandskoeffizient (dimensionslos);
lambda_m = 1.05; % Drehmassenzuschlagsfaktor (dimensionslos);

i_ges = 9.2;   % Gesamtgetriebe�bersetzung;
m_Fzg = 650;   % Fahrzeugmasse, inkl. Fahrer;
A_Stirn = 1.2; % Stirnfl�che;
    
m_Fzg_eff = m_Fzg*lambda_m; % effektive Fahrzeugmasse; Sie ber�cksichtigt
    % auch die Tr�gheit aller rotierenden Massen.
    
    
%% Wichtige Hinweise bzgl. Schrittweiten:

% Alle Gr��en sind in SI-Einheiten anzugeben!

% Im Folgenden werden die Schrittweiten festgelegt.
% Je kleiner die Schrittweite, desto genauer werden die Kurven, Kennfelder,
% Plots, Simulationsberechnungen etc.
% Allerdings steigt dadurch auch der Rechenaufwand.
% Hier muss ein sinnvoller Kompromiss gefunden werden.

% Die Schrittweiten f�r Winkelgeschwindigkeiten W und f�r Drehmomente M
% sollten sich an der maximalen Winkelgeschwindigkeit W_max und am
% Nennmoment M_N orientieren.
% Die Schrittweite f�r die Rotorfrquenz w_2 sollte sich an der Statorfluss-
% Kipprotorfrequenz w_2Psi1k orientieren.


%% Schrittweiten initialisieren:
    
W_Schrittweite = 5; % in rad/s! LH Erst mal nicht �NDERN
M_Schrittweite = 2; % in Nm!

w2_Schrittweite = 2; % in rad/s! Schrittweite der Rotorfrequenz w_2
    % f�r das Rotorfrequenzdiagramm RFD;

t_Schrittweite = 0.1; % Schrittweite f�r die Simulation in Simulink;
% XY-Graph in Simulink ergab: 0.5 Sekunden sind ein guter Kompromiss.
% Bei 1 Sekunde ist der Graph sehr grob.
% Bei 0.25 Sekunden ist der Graph nicht wesentlich genauer als bei
% 0.5 Sekunden.
% ABER: Auch wenn sich der XY-Graph kaum noch �ndert, muss bedacht werden,
    % dass die numerische Differentiation f�r 0.5 Sekunden immer noch
    % relativ ungenau sein kann. => W�hle sicherheitshalber 0.1 Sekunden!

% NEDC: Die Schrittweiten variieren sehr stark. Viele Zeitwerte liegen
    % nicht auf ganzzahligen Sekundenwerten. Zum Teil gibt es auch
    % Schrittweiten unter 1 Sekunde.   
% CADC: Die Schrittweite betr�gt ausnahmslos 1 Sekunde.
% FTP72: Die Schrittweite betr�gt fast immer 1 Sekunde. Nur an wenigen
    % Stellen ist sie kleiner.
    
% Auch wenn in Simulink die Datenwerte linear interpoliert werden:
% Gr��er als 1 Sekunde sollte t_Schrittweite keinesfalls sein.
% Hier muss ein guter Kompromiss gefunden werden zwischen Genauigkeit
% und Rechenaufwand.


%% H�ndisches initialisieren von Maschinenparameteren

% Sofern Sie Parameter aus einem Datenblatt eintragen wollen und nicht mit
% Auslegungstool berechnet werden sollen, k�nnen Sie hier diese Parameter
% initialisieren. Daf�r m�ssen im "ASM_Kennfeld_Tool" die Bereiche
% "Maschinendaten der ASM" und "Maschinenauslegung-Berechnung"
% auskommentiert werden. Anschlie�end k�nnen Sie die Daten direkt hier
% eintragen.



%% Wichtige Hinweise bzgl. prim�re ASM-Parameter:

% Im Folgenden werden die prim�ren ASM-Parameter initialisiert.
% "prim�r" bedeutet, dass sich ALLE weiteren ASM-Gr��en,
    % wie z. B. Nennrotofrequenz w_2N, Nennmoment M_N,
    % Nennwinkelgeschwindigkeit W_N, Nennleistung P_mechN, Kurven,
    % Kennfelder etc. 
% aus den prim�ren ASM-Parametern berechnen lassen!
    
% Die Berechnung aller weiteren Gr��en wird in separaten Skripten vollzogen
% werden. Es sei darauf hingewiesen, dass durch Modellungenauigkeiten die
% berechneten Werte von den tats�chlich gemessenen Werten abweichen k�nnen.
    
% Alle Gr��en sind in SI-Einheiten anzugeben!
    % (Kreis-) Frequenzen w:    rad/s
        % Ein kleines w steht f�r ein kleines omega.
        % Es gilt w = 2*pi*f mit der Frequenz f.
        % SI-Einheit von f: 1/s = Hz
    % Winkelgeschwindigkeiten W: rad/s (NICHT in rpm bzw. 1/min!)
        % Ein gro�es W steht f�r ein gro�es Omega.
        % Es gilt W = 2*pi*n mit der Drehzahl n.
        % SI-Einheit von n: 1/s, nicht 1/min!
    % Str�me I:                      A
    % Spannungen U:                  V
    % Flussverkettungen Psi:       V*s
    % Widerst�nde R:               V/A = Ohm
    % Induktivit�ten L:          V*s/A = Ohm*s
    
% Alle Gr��en, die von diesem Programm berechnet werden, werden ebenfalls
    % in SI-Einheiten angegeben! Z. B.:
    % Drehmomente M:             V*A*s = N*m
    % Leistungen P:                V*A = W
    
% Index 1: Stator
% Index 2: Rotor
% Index N: Nenn-

% Alle Rotorgr��en sind so anzugeben, dass sie bereits auf die Statorseite
    % umgerechnet worden sind!
% Alle Widerst�nde und Induktivit�ten sind als strangbezogen Werte
    % anzugeben!
% Alle Str�me, Spannungen und Flussverkettungen sind als strangbezogene 
    % Effektivwerte anzugeben!

% Es sei ausdr�cklich darauf hingewiesen, dass s�mtliche Matlab-Skripte,
    % Matlab-Funktionen, Simulink-Modelle etc. auf den Betrieb mit
    % variabler Spannung und variabler Frequenz ausgerichtet sind.
    % Viele gewohnte Gleichungen und Betrachtungsweisen, welche auf dem
    % Betrieb am starren Netz basieren, sind daher hier nicht oder nur
    % bedingt �bertragbar!
    


    
%% 7 Maschinenkonstanten:

%       m = 3;                 % Strangzahl, fast immer 3
%       p = 2;                 % Polpaarzahl
%     R_1 = 0.04;              % Statorwiderstand
%     R_2 = 0.02;              % Rotorwiderstand
%    gamma_s= 1.8;               %S�ttigungsfaktor (zwischen 1,5-3 w�hlen)
%     L_h = 2.27/1000;         % Hauptinduktivit�t
%L_sigma1 = gamma_s*0.08/1000;         % Statorstreuinduktivit�t
%L_sigma2 = gamma_s*0.20/1000;         % Rotorstreuinduktivit�t


%% 4 Betriebsgrenzen:

%I_1N = 81.97; % Nennstrom in einem Statorstrang;
    % Er sei der h�chste Strom, welcher in einem Statorstrang thermisch
    % dauerhaft ertragen werden kann.
    % Sei I_1LN der h�chstm�gliche Strom in einem
        % Stator-Anschlussleiter bei Strangzahl m = 3.
    % Bei Stern-Schaltung gilt:   I_1N = I_1LN;
    % Bei Dreieck-Schaltung gilt: I_1N = I_1LN/sqrt(3);
    
%U_1N = 217/sqrt(3); % Nennspannung in einem Statorstrang;
    % Sie sei die h�chste Spannung, welche von der Spannungsquelle auf
        % einen (!) Statorstrang geschaltet werden kann.
    % Sei U_1LLN die h�chstm�gliche Spannung zwischen zwei
        % Stator-Anschlussleitern bei Strangzahl m = 3.
    % Bei Stern-Schaltung gilt:   U_1N = U_1LLN/sqrt(3);
    % Bei Dreieck-Schaltung gilt: U_1N = U_1LLN;    
    
%Psi_1N = 0.15708; % Nennstatorflussverkettung (wie immer strangbezogen!);
    % Sie sei die h�chstm�gliche Flussverkettung, bevor der magnetische
    % Kreis im Stator der ASM in S�ttigung ger�t.
    % Dieser Wert ist unabh�ngig von der Verschaltungsart
    % (Stern oder Dreieck)!
    % Oft ist Psi_1N nicht direkt angegeben. In diesem Fall
    % l�sst sich Psi_1N berechnen �ber Psi_1N = U_1N/w_1N,
    % wobei w_1N = 2*pi*f_1N die Nennstatorfrequenz ist.
    % Am starren Netz in Deutschland ist f_1N = 50 Hz.

%W_max = 1250; % h�chste (z. B. aus mechanischen Gr�nden) zul�ssige
    % Winkelgeschwindigkeit; Sie wird in der Regel durch die Fliekr�fte
    % begrenzt. Achtung: Wie immer in SI-Einheit anzugeben, d. h. in rad/s!    

    
    



    
%% Prim�re Parameter f�r die Eisenverlustberechnung
% Dieses Skript dient zur Definition aller Parameter, die f�r die sp�tere
% Berechnung der Eisenverluste notwendig sind.
% Es werden durchgehend die folgenden Indizes verwendet:
    % r - R�cken
    % z - Zahn
    % S - Stator
    % L - L�ufer
% B_rL bezeichnet bspw. die auftretende magn. Flussdichte im Statorr�cken

%% 1. Bekannte Parameter aus der ASM-Auslegung:
% Im Folgenden werden alle notwendigen Parameter aus dem Tool zur
% Motorauslegung �bernommen. Diese werden ben�tigt, um sp�ter �ber die 
% Flussdichte und das in den sekund�ren Parametern bestimmte Volumen
% aus den spezifischen Eisenverlusten die tats�chlichen Verluste zu 
% berechnen

%D_a = 0.1588;           % Au�endurchmesser des Stators in m
%D = 0.1000;             % Bohrungsdurchmesser des Stators in m
%D_L = 0.1951;           % Au�endurchmesser des L�ufers in m
%delta = 0.5400;         % H�he des Luftspalts in mm
%N_S = 36;               % Anzahl der Statornuten in 1
%N_L = 28;               % Anzahl der L�ufernuten in 1
%h_nS = 0.0114;          % Statornuth�he in m
%h_nL = 0.0150;          % L�ufernuth�he in m
%A_nS = 90.1105;         % Nutquerschnittsfl�che im Stator in mm^2
%A_nL = 51.1622;         % Nutquerschnittsfl�che im L�ufer in mm^2
%A_rS = 2.1247e+03;      % Statorr�ckenfl�che in mm^2
%A_rL = 2.6416e+03;      % L�uferr�ckenfl�che in mm^2
%B_rS_max = 1.7;         % maximal auftretende St�nderr�ckeninduktion in T
%B_rL_max = 1.3;         % maximal auftretende L�uferr�ckeninduktion in T
%B_zS_tat = 1.8053;      % tats�chliche Zahninduktion im Stator in T
%B_zL_tat = 1.6768;      % tats�chliche Zahninduktion im L�ufer in T
%h_rS = 0.0144;          % St�nderr�ckenh�he in m
%h_rL = 0.0180;          % L�uferr�ckenh�he in m
%l_Fe = 0.1550;          % L�nge des Blechpakets in m

%M_n = 40.0161;          % Nennmoment in Nm
%n_n = 3820;             % Nenndrehzahl in 1/min
%p = 2;                  % Polpaarzahl in 1


%% 2. Definition von Bearbeitungszuschl�gen und Fitparametern:
%k = 1;          %Faktor k f�r Hystereseverluste in 1
%C = 1;          %Faktor C f�r Excessverluste in 1
%k_bh = 1.5;     %Bearbeitungszuschlag f�r Hystereseverluste in 1
%k_bw = 1.5;     %Bearbeitungszuschlag f�r Wirbelstromverluste in 1

% Die Fitparameter k und C m�ssen jeweils manuell f�r die gew�nschte
% Flussdichte an die gemessenen Verluste angepasst werden. Die jeweiligen
% Fits sind im Ordner 04_Fitparameter zu finden.
% [...]


