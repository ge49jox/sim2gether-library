function [NewBuild] = CheckIfNewEffMapHorlbeckASMisrequired (P_n,p, U_n, m_st, n_n, n_max,Material_Stator,Material_Laeufer,Schaltung,Bauweise,Kuehlung,Betriebsstrategie)
 

persistent P_n_Old;
persistent p_Old;
persistent U_n_Old;
persistent m_st_Old;
persistent n_n_Old;
persistent n_max_Old;
persistent Material_Stator_Old;
persistent Material_Laeufer_Old;
persistent Schaltung_Old;
persistent Bauweise_Old;
persistent Kuehlung_Old;
persistent Betriebsstrategie_Old;

%Equalp = strcmp(p_Old, p);
EqualMaterial_Stator = strcmp(Material_Stator_Old,Material_Stator);
EqualMaterial_Laeufer = strcmp(Material_Laeufer_Old,Material_Laeufer);
EqualSchaltung = strcmp (Schaltung_Old,Schaltung);
EqualBauweise = strcmp (Bauweise_Old,Bauweise);
EqualKuehlung = strcmp (Kuehlung_Old,Kuehlung);
EqualBetriebsstrategie = strcmp (Betriebsstrategie_Old,Betriebsstrategie);

if (P_n_Old==P_n) & (U_n_Old==U_n) & (n_n_Old==n_n) & (n_max_Old == n_max)& (m_st_Old == m_st) & (p_Old==p) &  (EqualMaterial_Stator==1) & (EqualMaterial_Laeufer==1) & ( EqualSchaltung==1) & (EqualBauweise==1) & (EqualKuehlung==1) & (EqualBetriebsstrategie==1)
    NewBuild=0
else
    NewBuild=1
    
P_n_Old=P_n;
p_Old=p;
U_n_Old=U_n;
m_st_Old=m_st;
n_n_Old=n_n;
n_max_Old=n_max;
Material_Stator_Old=Material_Stator;
Material_Laeufer_Old=Material_Laeufer;
Schaltung_Old=Schaltung;
Bauweise_Old=Bauweise;
Kuehlung_Old=Kuehlung;
Betriebsstrategie_Old=Betriebsstrategie;
    
end



end