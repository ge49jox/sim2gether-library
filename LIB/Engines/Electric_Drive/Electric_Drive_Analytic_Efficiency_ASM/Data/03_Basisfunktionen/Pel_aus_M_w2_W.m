function [ P_el ] = Pel_aus_M_w2_W(M,w_2,W,Maschinenkonstanten_10, Psi_1N )

% Diese Funktion berechnet die elektrische Leistung P_el aus dem 
% Drehmoment M, der Rotorfrequenz w_2 und der Winkelgeschwindigkeit W.
% P_el ist die elektrische Wirkleistung, die der ASM von der
% Spannungsquelle zugef�hrt wird.


%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
         % p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
       % R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
       % L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);


%% Berechnung von P_el:    
    
P_el = M*W + Pv_aus_M_w2(M,w_2,Maschinenkonstanten_10, Psi_1N );
   

end % Hier endet die Funktion.


