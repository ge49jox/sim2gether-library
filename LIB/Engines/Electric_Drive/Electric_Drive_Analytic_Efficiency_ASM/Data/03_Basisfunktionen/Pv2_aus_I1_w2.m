function [ P_v2 ] = Pv2_aus_I1_w2(I_1,w_2,Maschinenkonstanten_10)

% Diese Funktion berechnet die Rotorverlustleistung P_v2 aus dem 
% Statorstrom I_1 und der Rotorfrequenz w_2.

%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
           m = Maschinenkonstanten_10(1);       
         % p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
       % L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);


%% Berechnung von P_v2:
   
P_v2 = m*R_2*(I2_aus_I1_w2(I_1,w_2,Maschinenkonstanten_10))^2;

end % Hier endet die Funktion.
