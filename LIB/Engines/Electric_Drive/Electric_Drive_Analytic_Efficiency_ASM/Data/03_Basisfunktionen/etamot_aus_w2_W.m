function [ eta_mot ] = etamot_aus_w2_W(w_2,W,Maschinenkonstanten_10)

% Diese Funktion berechnet den motorischen Wirkungsgrad eta_mot aus der 
% Rotorfrequenz w_2 und der Winkelgeschwindigkeit W.
% Es gilt: eta_mot = P_mech/P_el = M*W/(M*W + P_v)
% Sei W positiv. Dann gilt im motorischen Betrieb: M ist positiv.
% => w_2 muss ebenfalls positiv sein.
% P_v ist immer positiv!


%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
         R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
         L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);
     
     
%% Berechnung von eta_mot:
% Es muss auch der Fall ber�cksichtigt werden, dass eta_mot f�r
    % NEGATIVES w_2, d. h. f�r den Generator-Betrieb berechnet werden will.
    
if w_2 < 0
   error('Es wird eta_mot f�r den Generator-Betrieb (w_2 < 0) berechnet! Dies macht keinen Sinn!');
end 

if (w_2 ~= 0) && (W ~= 0)
    eta_mot = (W*w_2)/( W*w_2 + (1/p)*(R_1*R_2^2 + ...
              w_2^2*(R_1*L_2^2 + R_2*L_h^2))/(L_h^2*R_2) );
          
else % d. h. (w_2 == 0) && (W == 0)
    eta_mot = 0;
end


end % Hier endet die Funktion.

