function [ I_1 ] = I1_aus_U1_w1_W(U_1,w_1,W,Maschinenkonstanten_10)

% Diese Funktion berechnet den Statorstrangstrom I_1 aus der
% Statorstrangspannung U_1, der Statorfrequenz w_1 und der
% Winkelgeschwindigkeit W=2*pi*n.


%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
       % R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
       % L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);


%% Berechnung von I_1:

w_2 = w_1 - p*W;
I_1 = I1_aus_U1_w1_w2(U_1,w_1,w_2,Maschinenkonstanten_10);


end % Hier endet die Funktion.
