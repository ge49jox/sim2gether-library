function [ w_2 ] = w2_aus_U1_W_M(U_1,W,M,Maschinenkonstanten_10)

% Diese Funktion berechnet die Rotorfrequenz w_2 aus der
% Statorspannung U_1, der Winkelgeschwindigkeit W und dem Drehmoment M.

%% Dies ist der trivialste Fall:
if M == 0
    w_2 = 0;
   return; 
end  

%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
       % m = Maschinenkonstanten_10(1);       
       % p = Maschinenkonstanten_10(2);         
     % R_1 = Maschinenkonstanten_10(3);       
       R_2 = Maschinenkonstanten_10(4);       
     % L_h = Maschinenkonstanten_10(5);       
% L_sigma1 = Maschinenkonstanten_10(6);  
% L_sigma2 = Maschinenkonstanten_10(7);  
     % L_1 = Maschinenkonstanten_10(8);       
       L_2 = Maschinenkonstanten_10(9);      
     sigma = Maschinenkonstanten_10(10);

w_2Psi1k = R_2/(sigma*L_2); % ben�tigt f�r fzero-Startintervall;

%% wichtige Hinweise:

% ACHTUNG: Diese Funktion muss sowohl f�r den Motor-Betrieb, als auch
    % f�r den Generator-Betrieb g�ltig sein!
    
% ACHTUNG: Diese Funktion ist die einzige Basisfunktion, welche nicht
    % direkt analytisch berechnet werden kann! Die Berechnung muss
    % numerisch in mehreren Iterationsschritten erfolgen. Diese Funktion
    % wird zur Berechnung von Kennfeldmatrizen extrem h�ufig aufgerufen
    % werden. Deshalb ist es hier besonders wichtig, f�r schnelle
    % Algorithmen zu sorgen!
    
% Problemstellung:    
% Finde w_2 so, dass gilt:
    % M = M_aus_U1_w2_W(U_1,w_2,W,Maschinenkonstanten_10);
    % M, U_1 und W sind gegeben, w_2 ist gesucht.
% => M_aus_U1_w2_W(U_1,w_2,W,Maschinenkonstanten_10) - M = 0;
% => Nullstellenproblem!

% Beurteilung von fzero, fsolve und fminbnd:
    % fzero: sucht nach einem Vorzeichenwechsel; Speziell im flachen
        % Bereich des Hochpunkts der U_1N-Grenzkurve ist fzero keine
        % gute Wahl. Schon die Nullstellensuche von z. B. f(x) = x^2
        % f�hrt bei fzero zu einem Abbruch. Es k�nnte folgende Meldung 
        % erscheinen:
            % Exiting fzero: aborting search for an interval containing a
            % sign change because NaN or Inf function value encountered
            % during search. (Function value at -Inf is NaN.)
            % Check function or try again with a different starting value.
        % Au�erdem: Im Normalfall existieren immer zwei (!) Schnittpunkte
        % bzw. Nullstellen. Gesucht ist immer jene Nullstelle, die n�her an
        % der Vertikalachse (= M-Achse) liegt. Bei fzero kann dies nicht
        % garantiert werden.
        % => fzero ist ungeeignet.
    % fminbnd: findet das Minimum einer Funktion f(x) innerhalb eines
        % vorgegebenen Intervalls; f ist eine single variable function.
        % Dieser Fall liegt hier vor. fminbnd basiert auf dem
        % Goldenen-Schnitt-Suchverfahren und auf der parabolischen
        % Interpolation. Quadriert man die Nullstellenfunktion, so kann
        % auch fminbnd benutzt werden. Das Problem eines Abbruchs
        % besteht hier nicht. Allerdings kann auch fminbnd nicht
        % garantieren, die richtige Nullstelle gefunden zu haben.
        % => fminbnd ist ungeeignet.               
    % fsolve: speziell gedacht zum L�sen eines Systems nichtlinearer 
        % Gleichungen. Im Modus optimset('Algorithm','trust-region-dogleg')
        % (default) wird das Newton-Verfahren angewandt. Aufgrund der
        % Kr�mmung der U_1N-Grenzkurve wird das Newton-Verfahren
        % garantiert die gew�nschte Nullstelle liefern, wenn als Startwert
        % w_2 = 0 gew�hlt wird.
        % => fsolve ist gut geeignet.

% Schneller als fsolve ist jedoch das Newton-Verfahren unter Benutzung der 
    % Ableitungsfunktion in analytischer Form. Dieser Weg wird im 
    % Folgenden beschritten:
    
% Definition der Nullstellenfunktion:
% f(w2) = M_aus_U1_w2_W(U_1,w_2,W,Maschinenkonstanten_10) - M;
    % -M nicht vergessen!
  
% Newton-Verfahren allgemein:
    % x_n+1 = x_n - f(x_n)/f'(x_n) := F;
    % F ist die Fixpunktiterationsfunktion.

    
%% Berechnung von w_2:
    

x_nachher = 0; % Startwert der Newton-Iteration; garantiert, dass stets die
    % richtige Nullstelle gefunden wird; x steht f�r w_2.
verhaeltnis = 1337; % Initialisierung mit einem gro�en Dummy-Wert,
    % damit die while-Schleife auch ausgef�hrt wird.

while verhaeltnis > (1 + 1e-4)
    % Zur Information:
    % eps returns the distance from 1.0 to the next largest
    % double-precision number, that is eps = 2^(-52) = 2.2e-16.     

    x_vorher = x_nachher; 

    % M_aus_U1_w2_W() unterscheidet sich von der
    % Nullstellenfunktion f nur durch das -M. Das -M hat jedoch
    % auf die Ableitung keinen Einfluss, sodass direkt
    % DM_aus_U1_w2_W_nach_Dw2() benutzt werden darf.

    x_nachher = x_vorher - ...
        (M_aus_U1_w2_W(U_1,x_vorher,W,Maschinenkonstanten_10) - M)/ ...
        DM_aus_U1_w2_W_nach_Dw2(U_1,x_vorher,W,Maschinenkonstanten_10);

    % Wenn die Iteration konvergiert, dann muss stets gelten:
        % abs(x_vorher) < abs(x_nachher), d. h. abs(x) muss monoton (!)
        % zunehmen. Sollte abs(x) w�hrend der Iteration
        % irgendwann wieder abnehmen, oder sollte x irgendwann das
        % Vorzeichen wechseln, so ist dies ein klares Zeichen
        % daf�r, dass die Iteration nicht konvergieren kann, da
        % der M-Wert au�erhalb des Wertebereichs der U_1N-Grenzkurve 
        % liegt. In diesem Fall springen die Tangenten hin und her.
    % Wenn die Newton-Iteration mit Startwert 0 divergiert, dann gilt
        % f�r postive M, dass auf jeden Fall kein Schnittpunkt
        % existiert, da die U_1N-Grenzkurve f�r positive w_2 bzw. M 
        % bis zum Hochpunkt durchgehend rechtsgekr�mmt ist.
    % Wenn f�r negative M die Newton-Iteration divergiert, kann 
        % trotzdem ein ganz normaler Schnittpunkt existieren. Damit 
        % stets Konvergenz vorliegt (falls ein Schnittpunkt existiert), 
        % m�sste die Kurve linksgekr�mmt sein, was jedoch nicht immer 
        % der Fall ist.
    % F�r die seltenen F�lle von Divergenz des Newton-Verfahrens soll
        % daher zus�tzlich �ber fzero eine L�sung gesucht werden,
        % allerdings nur f�r negative M!

    if (abs(x_vorher) > abs(x_nachher)) ...
            || (sign(x_vorher)*sign(x_nachher) == -1) % d. h. Iteration
                % divergiert;
        if M < 0
            Hilfsfunktion1 = ...
            @(a,x,b,c) M_aus_U1_w2_W(a,x,b,c) - M; % -M nicht 
            % vergessen! 
            a = U_1;
            % x = w_2, wird gesucht;
            b = W;
            c = Maschinenkonstanten_10;

            Hilfsfunktion2 = @(x) Hilfsfunktion1(a,x,b,c);
            my_options = optimset('Display','off','TolX',1e-4);
            
            % TolX is a lower bound on the size of a step, meaning the norm
            % of (x_i � x_(i+1)). If the solver attempts to take a step
            % that is smaller than TolX, the iterations end. TolX is
            % sometimes used as a relative bound, meaning iterations end
            % when |(x_i � x_(i+1))| < TolX*(1 + |x_i|), or a similar
            % relative measure.
            
            % => Was genau bedeutet "sometimes"?!
            % Wann ist TolX absolut zu verstehen, wann relativ?!
            
            w_2 = fzero(Hilfsfunktion2,[-w_2Psi1k,0],my_options);
            % Im Generator-Betrieb (M < 0) sind w_2-Werte < -w_2Psi1k
            % nicht von Interesse.           
            return;
        end
        
        w_2 = NaN;
        return;
    end    

    verhaeltnis = abs(x_nachher/x_vorher); % Das abs() dient nur der
        % Sicherheit.

end % while


w_2 = x_nachher; % R�ckgabewert der Funktion;    
    
% Diese Funktion wurde intensiv �ber Geogebra getestet. Alle Ergebnisse
    % waren korrekt.

end % Hier endet die Funktion.

