function [ DM_nach_Dw2 ] = ...
    DM_aus_U1_w2_W_nach_Dw2(U_1,w_2,W,Maschinenkonstanten_10)

% Die Funktion M = f(U_1,w_2,W) bzw. M_aus_U1_w2_W(...) wird f�r ein festes
    % U_1 und f�r ein festes W analytisch nach w_2 abgeleitet.
% Diese Ableitungsfunktion wird an der Stelle w_2 ausgewertet und der
    % Zahlenwert in DM_nach_Dw2 gespeichert. DM_nach_Dw2 ist der
    % R�ckgabewert der Funktion.
% Die explizite analytische Ableitungsfunktion dient dazu, die Konvergenz
    % von Algorithmen deutlich zu beschleunigen bzw. den Rechenaufwand
    % deutlich zu reduzieren. Die Ableitung kann im Newton-Verfahren
    % angewandt werden.


%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
         m = Maschinenkonstanten_10(1);       
         p = Maschinenkonstanten_10(2);         
       R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
       L_h = Maschinenkonstanten_10(5);       
% L_sigma1 = Maschinenkonstanten_10(6);  
% L_sigma2 = Maschinenkonstanten_10(7);  
       L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);

       
%% Berechnung von DM_nach_Dw2:

% In folgenden Kommentaren wird w_2 zu w2 und w_1 zu w1:

% f(w2) = M_aus_U1_w2_W(U_1,w2,W);
%                         w2
% f(w2) = A* ---------------------------- 
%             (B*w1+C*w2)^2+(D*w1*w2-E)^2
% f(w2) = A*u/v;

% ACHTUNG: w1 h�ngt von w2 ab, da w1 = p*W + w2 !
% => dw1/dw2 = 1 !

% mit:
    A = U_1^2*m*p*L_h^2*R_2;
    B = L_1*R_2;
    C = L_2*R_1;
    D = sigma*L_1*L_2;
    E = R_1*R_2;
    
w_1 = p*W + w_2;
    
   % u = w_2; wird nicht ben�tigt, da w_2 direkt benutzt wird;
    v = (B*w_1+C*w_2)^2+(D*w_1*w_2-E)^2;
  
% Es gilt: D = totale Ableitung, d = partielle Ableitung;
% Wenn y = f(x1,x2), dann ist Dy/Dt = df/dx1*Dx1/Dt + df/dx2*Dx2/dt.
% Somit gilt:
% f' = Df/Dw2 = df/dw2*Dw2/Dw2 + df/dw1*Dw1/Dw2;
% f' = df/dw2*1 + df/dw1*1

%            1*v - w2*( 2*(B*w1+C*w2)*C + 2*(D*w1*w2-E)*D*w1 )
% df/dw2 = A*-------------------------------------------------
%                                 v^2

%            0*v - w2*( 2*(B*w1+C*w2)*B + 2*(D*w1*w2-E)*D*w2 )
% df/dw1 = A*-------------------------------------------------
%                                 v^2
% => 
%        v - w2*( 2*(B+C)*(B*w1+C*w2) + 2*D*(w1+w2)*(D*w1*w2-E) ) 
% f' = A*--------------------------------------------------------
%                                 v^2
% f' ist korrekt! �berpr�ft mit Geogebra:
    % Ableitung des Computers und Formelausdruck stimmen im Kurvenverlauf
    % exakt �berein.
    
% Es gilt: f' = DM_nach_Dw2;

DM_nach_Dw2 = (A/v^2)* ...
              ( v - w_2*( 2*(B+C)*(B*w_1+C*w_2) + ...
              2*D*(w_1+w_2)*(D*w_1*w_2-E) ) );

% Diese Funktion wurde mit verschiedensten Zahlenwerten in Geogebra
    % �berpr�ft! Es kamen stets dieselben Werte heraus!
       
    
end % Hier endet die Funktion

