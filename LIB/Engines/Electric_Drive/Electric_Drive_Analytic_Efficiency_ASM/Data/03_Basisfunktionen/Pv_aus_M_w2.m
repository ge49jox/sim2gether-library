function [ P_v ] = Pv_aus_M_w2(M,w_2,Maschinenkonstanten_10, ...
    Psi_1N)

% Diese Funktion berechnet die Gesamtverlustleistung P_v aus dem 
% Drehmoment M und der Rotorfrequenz w_2.
% Diese Funktion ist wichtig f�r die Berechnung des Energieverbrauchs
    % und des Wirkungsgrads.

    
%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
         R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
         L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);

           
%% Berechnung von P_v:   

if sign(M*w_2) == -1
   error('Vorzeichen von M und w_2 m�ssen �bereinstimmen!'); 
end    

if w_2 == 0
    %Psi_1N = 0.1190% Wenn error: 0.1190 oder evalin('base','Psi_1N') oder Psi_1N = L_h*I_mu in hauptprogramm_auslegung neu berechnen;
    % F�r den sehr seltenen Fall w_2 == 0 holt sich diese Funktion
        % Psi_1N nicht als Argument, sondern aus dem base-workspace.
        % Erkl�rung siehe unten!
    P_v = Pv_aus_Psi1_w2(Psi_1N,w_2,Maschinenkonstanten_10); % Psi_1N!
    % Erkl�rung siehe unten!
else 
    % Die folgende Formel versagt bei w_2 = 0 (=> M = 0)!
    P_v = abs(M*(1/p)*(R_1*R_2^2 + w_2^2*(R_1*L_2^2 + R_2*L_h^2))/...
          (w_2*L_h^2*R_2));
        % Die Betragsbildung abs() dient nur der Sicherheit, da P_v immer
        % positiv sein muss. M und w_2 m�ssen ja stets dasselbe
            % Vorzeichen haben, sodass sich dieses aus der Formel
            % herausk�rzt.
end    

% Die letzte Formel versagt bei w_2 = 0. Denn bei w_2 = 0 muss automatisch
    % auch M = 0 sein. Dies w�rde zu 0/0 f�hren, was nicht definiert ist.
% F�r diesen Spezialfall muss man P_v �ber Psi_1 berechnen.
    % In den meisten F�llen ist Psi_1 jedoch nicht direkt bekannt.
    % Deshalb wurde Psi_1 = Psi_1N gew�hlt. Dies mag sehr willk�rlich
    % erscheinen, ist jedoch nicht besonders schlimm, denn:
% Im Betrieb mit optimaler Betriebsstrategie tritt w_2 = 0 nie auf.
% Im Betrieb mit konventioneller Betriebsstrategie tritt
    % w_2 = 0 sehr wohl auf, allerdings nur (!) bei M = 0, d. h. nur
    % auf der W-Achse (Winkelgeschwindigkeit) in der M-W-Ebene.
    % Bis zur Nennwinkelgeschwindigkeit W_N w�rde Psi_1 = Psi_1N
    % sogar stimmen, d. h. es w�rde kein Fehler entstehen.
    % Nur f�r W > W_N w�rde ein zu hoher Fluss und damit eine zu hohe
    % Verlustleistung P_v vorget�uscht werden. Dies macht jedoch kaum etwas
    % aus, da der Bereich auf der W-Achse verschwindend klein ist.
    % Das gesamte Kennfeld w�re in seiner Korrektheit praktisch 
    % unbeeintr�chtigt.

    
end % Hier endet die Funktion

