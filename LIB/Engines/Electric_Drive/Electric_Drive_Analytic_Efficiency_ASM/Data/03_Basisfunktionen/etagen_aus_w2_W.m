function [ eta_gen ] = etagen_aus_w2_W(w_2,W,Maschinenkonstanten_10)

% Diese Funktion berechnet den generatorischen Wirkungsgrad eta_gen aus der 
% Rotorfrequenz w_2 und der Winkelgeschwindigkeit W.
% Es gilt: eta_gen = (-P_el)/(-P_mech) = (-M*W - P_v)/(-M*W)
% Sei W positiv. Dann gilt im generatorischen Betrieb: M ist negativ.
% => w_2 muss ebenfalls negativ sein.
% P_v ist immer positiv!
% F�r sehr kleine W kann es sein, dass (-M*W) (ist positiv) kleiner ist
    % als P_v! D. h. P_el w�re positiv! Man m�sste also dem ASM-Stator
    % Energie zuf�hren, obwohl die ASM gebremst wird (M ist negativ).
% => F�r sehr kleine W kann eta_gen NEGATIV sein!
    % Meistens ist eta_gen jedoch positiv (bei w_2 < 0).
% Hingegen ist eta_mot ausnahmslos positiv (bei w_2 > 0).

% Es gilt:
    % wenn w_2 > 0 bzw. M > 0 => Motor-Betrieb     => benutze eta_mot
    % wenn w_2 < 0 bzw. M < 0 => Generator-Betrieb => benutze eta_gen
% Nat�rlich k�nnte man auch im Motor-Betrieb eta_gen ausrechnen, dies
    % w�rde jedoch wenig Sinn machen.

% Anmerkung: Es gilt ganz allgemein: eta_gen = 1/eta_mot;
    % denn: eta_mot = P_mech/P_el; eta_gen = (-P_el)/(-P_mech);
    
    
%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
         R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
         L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);

     
%% Berechnung von eta_gen:
% Es muss auch der Fall ber�cksichtigt werden, dass eta_gen f�r
    % POSITIVES w_2, d. h. f�r den Motor-Betrieb berechnet werden will.
    
if w_2 > 0
   error('Es wird eta_gen f�r den Motor-Betrieb (w_2 > 0) berechnet! Dies macht keinen Sinn!');
end 

if (w_2 ~= 0) && (W ~= 0)
    eta_gen = ( W*w_2 + (1/p)*(R_1*R_2^2 + ...
                w_2^2*(R_1*L_2^2 + R_2*L_h^2))/(L_h^2*R_2) )...
                / (W*w_2);
else % d. h. mathematisch nicht definiert:
    eta_gen = 0; % Das mathematisch streng korrekte Ergebnis w�re
    % +-Inf. Dies f�hrt in Kennfeld-Plots jedoch zu inkorrekten
    % Darstellungen f�r M = 0.
end
    

end % Hier endet die Funktion.
