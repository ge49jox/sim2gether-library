function [ w_2 ] = w2_aus_Psi1_M(Psi_1,M,Maschinenkonstanten_10)

% Diese Funktion berechnet die Rotorfrequenz w_2 aus der
% Statorflussverkettung Psi_1 und dem Drehmoment M.


%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
           m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);

   
%% Berechnung von w_2:   

M_Psi1k = Psi_1^2*(m*p/2)*(1-sigma)/(sigma*L_1); % Kippmoment bei
    % konstanter Statorflussverkettung Psi_1;
% Man k�nnte diesen Wert auch als Argument �bergeben, aber in diesem Fall
    % ist der Rechenaufwand sehr gering, sodass M_Psi1k direkt hier
    % berechnet wird. Damit ist ein zus�tzliches Argument nicht n�tig.
    
if M > M_Psi1k
    error('Das eingegebene Drehmoment M muss kleiner sein als das zugeh�rige Kippmoment +M_Psi1k!');
elseif M < -M_Psi1k
    error('Das eingegebene Drehmoment M muss gr��er sein als das zugeh�rige Kippmoment -M_Psi1k!');
elseif M == 0
    w_2 = 0;
else
    M_sub = M/M_Psi1k; % M_sub ist nur eine Substitutionsvariable.

    w_2 = (R_2/(sigma*L_2))*M_sub/(1 + sqrt(1 - M_sub^2));

    % Achtung: Ist M > M_Psi1k, so wird der Ausdruck unter der Wurzel negativ.
    % Dann ist w_2 nicht definiert. Dies macht auch Sinn, da bei konstantem
    % Psi_1 keinesfalls ein M > M_Psi1k sein kann. M_Psi1k stellt ja gerade den
    % Maximalwert (= Wert im Kipppunkt) dar.
end


end % Hier endet die Funktion.
