function [ P_v1 ] = Pv1_aus_I1(I_1,Maschinenkonstanten_10)

% Diese Funktion berechnet die Statorverlustleistung P_v1 aus dem 
% Statorstrom I_1. Diese Funktion ist trivial, wird aber trotzdem aus
% Gr�nden der Vollst�ndigkeit angegeben.


%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
           m = Maschinenkonstanten_10(1);       
         % p = Maschinenkonstanten_10(2);         
         R_1 = Maschinenkonstanten_10(3);       
       % R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
       % L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);


%% Berechnung von P_v1:
   
P_v1 = m*R_1*I_1^2;

end % Hier endet die Funktion.
