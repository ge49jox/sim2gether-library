function [ M ] = M_aus_Psi2_w2(Psi_2,w_2,Maschinenkonstanten_10)

% Diese Funktion berechnet das Drehmoment M aus der Statorflussverkettung 
% Psi_1 und der Rotorfrequenz w_2.


%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
           m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
       % L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);


%% Berechnung von M:

M = Psi_2^2*(m*p)*w_2/R_2;

end % Hier endet die Funktion.

