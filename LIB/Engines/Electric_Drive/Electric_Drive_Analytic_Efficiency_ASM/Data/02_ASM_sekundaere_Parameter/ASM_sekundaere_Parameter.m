%% wichtige Hinweise:
   
% Dieses Skript berechnet sekundäre ASM-Parameter, welche sich alle
% aus den primären Parametern berechnen lassen.
    

%% Statorinduktivität, Rotorinduktivität, Blondel'scher Streukoeffizient: 
    
  L_1 = L_h + L_sigma1;      % Statorinduktivität
  L_2 = L_h + L_sigma2;      % Rotorinduktivität
sigma = 1 - L_h^2/(L_1*L_2); % Blondel'scher Streukoeffizient        

%% wenn dieser Fall eintritt ist die streuinduktivität zu ungenau und sigma zu groß, es sollte dann manuell korrigiert werden, hier sind die WErte des Visio.M eingesetzt
if sigma >0.1
L_1 = L_h *1.03;      % Statorinduktivität
L_2 = L_h *1.045;      % Rotorinduktivität 
  sigma = 1 - L_h^2/(L_1*L_2);
end
if sigma <0.07
L_1 = L_h *1.03;      % Statorinduktivität
L_2 = L_h *1.045;      % Rotorinduktivität 
  sigma = 1 - L_h^2/(L_1*L_2);
end

%% charakteristische Rotorfrequenzen, vom kleinsten zum größten Wert:

w_2opt = R_2/sqrt(L_2^2 + (R_2/R_1)*L_h^2); % Rotorfrequenz, bei der bei
    % gegebenem Drehmoment M die Gesamtverlusleistung P_v minimal wird;
    % Dies ist gleichzeitig jene Rotorfrequenz, bei der Wirkungsgrad
    % maximal wird. => optimale Rotorfrequenz;
w_2I1k = R_2/L_2; % Rotorfrequenz, bei der das Drehmoment M kippt bei
    % konstantem Statorstrom I_1;
    M_I1Nk = I_1N^2*(m*p/2)*L_h^2/L_2; % zugehöriges Kippmoment bei 
            % I_1 = I_1N;
        % Es ließe sich auch aus einer Funktion berechnen:
        % M_I1Nk = M_aus_I1_w2(I_1N,w_2I1k);    
w_2Psi1k = R_2/(sigma*L_2); % Rotorfrequenz, bei der das Drehmoment M kippt
    % bei konstanter Statorflussverkettung Psi_1;
    M_Psi1Nk = Psi_1N^2*(m*p/2)*(1-sigma)/(sigma*L_1); % zugehöriges
        % Kippmoment bei Psi_1 = Psi_1N.
        % Es ließe sich auch aus einer Funktion berechnen:
        % M_Psi1Nk = M_aus_Psi1_w2(Psi_1N,w_2Psi1k);
  
        
%% Parameter-Vektoren:
% Mehrere Parameter-Werte werden in einem Parameter-Vektor
    % zusammengefasst. Dieser Zeilen-Vektor kann dann bequem einer Funktion
    % übergeben werden, ohne alle Werte einzeln übergeben zu müssen.
    % Natürlich kann es dabei vorkommen, dass einzelne Werte nicht benötigt
    % werden. Dies ist jedoch kein Problem.
    
% Parameter-Vektor für die 7 Maschinenkonstanten UND für
    % Statorinduktivität, Rotorinduktivität,
    % Blondel'scher Streukoeffizient:
Maschinenkonstanten_10 = [ ...
    m,        ... % Index  1
    p,        ... % Index  2
    R_1,      ... % Index  3
    R_2,      ... % Index  4
    L_h,      ... % Index  5
    L_sigma1, ... % Index  6
    L_sigma2, ... % Index  7
    L_1,      ... % Index  8, sekundärer Parameter
    L_2,      ... % Index  9, sekundärer Parameter
    sigma     ... % Index 10, sekundärer Parameter
];

% Parameter-Vektor für die 4 Betriebsgrenzen:
Betriebsgrenzen_4 = [ ...
    I_1N,   ... % Index  1
    U_1N,   ... % Index  2
    Psi_1N, ... % Index  3
    W_max   ... % Index  4
];

% Parameter-Vektor für die 3 charakteristischen Rotorfrequenzen MIT den
    % zugehörigen Kippmomenten:
charakt_w2_M_5 = [ ...
    w_2opt,   ... % Index  1
    w_2I1k,   ... % Index  2
    w_2Psi1k, ... % Index  3
    M_I1Nk,   ... % Index  4
    M_Psi1Nk, ... % Index  5
];


       


