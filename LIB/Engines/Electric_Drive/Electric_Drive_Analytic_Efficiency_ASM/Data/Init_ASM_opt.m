function [KF_opt_real_Pelmotgen_Matrix, KF_opt_real_Pelmotgen_Sim_Matrix, W_Vektor, M_Vektor_motgen,GK_M_W_Matrix_6  ] = Init_ASM_opt( P_n,p, U_n, m, n_n, n_max,Material_Stator,Material_Laeufer,Schaltung,Bauweise,Kuehlung)
%INIT_ASM Summary of this function goes here
%   Detailed explanation goes here


%% ASM-Kennfeld Tool

%Mit diesem Programm k�nnen Sie die Kennfelder einer ASM berechnen. Es
%werden alle relevanten Faktoren ber�cksichtigt (Verluste, S�ttigung,
%�berlast). Im Anschluss an die Kennfeldberechnung erfolgt die Fahrzyklus-
%Simulation. Die Reihenfolge der einzelnen Skripte darf auf keinen Fall
%ge�ndert werden. Fahrzeugparameter (Masse, Aerodynamik,Reifendimension etc.
% werden �ber das Skript "primaere_Parameter_xxx" variiert und initialisiert. 

%Im Feld "Maschinendaten der ASM" k�nnen die Ausgangsdaten der ASM
%festgelegt und ver�ndert werden. Auf Basis dieses Maschinendatensatzes
%erfolgt die gesamte Auslegung der Maschine, die Kennfeldberechnung und die
%Verbrauchssimulation.
%% alle Unterordner zum Suchpfad hinzuf�gen:
    %addpath(genpath(pwd));

%% Maschinendaten der ASM

%    P_n=22;             % Nennleistung in kW
%    p = 2;               % Polpaarzahl w�hlen zwischen 2 und 5
%    U_n=200;           % Nennspannung in V in einem Statorstrang
%    m= 3;               % Strangzahl
%    n_n= 4000;          % Nenndrehzahl in 1/min
%    n_max = 10000;        % Maximaldrehzahl f�r Kennfeld
   
  
   
  % Material_Stator= 'Kupfer';             % bzw. Aluminium
%    Material_Laeufer= 'Aluminium';         % bzw. Kupfer
%    Schaltung= 'Stern';                    % bzw. Dreieck
%    Bauweise= 'geschlossen';               % bzw. offen
%    Kuehlung= 'Fluessig';                  % bzw. Luft
   
   
  
%% Maschinendaten der ASM h�ndisch eingeben
% Wenn Daten aus einem Datenblatt eingegeben werden m�chten, kann dies
% direkt im Skript "primaere_Parameter_xxx" geschehen. Einfach die
% einzulesenden Daten aus den Kommentarfunktionen holen und initialisieren.

%% Maschinenauslegung-Berechnung
% In dieser Section werden ale ben�tigten Parameter aus den ASM-
% Maschinendaten berechnet und initialisiert. Im Skript "Abstimmung" 
% erfolgt eine Abstimmung der Indizes der Parameter.

    hauptprogramm_auslegung;
       
%% Initialisierung der Fahrzeugparameter:
    primaere_Parameter_Visio_M;
    
 %% sekund�re ASM-Parameter berechnen:      
    ASM_sekundaere_Parameter;
    
%% Nennwerte berechnen:
    Nennwerte;

%% Crossoverwerte berechnen:
    Crossoverwerte;

%% Optimalwerte berechnen:
    Optimalwerte;

%% Vektoren berechnen:
    Vektoren_neu;

%% Eisenverluste berechnen:
    Hauptskript_Eisenverluste_ASM;    

%% Rotorfrequenzdiagramm berechnen:
    Rotorfrequenzdiagramm_neu;
     
%% �berlast:
    Ueberlastbereich;

%% Plot-Optionen initialisieren:    
    Plot_Optionen_neu;
 
%% Rotorfrequenzdiagramm plotten:
    %Rotorfrequenzdiagramm_Plot_neu;
     
%% Grenzkurven berechnen:
    Grenzkurven;     
    Grenzkurven_neu;
    
%% Drehmoment-Grenzkurven plotten:
    % inkl. �berlast:
   % Grenzkurven_M_Plot_neu;
%% Grenzkurven der mechanischen Leistung P_mech plotten:
    %inkl. �berlast:
   % Grenzkurven_Pmech_Plot_neu;
    %% Grenzkurven der Rotorfrequenz w_2 plotten:
   %  Grenzkurven_w2_Plot;   

   
%% Kennfeld-Matrizen berechnen und plotten:
         
%% Kennfeld-Matrizen zur konventionellen Betriebsstrategie:

%% KF_konv_real_Pelmotgen_Matrix:     
% berechnen (ca. 20-40 Sekunden):
%              KF_konv_real_Pelmotgen;
%              KF_konv_real_Pelmotgen_Plot;


%% Eta_Kennfeld f�r konservative Betriebsstrategie
             %Etakennfeld_kon;
             %KF_eta_mot_konv_Plot;
%% KF_konv_real_Pelmotgen_Sim_Matrix (f�r Simulation ben�tigt):       

            % KF_konv_real_Pelmotgen_Sim;
             %KF_konv_real_Pelmotgen_Sim_Plot;
% 
%% Kennfeld-Matrizen zur optimalen Betriebsstrategie:
       
%% KF_opt_real_Pelmotgen_Matrix:      
% berechnen (ca. 20-40 Sekunden):
             KF_opt_real_Pelmotgen;
             KF_opt_real_Pelmotgen_Plot;
           
%% KF_opt_real_Pelmotgen_Sim_Matrix (f�r Simulation ben�tigt):        
            KF_opt_real_Pelmotgen_Sim;
%              KF_opt_real_Pelmotgen_Sim_Plot;         
                     
%% Eta_Kennfeld f�r optimale Betriebsstrategie
%              Etakennfeld_opt;
%              KF_eta_mot_opt_Plot;            
%% Fahrzyklen initialisieren:
  %   Fahrzyklen;
 
%% "Simulationsmodell.slx" in Simulink �ffnen:
    % Simulationsmodell;
% %% Tragen Sie jetzt ganz links im Simulink-Modell unter
% % "Bitte hier die gew�nschte Zyklus_Nummer eintragen:"
% % die Nummer des Fahrzyklus ein,
% % welchen Sie simulieren m�chten.
%     
%% Das Ausf�hren dieser Zelle startet die Simulation:
 %set_param('Simulationsmodell','SimulationCommand','start')
% % Alternativ k�nnen Sie nat�rlich auch in Simulink
% % oben in der Navigationsleiste den gro�en gr�nen Button "Run" dr�cken.
% 
% %% Ist die Simulation abgeschlossen, wird ein kurzer Ton zu h�ren sein.
% % Sie k�nnen jetzt in Simulink die wichtigsten Simulationsergebnisse
% % betrachten.
% 
% % Ganz links im Modell, kurz nach dem gro�en Block zum Einlesen aller
% % Fahrzyklen, befindet sich ein Scope, um das Geschwindigkeitsprofil
% % eines Fahrzyklus v in km/h anzeigen zu lassen.
% 
% % Vergessen Sie bei Betrachtung von Scope-Plots nicht, oben in der
% % Scope-Leiste auf den Autoscale-Button zu dr�cken (Viereck mit vier
% % Pfeilen).
% 
% % Ein kleines Fenster wird einen X-Y-Plot anzeigen, welcher
% % die W-M-Trajektorie (W = Winkelgeschwindigkeit der ASM,
% % M = Drehmoment der ASM) plottet (schon zur Laufzeit).
% 
% % Die wichtigsten Endergebnisse sehen sie im rechten Bereich des Modells:
%     % Scope f�r den Plot des absoluten elektrischer Energieverbrauchs E_el;
%     % spezifischer elektrischer Energieverbrauch e_el = Energie/Strecke;
%     % relative Reichweiten�nderung;
%     % relative Verbrauchs�nderung;
% 
% 
% %% Die Simulation hat jetzt die wichtigsten Daten in den Workspace
% % geschrieben.
% % Auf Basis dieser Daten werden im Folgenden die Plots erstellt.
% % Es liegt auf der Hand, dass ein Plot nur dann etwas anzeigen 
% % kann, wenn der zugeh�rige Zyklus simuliert worden ist.
% % M�chten Sie einen weiteren Zyklus simulieren, tragen Sie bitte eine
% % neue Zyklus_Nummer ein und starten sie erneut die Simulation.
% 
%% Lastpunktverteilung:
% Die folgenden Abschnitte k�nnne erst nach der Simulation ausgef�hrt
% werden. Sie plotten die Lastpunkte der Fahrzyklen in die
% Kennfelder und berechnen zus�tzlich die Anzahl der Lastpunkte im
% �berlastbereich sowie den �berlastfaktor (Verh�ltnis von maximalem Moment
% des Fahrzyklus zum Nennmoment der Maschine)
%% NEDC: 
% Fahrzyklus-Trajektorie:
    % NEDC_M_W_Plot;
    % Lastpunkte_NEDC

   

end

