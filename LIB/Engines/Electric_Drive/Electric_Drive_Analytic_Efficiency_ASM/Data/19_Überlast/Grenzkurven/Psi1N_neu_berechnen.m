% Dieses Skript berechnet Psi1N aus den Werten des Nennpunktes, wenn dieser
% unter der Statorflussgrenzkurve zu liegen kommt. Das basiert auf der
% �berlegung, dass im Nennpunkt die Maschine im maximalen Fluss betrieben
% wird (Auslegungskriterium) und sich dadurch die Streuung des Wertes, der
% ja nur �ber eine grobe Richtlinie abgesch�tzt wird, einschr�nken l�sst.


% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
         % p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);
I_1=I_1N;
w_2=w_2N;
       
Psi1N_neu = Psi1_aus_I1_w2(I_1,w_2,Maschinenkonstanten_10);




