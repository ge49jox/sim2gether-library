%% Dieses Skript berechnet und plottet die Statorflussgrenzkurve

% Dieses Skript greift auf w2_Vektor_motgen zu.
smax = length(w2_Vektor_motgen); % h�chster Spaltenindex

RFD_Psi1N_Vektor = zeros(1,smax); % Initialisierung;

for s = 1:smax
    
    w_2 = w2_Vektor_motgen(s);
    
    % I_1N-Grenzkurve:
   % RFD_I1N_Vektor(s) = M_aus_I1_w2(I_1N,w_2,Maschinenkonstanten_10);
    
    % Psi_1N-Grenzkurve:
    RFD_Psi1N_Vektor(s) = M_aus_Psi1_w2(Psi_1N,w_2,Maschinenkonstanten_10);
end


%% Kurven plotten:

gcf = figure( ...
    'PaperUnits','centimeters', ...
    'Units','centimeters', ...
    'Position',[ ...
        Plot_figure_x, ...
        Plot_figure_y, ...
        1.5*Plot_figure_Breite, ...
        1.1*Plot_figure_Hoehe ...
    ], ...
    'Resize',Plot_Resize_String ...
);

gca = axes( ...
    'XLim',[0, w2_Vektor_motgen(end)], ...
    'YLim',[0, +1.1*M_Psi1Nk] ...
);


hold on;
grid on;
set(gca,'layer',Plot_Layer_String);

box on;

title('Statorflussgrenzkurve');
xlabel('Rotorfrequenz \omega_2/(rad/s)','Fontsize', 14);
ylabel('Drehmoment M/(Nm)','Fontsize', 14);

% x-Achse:
plot([w2_Vektor_motgen(1), w2_Vektor_motgen(end)],[0,0], ...
    'black-','LineWidth',1);

% y-Achse:
plot([0,0],[-2*M_Psi1Nk,+2*M_Psi1Nk], ...
    'black-','LineWidth',1);

%%
% Psi_1N-Grenzkurve:
h2 = plot(w2_Vektor_motgen,RFD_Psi1N_Vektor, ...
    'blue-','LineWidth',2);


% +w_2Psi1k:
h12 = plot([w_2Psi1k,w_2Psi1k],[-2*M_Psi1Nk,+2*M_Psi1Nk], ...
    'blue--','LineWidth',1);


legend( ...
    [h2  h12],{ ...
       
        '\Psi_{1N}-Grenzkurve', ...
      
        '\pm\omega_{2\Psi_1k}', ...
    }, ...
    'Location','NorthWest' ...
);



% -w_2Psi1k:
h12 = plot([-w_2Psi1k,-w_2Psi1k],[-2*M_Psi1Nk,+2*M_Psi1Nk], ...
    'blue--','LineWidth',1);

