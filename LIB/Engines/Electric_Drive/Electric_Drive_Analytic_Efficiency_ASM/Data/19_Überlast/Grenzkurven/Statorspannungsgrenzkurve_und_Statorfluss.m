%% Statotspannungskurve:
% Dieses Skript berechnet und plottet die statorspannungsbasierte
% Drehmomentgleichung in Abh�ngigkeit verschiedener Winkelgeschwindigkeiten

% Dieses Skript greift auf w2_Vektor_motgen zu.
smax = length(w2_Vektor_motgen); % h�chster Spaltenindex

RFD_U1N_a_Vektor = zeros(1,smax); % Initialisierung, W = W_mot_ue;
RFD_U1N_b_Vektor = zeros(1,smax); % Initialisierung, W = W_HOP;
RFD_U1N_c_Vektor = zeros(1,smax); % Initialisierung, W = W_Nmot;
RFD_U1N_d_Vektor = zeros(1,smax); % Initialisierung, W = 2*W_Nmot
RFD_U1N_e_Vektor = zeros(1,smax); % Initialisierung, W = W_max
 
for s = 1:smax
    
    w_2 = w2_Vektor_motgen(s);
    
    % U_1N-Grenzkurven:
    RFD_U1N_a_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,W_mot_ue,Maschinenkonstanten_10);
    RFD_U1N_b_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,W_HOP-2.5,Maschinenkonstanten_10);
    RFD_U1N_c_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,W_Nmot,Maschinenkonstanten_10);
    RFD_U1N_d_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,2*W_Nmot,Maschinenkonstanten_10);
    RFD_U1N_e_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,W_max,Maschinenkonstanten_10);
    
end

%% U_1N-HOP-Kurve: Menge aller Hochpunkte der U_1N-Grenzkurven:
% Dazu geh�rt eine Matrix mit 3 Zeilen und ca. doppelt sovielen Spalten,
% wie W_Vektor Elemente hat.

% 1. Zeile: W_Vektor_lang;
% 2. Zeile: w_2-Werte;
% 3. Zeile: zugeh�rige M-Werte;

smax = length(W_Vektor_lang);

RFD_U1N_HOP_Matrix_3 = zeros(3,smax); % Initialisierung;

RFD_U1N_HOP_Matrix_3(1,:) = W_Vektor_lang;

for s = 1:smax
    
    W = W_Vektor_lang(s);
    
    Hilfsfunktion1 = @(a,x,b,c) -M_aus_U1_w2_W(a,x,b,c);
        % Minuszeichen! wegen fminbnd!
        
    a = U_1N;
    % x = w_2, wird gesucht;
    b = W;
    c = Maschinenkonstanten_10;
    
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,x,b,c);
        
    [w_2,MHOP] = fminbnd(Hilfsfunktion2,0,w_2Psi1k);
    MHOP = abs(MHOP); % abs() nicht vergessen! wegen fminbnd!
    
    RFD_U1N_HOP_Matrix_3(2,s) = w_2;
    RFD_U1N_HOP_Matrix_3(3,s) = MHOP;
    
end


M=max(RFD_U1N_a_Vektor) %definiert das maximale Moment um die Achsen zu skalieren
%% Kurven plotten:

gcf = figure( ...
    'PaperUnits','centimeters', ...
    'Units','centimeters', ...
    'Position',[ ...
        Plot_figure_x, ...
        Plot_figure_y, ...
        1.5*Plot_figure_Breite, ...
        1.1*Plot_figure_Hoehe ...
    ], ...
    'Resize',Plot_Resize_String ...
);

gca = axes( ...
    'XLim',[0, w2_Vektor_motgen(end)], ...
    'YLim',[0, +1.1*M_Psi1Nk] ...
);


hold on;
grid on;
set(gca,'layer',Plot_Layer_String);

box on;

%title('Rotorfrequenzdiagramm (RFD)');
xlabel('Rotorfrequenz \omega_2/(rad/s)','Fontsize',14);
ylabel('Drehmoment M/(Nm)','Fontsize',14);

% x-Achse:
plot([w2_Vektor_motgen(1), w2_Vektor_motgen(end)],[0,0], ...
    'black-','LineWidth',1);

% y-Achse:
plot([0,0],[-2*M_Psi1Nk,+2*M_Psi1Nk], ...
    'black-','LineWidth',1);

%%

% U_1N-Grenzkurven:
h3 = plot(w2_Vektor_motgen,RFD_U1N_a_Vektor, ...
    'green:','LineWidth',2);
h4 = plot(w2_Vektor_motgen,RFD_U1N_b_Vektor, ...
    'green-','LineWidth',2);
h5 = plot(w2_Vektor_motgen,RFD_U1N_c_Vektor, ...
    'green-.','LineWidth',2);
h6 = plot(w2_Vektor_motgen,RFD_U1N_d_Vektor, ...
    'green-','Color',Plot_meingruen,'LineWidth',2);
h7 = plot(w2_Vektor_motgen,RFD_U1N_e_Vektor, ...
    'green--','LineWidth',2);

% U_1N-HOP-Kurve:
h8 = plot(RFD_U1N_HOP_Matrix_3(2,:),RFD_U1N_HOP_Matrix_3(3,:), ...
    'green--','Color',Plot_meingruen,'LineWidth',2);

% Psi_1N-Grenzkurve:
h2 = plot(w2_Vektor_motgen,RFD_Psi1N_Vektor, ...
    'blue-','LineWidth',2);


% +w_2Psi1k:
h12 = plot([w_2Psi1k,w_2Psi1k],[-2*M_Psi1Nk,+2*M_Psi1Nk], ...
    'blue--','LineWidth',1);


legend( ...
    [h3 h4 h5 h6 h7 h8 h2 h12],{ ...
        'U_{1N}-Grenzkurve f�r \Omega = \Omega_�', ...
        'U_{1N}-Grenzkurve f�r \Omega = \Omega_{HOP}', ...
        'U_{1N}-Grenzkurve f�r \Omega = \Omega_N', ...
        'U_{1N}-Grenzkurve f�r \Omega = 2*\Omega_N', ...
        'U_{1N}-Grenzkurve f�r \Omega = \Omega_{max}', ...
        'U_{1N}-HOP-Kurve', ...
        '\Psi_{1N}-Grenzkurve', ...
        '\pm\omega_{2\Psi_1k}'...
    }, ...
    'Location','NorthWest' ...
);

