% Dieses Skript schreibt folgende Zeilenvektoren in den Workspace:
    % W_Vektor
    % W_Vektor_lang (f�r U_1N-HOP-Kurve im RFD)
    % M_Vektor_mot
    % M_Vektor_gen
    % M_Vektor_motgen
    % w2_Vektor_mot
    % w2_Vektor_gen
    % w2_Vektor_motgen (f�r das Rotorfrequenzdiagramm RFD)    
% Diese werden sp�ter ben�tigt werden, z. B. bei der Berechnung von
% Kennfeld-Matrizen oder bei Plots.

%% W_Vektor

W_Vektor = 0:W_Schrittweite:W_max; % Werte von 0 aufsteigend;
% Ist W_max/W_Schrittweite nicht ganzzahlig, so ist der exakte Wert von
% W_max am Ende des Vektors nat�rlich nicht enthalten. Der Vektor endet 
% dann mit einem etwas kleineren Wert, n�mlich mit
% (floor(W_max/W_Schrittweite))*W_Schrittweite.

W_Vektor_lang = 0:W_Schrittweite:(2*W_max); % f�r ben�tigt f�r die
    % U_1N-HOP-Kurve im RFD;

%% M_Vektor_motgen

M_Vektor_mot = 0:M_Schrittweite:(M_Psi1Nk); % Werte von 0 aufsteigend;
% Ist M_N/M_Schrittweite nicht ganzzahlig, so ist der exakte Wert von +M_N
% am Ende des Vektors nat�rlich nicht enthalten. Der Vektor endet dann mit 
% einem etwas kleineren Wert, n�mlich mit
% (floor(M_N/M_Schrittweite))*M_Schrittweite.

M_Vektor_gen = 0:(-M_Schrittweite):(-M_Psi1Nk);
M_Vektor_gen = fliplr(M_Vektor_gen);% Werte von -M_N aufsteigend zur 0;
% fliplr() kehrt die Reihenfolge der Eintr�ge um.
% Die 0 ist auf jeden Fall enthalten (an letzter Stelle).
% Ist M_N/M_Schrittweite nicht ganzzahlig, so ist der exakte Wert von -M_N
% am Anfang des Vektors nat�rlich nicht enthalten. Der Vektor beginnt dann 
% mit einem etwas gr��eren Wert, n�mlich mit
% -(floor(M_N/M_Schrittweite))*M_Schrittweite.

hilfszahl_M_mot = length(M_Vektor_mot) - 1;
hilfszahl_M_gen = length(M_Vektor_gen) - 1;
% Beide Hilfszahlen m�ssen nat�rlich gleich lang sein!
% Bsp.: 
% Wenn M_Vektor_mot = [  0,+10,+20,+30,+40] ist,
    % dann ist hilfszahl_M_mot = 4.
% Wenn M_Vektor_gen = [-40,-30,-20,-10,  0] ist,
    % dann ist hilfszahl_M_gen = 4.
    
M_Vektor_motgen = zeros(1,hilfszahl_M_gen + 1 + hilfszahl_M_mot);
% Initialisierung;
% genau eine Zeile; Das +1 in der Mitte steht f�r die M-Wert 0.
% Jetzt wird M_Vektor_motgen so aufgef�llt, dass die Indices im
% Generator-Bereich beginnen und im Motor-Bereich enden:
M_Vektor_motgen(1:(hilfszahl_M_gen +1)) = M_Vektor_gen;
M_Vektor_motgen((hilfszahl_M_gen + 1): end) = M_Vektor_mot;
% "end" ist hier gleichbedeutend mit hilfszahl_M_mot + 1 + hilfszahl_M_gen.

% M_Vektor_motgen k�nnte z. B. so aussehen:
    % [-40,-30,-20,-10,0,+10,+20,+30,+40]

clearvars hilfszahl_M_mot hilfszahl_M_gen % werden nicht mehr ben�tigt;

%% w2_Vektor_motgen:

w2_Vektor_mot = 0:w2_Schrittweite:(1.1*w_2Psi1k); 
% Werte von 0 aufsteigend;
% "1.1*", damit w_2Psi1k sicher im Vektor enthalten ist, vorausgesetzt,
% w2_Schrittweite wurde nicht extrem gro� gew�hlt;

w2_Vektor_gen = 0:(-w2_Schrittweite):(-1.1*w_2Psi1k);
w2_Vektor_gen = fliplr(w2_Vektor_gen);
% fliplr() kehrt die Reihenfolge der Eintr�ge um.
% Die 0 ist auf jeden Fall enthalten (an letzter Stelle).

hilfszahl_w2_mot = length(w2_Vektor_mot) - 1;
hilfszahl_w2_gen = length(w2_Vektor_gen) - 1;
% Beide Hilfszahlen m�ssen nat�rlich gleich lang sein!
% Bsp.: 
% Wenn w2_Vektor_mot = [  0,+10,+20,+30,+40] ist,
    % dann ist hilfszahl_w2_mot = 4.
% Wenn w2_Vektor_gen = [-40,-30,-20,-10,  0] ist,
    % dann ist hilfszahl_w2_gen = 4.
    
w2_Vektor_motgen = zeros(1,hilfszahl_w2_gen + 1 + hilfszahl_w2_mot);
% Initialisierung;
% genau eine Zeile; Das +1 in der Mitte steht f�r die w2-Wert 0.
% Jetzt wird w2_Vektor_motgen so aufgef�llt, dass die Indices im
% Generator-Bereich beginnen und im Motor-Bereich enden:
w2_Vektor_motgen(1:(hilfszahl_w2_gen +1)) = w2_Vektor_gen;
w2_Vektor_motgen((hilfszahl_w2_gen + 1): end) = w2_Vektor_mot;
% "end" ist hier gleichbedeutend mit 
% hilfszahl_w2_mot + 1 + hilfszahl_w2_gen.

% w2_Vektor_motgen k�nnte z. B. so aussehen:
    % [-40,-30,-20,-10,0,+10,+20,+30,+40]

clearvars hilfszahl_M_mot hilfszahl_M_gen ... % werden nicht mehr ben�tigt;
    hilfszahl_w2_mot hilfszahl_w2_gen