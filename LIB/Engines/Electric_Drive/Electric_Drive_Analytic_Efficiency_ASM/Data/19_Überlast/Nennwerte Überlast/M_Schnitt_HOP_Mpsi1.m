% Dieses Skript wirft den Momementenwert aus bei welchem sich die U1_HOP 
% bzw. die U1_TI Kurve und die MPsi1N Kurve schneiden. 
% Mit diesem Momentenwerte kann dann der dazugehörige w2 und W Wert
% aus der Matrix rausgelesen werden.

smax = length(W_Vektor_lang); % höchster Spaltenindex

 for s=1:smax
     
   M_HOP=  min(abs(Momentenmatrix_HOPw2_in_MPsi1(1,s)-RFD_U1N_HOP_Matrix_3(3,s)));
    
 end

 for s=1:smax
    
   M_TIP= min(abs(Momentenmatrix_TIPw2_in_MPsi1(1,s)-RFD_U1N_TIP_Matrix_3(3,s)));
   
 end