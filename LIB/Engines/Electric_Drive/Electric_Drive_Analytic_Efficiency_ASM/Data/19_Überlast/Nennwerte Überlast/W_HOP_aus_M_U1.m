% Dieses Skript wirft den gro� Omegawert aus bei welchem sich die U1_HOP
% bzw. die U1_TIP Kurve und die MPsi1N Kurve schneiden.


% Maschinenkonstanten_10:
           m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
         R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
         L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);
%% HOP:

aa=L_1^2*R_2^2+w2_HOP^2*sigma^2*L_1^2*L_2^2;
bb=2*L_1*L_2*R_1*R_2*w2_HOP-2*L_1*L_2*R_1*R_2*w2_HOP*sigma;
cc=R_1^2*R_2^2+w2_HOP^2*L_2^2*R_1^2-U_1N^2*m*p*w2_HOP*L_h^2*R_2/M_HOP;

w_1_HOP_pos= (-bb+(bb^2-4*aa*cc)^(1/2))/(2*aa);
w_1_HOP_neg= (-bb-(bb^2-4*aa*cc)^(1/2))/(2*aa);   

W_HOP=(w_1_HOP_pos-w2_HOP)/p;

%% TIP:

w2_TIPn=-w2_TIP;

aaa=L_1^2*R_2^2+w2_TIPn^2*sigma^2*L_1^2*L_2^2;
bbb=2*L_1*L_2*R_1*R_2*w2_TIPn-2*L_1*L_2*R_1*R_2*w2_TIPn*sigma;
ccc=R_1^2*R_2^2+w2_TIPn^2*L_2^2*R_1^2-U_1N^2*m*p*w2_TIPn*L_h^2*R_2/M_TIP;

w_1_TIP_pos= (-bbb+(bbb^2-4*aaa*ccc)^(1/2))/(2*aaa);
w_1_TIP_neg= (-bbb-(bbb^2-4*aaa*ccc)^(1/2))/(2*aaa);   

W_TIP=(w_1_TIP_pos-w2_TIPn)/p;




clearvars aa bb cc w2_TIPn aaa bbb ccc