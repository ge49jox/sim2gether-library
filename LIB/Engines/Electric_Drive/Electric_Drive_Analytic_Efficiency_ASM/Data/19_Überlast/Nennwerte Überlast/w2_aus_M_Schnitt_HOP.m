% Dieses Skript berechnet die Rotorfrequenz die sich aus dem Schnittpunkt
% der HOP/TIP-Kurve und der MPsi1 Kurve ergibt!



% Maschinenkonstanten_10:
           m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);
   
%% HOP:       
       Psi_1=Psi_1N;
       M=M_HOP;

w2_HOP=w2_aus_Psi1_M(Psi_1,M,Maschinenkonstanten_10);

%% TIP:
       Psi_1=Psi_1N;
       M=M_TIP;

w2_TIP=-w2_aus_Psi1_M(Psi_1,M,Maschinenkonstanten_10);

    
    