% Dieses Skript setzt alle w2 Werte aus der HOP/TIP Matrix in die
% Statorflussbasierte Drehmomentgleichung ein und speichert die
% entstehenden Momentenwerte in eine Matrix: 
% Momentenmatrix_HOPw2_in_MPsi1_eingesetzt 
% Momentenmatrix_TIPw2_in_MPsi1_eingesetzt
% Diese Werte k�nnen nun mit einem Wert aus der Momentenspalte der HOP
% Matrix verglichen werden und der dazugeh�rige w2 und W Wert abgelesen
% werden. Geschieht in einem seperaten Skript.

smax = length(W_Vektor_lang); % h�chster Spaltenindex
Momentenmatrix_HOPw2_in_MPsi1=zeros(1,smax); % Null besetzen
Momentenmatrix_TIPw2_in_MPsi1=zeros(1,smax);


for s=1:smax
    
   
Psi_1=Psi_1N;
w_2=RFD_U1N_HOP_Matrix_3(2,s);
           m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
         L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);
       
       
Momentenmatrix_HOPw2_in_MPsi1(1,s)=M_aus_Psi1_w2(Psi_1,w_2,Maschinenkonstanten_10);

end



for s=1:smax
    
   
Psi_1=Psi_1N;
w_2=RFD_U1N_TIP_Matrix_3(2,s);
           m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
         L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);
       
       
Momentenmatrix_TIPw2_in_MPsi1(1,s)=M_aus_Psi1_w2(Psi_1,w_2,Maschinenkonstanten_10);

end

