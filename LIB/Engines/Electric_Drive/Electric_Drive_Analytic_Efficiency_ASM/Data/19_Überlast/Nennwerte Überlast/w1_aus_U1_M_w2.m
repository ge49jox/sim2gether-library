function [ w1 ] = w1_aus_U1_M_w2(U_1,M,w_2,Maschinenkonstanten_10)

% Diese Funktion berechnet die Winkelgeschwindigkeit aus der Statorstrangspannung 
% U_1, der Statorfrequenz w_1 und dem Drehmoment M.

%% Einlesen der ASM-Parameter:
% Parameter, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.
 
% Maschinenkonstanten_10:
           m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
         R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
         L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);

  
%% Berechnung von w1:

aa=L_1^2*R_2^2+w_2^2*sigma^2*L_1^2*L_2^2;
bb=2*L_1*L_2*R_1*R_2*w_2-2*L_1*L_2*R_1*R_2*w_2*sigma;
cc=R_1^2*R_2^2+w_2^2*L_2^2*R_1^2-U_1^2*m*p*w_2*L_h^2*R_2/M;

w_1= (-bb+(bb^2-4*aa*cc)^(1/2))/(2*aa);
end





