function [W]=W_mot_ue_aus_ASM_Daten_f(Maschinenkonstanten_10, ...
    Betriebsgrenzen_4, ...
    charakt_w2_M_5...
    )
    
%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);
       
% Betriebsgrenzen_4:
      I_1N = Betriebsgrenzen_4(1);
      U_1N = Betriebsgrenzen_4(2);
    Psi_1N = Betriebsgrenzen_4(3);
     W_max = Betriebsgrenzen_4(4);

% charakt_w2_M_5:
    % w_2opt = charakt_w2_M_5(1);
      w_2I1k = charakt_w2_M_5(2);
    w_2Psi1k = charakt_w2_M_5(3);
      M_I1Nk = charakt_w2_M_5(4);
    M_Psi1Nk = charakt_w2_M_5(5);
    
%%  Berechnung von W_mot_ue
% W_mot_ue muss so gew�hlt sein, dass folgende Bedingung erf�llt ist:
% M_aus_U1_w2_W(U_1N,+w_2Psi1k,W_mot_ue,...) = +M_Psi1Nk;
% => Nullstellenproblem: M_aus_U1_w2_W(U_1N,+w_2Psi1k,W_mot_ue,...) - M_Psi1Nk = 0;
% Diese Gleichung hat immer genau eine L�sung mit klarem Vorzeichenwechsel
% (siehe RFD). => fzero ist sehr gut geeignet!

Hilfsfunktion1 = @(a,b,x,c) M_aus_U1_w2_W(a,b,x,c) - M_Psi1Nk; 
    a = U_1N;
    b = +w_2Psi1k;
    % x = W_mot_ue, wird gesucht
    c = Maschinenkonstanten_10;
Hilfsfunktion2 = @(x) Hilfsfunktion1(a,b,x,c);
W_mot_ue = fzero(Hilfsfunktion2,[0,W_max]);
% Hiermit ist W_mot_ue bestimmt.

%% Berechnung von P_mech_ue

P_mech_ue = +M_Psi1Nk*W_mot_ue;
