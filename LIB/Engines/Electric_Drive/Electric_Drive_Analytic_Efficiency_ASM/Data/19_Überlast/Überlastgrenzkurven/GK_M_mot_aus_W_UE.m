function [ M ] = GK_M_mot_aus_W_UE( ...
    W, ...
    Maschinenkonstanten_10, ...
    Betriebsgrenzen_4, ...
    Nennwerte_8, ...
    Crossoverwerte_6, ...
    Ueberlast_Matrix ...
)

%% Wichtige Hinweise:

% Diese Funktion berechnet f�r eine bestimmte Winkelgeschwindigkeit W
% (W>=0) das h�chstm�glich erzeugbare motorische Drehmoment M mit �berlast.

% GK: Grenzkurve
% RFD: Rotorfrequenzdiagramm;
    % Horizontalachse: w_2
    % Vertikalachse:   M


%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
            m = Maschinenkonstanten_10(1);       
            p = Maschinenkonstanten_10(2);         
        % R_1 = Maschinenkonstanten_10(3);       
          R_2 = Maschinenkonstanten_10(4);       
        % L_h = Maschinenkonstanten_10(5);       
   % L_sigma1 = Maschinenkonstanten_10(6);  
   % L_sigma2 = Maschinenkonstanten_10(7);  
          L_1 = Maschinenkonstanten_10(8);       
          L_2 = Maschinenkonstanten_10(9);      
        sigma = Maschinenkonstanten_10(10);
      
% Betriebsgrenzen_4:
      I_1N = Betriebsgrenzen_4(1);
      U_1N = Betriebsgrenzen_4(2);
    Psi_1N = Betriebsgrenzen_4(3);
   % W_max = Betriebsgrenzen_4(4);

% charakt_w2_M_5 wird nicht als Argument �bergeben, rentiert sich nicht;
    w_2Psi1k = R_2/(sigma*L_2);

% Nennwerte_8:
          w_2N = Nennwerte_8(1);
           M_N = Nennwerte_8(2);  
        W_Nmot = Nennwerte_8(3);  
      % W_Ngen = Nennwerte_8(4);  
     % w_1Nmot = Nennwerte_8(5);  
     % w_1Ngen = Nennwerte_8(6);  
  % P_mechNmot = Nennwerte_8(7);  
  % P_mechNgen = Nennwerte_8(8);

  
% Crossoverwerte_6:
     W_Cmot = Crossoverwerte_6(1);
   % W_Cgen = Crossoverwerte_6(2);
  % w_2Cmot = Crossoverwerte_6(3);
  % w_2Cgen = Crossoverwerte_6(4);
   % M_Cmot = Crossoverwerte_6(5);
   % M_Cgen = Crossoverwerte_6(6);
   
 % �berlastwerte
 M_HOP=     Ueberlast_Matrix (1);
 W_mot_ue=  Ueberlast_Matrix (2);
 w2_HOP=    Ueberlast_Matrix (3);
 W_HOP=     Ueberlast_Matrix (4);

 M_Psi1Nk= Psi_1N^2*(m*p/2)*((1-sigma)/(sigma*L_1));


%% Berechnung von M:

if W < 0  
    error('Die Winkelgeschwindigkeit W darf nicht negativ sein!');
    
elseif W <= W_mot_ue % d. h. 0 <= W <= W_Nmot
    M = +M_Psi1Nk;
    
elseif W < W_HOP % d. h. W_mot_ue < W < W_HOP
    % Berechne den nichttrivialen Schnittpunkt (ntsp) der U_1N-Grenzkurve
    % mit der Psi1_N-Grenzkurve im RFD. -> w2ntsp;
    
    Hilfsfunktion1 = @(a,b,x,c,d) M_aus_Psi1_w2(a,x,d) - ... % Minus!
                                M_aus_U1_w2_W(b,x,c,d);
        a = Psi_1N;
        b = U_1N;
        % x = w_2, wird gesucht;
        c = W;
        d = Maschinenkonstanten_10;
        
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,b,x,c,d);   
    w2ntsp = fzero(Hilfsfunktion2,[0.9*w_2N,w_2Psi1k]);
    % Das "0.9*" dient nur der Sicherheit, damit das Intervall etwas
    % gr��er ist.
    M = M_aus_U1_w2_W(U_1N,w2ntsp,W,Maschinenkonstanten_10);
    % einfach eingesetzt;
    % Einsetzen in M_aus_U1_w2_W(U_1N,w2ntsp,W,...) w�rde ebenso gehen.
     
else % d. h. W_HOP <= W   
    % Berechne Hochpunkt (w2HOP,MHOP) der U_1N-Grenzkurve im RFD.
    % Matlab kennt aber nur Minimierungsprobleme.
    % L�sung: Ein Maximum von f(x) ist ein Minimum von -f(x)

    Hilfsfunktion1 = @(a,x,b,c) -M_aus_U1_w2_W(a,x,b,c); % Minuszeichen!
        a = U_1N;
        % x = w2HOP, wird gesucht, aber sp�ter nicht ben�tigt => ~
        b = W;
        c = Maschinenkonstanten_10;
        
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,x,b,c);
    [~,MHOP] = fminbnd(Hilfsfunktion2,w_2N,w_2Psi1k);
    % Dieses Minimum (bzw. Maximum) existiert immer!
    MHOP = abs(MHOP); % Der MHOP-Wert, der von fminbnd geliefert wird,
        % ist negativ, da in Hilfsfunktion1 ein Minuszeichen gesetzt
        % werden musste. Der gesuchte MHOP-Wert ist nat�rlich positiv!
    M = MHOP;
    
end


end % Hier endet die Funktion.   

