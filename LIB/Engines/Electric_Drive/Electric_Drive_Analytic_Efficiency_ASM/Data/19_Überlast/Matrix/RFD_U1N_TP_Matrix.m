%% U_1N-HOP-Kurve: Menge aller Hochpunkte der U_1N-Grenzkurven:
% Dazu geh�rt eine Matrix mit 3 Zeilen und ca. doppelt sovielen Spalten,
% wie W_Vektor Elemente hat.

% 1. Zeile: W_Vektor_lang;
% 2. Zeile: w_2-Werte;
% 3. Zeile: zugeh�rige M-Werte;

smax = length(W_Vektor_lang);

RFD_U1N_TP_Matrix_3 = zeros(3,smax); % Initialisierung;

RFD_U1N_TP_Matrix_3(1,:) = W_Vektor_lang;

for s = 1:smax
    
    W = W_Vektor_lang(s);
    
    Hilfsfunktion1 = @(a,x,b,c) -M_aus_U1_w2_W(a,x,b,c);
        % Minuszeichen! wegen fminbnd!
        
    a = U_1N;
    % x = w_2, wird gesucht;
    b = W;
    c = Maschinenkonstanten_10;
    
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,x,b,c);
    
   
    
    [w_2,TP] = fminbnd(-w_2Psi1k,0,Hilfsfunktion2);
    %TP = abs(TP); % abs() nicht vergessen! wegen fminbnd!
    
    RFD_U1N_TP_Matrix_3(2,s) = w_2;
    RFD_U1N_TP_Matrix_3(3,s) = TP;
    
end

clearvars smax s a b c... % wird nicht mehr ben�tigt;
    Hilfsfunktion1 Hilfsfunktion2 W w_2