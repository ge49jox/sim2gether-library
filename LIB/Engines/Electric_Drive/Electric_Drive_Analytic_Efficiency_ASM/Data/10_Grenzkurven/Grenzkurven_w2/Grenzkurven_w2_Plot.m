%% Wichtige Hinweise:

% Dieses Skript plottet die 2 Grenzkurven der folgenden Matrix:
    % GK_w2_W_Matrix_2
    
    
%% alte gca und gcf l�schen:
clearvars gca gcf
        
%% Kurven plotten:

gcf = figure( ...
    'PaperUnits','centimeters', ...
    'Units','centimeters', ...
    'Position',[ ...
        Plot_figure_x, ...
        Plot_figure_y, ...
        1.3*Plot_figure_Breite, ...
        1.1*Plot_figure_Hoehe ...
    ], ...
    'Resize',Plot_Resize_String ...
);

gca = axes( ...
    'XLim',[Plot_x_min, Plot_x_max], ...
    'YLim',[1.2*(-w_2Psi1k), 1.2*(+w_2Psi1k)] ...    
);

% "*1.2", um den Anzeigebereich etwas zu vergr��ern;

hold on;
grid on;
set(gca,'layer',Plot_Layer_String);

box on;

title('Grenzkurven der Rotorfrequenz');
xlabel('Winkelgeschwindigkeit \Omega/(rad/s)');
ylabel('Rotorfrequenz \omega_2/(rad/s)');

% x-Achse:
plot([0,Plot_x_max],[0,0],'black-','LineWidth',1);

% Rotorfrequenz-Grenzkurven:
h2 = plot(W_Vektor,GK_w2_W_Matrix_2(1,:), ...
    Plot_GK_mot_LineStyle_String,'LineWidth',Plot_GK_LineWidth);
h7 = plot(W_Vektor,GK_w2_W_Matrix_2(2,:), ...
    Plot_GK_gen_LineStyle_String,'LineWidth',Plot_GK_LineWidth);

% horizontale Linien der optimalen Rotorfrequenz:
h4 = plot([0,Plot_x_max],[+w_2opt,+w_2opt],'red-','LineWidth',1);
h5 = plot([0,Plot_x_max],[-w_2opt,-w_2opt],'green-','LineWidth',1);

% horizontale Linien der Nenn-Rotorfrequenz:
h3 = plot([0,Plot_x_max],[+w_2N,+w_2N],'red-.','LineWidth',1);
h6 = plot([0,Plot_x_max],[-w_2N,-w_2N],'green-.','LineWidth',1);

% horizontale Linien der Psi_1-Kipp-Rotorfrequenz:
h1 = plot([0,Plot_x_max],[+w_2Psi1k,+w_2Psi1k],'red:','LineWidth',2);
h8 = plot([0,Plot_x_max],[-w_2Psi1k,-w_2Psi1k],'green:','LineWidth',2);

legend( ...
    [h1 h2 h3 h4 h5 h6 h7 h8],{ ...
        'motorische Statorfluss-Kippfrequenz (+\omega_{2\Psi 1 k})', ...
        'motorische Grenzkurve von \omega_2', ...
        'motorische Nenn-Rotorfrequenz (+\omega_{2N})', ...
        'motorische optimale Rotorfrequenz (+\omega_{2opt})', ...  
        'generatorische optimale Rotorfrequenz (-\omega_{2opt})', ... 
        'generatorische Nenn-Rotorfrequenz (-\omega_{2N})', ...
        'generatorische Grenzkurve von \omega_2', ...
        'generatorische Statorfluss-Kippfrequenz (-\omega_{2\Psi 1 k})' ...
    }, ...
    'Location','NorthWest' ...
);







clearvars a b c exponent ... % werden nicht mehr ben�tigt
    gca gcf X Y C h ... 
    hcb colorTitleHandle titleString ....
    h1 h2 h3 h4 h5 h6 h7 h8
    
