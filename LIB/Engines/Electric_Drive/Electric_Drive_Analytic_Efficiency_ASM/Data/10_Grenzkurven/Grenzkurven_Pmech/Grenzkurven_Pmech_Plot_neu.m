%% Wichtige Hinweise:

% Dieses Skript plottet die 2 Grenzkurven der folgenden Matrix:
    % GK_Pmech_W_Matrix_2
    
    
%% alte gca und gcf l�schen:
clearvars gca gcf
        
%% Kurven plotten:

gcf = figure( ...
    'PaperUnits','centimeters', ...
    'Units','centimeters', ...
    'Position',[ ...
        Plot_figure_x, ...
        Plot_figure_y, ...
        1.3*Plot_figure_Breite, ...
        1.1*Plot_figure_Hoehe ...
    ], ...
    'Resize',Plot_Resize_String ...
);

gca = axes( ...
    'XLim',[Plot_x_min, Plot_x_max], ...
    'YLim',[(1/1000)*1.4*P_gen_ue, (1/1000)*1.4*abs(P_gen_ue)] ...    
);
% "(1/1000)", damit P_mech in kW angezeigt wird;
% "*1.4", um den Anzeigebereich etwas zu vergr��ern;

hold on;
grid on;
set(gca,'layer',Plot_Layer_String);

box on;

title('Grenzkurven der mechanischen Leistung inkl. �berlast');
xlabel('Winkelgeschwindigkeit \Omega/(rad/s)');
ylabel('mechanische Leistung P_{mech}/(kW)');

% x-Achse:
plot([0,Plot_x_max],[0,0],'black-','LineWidth',1);

% Drehmoment-Grenzkurven:
h1 = plot(W_Vektor,(1/1000)*GK_Pmech_W_Matrix_2(1,:), ...
    Plot_GK_mot_LineStyle_String,'LineWidth',Plot_GK_LineWidth);
h2 = plot(W_Vektor,(1/1000)*GK_Pmech_W_Matrix_2(2,:), ...
    Plot_GK_gen_LineStyle_String,'LineWidth',Plot_GK_LineWidth);
% "(1/1000)", damit P_mech in kW angezeigt wird.

legend( ...
    [h1 h2],{ ...
        'motorische Grenzkurve von P_{mech}', ...
        'generatorische Grenzkurve von P_{mech}' ...
    }, ...
    'Location','East' ...
);


%% Verbesserung der Tick-Anzeige auf der P_mech-Achse:

% Bsp.: P_mechNgen = -163500; (nat�rlich in Watt!)        
a = 2*abs(P_gen_ue/1000)/10; % Schrittweite bei der Anzeige von P_el
    % auf der Colorbar; Bsp.: a = 32.7;
exponent = floor(log10(a)); % Bsp: exponent = floor(1.5145) = 1;    
b = floor(a/(10^exponent)); % Bsp.: b = floor(3.27) = 3
% Auswahl: 1,2,5: Suche jene Zahl heraus, die b "am n�chsten" liegt:
if (1 <= b)&&(b < 2)
   c = 1;
elseif (2 <= b)&&(b < 5)
    c = 2; % Bsp.: c = 2
elseif (5 <= b)&&(b <= 9)
    c = 5;
end

a = c*10^exponent; % Bsp: a = 2*10^1 = 20;
% => aus einer Schrittweite von 32.7 wurde eine "sch�nere" Schrittweite
    % von 20.
 
ytickvec = ...
    a*floor((1/a)*(1.4*P_gen_ue)/1000):(a): ...
    a*ceil((1/a)*abs(1.4*P_gen_ue)/1000);
    % sorgt daf�r, dass bei a-Schritten der Anfangswert des Vektors 
    % durch a teilbar ist, damit auch die 0 enthalten ist.
    % "(1/1000)", damit in kW;
    % "*1.4", um den Anzeigebereich etwas zu vergr��ern;
    
set(gca,'YTick',ytickvec);



clearvars a b c exponent ... % werden nicht mehr ben�tigt
    gca gcf X Y C h ... 
    hcb colorTitleHandle titleString ....
    h1 h2 h3 h4 h5 h6 h7 h8 ...
    ytickvec
    
