function [ M ] = GK_M_optgen_aus_W( ...
    W, ...
    Maschinenkonstanten_10, ...
    Betriebsgrenzen_4, ...
    Optimalwerte_3 ...
)

%% Wichtige Hinweise:

% Diese Funktion berechnet f�r eine bestimmte Winkelgeschwindigkeit W
% (W>=0) das kleinststm�glich erzeugbare generatorische Drehmoment M bei
% Einhaltung der optimalen Betriebsstrategie, d. h. w_2 = -w_2opt.

% GK: Grenzkurve
% RFD: Rotorfrequenzdiagramm;
    % Horizontalachse: w_2
    % Vertikalachse:   M


%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
          % m = Maschinenkonstanten_10(1);       
          % p = Maschinenkonstanten_10(2);         
          R_1 = Maschinenkonstanten_10(3);       
          R_2 = Maschinenkonstanten_10(4);       
          L_h = Maschinenkonstanten_10(5);       
   % L_sigma1 = Maschinenkonstanten_10(6);  
   % L_sigma2 = Maschinenkonstanten_10(7);  
        % L_1 = Maschinenkonstanten_10(8);       
          L_2 = Maschinenkonstanten_10(9);      
      % sigma = Maschinenkonstanten_10(10);
       
% Betriebsgrenzen_4:
    % I_1N = Betriebsgrenzen_4(1);
      U_1N = Betriebsgrenzen_4(2);
  % Psi_1N = Betriebsgrenzen_4(3);
   % W_max = Betriebsgrenzen_4(4);

% charakt_w2_M_5 wird nicht als Argument �bergeben, rentiert sich nicht;
    w_2opt = R_2/sqrt(L_2^2 + (R_2/R_1)*L_h^2);

% Optimalwerte_3:
       M_Nopt = Optimalwerte_3(1);
  % W_Noptmot = Optimalwerte_3(2);
    W_Noptgen = Optimalwerte_3(3);


%% Berechnung von M:

% Das RFD besitzt drei Grenzkurven, je f�r Psi_1N, U_1N und I_1N.

if W < 0
    error('Die Winkelgeschwindigkeit W darf nicht negativ sein!'); 
elseif W <= W_Noptgen % d. h. 0 <= W <= W_Noptgen
    M = -M_Nopt; % Minuszeichen!
else % d. h. W_Noptgen < W
    M = M_aus_U1_w2_W(U_1N,-w_2opt,W,Maschinenkonstanten_10);
    % Minuszeichen bei w_2opt!
end
    

end % Hier endet die Funktion.






