function [ M ] = GK_M_gen_aus_W( ...
    W, ...
    Maschinenkonstanten_10, ...
    Betriebsgrenzen_4, ...
    Nennwerte_8, ...
    Crossoverwerte_6 ...
)


%% Wichtige Hinweise:

% Diese Funktion berechnet f�r eine bestimmte Winkelgeschwindigkeit W
% (W>=0) das kleinstm�glich erzeugbare generatorische Drehmoment M.

% GK: Grenzkurve
% RFD: Rotorfrequenzdiagramm;
    % Horizontalachse: w_2
    % Vertikalachse:   M


%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
          % m = Maschinenkonstanten_10(1);       
          % p = Maschinenkonstanten_10(2);         
        % R_1 = Maschinenkonstanten_10(3);       
          R_2 = Maschinenkonstanten_10(4);       
        % L_h = Maschinenkonstanten_10(5);       
   % L_sigma1 = Maschinenkonstanten_10(6);  
   % L_sigma2 = Maschinenkonstanten_10(7);  
        % L_1 = Maschinenkonstanten_10(8);       
          L_2 = Maschinenkonstanten_10(9);      
        sigma = Maschinenkonstanten_10(10);
       
% Betriebsgrenzen_4:
      I_1N = Betriebsgrenzen_4(1);
      U_1N = Betriebsgrenzen_4(2);
  % Psi_1N = Betriebsgrenzen_4(3);
   % W_max = Betriebsgrenzen_4(4);

% charakt_w2_M_5 wird nicht als Argument �bergeben, rentiert sich nicht;
    w_2Psi1k = R_2/(sigma*L_2);

% Nennwerte_8:
          w_2N = Nennwerte_8(1);
           M_N = Nennwerte_8(2);  
      % W_Nmot = Nennwerte_8(3);  
        W_Ngen = Nennwerte_8(4);  
     % w_1Nmot = Nennwerte_8(5);  
     % w_1Ngen = Nennwerte_8(6);  
  % P_mechNmot = Nennwerte_8(7);  
  % P_mechNgen = Nennwerte_8(8);
  
% Crossoverwerte_6:
   % W_Cmot = Crossoverwerte_6(1);
     W_Cgen = Crossoverwerte_6(2);
  % w_2Cmot = Crossoverwerte_6(3);
  % w_2Cgen = Crossoverwerte_6(4);
   % M_Cmot = Crossoverwerte_6(5);
   % M_Cgen = Crossoverwerte_6(6);


%% Berechnung von M:

if W < 0
    error('Die Winkelgeschwindigkeit W darf nicht negativ sein!');  
    
elseif W <= W_Ngen % d. h. 0 <= W <= W_Ngen
    M = -M_N; % Minuszeichen!
    
elseif W < W_Cgen % d. h. W_Ngen < W < W_Cgen
    % Berechne den nichttrivialen Schnittpunkt (ntsp) der U_1N-Grenzkurve
    % mit der I_1N-Grenzkurve im RFD. -> w2ntsp;
    
    Hilfsfunktion1 = @(a,b,x,c,d) M_aus_I1_w2(a,x,d) - ... % Minus!
                                  M_aus_U1_w2_W(b,x,c,d);
        a = I_1N;
        b = U_1N;
        % x = w_2, wird gesucht; 
        c = W;
        d = Maschinenkonstanten_10;
        
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,b,x,c,d);   
    w2ntsp = fzero(Hilfsfunktion2,[-w_2Psi1k,-0.9*w_2N]); % ist negativ!
    % Das "0.9*" dient nur der Sicherheit, damit das Intervall etwas
    % gr��er ist.
    M = M_aus_I1_w2(I_1N,w2ntsp,Maschinenkonstanten_10); % ist negativ!
    % einfach eingesetzt;
    % Einsetzen in M_aus_U1_w2_W(U_1N,w2ntsp,W,...) w�rde ebenso gehen. 
    
else % d. h. W_Cgen <= W   
    % Berechne Tiefpunkt (w2TIP,MTIP) der U_1N-Grenzkurve im RFD.

    Hilfsfunktion1 = @(a,x,b,c) +M_aus_U1_w2_W(a,x,b,c); % Pluszeichen!
        a = U_1N;
        % x = w2TIP, wird gesucht, aber sp�ter nicht ben�tigt => ~
        b = W;
        c = Maschinenkonstanten_10;
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,x,b,c);
    [~,MTIP] = fminbnd(Hilfsfunktion2,-w_2Psi1k,-w_2N); % ist negativ!
    % Dieses Minimum existiert immer!
    M = MTIP;
    
end


end % Hier endet die Funktion.   




