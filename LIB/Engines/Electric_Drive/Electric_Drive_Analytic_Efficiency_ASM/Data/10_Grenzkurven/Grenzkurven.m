%% Wichtige Hinweise:

% Dieses Skript speichert alle 4 Grenzkurven (GK) ab in der Matrix 
    % GK_M_W_Matrix_4.

% Diese Matrix hat 4 Zeilen. Jede Zeile steht f�r eine GK:
    % 1. Zeile: GK_M_W_mot    (alle Werte positiv)
    % 2. Zeile: GK_M_W_optmot (alle Werte positiv)
    % 3. Zeile: GK_M_W_optgen (alle Werte negativ)
    % 4. Zeile: GK_M_W_gen    (alle Werte negativ)
    
% Diese Matrix hat floor(W_max/W_schrittweite) + 1 Spalten.
  
% M_mot ist das h�chstm�gliche Moment (im Motor-Betrieb) in Abh�ngigkeit
    % der Winkelgeschwindigkeit W.
% M_optmot ist das h�chstm�gliche Moment im Optimal-Betrieb
    % (im Motor-Betrieb) in Abh�ngigkeit der Winkelgeschwindigkeit W.
% M_optgen ist das kleinstm�gliche Moment im Optimal-Betrieb
    % (im Generator-Betrieb) in Abh�ngigkeit der Winkelgeschwindigkeit W.    
% M_gen ist das kleinstm�gliche Moment (im Generator-Betrieb) in 
    % Abh�ngigkeit der Winkelgeschwindigkeit W.

    
% Bevor dieses Skript ausgef�hrt werden kann, m�ssen zuvor andere
    % Skripte ausgef�hrt worden sein (siehe Hauptskript.m).
    % Dazu geh�rt auch Vektoren.m.
    
 
%% Berechnung von GK_M_W_Matrix_4:

smax = length(W_Vektor); % h�chster Spaltenindex;
GK_M_W_Matrix_4 = zeros(4,smax); % Initialisierung;

for s = 1:smax
    
    GK_M_W_Matrix_4(1,s) = GK_M_mot_aus_W( ...
        W_Vektor(s), ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4, ...
        Nennwerte_8, ...
        Crossoverwerte_6 ...
    );
    
    GK_M_W_Matrix_4(2,s) = GK_M_optmot_aus_W( ...
        W_Vektor(s), ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4, ...
        Optimalwerte_3 ...
    );
    
    GK_M_W_Matrix_4(3,s) = GK_M_optgen_aus_W( ...
        W_Vektor(s), ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4, ...
        Optimalwerte_3 ...
    );
    
    GK_M_W_Matrix_4(4,s) = GK_M_gen_aus_W( ...
        W_Vektor(s), ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4, ...
        Nennwerte_8, ...
        Crossoverwerte_6 ...
    );

end    


%% Berechnung von Hyperbeln_M_W_Matrix_4:

% Au�erdem schreibt dieses Skript eine weitere Matrix in den Workspace:
    % Hyperbeln_M_W_Matrix_4;
% Diese Matrix hat genauso viele Zeilen und Spalten wie
    % GK_M_W_Matrix_4.
% Die 4 Zeilen von Hyperbeln_M_W_Matrix_4 bedeuten Folgendes:
    % 1. Zeile: Drehmoment-Hyperbel konstanter Leistung durch den
                % motorischen Nennpunkt;
    % 2. Zeile: Drehmoment-Hyperbel konstanter Leistung durch den
                % Punkt mit W = W_max und h�chstem M-Wert;
    % 3. Zeile: Drehmoment-Hyperbel konstanter Leistung durch den
                % Punkt mit W = W_max und kleinstem M-Wert;
    % 4. Zeile: Drehmoment-Hyperbel konstanter Leistung durch den
                % generatorischen Nennpunkt;

smax = length(W_Vektor); % h�chster Spaltenindex;
Hyperbeln_M_W_Matrix_4 = NaN*ones(4,smax); % Initialisierung;
% In der ersten Spalte (W = 0) soll sp�ter NaN stehen bleiben.

P_mechmotmax_Wmax = GK_M_W_Matrix_4(1,end)*W_Vektor(end);
% h�chstm�gliche mechanische Leistung (Motor-Betrieb) bei h�chster
    % Winkelgeschwindigkeit W = W_max;

P_mechgenmin_Wmax = GK_M_W_Matrix_4(4,end)*W_Vektor(end);
% kleinstm�gliche (betragsm��ig gr��tm�gliche) mechanische Leistung
    % (Generator-Betrieb) bei h�chster Winkelgeschwindigkeit W = W_max;    
    
% P_mechNmot und PmechNgen wurden bereits durch das Skript "Nennwerte.m"
    % berechnet.

for s = 2:smax % Die Schleife muss bei s = 2 beginnen, da bei s = 1
        % W = 0 w�re. D. h. die Hyperbel w�re dort nicht definiert.
    W = (s - 1)*W_Schrittweite; % s = 1 => W = 0 => Passt!
    Hyperbeln_M_W_Matrix_4(1,s) = P_mechNmot/W;
    Hyperbeln_M_W_Matrix_4(2,s) = P_mechmotmax_Wmax/W;
    Hyperbeln_M_W_Matrix_4(3,s) = P_mechgenmin_Wmax/W;
    Hyperbeln_M_W_Matrix_4(4,s) = P_mechNgen/W;
    
end


%% Berechnung von GK_Pmech_W_Matrix_2:

% Au�erdem schreibt dieses Skript eine weitere Matrix in den Workspace:
    % GK_Pmech_W_Matrix_2;
% Die 2 Zeilen von GK_Pmech_W_Matrix_2 bedeuten Folgendes:
    % 1. Zeile: Grenzkurve der mechanischen Leistung P_mech �ber der
                % Winkelgeschwindigkeit W im Motor-Betrieb;
    % 2. Zeile: Grenzkurve der mechanischen Leistung P_mech �ber der
                % Winkelgeschwindigkeit W im Generator-Betrieb;

smax = length(W_Vektor); % h�chster Spaltenindex;
GK_Pmech_W_Matrix_2_Nenn = zeros(2,smax); % Initialisierung;                
                
for s = 1:smax
    
    W = (s - 1)*W_Schrittweite; % s = 1 => W = 0 => Passt!
    GK_Pmech_W_Matrix_2_Nenn(1,s) = GK_M_W_Matrix_4(1,s)*W_Vektor(s);
    % Indices: (1,s) und (1,s)!
    GK_Pmech_W_Matrix_2_Nenn(2,s) = GK_M_W_Matrix_4(4,s)*W_Vektor(s);
    % Indices: (1,s) und (4,s)! 4!
    
end

P_mechmotmax_Nenn = max(GK_Pmech_W_Matrix_2_Nenn(1,:));
% h�chste �berhaupt erreichbare mechanische Leistung (Motor-Betrieb);

P_mechgenmin_Nenn = min(GK_Pmech_W_Matrix_2_Nenn(2,:));
% kleinste (betragsm��ig gr��te) �berhaupt erreichbare mechanische Leistung
    % (Generator-Betrieb);


%% Berechnung von GK_w2_W_Matrix_2:

% Au�erdem schreibt dieses Skript eine weitere Matrix in den Workspace:
    % GK_w2_W_Matrix_2;
% Die 2 Zeilen von GK_w2_W_Matrix_2 bedeuten Folgendes:
    % 1. Zeile: Rotorfrequenz �ber der Winkelgeschwindigkeit W im
            % Motor-Betrieb, damit sich das h�chstm�gliche Drehmoment M
            % einstellt;
    % 2. Zeile: Rotorfrequenz �ber der Winkelgeschwindigkeit W im
            % Generator-Betrieb, damit sich das kleinstm�gliche 
            % (betragsm��ig gr��tm�gliche) Drehmoment M einstellt;

smax = length(W_Vektor); % h�chster Spaltenindex;
GK_w2_W_Matrix_2 = zeros(2,smax); % Initialisierung;                
                
for s = 1:smax
    
    W = (s - 1)*W_Schrittweite; % s = 1 => W = 0 => Passt!
    GK_w2_W_Matrix_2(1,s) = GK_w2_mot_aus_W( ...
            W, ...
            Maschinenkonstanten_10, ...
            Betriebsgrenzen_4, ...
            Nennwerte_8, ...
            Crossoverwerte_6 ...
        );
    GK_w2_W_Matrix_2(2,s) = GK_w2_gen_aus_W( ...
            W, ...
            Maschinenkonstanten_10, ...
            Betriebsgrenzen_4, ...
            Nennwerte_8, ...
            Crossoverwerte_6 ...
        );
    
end




    
clearvars s smax W % wird nicht mehr ben�tigt;
    
    
    
    