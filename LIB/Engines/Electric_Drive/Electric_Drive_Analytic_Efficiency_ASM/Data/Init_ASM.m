function [KF_opt_real_Pelmotgen_Matrix,KF_opt_real_Pelmotgen_Sim_Matrix, W_Vektor, M_Vektor_motgen,GK_M_W_Matrix_6,J,mass,Jx,Jy,Jz ] = Init_ASM( P_n,p, U_n, m_st, n_n, n_max,Material_Stator,Material_Laeufer,Schaltung,Bauweise,Kuehlung,Betriebsstrategie )


%rmpath(genpath(strcat(erase(which('LIB_Electric_Drive'),'\LIB_Electric_Drive.slx'),'\Electric_Drive_Analytic_Efficiency_PSM')))



persistent KF_opt_real_Pelmotgen_Matrix_Old;
persistent KF_opt_real_Pelmotgen_Sim_Matrix_Old;
persistent W_Vektor_Old;
persistent M_Vektor_motgen_Old;
persistent GK_M_W_Matrix_6_Old;


[NewBuild]=CheckIfNewEffMapHorlbeckASMisrequired (P_n,p, U_n, m_st, n_n, n_max,Material_Stator,Material_Laeufer,Schaltung,Bauweise,Kuehlung,Betriebsstrategie)

if NewBuild==1

if strcmp(Betriebsstrategie,'optimal')
[KF_opt_real_Pelmotgen_Matrix,KF_opt_real_Pelmotgen_Sim_Matrix, W_Vektor, M_Vektor_motgen,GK_M_W_Matrix_6  ] =...
    Init_ASM_opt( P_n,p, U_n, m_st, n_n, n_max,Material_Stator,Material_Laeufer,Schaltung,Bauweise,Kuehlung);
end

if strcmp(Betriebsstrategie,'konvetionell')
[KF_konv_real_Pelmotgen_Matrix,KF_konv_real_Pelmotgen_Sim_Matrix, W_Vektor, M_Vektor_motgen,GK_M_W_Matrix_6  ] =...
    Init_ASM_konv( P_n,p, U_n, m_st, n_n, n_max,Material_Stator,Material_Laeufer,Schaltung,Bauweise,Kuehlung);
end

KF_opt_real_Pelmotgen_Matrix_Old=KF_opt_real_Pelmotgen_Matrix;
KF_opt_real_Pelmotgen_Sim_Matrix_Old=KF_opt_real_Pelmotgen_Sim_Matrix;
W_Vektor_Old=W_Vektor;
M_Vektor_motgen_Old=M_Vektor_motgen;
GK_M_W_Matrix_6_Old=GK_M_W_Matrix_6;
end

if NewBuild==0
    
KF_opt_real_Pelmotgen_Matrix=KF_opt_real_Pelmotgen_Matrix_Old;
KF_opt_real_Pelmotgen_Sim_Matrix=KF_opt_real_Pelmotgen_Sim_Matrix_Old;
W_Vektor=W_Vektor_Old;
M_Vektor_motgen=M_Vektor_motgen_Old;
GK_M_W_Matrix_6=GK_M_W_Matrix_6_Old;
  
end

%% Calculation of Mass and J_red und Inertia

typ_EM = 'ASM'
M_n=P_n/(2*pi*n_n/60);

[J,mass] = Traegheit_EM( M_n, n_n, typ_EM);
[Jx,Jy,Jz] = TraegheitAchsen_EM( typ_EM, M_n, n_n );

%addpath(genpath(strcat(erase(which('LIB_Electric_Drive'),'\LIB_Electric_Drive.slx'),'\Electric_Drive_Analytic_Efficiency_PSM')))

end

