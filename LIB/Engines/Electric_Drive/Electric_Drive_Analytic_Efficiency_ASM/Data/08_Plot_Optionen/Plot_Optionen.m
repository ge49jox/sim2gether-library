%% Wichtige Hinweise:
% Dieses Skript definiert wichtige Werte von Plots.
% Diese Werte d�rfen editiert werden, um sie an pers�nliche 
    % Pr�ferenzen anzupassen.
% Werden die Werte in diesem Skript ge�ndert und wird dann das Skript
    % ausgef�hrt, so werden alle Werte einheitlich auf alle Plots
    % �bertragen.

%% Allgemeine Optionen:

Plot_meingruen = [0,0.5,0]; % definiert eigenes gr�n, da das Standard-Gr�n
    % viel zu hell ist;

Plot_figure_x = 1; % x-Wert der figure auf dem Bildschirm;
Plot_figure_y = 2; % y-Wert der figure auf dem Bildschirm;
Plot_figure_Breite  = 20; % Breite der figure;
Plot_figure_Hoehe = 20; % H�he der figure;

Plot_x_min = 0;                      % kleinster x-Wert;
Plot_x_max = W_max + 2*W_Schrittweite; % gr��ter x-Wert;
% Das "2*W_Schrittweite" dient dazu, dass in Kennfeld-Plots die
    % Grenzkurven gerade noch deutlich zu erkennen sind.

Plot_y_min = -M_N - 2*M_Schrittweite;  % kleinster y-Wert;
Plot_y_max = +M_N + 2*M_Schrittweite;  % gr��ter y-Wert;
Plot_y_min_neu = -M_Psi1Nk - 2*M_Schrittweite;  % kleinster y-Wert;
Plot_y_max_neu = +M_Psi1Nk + 2*M_Schrittweite;  % gr��ter y-Wert;

% Das "+-2*M_Schrittweite" dient dazu, dass in Kennfeld-Plots die
    % Grenzkurven gerade noch deutlich zu erkennen sind.
    
Plot_Layer_String = 'top'; % Bei 'top' wird das Gitternetz (grid) �ber
    % alle weiteren Plots gelegt und ist somit sichtbar;
    % Dies ist besonders wichtig bei Kennfeld-Plots.
      
Plot_Resize_String = 'On'; % legt fest, ob sich das figure-Fenster mit der Maus
    % in der Gr��e ver�ndern l�sst;

%% allg. Kennfeld:

Plot_Colorbar_Position_String = 'NorthOutside'; % legt fest, wo die Colorbar
    % erscheinen soll. Bei 'North' wird die Colorbar innerhalb des
    % Achsenbereichs geplottet.

%% eta-Kennfeld:

Plot_Contour_Levels_eta = 0.5:0.01:1; % In Kennfeld-Plots sollen nur
    % diese Wirkungsgrad-Isolinien angezeigt werden.     
Plot_Label_Vektor_eta = [0.8,0.9,0.92,0.94,0.96,0.98];
    % In Kennfeld-Plots sollen nur
    % diese Wirkungsgrad-Isolinien beschriftet werden.

%% P_el-Kennfeld:

% Bsp.: P_mechNgen = -163500; (nat�rlich in Watt!)        
a = 2*abs(P_gen_ue/1000)/10; % Schrittweite bei der Anzeige von P_el
    % auf der Colorbar; Bsp.: a = 32.7;
exponent = floor(log10(a)); % Bsp: exponent = floor(1.5145) = 1;    
b = floor(a/(10^exponent)); % Bsp.: b = floor(3.27) = 3
% Auswahl: 1,2,5: Suche jene Zahl heraus, die b "am n�chsten" liegt:
if (1 <= b)&&(b < 2)
   c = 1;
elseif (2 <= b)&&(b < 5)
    c = 2; % Bsp.: c = 2
elseif (5 <= b)&&(b <= 9)
    c = 5;
end

a = c*10^exponent; % Bsp: a = 2*10^1 = 20;
% => aus einer Schrittweite von 32.7 wurde eine "sch�nere" Schrittweite
    % von 20.

Plot_Colorbar_Tick_Vektor_Pel = ...
    a*floor((1/a)*P_gen_ue/1000):(a):a*ceil((1/a)*abs(P_gen_ue)/1000);
    % sorgt daf�r, dass bei a-Schritten der Anfangswert des Vektors 
    % durch a teilbar ist, damit auch die 0 enthalten ist.
    % "(1/1000)", damit in kW;    
    
% "(a/2)": damit mehr Isolinien und Beschriftungen zu sehen sind als
% die Anzahl der Ticks an der Colorbar;
Plot_Contour_Levels_Pel = ...
    a*floor((1/a)*P_gen_ue/1000):(a/2):a*ceil((1/a)*abs(P_gen_ue)/1000);
    % In Kennfeld-Plots sollen nur diese elektrische-Leistung-Isolinien
        % angezeigt werden.
    % P_mechNgen (negativ!) ist betragsm��ig stets gr��er als P_mechNmot.
    % Das "/1000" deshalb, da P_el in kW angezeigt werden soll.
Plot_Label_Vektor_Pel = ...
    a*floor((1/a)*P_gen_ue/1000):(a/2):a*ceil((1/a)*abs(P_gen_ue)/1000);
    % In Kennfeld-Plots sollen nur diese elektrische-Leistung-Isolinien
        % beschriftet werden. 
           
 clearvars a b c exponent; % wird nicht mehr ben�tigt;
    
%% Grenzkurven:

Plot_GK_LineWidth = 2; % Dicke der Grenzkurven;
Plot_GK_mot_LineStyle_String = 'black-';
Plot_GK_optmot_LineStyle_String = 'blue--';
Plot_GK_optgen_LineStyle_String = 'black--';
Plot_GK_gen_LineStyle_String = 'black-';
Plot_GK_mot_ue_LineStyle_String = 'red-';
Plot_GK_gen_ue_LineStyle_String = 'red-';

Plot_GK_mot_Legend_String    = 'motorische Grenzkurve';
Plot_GK_optmot_Legend_String = 'motorische Optimal-Grenzkurve';
Plot_GK_optgen_Legend_String = 'generatorische Optimal-Grenzkurve';
Plot_GK_gen_Legend_String    = 'generatorische Grenzkurve';
Plot_GK_mot_ue_Legend_String = 'motorische �berlast-Grenzkurve';
Plot_GK_gen_ue_Legend_String = 'generatorische �berlast-Grenzkurve';

%% Fahrzylken:

Plot_Fahrzyklus_LineWidth = 1; % Dicke der Fahrzyklus-Kurve;
Plot_Fahrzyklus_LineStyle_String = 'red-';


