function [ w_2 ] = opt_ideal_w2_aus_M( ...
    M, ...
    Maschinenkonstanten_10 ...
)

%% Wichtige Hinweise:

% Diese Funktion berechnet w_2 so, dass sich das gew�nschte M einstellt,
% bei optimaler Betriebsstrategie im Idealfall.

% optimal ideal: Im Motor-Betrieb     gilt stets: w_2 = +w_2opt.
%                Im Generator-Betrieb gilt stets: w_2 = -w_2opt.

% Nat�rlich kann dies im Realfall nicht immer aufrechterhalten
% werden, aber diese Funktion soll ja den Idealfall zeigen.

% Im Fall "optimal ideal" hat die Winkelgeschwindigkeit W
    % keinerlei Einfluss auf w_2, weshalb sie auch nicht als Argument der
    % Funktion auftaucht.
     
    
%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
          % m = Maschinenkonstanten_10(1);       
          % p = Maschinenkonstanten_10(2);         
          R_1 = Maschinenkonstanten_10(3);       
          R_2 = Maschinenkonstanten_10(4);       
          L_h = Maschinenkonstanten_10(5);       
   % L_sigma1 = Maschinenkonstanten_10(6);  
   % L_sigma2 = Maschinenkonstanten_10(7);  
        % L_1 = Maschinenkonstanten_10(8);       
          L_2 = Maschinenkonstanten_10(9);      
      % sigma = Maschinenkonstanten_10(10);

w_2opt = R_2/sqrt(L_2^2 + (R_2/R_1)*L_h^2);


%% Berechnung von w_2:

if M == 0
    w_2 = 0;
elseif M > 0
    w_2 = +w_2opt;
else % d. h. M < 0
    w_2 = -w_2opt;
end


end % Hier endet die Funktion.

