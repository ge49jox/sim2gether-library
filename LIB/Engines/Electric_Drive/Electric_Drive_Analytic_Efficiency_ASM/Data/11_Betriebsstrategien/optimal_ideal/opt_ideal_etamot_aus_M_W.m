function [ eta_mot ] = opt_ideal_etamot_aus_M_W( ...
    M, ...
    W, ...
    Maschinenkonstanten_10 ...
)

%% Wichtige Hinweise:

% Diese Funktion berechnet den motorischen Wirkungsgrad eta_mot
% bei optimaler Betriebsstrategie im Idealfall.

% optimal ideal: Im Motor-Betrieb     gilt stets: w_2 = +w_2opt.
%                Im Generator-Betrieb gilt stets: w_2 = -w_2opt.

% Nat�rlich kann dies im Realfall nicht immer aufrechterhalten
% werden, aber diese Funktion soll ja den Idealfall zeigen.


%% Berechnung von eta_mot:

eta_mot = etamot_aus_w2_W( ...
    opt_ideal_w2_aus_M(M,Maschinenkonstanten_10), ...
    W, ...
    Maschinenkonstanten_10 ...
);


end % Hier endet die Funktion

