function [ P_el ] = opt_ideal_Pel_aus_M_W( ...
    M, ...
    W, ...
    Maschinenkonstanten_10 ...
)

%% Wichtige Hinweise:

% Diese Funktion berechnet die elektrische Leistung P_el
% bei optimaler Betriebsstrategie im Idealfall.

% optimal ideal: Im Motor-Betrieb     gilt stets: w_2 = +w_2opt.
%                Im Generator-Betrieb gilt stets: w_2 = -w_2opt.

% Nat�rlich kann dies im Realfall nicht immer aufrechterhalten
% werden, aber diese Funktion soll ja den Idealfall zeigen.


%% Berechnung von P_el:

P_el = Pel_aus_M_w2_W( ...
    M,...
    opt_ideal_w2_aus_M(M,Maschinenkonstanten_10), ...
    W, ...
    Maschinenkonstanten_10 ...
);

end



