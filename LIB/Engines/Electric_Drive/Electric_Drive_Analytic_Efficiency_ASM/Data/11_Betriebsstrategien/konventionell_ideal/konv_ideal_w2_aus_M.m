function [ w_2 ] = konv_ideal_w2_aus_M( ...
    M, ...
    Maschinenkonstanten_10, ...
    Betriebsgrenzen_4 ...
)

%% Wichtige Hinweise:

% Diese Funktion berechnet w_2 so, dass sich das gew�nschte M einstellt,
% bei konventioneller Betriebsstrategie im Idealfall.

% konventionell ideal: Es gilt stets: Psi_1 = Psi_1N.
% Nat�rlich kann Psi_1 = Psi_1N im Realfall nicht immer aufrechterhalten
% werden, aber diese Funktion soll ja den Idealfall zeigen.

% Im Fall "konventionell ideal" hat die Winkelgeschwindigkeit W
    % keinerlei Einfluss auf w_2, weshalb sie auch nicht als Argument der
    % Funktion auftaucht.


%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
          % m = Maschinenkonstanten_10(1);       
          % p = Maschinenkonstanten_10(2);         
        % R_1 = Maschinenkonstanten_10(3);       
        % R_2 = Maschinenkonstanten_10(4);       
        % L_h = Maschinenkonstanten_10(5);       
   % L_sigma1 = Maschinenkonstanten_10(6);  
   % L_sigma2 = Maschinenkonstanten_10(7);  
        % L_1 = Maschinenkonstanten_10(8);       
        % L_2 = Maschinenkonstanten_10(9);      
      % sigma = Maschinenkonstanten_10(10);
       
% Betriebsgrenzen_4:
    % I_1N = Betriebsgrenzen_4(1);
    % U_1N = Betriebsgrenzen_4(2);
    Psi_1N = Betriebsgrenzen_4(3);
   % W_max = Betriebsgrenzen_4(4);

     
%% Berechnung von w_2:

w_2 = w2_aus_Psi1_M(Psi_1N,M,Maschinenkonstanten_10);


end % Hier endet die Funktion.

