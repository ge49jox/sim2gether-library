function [ w_2 ] = opt_real_w2_aus_M_W( ...
    M, ...
    W, ...
    Maschinenkonstanten_10, ...
    Betriebsgrenzen_4, ...
    Nennwerte_8 ...
)

%% Wichtige Hinweise:

% Diese Funktion berechnet w_2 bei optimaler Betriebsstrategie im
    % Realfall.
% optimal real:
    % im Motor-Betrieb:
        % w_2 wird SOWEIT WIE M�GLICH auf +w_2opt gehalten.
    % im Generator-Betrieb:
        % w_2 wird SOWEIT WIE M�GLICH auf -w_2opt gehalten.   
    
% Im Fall "optimal real" hat die Winkelgeschwindigkeit W
    % sehr wohl Einfluss auf w_2, weshalb sie auch als Argument der
    % Funktion auftauchen muss.    
    
% ACHTUNG! Aus Gr�nden der Geschwindigkeit pr�ft diese Funktion NICHT
% die Einhaltung der Grenzkurven! Es kann daher sein, dass diese
% Funktion ein Ergebnis liefert, obwohl M so gro� ist, dass bei W
% die Grenzkurven verletzt werden w�rden. Bei Einhaltung der Grenzkurven
% liefert diese Funktion jedoch stets das richtige Ergebnis.    
    
% Achtung: Diese Funktion muss gelten sowohl f�r den Motorbetrieb, als
% auch f�r den Generatorbetrieb!


%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10: Der gesamte Vektor wird trotzdem ben�tigt!
          % m = Maschinenkonstanten_10(1);       
          % p = Maschinenkonstanten_10(2);         
          R_1 = Maschinenkonstanten_10(3);       
          R_2 = Maschinenkonstanten_10(4);       
          L_h = Maschinenkonstanten_10(5);       
   % L_sigma1 = Maschinenkonstanten_10(6);  
   % L_sigma2 = Maschinenkonstanten_10(7);  
        % L_1 = Maschinenkonstanten_10(8);       
          L_2 = Maschinenkonstanten_10(9);      
      % sigma = Maschinenkonstanten_10(10);
       
% Betriebsgrenzen_4:
      I_1N = Betriebsgrenzen_4(1);
      U_1N = Betriebsgrenzen_4(2);
    Psi_1N = Betriebsgrenzen_4(3);
   % W_max = Betriebsgrenzen_4(4);     

% Nennwerte_8:
        % w_2N = Nennwerte_8(1);
         % M_N = Nennwerte_8(2);
        W_Nmot = Nennwerte_8(3);
        W_Ngen = Nennwerte_8(4);
     % w_1Nmot = Nennwerte_8(5);
     % w_1Ngen = Nennwerte_8(6);
  % P_mechNmot = Nennwerte_8(7);
  % P_mechNgen = Nennwerte_8(8);


w_2opt = R_2/sqrt(L_2^2 + (R_2/R_1)*L_h^2); 
  
  
%% Berechnung von w_2:    
    
W_Nmax = max(W_Nmot,W_Ngen);
% Erfahrungsgem�� sollte W_Ngen stets der gr��ere Wert sein.
    % So ist man aber auf der sicheren Seite.
% W_Nmax wird sp�ter bei Fallunterscheidungen ben�tigt werden.

if M == 0
   w_2 = 0;
elseif M > 0 % => Motor-Betrieb!
   
    if W < 0
        error('Die Winkelgeschwindigkeit W darf nicht negativ sein!'); 
    elseif W <= W_Nmax % d. h. 0 <= W <= W_Nmax
        % Dieser Block kann im Motor-Betrieb immer angewandt werden.
        % Allerdings ist er f�r W_Nmax < W nicht mehr in voller
        % Form notwendig, weshalb dann ein eigener reduzierter Block
        % angewandt wird, um den Rechenaufwand zu verringern.
        w_2Psi1N = w2_aus_Psi1_M(Psi_1N,M,Maschinenkonstanten_10);
          w_2I1N = w2_aus_I1_M(I_1N,M,Maschinenkonstanten_10);
          w_2U1N = w2_aus_U1_W_M(U_1N,W,M,Maschinenkonstanten_10);
             w_2 = max(max(+w_2opt,w_2U1N),max(w_2Psi1N,w_2I1N));
             % w_2 muss der rechteste Wert sein.
             % GANZ WICHTIG! +w_2opt nicht vergessen!
             % Bei konventioneller Betriebsstrategie taucht das +w_2opt
                % nicht auf!
    else % d. h. W_Nmax < W
        w_2U1N = w2_aus_U1_W_M(U_1N,W,M,Maschinenkonstanten_10);
           w_2 = max(+w_2opt,w_2U1N);
             % w_2 muss der rechteste Wert sein.
             % GANZ WICHTIG! +w_2opt nicht vergessen!
             % Bei konventioneller Betriebsstrategie taucht das +w_2opt
                % nicht auf!           
    end 
    
else % d. h. M < 0 => Generator-Betrieb!
   
    if W < 0
        error('Die Winkelgeschwindigkeit W darf nicht negativ sein!'); 
    elseif W <= W_Nmax % d. h. 0 <= W <= W_Nmax
        % Dieser Block kann im Generator-Betrieb immer angewandt werden.
        % Allerdings ist er f�r W_Nmax < W nicht mehr in voller
        % Form notwendig, weshalb dann ein eigener reduzierter Block
        % angewandt wird, um den Rechenaufwand zu verringern.
        w_2Psi1N = w2_aus_Psi1_M(Psi_1N,M,Maschinenkonstanten_10);
          w_2I1N = w2_aus_I1_M(I_1N,M,Maschinenkonstanten_10);
          w_2U1N = w2_aus_U1_W_M(U_1N,W,M,Maschinenkonstanten_10);
             w_2 = min(min(-w_2opt,w_2U1N),min(w_2Psi1N,w_2I1N));
             % w_2 muss der linkeste Wert sein.
             % GANZ WICHTIG! -w_2opt nicht vergessen!
             % Bei konventioneller Betriebsstrategie taucht das -w_2opt
                % nicht auf!             
    else % d. h. W_Nmax < W
        w_2U1N = w2_aus_U1_W_M(U_1N,W,M,Maschinenkonstanten_10);
           w_2 = min(-w_2opt,w_2U1N);
             % w_2 muss der linkeste Wert sein.
             % GANZ WICHTIG! -w_2opt nicht vergessen!
             % Bei konventioneller Betriebsstrategie taucht das -w_2opt
                % nicht auf!  
    end     
    
end % Hier endet der if-Block.  


end % Hier endet die Funktion.


