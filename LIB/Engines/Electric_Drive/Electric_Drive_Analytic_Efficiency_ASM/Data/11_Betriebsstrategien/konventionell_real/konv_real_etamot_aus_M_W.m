function [ eta_mot ] = konv_real_etamot_aus_M_W( ...
    M, ...
    W, ...
    Maschinenkonstanten_10, ...
    Betriebsgrenzen_4, ...
    Nennwerte_8 ...
)

%% Wichtige Hinweise:

% Diese Funktion berechnet den motorischen Wirkungsgrad eta_mot
% bei konventioneller Betriebsstrategie im Realfall.

% konventionell real: Die Statorflussverkettung Psi_1 wird
    % SOWEIT WIE M�GLICH auf ihrem Nennwert Psi_1N gehalten.

%% Berechnung von eta_mot:
    
eta_mot = etamot_aus_w2_W( ...
    konv_real_w2_aus_M_W(M,W, ...
        Maschinenkonstanten_10,Betriebsgrenzen_4,Nennwerte_8), ...
    W, ...
    Maschinenkonstanten_10 ...
);


end % Hier endet die Funktion

