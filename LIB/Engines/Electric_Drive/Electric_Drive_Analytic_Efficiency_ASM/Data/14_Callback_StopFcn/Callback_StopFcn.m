%% Wichtige Hinweise:

% Dieses Callback-Skript wird ausgef�hrt, wenn die Simulink-Simulation
% gestoppt hat.
% Das Ergebnis der Simulation wird als "Simulation_Ergebnis" im
% Workspace abgespeichert.
% Ebenso wird die vom Endbenutzer gew�hlte Zyklus-Nummer als
% "Zyklus_Nummer" im Workspace abgespeichert.

% Dieses StopFcn-Callback-Skript sorgt nun daf�r, dass das im Workspace
% gespeicherte "Simulation_Ergebnis" dem richtigen Fahrzyklus zugeordnet
% wird. Dies geschieht �ber die Zyklus_Nummer ganz links im Modell.
% Bsp.: Ist Zyklus_Nummer = 2 und hat die Simulation gestoppt,
% so ist der UDC simuliert worden.
% In diesem Fall wird der Inhalt von "Simulation_Ergebnis" in 
% "Simulation_UDC" geschrieben.

if     Zyklus_Nummer == 1
    Simulation_NEDC = Simulation_Ergebnis;
elseif Zyklus_Nummer == 2
    Simulation_UDC = Simulation_Ergebnis;
elseif Zyklus_Nummer == 3
    Simulation_EUDC = Simulation_Ergebnis;
elseif Zyklus_Nummer == 4
    Simulation_CADC_urban = Simulation_Ergebnis;
elseif Zyklus_Nummer == 5
    Simulation_CADC_rural = Simulation_Ergebnis;
elseif Zyklus_Nummer == 6
    Simulation_FTP72 = Simulation_Ergebnis;
end

clearvars Simulation_Ergebnis tout; % wird nicht mehr ben�tigt;