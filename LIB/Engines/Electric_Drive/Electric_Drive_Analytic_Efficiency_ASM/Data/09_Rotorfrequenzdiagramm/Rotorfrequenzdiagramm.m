%% Wichtige Hinweise:
% Dieses Skript berechnet die Grenzkruven des Rotorfrequenzdiagramms (RFD),
% welche sp�ter von einem Plot-Skript ben�tigt werden.

% Das RFD besitzt zwei Achsen:
    % Horizontalachse: Rotorfrequenz w_2 (in rad/s!);
    % Vertikalachse:   Drehmoment M (in Nm!);

% Zum RFD geh�ren:
    % I_1N-Grenzkurve
    % Psi_1N-Grenzkurve
    % U_1N-Grenzkruve f�r 
        % W = 0.5*W_Nmot
        % W = W_Nmot
        % W = 2*W_Nmot
        % W = W_Cmot
        % W = W_max
        


%% I_1N-Grenzkurve, Psi_1N-Grenzkurve, U_1N-Grenzkurven:

% Dieses Skript greift auf w2_Vektor_motgen zu.
smax = length(w2_Vektor_motgen); % h�chster Spaltenindex

RFD_I1N_Vektor = zeros(1,smax); % Initialisierung;

RFD_Psi1N_Vektor = zeros(1,smax); % Initialisierung;
 
RFD_U1N_a_Vektor = zeros(1,smax); % Initialisierung, W = 0.5*W_Nmot;
RFD_U1N_b_Vektor = zeros(1,smax); % Initialisierung, W = W_Nmot;
RFD_U1N_c_Vektor = zeros(1,smax); % Initialisierung, W = 2*W_Nmot;
RFD_U1N_d_Vektor = zeros(1,smax); % Initialisierung, W = W_Cmot
RFD_U1N_e_Vektor = zeros(1,smax); % Initialisierung, W = W_max
 
for s = 1:smax
    
    w_2 = w2_Vektor_motgen(s);
    
    % I_1N-Grenzkurve:
    RFD_I1N_Vektor(s) = M_aus_I1_w2(I_1N,w_2,Maschinenkonstanten_10);
    
    % Psi_1N-Grenzkurve:
    RFD_Psi1N_Vektor(s) = M_aus_Psi1_w2(Psi_1N,w_2,Maschinenkonstanten_10);
    
    % U_1N-Grenzkurven:
    RFD_U1N_a_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,0.5*W_Nmot,Maschinenkonstanten_10);
    RFD_U1N_b_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,W_Nmot,Maschinenkonstanten_10);
    RFD_U1N_c_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,2*W_Nmot,Maschinenkonstanten_10);
    RFD_U1N_d_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,W_Cmot,Maschinenkonstanten_10);
    RFD_U1N_e_Vektor(s) = ...
        M_aus_U1_w2_W(U_1N,w_2,W_max,Maschinenkonstanten_10);
    
end

%% U_1N-HOP-Kurve: Menge aller Hochpunkte der U_1N-Grenzkurven:
% Dazu geh�rt eine Matrix mit 3 Zeilen und ca. doppelt sovielen Spalten,
% wie W_Vektor Elemente hat.

% 1. Zeile: W_Vektor_lang;
% 2. Zeile: w_2-Werte;
% 3. Zeile: zugeh�rige M-Werte;

smax = length(W_Vektor_lang);

RFD_U1N_HOP_Matrix_3 = zeros(3,smax); % Initialisierung;

RFD_U1N_HOP_Matrix_3(1,:) = W_Vektor_lang;

for s = 1:smax
    
    W = W_Vektor_lang(s);
    
    Hilfsfunktion1 = @(a,x,b,c) -M_aus_U1_w2_W(a,x,b,c);
        % Minuszeichen! wegen fminbnd!
        
    a = U_1N;
    % x = w_2, wird gesucht;
    b = W;
    c = Maschinenkonstanten_10;
    
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,x,b,c);
        
    [w_2,MHOP] = fminbnd(Hilfsfunktion2,0,w_2Psi1k);
    MHOP = abs(MHOP); % abs() nicht vergessen! wegen fminbnd!
    
    RFD_U1N_HOP_Matrix_3(2,s) = w_2;
    RFD_U1N_HOP_Matrix_3(3,s) = MHOP;
    
end

clearvars smax s a b c... % wird nicht mehr ben�tigt;
    Hilfsfunktion1 Hilfsfunktion2 MHOP W w_2