
% Dieses Skript erstellt eine Matrix die genau die selbe Anzahl an Zeilen
% und Spalten hat, wie die dazugehörige P_el Matrix. Dadurch kann diese
% Matrix einfach zu den elektrischen Leistungen addiert werden. 

Laenge_Omega=length(W_Vektor) ;  
Laenge_M=length(M_Vektor_motgen);


P_Fe_Matrix=zeros(Laenge_M,Laenge_Omega);

W_Nmot_gerundet=floor(W_Nmot);
W_Nmot_gerundet_test= W_Nmot_gerundet/2;



if mod(W_Nmot_gerundet_test,1)
    W_Nmot_gerundet_gerade=W_Nmot_gerundet+1;
    
else
    W_Nmot_gerundet_gerade=W_Nmot_gerundet;
    
end

W_konstante_EV=floor(W_Nmot_gerundet_gerade/W_Schrittweite)+1; %% wegen festlegung in Primaere_parameter

xxxxx=P_Fe(:,W_konstante_EV); %%%% EV an Eckdrehzahl
%% eintragen EV konstant ab Eckdrehzahl
for nnn=W_konstante_EV:Laenge_Omega;
    
    P_Fe(:,nnn)=xxxxx*(1+P_n*W_Schrittweite*10^-5)^(nnn-W_konstante_EV+1); %% EV ab n_nenn konstatn mit leichter anhebung abhängig der Nennleistung
    
end

%% normale EV bis Eckdrehzahl


for n=1:Laenge_M;
    
   P_Fe_Matrix(n,:)=P_Fe; 
   
end

P_Fe_Matrix_halb=0.5*P_Fe_Matrix;  

r=floor(M_N);
o=floor(M_Psi1Nk);
x=o-r+1;

for n=x:2*x;
    
   P_Fe_Matrix_halb(n,:)=P_Fe;
   
end






