% Im Folgenden wird das gesamte Vorgehen zur Berechnung der Fitparameter
% am Beispiel des St�nderr�ckens (rS) erkl�rt. Im Anschluss erfolgt die
% Anwendund der Prozedur analog f�r L�uferr�cken (rL), St�nder- (zS) und
% L�uferz�hne (zL)

% Interpolieren der jeweiligen Verlustleistung �ber die Frequenz f�r einen
% gegebenen Inputvektor der magnetischen Flussdichte:

P_fit_rS = zeros(length(B_rS_max),length(f));
for i = 1:1:length(B_rS_max)
    for j = 1:1:length(f)
    helper = interp2(B,f,M_p,B_rS_max(i),f)';
    P_fit_rS(i,j) = helper(j);
    end
end
 
% Berechnung der frequenzunabh�ngigen Faktoren der jeweiligen Verlustart:
% Der Faktor k_hyst enth�lt bspw. alle Parameter der Hystereseverluste, die
% nicht von f abh�ngig sind und wird als Konstante aus dem Hysterese-Term
% herausgezogen. Die Hystereseverluste werden damit wie folgt modelliert:
% p_hyst = k_hyst * f
% Dies geschieht analog f�r Wirbelstrom- und Excessverluste:
% p_w = k_w * f^2
% p_exc = k_exc *f^1.5
% Die berechneten Konstanten werden jeweils in einem Vektor abgelegt. Ihre 
% Position innerhalb dieses Vektors entspricht der jeweiligen Position der
% dazugeh�rigen Flussdichte im Vektor B_rS. Auf diese Weise werden die 
% Vektoren im Anschluss einfach an die Funktion 'createFit.m' �bergeben, 
% welche die Fitparameter k_rS und C_rS f�r die zugeh�rige Flussdichte B_rS
% an der jeweiligen Stelle bestimmt. Dadurch werden automatisiert f�r jeden
% Wert von B_rS �ber f die beiden Fitparameter k und C an der 
% entsprechenden Stelle gefittet und in einem Vektor abgelegt.

k_hyst_rS = zeros(1,length(B_rS_max));
for i = 1:length(B_rS_max)
k_hyst_rS(i) = 4 * H_c * B_rS_max(i) / rho;                      % Fitkonstante f�r Hystereseverluste
end

k_w_rS = zeros(1,length(B_rS_max));
for i = 1:length(B_rS_max)
    k_w_rS(i) = (pi^2 * sigma * d^2 * B_rS_max(i).^2) / (6*rho); % Fitkonstante f�r Wirbelstromverluste
end

k_exc_rS = zeros(1,length(B_rS_max));
for i = 1:length(B_rS_max)
    k_exc_rS(i) = B_rS_max(i).^1.5 / rho;                        % Fitkonstante f�r Excessverluste
end

% Definition der Verlustfunktion fitfunction_rS anhand der eben berechneten
% Konstanten, der Fitparameter k_rS und C_rS sowie der Freuquenz f. Diese 
% wird direkt in eine Variable vom Typ String umgewandelt, um sie als 
% 'fittype' an die Funktion 'createFit.m' zu �bergeben.

k_rS = zeros(1,length(B_rS_max));
C_rS = zeros(1,length(B_rS_max));
for i = 1:length(B_rS_max)
    fitfunction_rS = [num2str(k_hyst_rS(i)) '*k*f + ' num2str(k_w_rS(i)) '*f^2 + ' num2str(k_exc_rS(i)) '*C*f^1.5'];
    fit_rS = createFit(f, P_fit_rS(i,:), fitfunction_rS);
    coeffvals_rS = coeffvalues(fit_rS);
    k_rS(i) = coeffvals_rS(2);
    C_rS(i) = coeffvals_rS(1);
end


%% Bestimmung der Fitparameter f�r den L�uferr�cken:

P_fit_rL = zeros(length(B_rL_max),length(f));
for i = 1:1:length(B_rL_max)
    for j = 1:1:length(f)
    helper = interp2(B,f,M_p,B_rL_max(i),f)';
    P_fit_rL(i,j) = helper(j);
    end
end

k_hyst_rL = zeros(1,length(B_rL_max));
for i = 1:length(B_rL_max)
k_hyst_rL(i) = 4 * H_c * B_rL_max(i) / rho;                     % Fitkonstante f�r Hystereseverluste
end

k_w_rL = zeros(1,length(B_rL_max));
for i = 1:length(B_rL_max)
    k_w_rL(i) = (pi^2 * sigma * d^2 * B_rL_max(i).^2) / (6*rho); % Fitkonstante f�r Wirbelstromverluste
end

k_exc_rL = zeros(1,length(B_rL_max));
for i = 1:length(B_rL_max)
    k_exc_rL(i) = B_rL_max(i).^1.5 / rho;                        % Fitkonstante f�r Excessverluste
end

k_rL = zeros(1,length(B_rL_max));
C_rL = zeros(1,length(B_rL_max));
for i = 1:length(B_rL_max)
    fitfunction_rL = [num2str(k_hyst_rL(i)) '*k*f + ' num2str(k_w_rL(i)) '*f^2 + ' num2str(k_exc_rL(i)) '*C*f^1.5'];
    fit_rL = createFit(f, P_fit_rL(i,:), fitfunction_rL);
    coeffvals_rL = coeffvalues(fit_rL);
    k_rL(i) = coeffvals_rL(2);
    C_rL(i) = coeffvals_rL(1);
end


%% Bestimmung der Fitparameter f�r die Statorz�hne:

P_fit_zS = zeros(length(B_zS_tat),length(f));
for i = 1:1:length(B_zS_tat)
    for j = 1:1:length(f)
    helper = interp2(B,f,M_p,B_zS_tat(i),f)';
    P_fit_zS(i,j) = helper(j);
    end
end

k_hyst_zS = zeros(1,length(B_zS_tat));
for i = 1:length(B_zS_tat)
k_hyst_zS(i) = 4 * H_c * B_zS_tat(i) / rho;                     % Fitkonstante f�r Hystereseverluste
end

k_w_zS = zeros(1,length(B_zS_tat));
for i = 1:length(B_zS_tat)
    k_w_zS(i) = (pi^2 * sigma * d^2 * B_zS_tat(i).^2) / (6*rho); % Fitkonstante f�r Wirbelstromverluste
end

k_exc_zS = zeros(1,length(B_zS_tat));
for i = 1:length(B_zS_tat)
    k_exc_zS(i) = B_zS_tat(i).^1.5 / rho;                        % Fitkonstante f�r Excessverluste
end

k_zS = zeros(1,length(B_zS_tat));
C_zS = zeros(1,length(B_zS_tat));
for i = 1:length(B_zS_tat)
    fitfunction_zS = [num2str(k_hyst_zS(i)) '*k*f + ' num2str(k_w_zS(i)) '*f^2 + ' num2str(k_exc_zS(i)) '*C*f^1.5'];
    fit_zS = createFit(f, P_fit_zS(i,:), fitfunction_zS);
    coeffvals_zS = coeffvalues(fit_zS);
    k_zS(i) = coeffvals_zS(2);
    C_zS(i) = coeffvals_zS(1);
end


%% Bestimmung der Fitparameter f�r die L�uferz�hne:

P_fit_zL = zeros(length(B_zL_tat),length(f));
for i = 1:1:length(B_zL_tat)
    for j = 1:1:length(f)
    helper = interp2(B,f,M_p,B_zL_tat(i),f)';
    P_fit_zL(i,j) = helper(j);
    end
end

k_hyst_zL = zeros(1,length(B_zL_tat));
for i = 1:length(B_zL_tat)
k_hyst_zL(i) = 4 * H_c * B_zL_tat(i) / rho;                     % Fitkonstante f�r Hystereseverluste
end

k_w_zL = zeros(1,length(B_zL_tat));
for i = 1:length(B_zL_tat)
    k_w_zL(i) = (pi^2 * sigma * d^2 * B_zL_tat(i).^2) / (6*rho); % Fitkonstante f�r Wirbelstromverluste
end

k_exc_zL = zeros(1,length(B_zL_tat));
for i = 1:length(B_zL_tat)
    k_exc_zL(i) = B_zL_tat(i).^1.5 / rho;                        % Fitkonstante f�r Excessverluste
end

k_zL = zeros(1,length(B_zL_tat));
C_zL = zeros(1,length(B_zL_tat));
for i = 1:length(B_zL_tat)
    fitfunction_zL = [num2str(k_hyst_zL(i)) '*k*f + ' num2str(k_w_zL(i)) '*f^2 + ' num2str(k_exc_zL(i)) '*C*f^1.5'];
    fit_zL = createFit(f, P_fit_zL(i,:), fitfunction_zL);
    coeffvals_zL = coeffvalues(fit_zL);
    k_zL(i) = coeffvals_zL(2);
    C_zL(i) = coeffvals_zL(1);
end

