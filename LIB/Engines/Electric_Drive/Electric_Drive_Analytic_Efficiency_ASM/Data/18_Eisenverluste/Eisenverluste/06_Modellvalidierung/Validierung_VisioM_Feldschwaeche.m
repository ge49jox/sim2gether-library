%% Validierung der Visio M Maschine (mit Feldschw�chebereich):

%% !!! BITTE LESEN !!!
% Bevor das Programm gestartet wird m�ssen in der Funktion 'createFit.m'
% (Ordner '04_Berechnung_Fitparameter) in Zeile 30 die unteren Grenzen f�r
% die Fitparameter opts.Lower = [C k] = [0 0.9] gesetzt werden. Diese Werte
% stehen standardm��ig auf [0 0] (siehe Anmerkungen im Hauptskript, Z.85-98)

% Zur Nomenklatur:
% Grunds�tzlich bezeichnet 'f' den Vektor der Frequenz der beim fitten zur
% Zuweisung der Eingangsparameter zu den dazugeh�rigen Verlusten aus den
% Werkstoffdatenbl�ttern benutzt wird. Der Vektor 'freq' legt im Gegensatz
% dazu den vom Nutzer festgelegten Bereich der Frequenz fest, �ber den die
% Eisenverluste berechnet werden sollen. Nur der Vektor 'freq' sollte vom
% Nutzer des Tools angepasst werden und auf den gew�nschten Frequenzbereich
% eingestellt werden.

% Es sollte zuerst das Hauptskript ausgef�hrt werden, um alle Unterordner zum 
% Suchpfad hinzuf�gen, oder die Ordner manuell zum hinzugef�gt werden.


%% Start des Programms:

% Eingangsparameter:
p = 2;      % Polpaarzahl in 1
k_bh = 1.3; % Bearbeitungszuschlag Hystereseverluste in 1
k_bw = 1.3; % Bearbeitungszuschlag Wirbelstromverluste in 1

B = [0,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9];
f = [0,50,60,100,200,400,500];

rho = 7700;         % Dichte des Blechwerkstoffs in kg/m^3
d = 5e-04;          % Blechdicke in m
H_c = 123.7;        % Koerzitivfeldst�rke in A/m
sigma = 3.15e+06;   % Elektrische Leitf�higkeit des Blechs in 1/(Ohm*m)

mu_r0 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
p_0 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
mu_r50 = [0,3790,4184,4497,4732,4871,4897,4766,4426,3802,2822,1574,660,294,160,99];
p_50 = [0,0.72,0.96,1.23,1.52,1.83,2.18,2.56,2.99,3.48,4.03,4.70,5.47,6.14,6.71,7.58];
mu_r60 = [0,3747,4133,4445,4680,4826,4868,4761,4446,3803,2885,1572,659,294,160,99];
p_60 = [0,0.89,1.20,1.53,1.90,2.31,2.74,3.23,3.79,4.40,5.11,5.95,6.91,7.76,8.47,9.68];
mu_r100 = [0,3539,3890,4173,4385,4522,4582,4561,4387,3801,2813,1569,658,293,159,99];
p_100 = [0,1.89,2.28,2.94,3.67,4.48,5.38,6.40,7.55,8.88,10.34,11.82,13.47,15.23,17.11,19.1];
mu_r200 = [0,3075,3325,3507,3625,3686,3700,3678,3624,3440,2786,1554,652,290,157,98];
p_200 = [0,4.26,5.82,7.59,9.62,11.99,14.64,17.68,21.14,25.15,29.99,34.36,39.45,44.91,50.75,56.98];
mu_r400 = [0,2442,2553,2599,2596,2556,2496,2426,2347,2262,2100,1171,491,219,118,74];
p_400 = [0,11.42,15.82,21.13,27.45,35.01,43.73,53.76,65.32,78.43,93.77,108.74,125.69,143.97,163.57,184.49];
mu_r500 = [0,2271,2338,2348,2318,2259,2185,2105,2024,1940,1849,1031,432,193,104,65];
p_500 = [0,15.50,21.72,29.35,38.33,49.31,62.10,76.86,94.08,113.70,136.35,158.68,184.05,211.44,240.86,272.30];

M_mu = [mu_r0; mu_r50; mu_r60; mu_r100; mu_r200; mu_r400; mu_r500];
M_p = [p_0; p_50; p_60; p_100; p_200; p_400; p_500];

freq = [0:25:400];
n = 60/p*freq;

% Prozentualer Anteil der Induktion vom Peakwert im Nennpunkt (Werte grob
% abgelesen aus Datenblatt Visio M, S.10, Diagramm zur Luftspaltinduktion)
% in Schritten von 25 Hz:
fluxquotient = [1,1,1,1,1,1,1,0.9,0.79,0.71,0.65,0.60,0.55,0.51,0.48,0.45,0.42];

% Definition der Flussdichte im Statorr�cken im Nennpunkt (B_rS_input);
% Multiplikation mit dem prozentualen Anteil der Luftspaltinduktion ergibt
% Abssch�tzung der Werte der magn. Flussdichte im Feldschw�chebereich.
B_rS_input = 1.7;
B_rS_max = bsxfun(@times,B_rS_input,fluxquotient);

% Analoges Vorgehen f�r L�uferr�cken, sowie L�ufer- und Statorz�hne
B_zS_input = 1.82;
B_zS_tat = bsxfun(@times,B_zS_input,fluxquotient);
B_rL_input = 1.3;
B_rL_max = bsxfun(@times,B_rL_input,fluxquotient);
B_zL_input = 1.66;
B_zL_tat = bsxfun(@times,B_zL_input,fluxquotient);

% Parameter zur Motorgeometrie:
D_a = 0.155;
D = 0.1;
A_nS = 77.5805e-06; 
N_S = 36;
D_L = 0.0989;
D_welle = 0.035;
A_nL = 53.3387e-06;
N_L = 28;
l = 0.155;

% Berechnung des Motorvolumens:
% Annahme: Anteil der R�ckenfl�che an der gesamten L�uferfl�che entspricht
% 48,9%. Der Anteil der R�ckenfl�che im Stator 24.4%. DieseAbsch�tzung 
% erfolgt anhand der entsprechenden Daten aus dem ASM-Auslegungstool.

A_S = pi/4*(D_a^2-D^2) - N_S*A_nS;
V_S = A_S*l;
V_rS = 0.2440*V_S;
V_zS = (1-0.2440)*V_S;

A_L = pi/4*(D_L^2-D_welle^2) - N_L*A_nL;
V_L = A_L*l;
V_rL = 0.4890*V_L;
V_zL = (1-0.4890)*V_L;

mu_rS = interp2(B,f,M_mu,B_rS_max,freq); % Interpolieren von mue_r aus Datenbl�ttern
mu_zS = interp2(B,f,M_mu,B_zS_tat,freq);
mu_rL = interp2(B,f,M_mu,B_rL_max,freq);
mu_zL = interp2(B,f,M_mu,B_zL_tat,freq);

% Bestimmung der Fitparameter:
Fitparameter;

% Berechnung der Verluste:
p_rS = Eisenverluste(B_rS_max,freq,rho,H_c,sigma,mu_rS,d,k_rS,C_rS,k_bh,k_bw);
P_rS = p_rS * rho * V_rS;
p_zS = Eisenverluste(B_zS_tat,freq,rho,H_c,sigma,mu_zS,d,k_zS,C_zS,k_bh,k_bw);
P_zS = p_zS * rho * V_zS;
p_rL = Eisenverluste(B_rL_max,freq,rho,H_c,sigma,mu_rL,d,k_rL,C_rL,k_bh,k_bw);
P_rL = p_rL * rho * V_rL;
p_zL = Eisenverluste(B_zL_tat,freq,rho,H_c,sigma,mu_zL,d,k_rL,C_zL,k_bh,k_bw);
P_zL = p_zL * rho * V_zL;

P_Fe = P_rS + P_rL + P_zS + P_zL;
p_Fe = P_Fe / (rho*(V_rS+V_rL+V_zS+V_zL));

% Plot der Ergebnisse:
plot(n,P_Fe,'-o',n,p_Fe,'-x')
legend('ges. Verluste P_{Fe}','spez. Verluste p_{Fe}','Location','northwest')
grid on



%% M�glichkeiten zur Variation:
    % 1. k_bw = k_bh = 1.5 bzw. 1.3 -> je nach Literatur
    % 2. Untere Grenze f�r Fitparameter in Funktion createFit (Z.30)
        % opts.Lower = [C k] = [0 0] bzw. [0 0.9] -> 0.9 �bersch�tzt 
        % die Verluste bei hohen Frequenzen und gleichzeitig niedrigen 
        % Induktionen tendenziell, daf�r physikalisch sinnvoll und gute 
        % Genauigkeit bis Nennpunkt
    % 3. Variation des Vektors fluxquotient: diese Werte wurden als 
        % prozentualer Anteil der Flussdichte im Luftspalt aus dem 
        % Konturplot im VisioM Datenblatt abgelesen
        
   %% Anmerkung: 
   % Der Beste Fit ergibt sich bei der Einstellung der Bearbeitungszuschl�ge
   % k_bh = k_bw = 1.3 (in Z.26,27) und dem Setzen der unteren Grenzen f�r 
   % [C k] = [0 0.9] in der Funktion 'createFit.m' (in Z.30). Hierbei kommt 
   % es in Bereichen von Drehzahlen von gr��er 8000-10000 U/min zu einer 
   % �bersch�tzung der Auftretenden Verluste. Daf�r werden die Verluste bis
   % zum Nennbereich mit einer sehr hohen Genauigkeit berechnet.
   % Erfolgt der Fit mit den unteren Grenzen f�r [C k] = [0 0], so sollte
   % f�r k_bh = k_bw der Wert 1.5 gew�hlt werden. In diesem Fall werden die
   % Verluste insgesamt jedoch etwas zu optimistisch berechnet. Es werden
   % in diesem Fall zwar die mathematisch besten Fitparameter verwendet,
   % allerdings sind diese physikalisch weniger sinnvoll, als in der oben 
   % vorgeschlagenen Konfiguration.