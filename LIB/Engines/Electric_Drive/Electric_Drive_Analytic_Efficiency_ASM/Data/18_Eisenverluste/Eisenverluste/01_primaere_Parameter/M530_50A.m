%% �bernahme der Materialeigenschaften aus den Datenbl�ttern von ThyssenKrupp
%f�r den Stahl PowerCore M 530-50 A: mu_r50 bezeichnet hierbei den Vektor
%der relativen Permeabilit�t bei einer Frequenz von 50 Hz �ber den Bereich 
%der magnetischen Flussdichte von 0.5 bis 1.9 T; Analog hierzu bezeichnet 
%p_50 den Vektor der jeweils gemessenen spezifischen Verlustleistung in W/kg

rho = 7700;         %Dichte des Blechwerkstoffs in kg/m^3
d = 5e-04;          %Blechdicke in m
H_c = 123.7;        %Koerzitivfeldst�rke in A/m
sigma = 3.15e+06;   %Elektrische Leitf�higkeit des Blechs in 1/(Ohm*m)

mu_r50 = [3790,4184,4497,4732,4871,4897,4766,4426,3802,2822,1574,660,294,160,99];
p_50 = [0.72,0.96,1.23,1.52,1.83,2.18,2.56,2.99,3.48,4.03,4.70,5.47,6.14,6.71,7.58];
mu_r60 = [3747,4133,4445,4680,4826,4868,4761,4446,3803,2885,1572,659,294,160,99];
p_60 = [0.89,1.20,1.53,1.90,2.31,2.74,3.23,3.79,4.40,5.11,5.95,6.91,7.76,8.47,9.68];
mu_r100 = [3539,3890,4173,4385,4522,4582,4561,4387,3801,2813,1569,658,293,159,99];
p_100 = [1.89,2.28,2.94,3.67,4.48,5.38,6.40,7.55,8.88,10.34,11.82,13.47,15.23,17.11,19.1];
mu_r200 = [3075,3325,3507,3625,3686,3700,3678,3624,3440,2786,1554,652,290,157,98];
p_200 = [4.26,5.82,7.59,9.62,11.99,14.64,17.68,21.14,25.15,29.99,34.36,39.45,44.91,50.75,56.98];
mu_r400 = [2442,2553,2599,2596,2556,2496,2426,2347,2262,2100,1171,491,219,118,74];
p_400 = [11.42,15.82,21.13,27.45,35.01,43.73,53.76,65.32,78.43,93.77,108.74,125.69,143.97,163.57,184.49];
mu_r500 = [2271,2338,2348,2318,2259,2185,2105,2024,1940,1849,1031,432,193,104,65];
p_500 = [15.50,21.72,29.35,38.33,49.31,62.10,76.86,94.08,113.70,136.35,158.68,184.05,211.44,240.86,272.30];

%M_mu bezeichnet die Matrix mit den abgelegten Werten von mu_r, wobei
%sich die Reihen der Matrix auf die Flussdichte und die Spalten auf die
%Frequenz beziehen; analog dazu M_p f�r die entsprechende Verlustleistung
M_mu = [mu_r50; mu_r60; mu_r100; mu_r200; mu_r400; mu_r500];
M_p = [p_50; p_60; p_100; p_200; p_400; p_500];


%% Induzieren der Vektoren f�r Flussdichte und Frequenz (Interpolieren)
B = [0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9];
f = [50,60,100,200,400,500];
