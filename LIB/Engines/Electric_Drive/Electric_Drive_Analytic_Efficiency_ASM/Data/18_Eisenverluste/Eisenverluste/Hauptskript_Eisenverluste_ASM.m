%% Hauptskript zur Berechnung der Eisenverluste

%% !!! ALLGEMEINE HINWEISE ZUM TOOL !!!
% Im Quellcode war an einigen Stellen die elementweise Multiplikation bzw. 
% Division von Vektoren n�tig. Dies wurde mit der in Matlab integrierten
% Funktion bsxfun() gel�st:
    % @times: elementweise Multiplikation
    % @rdivide: elementweise Division durch die rechte Seite
% Ferner berechnet das Tool die auftretetenden Eisenverluste in
% elektrischen Maschinen aufgrund der Annahme einer konstanten magnetischen
% Flussdichte B �ber der Frequenz f, da aufgrund der Datengrundlage aus dem
% Tool zur Motorauslegung nur im Nennpunkt gegeben ist. Bis zu diesem Punkt
% werden die Verluste weitestgehend genau approximiert, dar�ber hinaus
% erfolgt allerdings im Feldschw�chebereich eine zu pessimistische Bewertung
% der Verluste. Das Tool ist jodoch bereits so ausgelegt, dass die magn. 
% Flussdichte als vektorielle Gr��e �bernommen wird. Hierf�r m�ssen jedoch 
% zwei Bedingungen eingehalten werden:
    % 1. Die Vektoren der magn. FLussdichte B und der Frequenz f m�ssen die
    % selbe L�nge haben.
    % 2. Falls die magn. Flussdichte doch als skalare Gr��e �bernommen wird,
    % siehe Anmerkungen in Funktion 'Wirbelstromverluste.m'

% Zur Nomenklatur:
% Grunds�tzlich bezeichnet 'f' den Vektor der Frequenz der beim fitten zur
% Zuweisung der Eingangsparameter zu den dazugeh�rigen Verlusten aus den
% Werkstoffdatenbl�ttern benutzt wird. Der Vektor 'freq' legt im Gegensatz
% dazu den vom Nutzer festgelegten Bereich der Frequenz fest, �ber den die 
% Eisenverluste berechnet werden sollen. Nur der Vektor 'freq' sollte vom 
% Nutzer des Tools angepasst werden und auf den gew�nschten Frequenzbereich
% eingestellt werden.    
    
%% alle Unterordner zum Suchpfad hinzuf�gen:
%addpath(genpath(pwd));

%% Initialisierung der Inputparameter:
primaere_Parameter;
% Initialisierung erfolgt nun im Hauptprogramm der Kennfeldauslegung
%% Wahl des Blechs und Initialisierung der zugeh�rigen Materialparameter:
M530_50A;

%% �bergabe des Rotor- und Statorvolumens:
sekundaere_Parameter;

%% Interpolation von mu_r
ddd=length(W_Vektor);
tics_f_nenn=(500-50)/(ddd-1); %dimensioniert die Winkelgeschwindigkeit in den Skripten gleich wenn die Werte in Zeile 17 ge�ndert werden, m�ssen sie auch hier angepasst werden.
f_nenn = [50:tics_f_nenn:500];
n_nenn = 2*pi*f_nenn/p;
mu_rS = interp2(B,f,M_mu,B_rS_max,f_nenn); % Interpolieren von mu_r aus Datenbl�ttern
mu_zS = interp2(B,f,M_mu,B_zS_tat,f_nenn);
mu_rL = interp2(B,f,M_mu,B_rL_max,f_nenn);
mu_zL = interp2(B,f,M_mu,B_zL_tat,f_nenn);

%% Bestimmung der Fitparameter
% Die Fitparameter k und C werden in dem eigenen Dokument 'Fitparameter.m'
% im Ordner '04_Berechnung_Fitparameter' bestimmt. In der Funktion
% 'createFit.m' im selben Ordner k�nnen in Zeile 30 die gew�nschten unteren
% Grenzen f�r die zu fittenden Koeffizienten bestimmt werden. Hierf�r
% m�ssen die Werte des Vektors 'opts.Lower = [C k];' entsprechend angepasst
% werden. Standardm��ig stehen die Werte f�r [C k] auf [0 0]. Dies ergibt
% den mathematisch besten Fit. M�chte man einen physikalisch sinnvollen Fit
% erzielen, so sollte der Wert von k ungef�hr auf 1 gesetzt werden (der
% Wert f�r C ergibt sich dann automatisch). Hierf�r ergeben sich in 
% einzelnen F�llen niedriger Induktionen jedoch schlechte Fit-Ergebnisse 
% mit gro�en Abweichungen bei hohen Frequenzen. Die Parameter stehen daher 
% stadardm��ig auf Null und k�nnen vom Benutzer des Tools manuell angepasst
% werden.
Fitparameter;

%% Berechnung der Eisenverluste:
% Es werden die folgenden Bezeichnungen verwendet:
    % p - spezifische Eisenverluste
    % P - tats�chliche auftretende Eisenverluste
% 1. Stator
p_rS = Eisenverluste(B_rS_max,f_nenn,rho,H_c,sigma,mu_rS,d,k_rS,C_rS,k_bh,k_bw); 
P_rS = p_rS * rho * V_rS;
p_zS = Eisenverluste(B_zS_tat,f_nenn,rho,H_c,sigma,mu_zS,d,k_zS,C_zS,k_bh,k_bw);
P_zS = p_zS * rho * V_zS;

P_S = P_rS + P_zS;       % Gesamtverluste im Stator in W

% 2. L�ufer
p_rL = Eisenverluste(B_rL_max,f_nenn,rho,H_c,sigma,mu_rL,d,k_rL,C_rL,k_bh,k_bw);
P_rL = p_rL * rho * V_rL;
p_zL = Eisenverluste(B_zL_tat,f_nenn,rho,H_c,sigma,mu_zL,d,k_zL,C_zL,k_bh,k_bw);
P_zL = p_zL * rho * V_zL;

P_L = P_rL + P_zL;       % Gesamtverluste im Rotor in W

% 3. Gesamte Eisenverluste in W
P_Fe = P_S + P_L;   
%% erzeugt aus dem Vektor P_Fe eine Matrix kompatibel zu den Kennfeldern
Eisenverluste_in_Kennfeld;
%%
% grafische Ausgabe der Ergebnisse 
% figure('NumberTitle','off','Name','Eisenverluste P in Abh�ngigkeit der Frequenz f');
% plot([0,n_nenn],[0,P_Fe]);
% xlabel('Drehzahl n in 1/min')
% ylabel('Verlustleistung P in W')
%%
 %M=[0,200]; % sp�ter ausblenden bzw. in Prim�re Inputparameter �bernehmen
 %figure('NumberTitle','off','Name','Verlustkennfeld');
 %contourf([0,n_nenn],M,[0,P_Fe;0,P_Fe],60)
 %colorbar
 %xlabel('Winkelgeschwindigkeit \Omega/(rad/s)')
 %ylabel('Drehmoment M in Nm')
 %hold on
%%
%X = [0,5250];
%Y = [145.5,145.5];
%X2 = [5250:15000];
%Y2 = @(x) (145.5*5250)./X2;
%plot(X,Y,'k-',X2,Y2(X2),'k-','LineWidth',2)
