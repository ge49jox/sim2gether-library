%% Berechnung der spezifischen Gesamtverluste
function p_fe = Eisenverluste(B_max,f,rho,H_c,sigma,mu_r,d,k,C,k_bh,k_bw)

p_hyst = Hystereseverluste(k,k_bh,H_c,B_max,f,rho);
p_w = Wirbelstromverluste(f,sigma,mu_r,k_bw,d,B_max,rho);
p_exc = Excessverluste(C,B_max,f,rho);

p_fe = p_hyst + p_w + p_exc;
