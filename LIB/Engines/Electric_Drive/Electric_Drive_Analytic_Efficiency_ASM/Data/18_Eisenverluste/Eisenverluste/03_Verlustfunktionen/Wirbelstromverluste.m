%% Berechnung der spezifischen Wirbelstromverluste gem�� Canders Paper
function p_w = Wirbelstromverluste (f,sigma,mu_r,k_bw,d,B_max,rho)
mu_0 = 4 * pi * 10^-7; %magnetische Feldkonstante in N/A^2 bzw. (V*s)/(A*m)
mu = mu_r * mu_0;   
beta = sqrt(pi * sigma * bsxfun(@times,f,mu'));
x = beta * d;

F = bsxfun(@times,(3./x),bsxfun(@rdivide,sinh(x)-sin(x),cosh(x)-cos(x)));
%Verdr�ngungsterm F: Die Funktion bxsfun erm�glicht die elementweise
%Multiplikation (@times) bzw. Division (@rdivide) von Vektoren/Matrizen

% mu %Auskommentieren
% x
F; %Auskommentieren

p_w_KV = k_bw/(6*rho) * (pi^2 * sigma * d^2 * B_max^2 * f.^2); %spezifische Wirbelstromverluste ohne Verdr�ngungsterm

p_w = bsxfun(@times,p_w_KV,F); %Ausgabe der spezifischen Wirbellstromverluste
