function [w_2N,M_N,W_Nmot,W_Ngen,  ... 
          w_1Nmot,w_1Ngen,P_mechNmot,P_mechNgen] = ...
    Nennwerte_aus_ASM_Daten( ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4, ...
        charakt_w2_M_5 ...
    )

%% wichtige Hinweise:

% Diese Funktion berechnet aus den ASM-Parametern folgende Nennwerte:
    % w_2N: Nennrotorfrequenz; Diese muss eingestellt werden, damit M_N
        % erzeugt werden kann.
        % Es gelten die einfachen Beziehungen:
            %      motorisch: w_2Nmot = +w_2N;
            %  generatorisch: w_2Ngen = -w_2N;
    % M_N: Nennmoment; F�r W <= W_N ist M_N das h�chste Drehmoment,
        % welches die Maschine dauerhaft erzeugen kann. Kurzzeitig k�nnten
        % h�here Str�me als I_1N zugelassen werden. Damit w�ren auch h�here
        % Drehmomente m�glich. Dauerhaft ist dies jedoch nicht m�glich, da
        % die ASM oder die Spannungsquelle irgendwann �berhitzen w�rden.
        % Es gelten die einfachen Beziehungen:
            %      motorisch: M_Nmot = +M_N;
            %  generatorisch: M_Ngen = -M_N;
     % W_Nmot: motorische Nennwinkelgeschwindigkeit;
        % W_Nmot ist die h�chste Winkelgeschwindigkeit,
            % bis zu der M_Nmot erzeugt werden kann.
     % W_Ngen: generatorische Nennwinkelgeschwindigkeit; 
        % W_Ngen ist die h�chste Winkelgeschwindigkeit,
            % bis zu der M_Ngen (ist negativ!) erzeugt werden kann.
     % In der Regel sind W_Nmot und W_Ngen in etwa gleich gro�.
     
% Die Berechnung der restlichen vier Gr��en ist absolut trivial (siehe
    % Skript ganz unten):
    % w_1Nmot: motorische Nennstatorfrequenz (wie immer in rad/s!)
    % w_1Ngen: generatorische Nennstatorfrequenz (wie immer in rad/s!)
    % P_mechNmot: motorische mechanische Nennleistung (in W)
    % P_mechNgen: generatorische mechanische Nennleistung (in W)

    
%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);
       
% Betriebsgrenzen_4:
      I_1N = Betriebsgrenzen_4(1);
      U_1N = Betriebsgrenzen_4(2);
    Psi_1N = Betriebsgrenzen_4(3);
     W_max = Betriebsgrenzen_4(4);

% charakt_w2_M_5:
    % w_2opt = charakt_w2_M_5(1);
      w_2I1k = charakt_w2_M_5(2);
    w_2Psi1k = charakt_w2_M_5(3);
      M_I1Nk = charakt_w2_M_5(4);
    M_Psi1Nk = charakt_w2_M_5(5);
    

%% Berechnung von w_2N und M_N:

% Der (motorische) Nennpunkt im Rotorfrequenzdiagramm (RFD)
% ist wie folgt definiert:
    % F�r 0 < w_2 betrachte man alle Punkte, die �ber der w_2-Achse liegen
    % UND unter der I_1N-Grenzkurve liegen UND unter der Psi_1N-Grenzkurve
    % liegen. Von all diesen Punkten suche man jenen heraus, der den
    % h�chsten M-Wert aufweist. Dieser Punkt ist der Nennpunkt.
% Der generatorische Nennpunkt ergibt sich einfach durch Spiegelung am
    % Ursprung, da die I_1N- und die Psi_1N-Grenzkurve
    % ungerade Funktionen sind.
    
 % Im Rotorfrequenzdiagramm (RFD) besitzen die I_1N-Grenzkurve und die
    % Psi_1N-Grenzkurve immer den trivialen Schnittpunkt
    % in (w_2,M) = (0,0);
% Der nichttriviale Schnittpunkt (ntsp) existiert nicht immer.
% Der ntsp wird hier analytisch berechnet, d. h. �ber eine hergeleitete
    % Formel.

% Pr�fe, ob der ntsp existiert:
Radikant = (I_1N^2*L_1^2 - Psi_1N^2)/(Psi_1N^2 - I_1N^2*sigma^2*L_1^2);
if Radikant > 0 % ntsp existiert (h�ufig der Fall);    
    w_2ntsp = (R_2/L_2)*sqrt(Radikant);
    if w_2ntsp <= w_2I1k % selten der Fall;
        w_2N = w_2I1k;       
         M_N = M_I1Nk;
    elseif w_2ntsp <= w_2Psi1k % h�ufig der Fall;
        w_2N = w_2ntsp;
         M_N = M_aus_I1_w2(I_1N,w_2N,Maschinenkonstanten_10);
    else % d. h. w_2Psi1k < w_2ntsp, selten der Fall;
        w_2N = w_2Psi1k;
         M_N = M_Psi1Nk;
    end        
else % ntsp existiert NICHT (selten der Fall);
    % Der Fall M_I1Nk = M_Psi1NK kann hier niemals auftreten, denn sonst
    % m�sste der ntsp existieren!
    if M_I1Nk > M_Psi1Nk % Die I_1N-Grenzkurve liegt vollst�ndig �ber der
        % Psi_1N-Grenzkurve (im Bereich w_2 > 0 !).
        w_2N = w_2Psi1k;
         M_N = M_Psi1Nk;
    else % d. h. M_I1Nk < M_Psi1Nk; Die Psi_1N-Grenzkurve liegt vollst�ndig
        % �ber der I_1N-Grenzkurve (im Bereich w_2 > 0 !).
        w_2N = w_2I1k;
         M_N = M_I1Nk;
    end    
end
% Hiermit sind w_2N und M_N bestimmt.


%% Berechnung von W_Nmot und W_Ngen:
% Es handelt sich um Werte, die nur einmal berechnet werden m�ssen.
% Bei der Berechnung von Kennfeldmatrizen wird diese Funktion nicht
% aufgerufen. Deshalb ist es nicht notwendig, die Geschwindigkeit der
% Nullstellensuchalgorithmen durch analytische Ma�nahmen zu erh�hen.
% Das Gegenteil ist bei w2_aus_U1_W_M(...) der Fall!

%% Berechnung von W_Nmot:

% W_Nmot muss so gew�hlt sein, dass folgende Bedingung erf�llt ist:
% M_aus_U1_w2_W(U_1N,+w_2N,W_Nmot,...) = +M_N;
% => Nullstellenproblem: M_aus_U1_w2_W(U_1N,+w_2N,W_Nmot,...) - M_N = 0;
% Diese Gleichung hat immer genau eine L�sung mit klarem Vorzeichenwechsel
% (siehe RFD). => fzero ist sehr gut geeignet!

Hilfsfunktion1 = @(a,b,x,c) M_aus_U1_w2_W(a,b,x,c) - M_N; % -M_N nicht
    % vergessen!
    a = U_1N;
    b = +w_2N;
    % x = W_Nmot, wird gesucht
    c = Maschinenkonstanten_10;
Hilfsfunktion2 = @(x) Hilfsfunktion1(a,b,x,c);
W_Nmot = fzero(Hilfsfunktion2,[0,W_max]);
% Hiermit ist W_Nmot bestimmt.


%% Berechnung von W_Ngen:

% W_Ngen muss so gew�hlt sein, dass folgende Bedingung erf�llt ist:
% M_aus_U1_w2_W(U_1N,-w_2N,W_Ngen,...) = -M_N;
% => Nullstellenproblem: M_aus_U1_w2_W(U_1N,-w_2N,W_Ngen,...) + M_N = 0;
% Diese Gleichung hat immer genau eine L�sung mit klarem Vorzeichenwechsel
% (siehe RFD). => fzero ist sehr gut geeignet!

Hilfsfunktion1 = @(a,b,x,c) M_aus_U1_w2_W(a,b,x,c) + M_N; % +M_N nicht
    % veregessen!
    a = U_1N;
    b = -w_2N;
    % x = W_Ngen, wird gesucht
    c = Maschinenkonstanten_10;
Hilfsfunktion2 = @(x) Hilfsfunktion1(a,b,x,c);
W_Ngen = fzero(Hilfsfunktion2,[0,W_max]);
% Hiermit ist W_Ngen bestimmt.

%% Berechnung von w_1Nmot, w_1Ngen, P_mechNmot, P_mechNgen:

w_1Nmot = p*W_Nmot + w_2N;
w_1Ngen = p*W_Ngen - w_2N;

P_mechNmot = +M_N*W_Nmot;
P_mechNgen = -M_N*W_Ngen;


end % Hier ist das Ende der Funktion