% Dieses Skript berechnet die noch fehlenden Nennwerte aus den
% ASM-Parametern:

% Einlesen der Nennwerte:
[w_2N,M_N,W_Nmot,W_Ngen,w_1Nmot,w_1Ngen,P_mechNmot,P_mechNgen] = ...
    Nennwerte_aus_ASM_Daten( ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4, ...
        charakt_w2_M_5 ...
    );

%% Parameter-Vektor f�r die 8 Nennwerte:
Nennwerte_8 = [ ...
    w_2N,       ... % Index 1
    M_N,        ... % Index 2
    W_Nmot,     ... % Index 3
    W_Ngen,     ... % Index 4
    w_1Nmot,    ... % Index 5
    w_1Ngen,    ... % Index 6
    P_mechNmot, ... % Index 7
    P_mechNgen  ... % Index 8
];