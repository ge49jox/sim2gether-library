% Dieses Skript berechnet alle Optimalwerte aus den ASM-Parametern:

[M_Nopt,W_Noptmot,W_Noptgen] = ...
    Optimalwerte_aus_ASM_Daten( ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4 ...
    );

%% Parameter-Vektor f�r die 3 Optimalwerte:
Optimalwerte_3 = [ ...
    M_Nopt,    ... % Index 1
    W_Noptmot, ... % Index 2
    W_Noptgen  ... % Index 3
];

%% Nachtrag: optimale Verlustwinkelgeschwindigkeit W_vopt:

W_vopt = (2/p)*sqrt(R_1^2*L_2^2 + R_1*R_2*L_h^2)/(L_h^2);