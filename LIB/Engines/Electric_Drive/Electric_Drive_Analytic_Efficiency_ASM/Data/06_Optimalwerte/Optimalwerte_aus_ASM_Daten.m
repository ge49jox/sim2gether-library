function [M_Nopt,W_Noptmot,W_Noptgen ] = ...
    Optimalwerte_aus_ASM_Daten( ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4 ...
    )

%% Wichtige Hinweise:

% Der Zusatz "opt" steht f�r den Betrieb bei optimaler Betriebsstrategie,
    % d. h. es wird soweit wie m�glich w_2 auf +- w_2opt gehalten.
    
% M_Nopt ist das h�chste Moment, welches bei optimaler Betriebsstrategie
    % erzeugt werden kann. Es gilt stets M_Nopt < M_N.
% W_Noptmot ist die h�chste Winkelgeschwindigkeit, bis zu der +M_Nopt
    % aufrecht erhalten werden kann.
% W_Noptgen ist die h�chste Winkelgeschwindigkeit, bis zu der -M_Nopt
    % aufrecht erhalten werden kann.
    
% Es gilt:
    % W_Nmot,W_Noptmot,W_Noptgen,W_Ngen
    % sind alle ungef�hr gleich (und v. a. positiv!).
% Ferner gelten die einfachen Beziehungen:
    % M_Noptmot = + M_Nopt
    % M_Noptgen = - M_Nopt
    
    
%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
         % m = Maschinenkonstanten_10(1);       
         % p = Maschinenkonstanten_10(2);         
         R_1 = Maschinenkonstanten_10(3);       
         R_2 = Maschinenkonstanten_10(4);       
         L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
       % L_1 = Maschinenkonstanten_10(8);       
         L_2 = Maschinenkonstanten_10(9);      
     % sigma = Maschinenkonstanten_10(10);
       
% Betriebsgrenzen_4:
      I_1N = Betriebsgrenzen_4(1);
      U_1N = Betriebsgrenzen_4(2);
    Psi_1N = Betriebsgrenzen_4(3);
     W_max = Betriebsgrenzen_4(4);

% optimale Rotorfrequenz:
    w_2opt = R_2/sqrt(L_2^2 + (R_2/R_1)*L_h^2);
    % wird aus Maschinenkonstanten_10 berechnet;
    % Einbindung von charakt_w2_M_5 rentiert sich nicht.

    
%% Berechnung von M_Nopt:

M_Nopt_I1N = M_aus_I1_w2(I_1N,+w_2opt,Maschinenkonstanten_10);
M_Nopt_Psi1N = M_aus_Psi1_w2(Psi_1N,+w_2opt,Maschinenkonstanten_10);
    % Meist ist M_Nopt_Psi1N << M_Nopt_I1N.
M_Nopt = min(M_Nopt_I1N,M_Nopt_Psi1N);
% Hiermit ist M_Nopt bestimmt.

%% Berechnung von W_Noptmot:

% Es muss gelten: M_aus_U1_w2_W(U_1N,+w_2opt,W_Noptmot,...) = +M_Nopt;
% Diese Gleichung hat immer genau eine L�sung mit klarem Vorzeichenwechsel
% (siehe RFD). => fzero ist sehr gut geeignet!

Hilfsfunktion1 = @(a,b,x,c) M_aus_U1_w2_W(a,b,x,c) - M_Nopt; 
    % -M_Nopt nicht vergessen, da Nullstellenproblem!

a = U_1N;
b = +w_2opt; % Plus!
% x = W_Noptmot, wird gesucht;
c = Maschinenkonstanten_10;

Hilfsfunktion2 = @(x) Hilfsfunktion1(a,b,x,c);
W_Noptmot = fzero(Hilfsfunktion2,[0,W_max]);
% Hiermit ist W_Noptmot bestimmt.


%% Berechnung von W_Noptgen:

% Es muss gelten: M_aus_U1_w2_W(U_1N,-w_2opt,W_Noptgen) = -M_Nopt
% Diese Gleichung hat immer genau eine L�sung mit klarem Vorzeichenwechsel
% (siehe RFD). => fzero ist sehr gut geeignet!

Hilfsfunktion1 = @(a,b,x,c) M_aus_U1_w2_W(a,b,x,c) + M_Nopt;
    % +M_Nopt nicht vergessen, da Nullstellenproblem!

a = U_1N;
b = -w_2opt; % Minus!
% x = W_Noptgen, wird gesucht;
c = Maschinenkonstanten_10;    
    
Hilfsfunktion2 = @(x) Hilfsfunktion1(a,b,x,c);
W_Noptgen = fzero(Hilfsfunktion2,[0,W_max]);
% Hiermit ist W_Noptgen bestimmt.


end % Hier endet die Funktion.

