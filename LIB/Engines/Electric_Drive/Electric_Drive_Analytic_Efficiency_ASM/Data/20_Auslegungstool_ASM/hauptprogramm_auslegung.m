%%

close all

%addpath(genpath(pwd))
% plothandle = questdlg('Diagramme Plotten?', ...
%     'Bitte wählen', ...
%     'JA','NEIN','NEIN');

plothandle = 'NEIN';

[ndata,text,alldata] = xlsread('Dokumentation_Dimensionierung.xlsx',2,'A1:F161');
%%INIT // Einlesen der Eingangsgrößen
% [mu_0,f_n1,U_n1,m1,P_n1,n_n1,Material_Stator1,Material_Laeufer1,Schaltung1...
%     ,Bauweise1,Kuehlung1,U_s,w,M_n,E_h,B_m_max,S_S_max,S_L_max,phi_n,phi_fe,...
%     B_zS_max,alpha_p_ang,B_zL_max,B_rS_zul,B_rL_zul] = initialization( ndata,text,alldata );

Initialisierung;
%% Hier können Parameter vorgegeben werden (insbesondere C_m,lambda,eta,cphi)
%B_m_max = 0.58; % aktivieren für IM Beispiel aus Müller 2008
S_R = 4.2;  % Ringstromdichte
%phi_n = 0.46;   % aktivieren für IM Beispiel aus Müller 2008
lambda = 2.5; % aktivieren für Visio.M
lambdaschalter = 0; % 1 = vorgegebenen lambda wird übernommen 0 = lambda wird aus p berechnet
eta=90;          % overrulen des Diagramms beim Visio M
etaschalter = 0; %selbe Funktion wie lamdaschalter
cphi=0.78;            % overrulen des Diagramms beim Visio M
cphischalter = 0; %selbe Funktion wie lamdaschalter

C_m = 2.67; % Test mit Wert der realen Maschine des Visio.M
cmschalter = 0; %selbe Funktion wie lamdaschalter

B_rS_zul = 1.7;
B_rL_zul = 1.3;
B_zS_soll = 1.82;
B_zS_min = 1.81;
B_zL_soll = 1.67;
B_zL_min = 1.65;

    %% Diagramm: Ausnutzungszahl C_m (Vogt S.414)
    %% ERMITTLUNG DER HAUPTABMESSUNGEN
    %% Berechnung der Polpaarzahl und der Synchrondrehzahl
    [n_s,p] = polpaarzahl(f_n,n_n);
    %%  Auslegungsparameter aus Diagrammen auslesen
    [k_red,C_m,eta,cphi ]= parameter_vordim_diagramme( plothandle,Material_Stator,...
        Bauweise,Kuehlung,p,P_n,etaschalter,cphischalter,cmschalter,eta,cphi,C_m);
    %% Strangstrom ermitteln
    [ P_el,I_n,I_n1,I_s,P_i ] = strangstrom( P_n,eta,U_n,cphi,m,U_s,E_h );
    %% Maschinendurchmesser in der Mitte des Luftspaltes in m
    [lambda,D,delta,T_p,l_i,l_fe,l ] = hauptabmessungen(P_n,p,C_m,n_s,Kuehlung,lambda,lambdaschalter );
    %% ENTWURF DER WICKLUNG
    %% parallele Zweige
    [ E_h_r,a] = parallelezweige( P_n,E_h,B_m_max,l_i,T_p,w,p);
    %% Statorwicklungsentwurf // Optimierung auf Wicklungsfaktor xi_1, q_S und B_max
    [w_s_opt,q_S,z_nS,w_s,N_S,T_nS,w_ges,z_S,q_S_alt,w_s_fehler,W,u,k_ges,y_d,...
        X_v,y_v,y_n,alpha_nS,et_v,et,Z,xi_1_tat,B_max,B_m_tat,B_m_ang,Phi_h,Phi_delta,Phi_delta_tat,Wicklungsart ]...
        = statorwicklungsentwurf(P_n,alpha_p_ang,B_m_max,a,l_i,T_p,E_h,w,D,p,m );
    
    %     while B_m_tat<B_m_max *0.95
    %         l_i = l_i + 2/1000; %idelle Länge um 2mm erhöhen
    %         l_fe = l_i - 2*delta/1000; l = l_fe;
    %         disp('Maschine verlängert');
    %         [ E_h_r,a] = parallelezweige(P_n,E_h,B_m_max,l_i,T_p,w,p);
    %         [w_s_opt,q_S,z_nS,w_s,N_S,T_nS,w_ges,z_S,q_S_alt,w_s_fehler,W,u,k_ges,y_d,...
    %             X_v,y_v,y_n,alpha_nS,et_v,et,Z,xi_1_tat,B_max,B_m_tat,B_m_ang,Phi_h,Phi_delta,Phi_delta_tat,Wicklungsart]...
    %             = statorwicklungsentwurf(P_n,alpha_p_ang,B_m_max,a,l_i,T_p,E_h,w,D,p,m);
    %         lambda = l_i / T_p;
    %     end
    
    %% Statorstrom, Strombelag und Nutquerschnitt im Stator berechnen
    [ I_s_zw,A_S,A_L,A_1S,A_nS,A_wS,A_wL]...
        = leiterquerschnitte(I_n1,a,z_S,D,cphi,S_S_max,S_L_max,N_S,phi_n,z_nS);
    %% Ermittlung der Nutform im Stator
    
    [b_zS,h_nS,b_nS,b_nS_m] ...
        = nutform_stator(D,N_S,B_max,T_nS,l_i,l_fe,phi_fe,B_zS_soll,A_nS);
    %% Ermittlung der Nutform im Rotor
    [ N_L,T_nL,A_nL,b_zL,h_nL,b_nL,q_L,h_nL_max ]...
        = nutform_rotor(delta,D,N_S,p,m,A_wL,B_max,l_i,l_fe,phi_fe,B_zL_soll );
    %% ENTWURF DES MAGNETKREIS
    %% Ständerrückenhöhe [m] und abgeschätzte Zahninduktion [T]
    [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
        gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
        = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
        N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);
    %% Magnetisierungdiagramm und Bestimmung der Zahnfeldstärke H aus Feldinduktion B
    [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
        = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
    %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
    [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
    B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
    B_zL_tat=B_max_tat*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);
    %% An dieser Stelle liegen alle geometrischen Größen vorläufig fest.
    % Ab hier anpassen und optimieren
    %% Statorzahngeometrie anpassen/ auf gewünschte Zahninduktion optimieren
    while B_zS_tat > B_zS_soll
        b_zS = b_zS + 0.0001;           % Zahn um 0.1mm verbreitern
        disp('Statorzahn um 0.1mm verbreitert')
        [ h_nS ] = statorzahnkorrektur(A_nS,b_zS,N_S,D );       % neue Nuthöhe berechnen und Magnetkreisparameter updaten
        b_nS = T_nS - b_zS;             %Nutbreite
        %% Rotorzahninduktion berechnen
        [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
            gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
            = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
            N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);
        [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
            = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
        %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
        [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
        B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
        [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
            = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
        %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
        [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
        B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
        B_zL_tat=B_max_tat*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);
        
    end
    while B_zS_tat < B_zS_min
        b_zS = b_zS - 0.0001;           % Zahn um 0.1mm verschmälern
        disp('Statorzahn um 0.1mm verschmälert')
        [ h_nS ] = statorzahnkorrektur(A_nS,b_zS,N_S,D );       % neue Nuthöhe berechnen und Magnetkreisparameter updaten
        b_nS = T_nS - b_zS;             %Nutbreite
        %% Rotorzahninduktion berechnen
        [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
            gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
            = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
            N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);
        [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
            = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
        %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
        [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
        B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
        [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
            = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
        %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
        [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
        B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
        B_zL_tat=B_max_tat*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);
        
    end
    while B_zL_tat > B_zL_soll
        b_zL = b_zL + 0.0001;
        disp('Rotorzahn um 0.1mm verbreitert')
        [ h_nL,b_nL,h_nL_max ] = rotorzahnkorrektur( delta,N_L,b_zL,D,A_nL,T_nL );
        if h_nL_max <= h_nL | ~isreal(h_nL)
            while h_nL_max <= h_nL | ~isreal(h_nL)
                l_i = l_i + 2/1000;
                l_fe = l_i-(2*delta/1000);
                l = l_fe;
                disp('Maschine um 2mm verlängert');
                [ E_h_r,a] = parallelezweige( P_n,E_h,B_m_max,l_i,T_p,w,p);
                [w_s_opt,q_S,z_nS,w_s,N_S,T_nS,w_ges,z_S,q_S_alt,w_s_fehler,W,u,k_ges,y_d,...
                    X_v,y_v,y_n,alpha_nS,et_v,et,Z,xi_1_tat,B_max,B_m_tat,B_m_ang,Phi_h,Phi_delta,Phi_delta_tat,Wicklungsart ]...
                    = statorwicklungsentwurf(P_n,alpha_p_ang,B_m_max,a,l_i,T_p,E_h,w,D,p,m );
                lambda = l_i / T_p;
                [ I_s_zw,A_S,A_L,A_1S,A_nS,A_wS,A_wL]...
                    = leiterquerschnitte(I_n1,a,z_S,D,cphi,S_S_max,S_L_max,N_S,phi_n,z_nS);
                [b_zS,h_nS,b_nS,b_nS_m] ...
                    = nutform_stator(D,N_S,B_max,T_nS,l_i,l_fe,phi_fe,B_zS_soll,A_nS);
                [ N_L,T_nL,A_nL,b_zL,h_nL,b_nL,q_L,h_nL_max ]...
                    = nutform_rotor(delta,D,N_S,p,m,A_wL,B_max,l_i,l_fe,phi_fe,B_zL_soll );
                [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
                    gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
                    = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
                    N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);
                
                %% Magnetisierungdiagramm und Bestimmung der Zahnfeldstärke H aus Feldinduktion B
                [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
                    = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
                %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
                [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
                B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
                B_zL_tat=B_max_tat*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);
            end
        else
            
            %% Rotorzahninduktion berechnen
            [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
                gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
                = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
                N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);
            [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
                = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
            %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
            [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
            B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
            [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
                = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
            %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
            [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
            B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
            B_zL_tat=B_max_tat*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);
            
        end
        while B_zS_tat > B_zS_soll
            b_zS = b_zS + 0.0001;           % Zahn um 0.1mm verbreitern
            disp('Statorzahn um 0.1mm verbreitert')
            [ h_nS ] = statorzahnkorrektur(A_nS,b_zS,N_S,D );       % neue Nuthöhe berechnen und Magnetkreisparameter updaten
            b_nS = T_nS - b_zS;             %Nutbreite
            %% Rotorzahninduktion berechnen
            [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
                gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
                = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
                N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);
            [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
                = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
            %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
            [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
            B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
            [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
                = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
            %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
            [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
            B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
            B_zL_tat=B_max_tat*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);
            
        end
    end
    while B_zL_tat < B_zL_min
        b_zL = b_zL - 0.0001;
        disp('Rotorzahn um 0.1mm verschmälert')
        [ h_nL,b_nL,h_nL_max ] = rotorzahnkorrektur( delta,N_L,b_zL,D,A_nL,T_nL );
        if h_nL_max <= h_nL | ~isreal(h_nL)
            while h_nL_max <= h_nL | ~isreal(h_nL)
                l_i = l_i + 2/1000;
                l_fe = l_i-(2*delta/1000);
                l = l_fe;
                disp('Maschine um 2mm verlängert');
                [ E_h_r,a] = parallelezweige( P_n,E_h,B_m_max,l_i,T_p,w,p);
                [w_s_opt,q_S,z_nS,w_s,N_S,T_nS,w_ges,z_S,q_S_alt,w_s_fehler,W,u,k_ges,y_d,...
                    X_v,y_v,y_n,alpha_nS,et_v,et,Z,xi_1_tat,B_max,B_m_tat,B_m_ang,Phi_h,Phi_delta,Phi_delta_tat,Wicklungsart ]...
                    = statorwicklungsentwurf(P_n,alpha_p_ang,B_m_max,a,l_i,T_p,E_h,w,D,p,m );
                lambda = l_i / T_p;
                [ I_s_zw,A_S,A_L,A_1S,A_nS,A_wS,A_wL]...
                    = leiterquerschnitte(I_n1,a,z_S,D,cphi,S_S_max,S_L_max,N_S,phi_n,z_nS);
                [b_zS,h_nS,b_nS,b_nS_m] ...
                    = nutform_stator(D,N_S,B_max,T_nS,l_i,l_fe,phi_fe,B_zS_soll,A_nS);
                [ N_L,T_nL,A_nL,b_zL,h_nL,b_nL,q_L,h_nL_max ]...
                    = nutform_rotor(delta,D,N_S,p,m,A_wL,B_max,l_i,l_fe,phi_fe,B_zL_soll );
                [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
                    gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
                    = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
                    N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);
                
                %% Magnetisierungdiagramm und Bestimmung der Zahnfeldstärke H aus Feldinduktion B
                [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
                    = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
                %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
                [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
                B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
                B_zL_tat=B_max_tat*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);
            end
        else
            
            %% Rotorzahninduktion berechnen
            [Phi_rS_max,h_rS,Phi_rL_max,h_rL,D_a,A_rS,A_rL,l_i_tat,T_nL_tat,b_sS,b_sL,...
                gamma_S,gamma_L,k_cS,k_cL,k_c,V_delta_ang,B_zS_abg,B_zL_abg]...
                = magnetkreis( Phi_delta_tat,l_fe,phi_fe,B_rS_zul,B_rL_zul,D,h_nS,delta,...
                N_L,b_nL,b_nS,T_nS,B_max,mu_0,l,b_zS,b_zL);
            [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
                = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
            %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
            [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
            B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
            [V_zS_abg,V_zL_abg,k,B_werte,fit_int]...
                = zahnab(plothandle,f_n,B_zS_abg,B_zL_abg,h_nS,h_nL,V_delta_ang );
            %% korrigerter Abplattungsfaktor und tatsächliche Zahninduktion [T]
            [alpha_p_tat,B_max_tat,V_delta_tat] = kor_alpha_p(plothandle,k,B_m_tat,k_c,delta,mu_0 );
            B_zS_tat=1.03*B_max_tat*T_nS*l_i_tat/(l_fe*phi_fe*b_zS);
            B_zL_tat=B_max_tat*T_nL_tat*l_i_tat/(l_fe*phi_fe*b_zL);
            
        end
    end
    %% Magnetisierungdiagramm und Bestimmung der tatsächlichen
    %  Zahnfeldstärke H aus Feldinduktion B
    [ V_zS_tat,V_zL_tat,H_zS_tat,H_zL_tat ] ...
        = zahntat(plothandle,B_werte,fit_int,B_zS_tat,B_zL_tat,h_nS,h_nL );
    %% maximal auftretende Rückeninduktionen in T
    B_rS_max=1.05*Phi_delta_tat/(2*A_rS/1000000);
    B_rL_max=Phi_delta_tat/(2*A_rL/1000000);
    %% Magnetisierungdiagramm und Bestimmung der tatsächlichen Rückenfeldstärke H aus Feldinduktion B
    [ H_rS_max,H_rL_max,T_rS,T_rL,h_rS_d_T_rS,h_rL_d_T_rL,sred_1,sred_2 ]...
        = tatrueck(plothandle,B_werte,B_rS_max,B_rL_max,fit_int,D,h_nS,p,delta,...
        h_nL,h_rS,h_rL,B_rS_zul,T_p);
    %% Spannungsabfall im Rücken in A
    V_rS = H_rS_max*sred_1;
    V_rL = H_rL_max*sred_2;
    %% gesamter magnetischer Spannungsabfall in A // Durchflutungsamplitude
    Theta=V_rS+V_rL+V_zS_tat+V_zL_tat+V_delta_tat;
    %% Magnetisierung-, Läufer- und Kurzschlussringstroms in A
    I_L = (2*m*w_s*xi_1_tat)*I_n1*cphi/(N_L); % Stabstrom 
    I_R = I_L/(2*sin(p*pi/(N_L)));
    A_R = I_R/S_R; % Kurzschlussringquerschnitt
    I_mu = Theta*pi*p/(m*w_s*xi_1_tat*sqrt(2)); % Magnetisierungsstrom
    %% Hauptinduktivität
    lambda_h = 3*T_p/(pi^2 * k_c *delta/1000); %_Hauptleitwert
    L_h = m*mu_0*2*T_p*l_i*(w_s*xi_1_tat)^2/((k_c*delta/1000)*pi*pi*p); % ungesättigte Hauptinduktivität
    L_h_unges = L_h;
    delta_s = k_c*delta; %ideelle Luftspaltlänge unter Berücksichtigung der Nutung
    delta_ss = mu_0*Theta/(B_max_tat); %ideelle Luftspaltlänge unter Berücksichtigung der Nutung und dem
    % magnetischem Spannungsabfall im Eisen
    L_h1 = L_h * (delta_s/1000)/delta_ss;  %gesättigte Induktivität S.531 Müller08
    L_hges1 = L_h1 * pi / (2*alpha_p_tat); % Berücksichtigung der Abplattung
    
    %% Wicklungswiderstände
    [ R_S_20,R_S_75,R_S_95,R_S_115,R_S_T,R_L_20_s,R_L_75_s,R_L_95_s,R_L_115_s,R_L_T_s,l_m1,l_w1]...
        = wicklungswiderstaende( D,Material_Stator,Material_Laeufer,l,T_p,U_n,A_1S,w_s,A_nL,delta,h_nL,q_L,N_L,p,A_R,xi_1_tat);
    
    %% Streuinduktivität L_sigma_1 und L_sigma_2
    [ L_sigma_1,L_sigma_2,L_sigma_2_s,L_1,sigma,sigma_1,sigma_2,L_sigma_schr,ue_h ] = ...
        streuinduktivitaeten(b_sS,delta,h_nS,b_nS,W,T_p,mu_0,l_i_tat,w_s,xi_1_tat,p,q_S,l_w1,L_hges1,...
        b_sL,h_nL,b_nL,D,N_L,N_S,Wicklungsart);
    %% bezogener Statorwiderstand rho_1
    R_1 = R_S_95; % 60 Grad Dauerbetrieb
    rho_1 = R_1/(w*L_1);
    %% delta_rho_1
    delta_rho_1 = sqrt(1+(rho_1/sigma)^2)*sqrt(1+rho_1^2);
    %% Kippmoment M_k
    M_k = 3 * p * U_s^2 * (1 - sigma)/(2 * w^2 * L_1 * sigma * delta_rho_1 *...
        (1+(rho_1*(1-sigma))/(sigma * delta_rho_1)));
    M_k_ohne_R1 = 3 * p * U_s^2 / (2*w^2*L_1*(sigma/(1-sigma)));
%% Rotorwiderstand
R_2_s = R_L_115_s;                 % Läuferwiderstand auf Statorseite bezogen bei 115 Grad
R_2 = R_2_s/ue_h^2;
    disp('Fertig')
    %% Werte für die weitere Berechnung ausgeben

R_2=R_2_s;
L_sigma1 = L_sigma_1;
L_sigma2 = L_sigma_2_s*2; 
L_h = L_hges1; 
I_1N=I_n;
U_1N=U_s;
l_Fe=l_fe;
Psi_1N = L_h*I_mu;

clearvars -except keepVariables W_Schrittweite M_Schrittweite P_n L_h1 L_hges1 m l_i cphi l_i_tat p Psi_1N W_max I_L U_1N R_1 R_2 L_h L_sigma1 L_sigma2 I_1N I_mu D_a phi_n ...
D D_L delta N_S N_L h_nS h_nL A_nS A_nL A_rS A_rL B_rS_max B_rL_max B_zS_tat B_zL_tat h_rS h_rL l_Fe n_n gamma_s Psi_1N
