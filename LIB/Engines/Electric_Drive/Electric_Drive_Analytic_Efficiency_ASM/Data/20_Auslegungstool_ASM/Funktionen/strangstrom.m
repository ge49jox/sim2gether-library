function [ P_el,I_n,I_n1,I_s,P_i ] = strangstrom( P_n,eta,U_n,cphi,m,U_s,E_h )
%% Erforderliche elektrische Wirkleistung P_w1 in kW
P_el=P_n/(eta/100);
%% Ermittlung des Nennstroms I_n in A
I_n=P_el*1000/(sqrt(3)*U_n*cphi); P_s = P_el/cphi;
%% Ermittlung des Nennstroms I_n1 je Strang (nach der Schaltung) in A
I_n1=P_s*1000/(U_s*m);
%% Strangstrom I_s (in einer Phase des Netz) in A
I_s=P_s*1000/(m*U_n);          % 25% h�herer Strom, nur bei Visio M
%% Innere Statorspannung U_i1 in V
%U_i1=C_m*U_n/((eta/100)*cphi);
%% Innere Scheinleistung P_si1 in kW
%P_si1=P_s*U_i1/U_n;
%% Innere Scheinleistung in kW
P_i=P_n*E_h/(U_s*cphi*eta/100);

end

