function [b_zS,h_nS,b_nS,b_nS_m] ...
    = nutform_stator(D,N_S,B_max,T_nS,l_i,l_fe,phi_fe,B_zS_soll,A_nS)
%%
syms x
alpha = 360/N_S;
b_zS = 1.03*B_max*T_nS*l_i/(l_fe*phi_fe*B_zS_soll);            % minimale Zahnbreite in m (3% h�here Zahninduktion als im Luftspalt)
f = @(x) pi*(((D+2*x)/2).^2 - (D/2)^2)*(alpha/360) - b_zS*x - A_nS/(1000*1000);
sol = vpa(solve(f),2);
h_nS = double(sol(1)); 

b_nS = T_nS - b_zS;     % Breite der Nut am Luftspalt 
b_nS_m = A_nS/(1000*1000)/h_nS;     % mittlere Breite der Nut 
end

