function [ V_zS_tat,V_zL_tat,H_zS_tat,H_zL_tat ] ...
            = zahntat(plothandle,B_werte,fit_int,B_zS_tat,B_zL_tat,h_nS,h_nL )
%% Magnetisierungdiagramm und Bestimmung der tats�chlichen Zahnfeldst�rke H aus Feldinduktion B
for i=1:length(B_werte)-1                                                                                                  % Bestimmung des relevanten Teilabschnitts
    if and(B_zS_tat>=B_werte(i),B_zS_tat<=B_werte(i+1))
        abschn_S=i;  
    end
end
for i=1:length(B_werte)-1                                                                                                  % Bestimmung des relevanten Teilabschnitts
    if and(B_zL_tat>=B_werte(i),B_zL_tat<=B_werte(i+1))
        abschn_L=i;  
    end
end
% Definition der Teilfunktion und Berechnung der Feldst�rke
xWerte_S=linspace(fit_int.breaks(abschn_S),fit_int.breaks(abschn_S+1),100);
cf_S=fit_int.coefs(abschn_S,:);
teilfkt_S=polyval(cf_S,xWerte_S-fit_int.breaks(abschn_S));
H_zS_tat = polyval(cf_S,B_zS_tat-fit_int.breaks(abschn_S));
xWerte_L=linspace(fit_int.breaks(abschn_L),fit_int.breaks(abschn_L+1),100);
cf_L=fit_int.coefs(abschn_L,:);
teilfkt_L=polyval(cf_L,xWerte_L-fit_int.breaks(abschn_L));
H_zL_tat = polyval(cf_L,B_zL_tat-fit_int.breaks(abschn_L));
% Diagramm der Magnetisierungskurve
    if strcmp(plothandle,'JA') == 1 
    figure(3)
    plot(linspace(fit_int.breaks(abschn_S),fit_int.breaks(abschn_S+1),100),teilfkt_S,'r-')
    plot(B_zS_tat,H_zS_tat,'rx')
    plot(linspace(fit_int.breaks(abschn_L),fit_int.breaks(abschn_L+1),100),teilfkt_L,'r-')
    plot(B_zL_tat,H_zL_tat,'rx')
    end
clear xWerte_S xWerte_L abschn_L abschn_S teilfkt_L teilfkt_S cf_L cf_S
%% tats�chlicher Spannungsabfall in den Z�hnen in A
V_zS_tat=H_zS_tat*h_nS;
V_zL_tat=H_zL_tat*h_nL;
end

