mu_0=ndata(1,6);
mu_0=4*pi*10^-7;
%% Einlesen der Motoreckdaten
%f_n=ndata(2,6);                          % in Hz        - Nennfrequenz in Hz
%U_n=ndata(3,6);                         % in V          - Nennspannung (line to line)
%m=ndata(4,6);                            % in 1          - Anzahl der Stränge
%P_n=ndata(5,6);                          % in kW         - Nennleistung
%n_n=ndata(6,6);                          % in 1/min      - Nenndrehzahl
%% Eingabe der Materialdaten
%Material_Stator=text(11,6);
%Material_Laeufer=text(12,6) ;
%% Eingabe der Schaltungsdaten
%Schaltung=text(13,6);
%% Eingabe der Bauweise
%Bauweise=text(14,6);
%% Eingabe der Kühlart
%Kuehlung=text(15,6);
%% Berechnung des Nenndrehmoments
M_n=P_n*1000*60/(2*pi*n_n);                 % in Nm         - Nenndrehmoment
%% Berchnung der notwendigen Umnrichterfrequenz
f_n= p*n_n/60*1.1;
W_max = n_max/60*pi*2;       % maximale Winkelgeschwindigkeit in rad/s
%% Winkelgeschwindigkeit der Versorgung in rad/s
w=2*pi*f_n;
%% Ermittlung der Strangspannung
if strcmp(Schaltung,'Dreieck')==1
    U_s=U_n;
elseif strcmp(Schaltung,'Stern')==1
    U_s=U_n/sqrt(3);
else
    disp('ungueltige Schaltung')
end

%% Ermittlung der induzierten Spannung E_h in V
if P_n<=30
    E_h=0.95*U_s;           % Literatur Vorfaktor [0.92....0.96]
else
    E_h=U_s;
end

%% ENTWURF DER WICKLUNG
%% Variablen für Wicklungsentwurf, Nutformen und Magnetkreis einlesen
B_m_max = ndata(41,6);
%% maximalen Stromdichte im Ständer in A/mm^2
S_S_max=ndata(80,6);
%% maximale Stromdichte S_l im Läufer in A/mm^2
if strcmp(Material_Laeufer,'Kupfer')==1
    S_L_max=ndata(97,6);
elseif strcmp(Material_Laeufer,'Aluminium')==1
    S_L_max=ndata(97,6);
else
    disp('ungueltiges Laeuferwicklungsmaterial')
end
%% Nutfüllfaktor phi_n in 1
phi_n=ndata(82,6);
phi_fe=0.95;
B_zS_max=ndata(85,6);                                  % Ständerzahninduktion in T
alpha_p_ang=1.4;                                       % Abplattungsfaktor
B_zL_max=ndata(103,6);                                 % Läuferzahninduktion in T
% B_rS_zul=1.80;                                       % max. zul. Ständerrückeninduktion in T (S.422)
B_rS_zul=ndata(116,6);
% B_rL_zul=1.60;                                       % max. zul. Läuferrückeninduktion in T (S.422)
B_rL_zul=ndata(119,6);