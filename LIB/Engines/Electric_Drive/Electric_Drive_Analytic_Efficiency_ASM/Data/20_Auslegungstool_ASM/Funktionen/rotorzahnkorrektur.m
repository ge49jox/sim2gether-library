function [ h_nL,b_nL,h_nL_max ] = rotorzahnkorrektur( delta,N_L,b_zL,D,A_nL,T_nL )
%% Korrektur der Läufernutgeometrie                                                             
D = D-2*delta/1000;
b_nL = T_nL - b_zL;
syms y
alpha_NL = 360/N_L;
f = @(y) pi*(-((D-2*y)/2).^2 + (D/2)^2)*(alpha_NL/360) - b_zL*y - A_nL/(1000*1000);
f1 = @(y) pi*(-((D-2*y)/2).^2 + (D/2)^2)*(alpha_NL/360) - b_zL*y - y*b_nL/2;
%Mit f1 kann die maximale Statornuthöhe in Abhängigkeit von der
%Statorzahnbreite berechnet werden. Es gilt: h_nL_max * b_nL_min /2 != A_nL (Fläche ist ein Kreissegment) 
sol2 = vpa(solve(f1),2);
h_nL_max = double(sol2(2));
sol = vpa(solve(f),2);
h_nL = double(sol(1)); 



end

