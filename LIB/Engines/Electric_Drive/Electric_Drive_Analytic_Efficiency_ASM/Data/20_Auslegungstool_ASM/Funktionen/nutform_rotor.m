function [ N_L,T_nL,A_nL,b_zL,h_nL,b_nL,q_L,h_nL_max ] =...
            nutform_rotor(delta,D,N_S,p,m,A_wL,B_max,l_i,l_fe,phi_fe,B_zL_soll )

%% Nutzahl am L�ufer in 1
N_L = N_S - 4*p;
alpha_NL = 360/N_L;
q_L = N_L/(2*p*m);
%% Nutteilung am L�ufer in 1
T_nL=(D-2*delta/1000)*pi/N_L;
%% Nutquerschnitt des L�ufers A_nS in mm^2 
A_nL=A_wL/N_L;
%% Ermittlung der Nutgeometrie im L�ufer 
D = D-2*delta/1000;
syms y
b_zL = B_max*T_nL*l_i/(l_fe*phi_fe*B_zL_soll);        % minimale Zahnbreite in m
b_nL = T_nL - b_zL;

f = @(y) pi*(-((D-2*y)/2).^2 + (D/2)^2)*(alpha_NL/360) - b_zL*y - A_nL/(1000*1000);
f1 = @(y) pi*(-((D-2*y)/2).^2 + (D/2)^2)*(alpha_NL/360) - b_zL*y - y*b_nL/2;

sol2 = vpa(solve(f1),2);
h_nL_max = double(sol2(2));
sol = vpa(solve(f),2);
h_nL = double(sol(1)); 



end

