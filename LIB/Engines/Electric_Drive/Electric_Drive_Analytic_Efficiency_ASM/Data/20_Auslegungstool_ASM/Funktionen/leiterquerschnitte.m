function [ I_s_zw,A_S,A_L,A_1S,A_nS,A_wS,A_wL ]...
            = leiterquerschnitte( I_n1,a,z_S,D,cphi,S_S_max,S_L_max,N_S,phi_n,z_nS)


%% Zweigstrom in A
I_s_zw=I_n1/a;
%% effektiver Strombelag am Statorumfang A_s in A/m
A_S=z_S*I_s_zw/(D*pi); 
%% effektiver Strombelag im L�ufer A_l in A/m
A_L=A_S*cphi;
%% Leiterquerschnitt im St�nder A_1S in mm^2
A_1S=I_s_zw/S_S_max;
%% Nutquerschnitt des Stators A_nS in mm^2
A_nS=z_nS*A_1S/phi_n;
%% Gesamter leitender Wicklungsquerschnitt des St�nders in A_wS in mm^2
A_wS=A_1S*N_S*z_nS;
%% Gesamter leitender Wicklungsquerschnitt des L�ufers in A_wL in mm^2
A_wL=A_wS*(S_S_max/S_L_max)*cphi; 


end

