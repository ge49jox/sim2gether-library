function [W_Cmot,W_Cgen,w_2Cmot,w_2Cgen,M_Cmot,M_Cgen] = ...
    Crossoverwerte_aus_ASM_Daten( ...
        Maschinenkonstanten_10, ...
        Betriebsgrenzen_4, ...
        charakt_w2_M_5, ...
        Nennwerte_8 ...
    )

%% wichtige Hinweise:
 
% RFD: Rotorfrequenzdiagramm;
    % Horizontalachse: w_2
    % Vertikalachse:   M

% Diese Funktion berechnet alle Crossoverwerte.
% Der Index C ist an das englische Wort "crossover" angelehnt.
    % Es bedeutet "Kreuzung" oder "�bergang".
% Sowohl W_Cmot, als auch W_Cgen sind positive Werte!
% w_2Cmot und M_Cmot sind ebenfalls positiv.
% Hingegen sind w_2Cgen und M_Cgen negativ.

% Definition von W_Cmot,w_2Cmot,M_Cmot
%(im Generator-Betrieb (gen) ganz analog):
    % Man plotte die U_1N-Grenzkurve f�r 0 <= W <= W_max (oder sogar
    % gr��er als W_max). Die Spur aller Hochpunkte der U_1N-Grenzkurve
    % bilden eine weitere Kurve, im Folgenden U_1N-HOP-Kurve genannt.
    % Der Schnittpunkt zwischen der U_1N-HOP-Kurve mit der I_1N-Grenzkurve
    % (I_1N, nicht U_1N!) sei der Punkt C. Jenes W, f�r das die
    % U_1N-Grenzkurve genau durch den Punkt C verl�uft, ist die
    % motorische Crossover-Winkelgeschwindigkeit W_Cmot.
% Der Punkt C hat die Koordinaten (w_2Cmot,M_Cmot).
    
    % F�r 0 <= W <= W_Nmot wird M begrenzt durch I_1N UND Psi_1N.
    % F�r W_Nmot <= W <= W_Cmot wird M begrenzt durch I_1N UND U_1N.
        % In diesem Bereich gilt Psi_1 < Psi_1N
        % (prim�rer Feldschw�chebereich).
    % F�r W_Cmot <= W wird M begrenzt NUR durch U_1N.
        % In diesem Bereich gilt weiterhin Psi_1 < Psi_1N
        % (sekund�rer Feldschw�chebereich).

% W_Cmot ist in jedem Fall gr��er als W_Nmot. F�r W_Nmot < W < W_Cmot
% ergibt sich das gr��te erreichbare Moment M aus dem nichttrivialen 
% Schnittpunkt (ntsp) der I_1N-Grenzkurve mit der U_1N-Grenzkurve im RFD.
% F�r W_Cmot < W ergibt sich das gr��te erreichbare Moment M aus dem
% Maximum der U_1N-Grenzkurve im Intervall w_2N < w_2 < w_2Psi1k.

% W_Cgen ist in jedem Fall gr��er als W_Ngen. F�r W_Ngen < W < W_Cgen
% ergibt sich das negativste erreichbare Moment M aus dem nichttrivialen 
% Schnittpunkt (ntsp) der I_1N-Grenzkurve mit der U_1N-Grenzkurve im RFD.
% F�r W_Cgen < W ergibt sich das negativste erreichbare Moment M aus dem
% Minimum der U_1N-Grenzkurve im Intervall -w_2Psi1k < w_2 < -w_2N.

% Es kann durchaus sein, dass W_C gr��er ist als W_max, wenn W_max ein
    % sehr kleiner Wert ist.

    

%% Einlesen von ASM-Daten:
% Daten, die in dieser Funktion nicht ben�tigt werden,
    % sind auskommentiert.

% Maschinenkonstanten_10:
           m = Maschinenkonstanten_10(1);       
           p = Maschinenkonstanten_10(2);         
       % R_1 = Maschinenkonstanten_10(3);       
       % R_2 = Maschinenkonstanten_10(4);       
       % L_h = Maschinenkonstanten_10(5);       
  % L_sigma1 = Maschinenkonstanten_10(6);  
  % L_sigma2 = Maschinenkonstanten_10(7);  
         L_1 = Maschinenkonstanten_10(8);       
       % L_2 = Maschinenkonstanten_10(9);      
       sigma = Maschinenkonstanten_10(10);
       
% Betriebsgrenzen_4:
      I_1N = Betriebsgrenzen_4(1);
      U_1N = Betriebsgrenzen_4(2);
  % Psi_1N = Betriebsgrenzen_4(3);
   % W_max = Betriebsgrenzen_4(4);

% charakt_w2_M_5:
    % w_2opt = charakt_w2_M_5(1);
      w_2I1k = charakt_w2_M_5(2);
    w_2Psi1k = charakt_w2_M_5(3);
    % M_I1Nk = charakt_w2_M_5(4);
  % M_Psi1Nk = charakt_w2_M_5(5);
    
% Nennwerte_8:
        % w_2N = Nennwerte_8(1);
         % M_N = Nennwerte_8(2);
        W_Nmot = Nennwerte_8(3);
        W_Ngen = Nennwerte_8(4);
     % w_1Nmot = Nennwerte_8(5);
     % w_1Ngen = Nennwerte_8(6);
  % P_mechNmot = Nennwerte_8(7);
  % P_mechNgen = Nennwerte_8(8);


%% Berechnung von W_Obergrenze: Ab hier beginnt die eigentliche Funktion.

% W_Obergrenze sei eine Obergrenze f�r W-Suchintervalle.
% W_Obergrenze kann durchaus gr��er sein als W_max.

M_I1N_w2Psi1k = M_aus_I1_w2(I_1N,w_2Psi1k,Maschinenkonstanten_10);
    % Dies ist der M-Wert f�r die I_1N-Grenzkurve, ganz rechts,
    % d. h. bei w_2Psi1k. Der Punkt C muss auf jeden Fall etwas links davon
    % liegen.

% Finde ein W_Obergrenze so, dass gilt:
    % M_I1N_w2Psi1k = M_aus_U1_w2_W(U_1N,w_2Psi1k,W_Obergrenze,...)
% F�r gro�e W (das ist hier der Fall) kann dies in guter N�herung
% analytisch erfolgen, siehe ASM-Formelsammlung:
    % F�r gro�e W und f�r w_2 ca. w_2Psi1k gilt in sehr guter N�herung:
    %     U_1^2 m*p  1-sigma
    % M = -----*---*---------  mit w_1 = w_2Psi1k+p*W;
    %     w_1^2  2  sigma*L_1
% =>    
W_Obergrenze = (1/p)*(sqrt( U_1N^2*m*p*(1-sigma)/...
    ( M_I1N_w2Psi1k*2*sigma*L_1 ) ) + w_2Psi1k) ;
% Im Motorbetrieb m�sste es eigentlich hei�en: - w_2Psi1k);
% Im Generatorbetrieb m�sste es eigentlich hei�en: + w_2Psi1k);
% Es wird + gew�hlt. Damit ist man auf der sicheren Seite, denn
% W_Obergrenze soll lieber etwas zu gro� sein, als zu klein.
                    
W_Obergrenze = 2*W_Obergrenze; % zur Sicherheit, da analytische Formel
% nicht exakt ist. Dadurch erh�ht sich nat�rlich der Rechenaufwand.
% Dies wird in Kauf genommen. Es geht nur darum, die Gr��enordnung von 
% W_Obergrenze zu kennen. Da die C-Werte nur einmal berechnet werden
% m�ssen, spielt der Rechenaufwand kaum eine Rolle.


%% Berechnung von W_Cmot,w_2Cmot,M_Cmot:

W_lg = W_Nmot;       % Startwert f�r linke  W-Grenze;
W_rg = W_Obergrenze; % Startwert f�r rechte W-Grenze;
% W_Cmot muss irgendwo dazwischen liegen!

% Diese while-Schleife setzt die Bisektion um:

while (W_rg/W_lg) > (1 + 2*eps) % Rechenaufwand spielt hier keine Rolle.
    % eps returns the distance from 1.0 to the next largest
    % double-precision number, that is eps = 2^(-52) = 2.2e-16.
    
    W_mitte = (W_lg + W_rg)/2; % teilt das aktuelle W-Suchintervall in zwei
        % gleich gro�e H�lften;    
    
    % Berechne Hochpunkt (w2HOP,MHOP) der U_1N-Grenzkurve im RFD
    % f�r verschiedene W mit W_Nmot < W < W_Obergrenze.
    % Matlab kennt aber nur Minimierungsprobleme.
    % L�sung: Ein Maximum von f(x) ist ein Minimum von -f(x).
        
    Hilfsfunktion1 = @(a,x,b,c) -M_aus_U1_w2_W(a,x,b,c); % Minuszeichen!
        a = U_1N;
        % x = w_2, wird gesucht;
        b = W_mitte;
        c = Maschinenkonstanten_10;
        
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,x,b,c);
    [w2HOP_mitte,MHOP_mitte] = fminbnd(Hilfsfunktion2,w_2I1k,w_2Psi1k);
    % Dieses Minimum (bzw. Maximum) existiert im Motorbetrieb immer!
    
    MHOP_mitte = abs(MHOP_mitte); % wegen des Minuszeichens!
        % Der urspr�ngliche Wert von MHOP ist ja positiv!
        % Je gr��er W, desto kleiner MHOP!
      
    M_I1N_mitte = M_aus_I1_w2(I_1N,w2HOP_mitte,Maschinenkonstanten_10); 
        % berechnet den M_Wert der I_1N-Grenzkurve �ber w2HOP;
        % Je gr��er W, desto gr��er w2HOP, desto kleiner M_1N_mitte!
    
    if MHOP_mitte > M_I1N_mitte % d. h. W_mitte war zu klein.
        W_lg = W_mitte;
        % W_rg bleibt.
    else % d. h. MHOP_mitte <= M_I1N_mitte, d. h. W_mitte war zu gro�.    
        % W_lg bleibt.
        W_rg = W_mitte;
    end
    
end

W_Cmot = W_rg; % W_lg und W_rg sind nach der Iteration fast gleich.
    % Benutze trotzdem W_rg, da etwas gr��er, damit das zugeh�rige MHOP
    % ein ganz klein wenig unter dem idealen Punkt C liegt, und nicht
    % dar�ber.

w_2Cmot = w2HOP_mitte;
 M_Cmot = MHOP_mitte;

 
%% Berechnung von W_Cgen:


W_lg = W_Ngen;       % Startwert f�r linke  W-Grenze;
W_rg = W_Obergrenze; % Startwert f�r rechte W-Grenze;

% Diese while-Schleife setzt die Bisektion um.

while (W_rg/W_lg) > (1 + 2*eps) % Rechenaufwand spielt hier keine Rolle.
    % eps returns the distance from 1.0 to the next largest
    % double-precision number, that is eps = 2^(-52) = 2.2e-16.
    
    W_mitte = (W_lg + W_rg)/2; % teilt das aktuelle W-Suchintervall in zwei
        % gleich gro�e H�lften;     
    
    % Berechne Tiefpunkt (w2TIP,MTIP) der U_1N-Grenzkurve im RFD
    % f�r verschiedene W mit W_Ngen < W < W_Obergrenze.
    
    Hilfsfunktion1 = @(a,x,b,c) +M_aus_U1_w2_W(a,x,b,c); % Pluszeichen!
        a = U_1N;
        % x = w_2, wird gesucht;
        b = W_mitte;
        c = Maschinenkonstanten_10;
        
    Hilfsfunktion2 = @(x) Hilfsfunktion1(a,x,b,c);
    [w2TIP_mitte, MTIP_mitte] = fminbnd(Hilfsfunktion2,-w_2Psi1k,-w_2I1k);
    % Dieses Minimum existiert im Generator-Betrieb immer!
    % Im Extremfall liegt das Minimum bei -w_2Psi1k.
    % Alles links davon ist irrelevant!
    
    M_I1N_mitte = M_aus_I1_w2(I_1N,w2TIP_mitte,Maschinenkonstanten_10);
        % berechnet den M_Wert der I_1N-Grenzkurve unter w2TIP;

    % Je gr��er W, desto kleiner ist abs(M_I1N_mitte), d. h. ein
    % gr��eres bzw. positveres M_I1N_mitte!
    
    if MTIP_mitte < M_I1N_mitte % d. h. W war zu klein.
        W_lg = W_mitte;
        % W_rg bleibt.
    else % d. h. MTIP_mitte >= M_I1N_mitte, d. h. W war zu gro�.    
        % W_lg bleibt.
        W_rg = W_mitte;
    end
    
end

W_Cgen = W_rg; % W_lg und W_rg sind nach der Iteration fast gleich.
    % Benutze trotzdem W_rg, da etwas gr��er, damit das zugeh�rige MTIP
    % ein ganz klein wenig �ber dem idealen Punkt C liegt, und nicht
    % darunter.

w_2Cgen = w2TIP_mitte;
 M_Cgen = MTIP_mitte;


end % Hier endet die Funktion.




