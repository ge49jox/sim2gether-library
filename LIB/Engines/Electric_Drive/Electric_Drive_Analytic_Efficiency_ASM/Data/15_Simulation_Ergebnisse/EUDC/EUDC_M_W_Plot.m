%% Wichtige Hinweise:

% Dieses Skript plottet das Drehmoment M �ber der Winkelgeschwindigkeit W
% des Fahrzyklus EUDC:
    % Simulation_EUDC.signals.values(:,1) �ber
    % Simulation_EUDC.signals.values(:,2).

% Au�erdem plottet dieses Skript alle 4 Grenzkurven der folgenden
% Matrix:
    % GK_M_W_Matrix_4(1,:) motorische Grenzkurve 
    % GK_M_W_Matrix_4(2,:) motorische Optimal-Grenzkurve
    % GK_M_W_Matrix_4(3,:) generatorische Optimal-Grenzkurve
    % GK_M_W_Matrix_4(4,:) genartorische Grenzkurve.
% �ber dem Winkelgeschwindigkeitsvektor W_Vektor.


% Die "Schlangenlinie" des Fahrzyklus darf die motorische Grenzkurve
    % niemals �berschreiten. Dies w�rde n�mlich bedeuten, dass
    % die ASM das erforderliche Vortriebsmoment nicht aufbringen kann.
% Die "Schlangenlinie" des Fahrzyklus darf aber sehr wohl
    % die generatorische Grenzkurve unterschreiten.
    % Dies w�rde bedeuten, dass die ASM das erforderliche
    % Verz�gerungsmoment nicht aufbringen kann. Dies macht jedoch nichts
    % aus. In so einem Fall w�rde das fehlende Verz�gerungsmoment durch
    % die mechanischen Bremsen abgedeckt werden.
    % In den eingebundenen Kennfeldmatrizen
        % KF_konv_real_Pelmotgen_Sim_Matrix und
        % KF_opt_real_Pelmotgen_Sim_Matrix
    % ist dies bereits dadurch ber�cksichtigt, dass alle NaN-Werte
    % unterhalb der generatorischen Grenzkurve durch den Wert eben dieser
    % Grenzkurve ersetzt worden sind.
    % Dadurch ist sichergestellt, dass die ASM so stark wie m�glich
    % verz�gert bzw. rekuperiert, auch wenn das Verz�gerungsmoment selbst
    % nicht von der ASM allein aufgebracht werden kann.
    % Der simulierte Energieverbrauch bleibt in diesem Fall somit korrekt.
    
%% alte gca und gcf l�schen:
clearvars gca gcf

%% Kurven plotten:

gcf = figure( ...
    'PaperUnits','centimeters', ...
    'Units','centimeters', ...
    'Position',[ ...
        Plot_figure_x, ...
        Plot_figure_y, ...
        Plot_figure_Breite, ...
        Plot_figure_Hoehe ...
    ], ...
    'Resize',Plot_Resize_String ...
);

gca = axes( ...
    'XLim',[Plot_x_min, Plot_x_max], ...
    'YLim',[1.3*Plot_y_min,1.3*Plot_y_max] ...
);


hold on;
grid on;
set(gca,'layer',Plot_Layer_String);

box on;

title('EUDC: Fahrzyklus-Trajektorie und Drehmoment-Grenzkurven');
xlabel('Winkelgeschwindigkeit \Omega/(rad/s)');
ylabel('Drehmoment M/(Nm)');

% x-Achse:
plot([0,Plot_x_max],[0,0],'black-','LineWidth',1);

% Fahrzyklus-Kurve:
h1 = plot( ...
        Simulation_EUDC.signals.values(:,1), ... % W
        Simulation_EUDC.signals.values(:,2), ... % M
        Plot_Fahrzyklus_LineStyle_String, ...
        'LineWidth',Plot_Fahrzyklus_LineWidth ...
);

% Drehmoment-Grenzkurven:
h2 = plot(W_Vektor,GK_M_W_Matrix_4(1,:), ...
    Plot_GK_mot_LineStyle_String,'LineWidth',Plot_GK_LineWidth);
h3 = plot(W_Vektor,GK_M_W_Matrix_4(2,:), ...
    Plot_GK_optmot_LineStyle_String,'LineWidth',Plot_GK_LineWidth);
h4 = plot(W_Vektor,GK_M_W_Matrix_4(3,:), ...
    Plot_GK_optgen_LineStyle_String,'LineWidth',Plot_GK_LineWidth);
h5 = plot(W_Vektor,GK_M_W_Matrix_4(4,:), ...
    Plot_GK_gen_LineStyle_String,'LineWidth',Plot_GK_LineWidth);


legend( ...
    [h1 h2 h3 h4 h5],{ ...
        'Fahrzyklus-Trajektorie', ...    
        Plot_GK_mot_Legend_String , ...
        Plot_GK_optmot_Legend_String , ...
        Plot_GK_optgen_Legend_String , ...
        Plot_GK_gen_Legend_String  ...
    } ...
);


clearvars gca gcf X Y C h ... % werden nicht mehr ben�tigt
    hcb colorTitleHandle titleString ....
    h1 h2 h3 h4 h5 h6 h7 h8
    
