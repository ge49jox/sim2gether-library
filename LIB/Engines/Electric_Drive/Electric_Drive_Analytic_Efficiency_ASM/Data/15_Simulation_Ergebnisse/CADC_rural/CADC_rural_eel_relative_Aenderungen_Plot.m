%% Wichtige Hinweise:

% Dieses Skript plottet den spezifischen elektrischen Energieverbrauch e_el
% (in Wh/(100km)!), 
% und zwar f�r beide Betriebsstrategien.

% Au�erdem wird noch angegeben:
    % relative Verbrauchs�nderung:  eel_opt/eel_konv - 1;
    % relative Reichweiten�nderung: eel_konv/eel_opt - 1;

% Hier gilt der Fahrzyklus CADC_rural:

%% alte gca und gcf l�schen:
clearvars gca gcf

%% Werte berechnen:

x_CADC_rural = Simulation_CADC_rural.signals.values(end,5);
    % zur�ckgelegte Strecke (in m!);

Eel_konv_CADC_rural = Simulation_CADC_rural.signals.values(end,3);
    % absoluter elektrischer Energieverbrauch (in J!)
    % bei konventioneller Betriebsstrategie;
Eel_opt_CADC_rural = Simulation_CADC_rural.signals.values(end,4);
    % absoluter elektrischer Energieverbrauch (in J!)
    % bei optimaler Betriebsstrategie;

eel_konv_CADC_rural = Eel_konv_CADC_rural/x_CADC_rural;
    % spezifischer elektrischer Energieverbrauch (in J/m = N!)
    % bei konventioneller Betriebsstrategie;
eel_opt_CADC_rural = Eel_opt_CADC_rural/x_CADC_rural;
    % spezifischer elektrischer Energieverbrauch (in J/m = N!)
    % bei optimaler Betriebsstrategie;
    
RVA_CADC_rural = eel_opt_CADC_rural/eel_konv_CADC_rural - 1;
    % RVA = relative Verbrauchs�nderung
    % der optimalen gegen�ber der konventionellen Betriebsstrategie;

RRA_CADC_rural = eel_konv_CADC_rural/eel_opt_CADC_rural - 1;
    % RRA = relative Reichweiten�nderung
    % der optimalen gegen�ber der konventionellen Betriebsstrategie;
    % In dieser Reihenfolge! Das ist kein Tippfehler!

    
%% Bars plotten:

gcf = figure( ...
    'PaperUnits','centimeters', ...
    'Units','centimeters', ...
    'Position',[ ...
        Plot_figure_x, ...
        Plot_figure_y, ...
        Plot_figure_Breite, ...
        Plot_figure_Hoehe ...
    ], ...
    'Resize',Plot_Resize_String ...
);

% gca = axes( ...
%     'XLim',[Plot_x_min, Plot_x_max], ...
%     'YLim',[1.3*Plot_y_min,1.3*Plot_y_max] ...
% );


hold on;
grid on;
set(gca,'layer',Plot_Layer_String);

box on;


subplot(2,1,1);
h1 = barh((1/0.036)*[eel_opt_CADC_rural,eel_konv_CADC_rural],'red');
    % "1/0.036", damit e_el nicht in J/m, sondern in Wh/(100km) angezeigt wird!
grid on;
set(gca,'layer',Plot_Layer_String);
set(gca, 'YTickLabel', {'optimal','konventionell'});

title('CADC-rural: Vergleich des spezifischen elektrischen Energieverbrauchs beider Betriebsstrategien');
xlabel('spezifischer elektrischer Energieverbrauch e_{el}/(Wh/(100km))');


subplot(2,1,2);
h2 = barh(100*[RRA_CADC_rural,RVA_CADC_rural],'green');
    % "100*", damit RRA und RVA in Prozent angezeigt werden!
grid on;
set(gca,'layer',Plot_Layer_String);

set(gca, 'YTickLabel',{'Reichweite','Verbrauch'});
% set(gca,'XLim',[-30,30]);

title('CADC-rural: relative �nderungen der optimalen gegen�ber der konventionellen Betriebsstrategie');
xlabel('relative �nderung in %');


clearvars gca gcf X Y C h ... % werden nicht mehr ben�tigt
    hcb colorTitleHandle titleString ....
    h1 h2 h3 h4 h5 h6 h7 h8
    
