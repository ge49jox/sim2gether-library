%% Wichtige Hinweise:

% Dieses Skript schreibt folgende Matrix in den Workspace:
    % KF_konv_ideal_etamotgen_Matrix
    
% Der Spaltenindex entspricht der Winkelgeschwindigkeit W.
% Der Zeilenindex entspricht dem Drehmoment M.
% In Matlab gebinnt die Indizierung mit 1 und nicht mit 0!

% bzgl. Schrittweite: siehe "Schrittweiten.m" im Ordner "Parameter".
% bzgl. _Vektor(_) : siehe "Vektoren.m" im Ordner "Vektoren".


%% Berechnung von KF_konv_ideal_etamotgen_Matrix:

smax = length(W_Vektor);        % gr��ter Spaltenindex
zmax = length(M_Vektor_motgen); % gr��ter Zeilenindex
hilfszahl_M = (zmax - 1)/2; % muss eine nat�rliche Zahl sein!

% M_Vektor_motgen k�nnte z. B. so aussehen:
    % [-40,-30,-20,-10,0,+10,+20,+30,+40]
    % => zmax = 9 => hilfszahl_M = 4
    % Der Wert M = 0 entspricht dem Index hilfszahl_M + 1!

KF_konv_ideal_etamotgen_Matrix = NaN*ones(zmax,smax);
% initialisiert alle Eintr�ge mit NaN.
% Dies spielt im Idealfall aber keine gro�e Rolle, da im Idealfall
% jeder Punkt des M-W-Kennfelds erreicht werden kann.
% Ganz anders ist es im Realfall. Dort MUSS mit NaN initialisiert werden!

%% Auff�llen des Motor-Bereichs (M > 0):
for s = 1:smax
    W = (s - 1)*W_Schrittweite; % Bei s = 1 ist W = 0. => Passt!
    % Im Idealbetrieb gibt es keine Grenzkurven!
    for z = 0:(+1):(+hilfszahl_M)
        M = z*M_Schrittweite; % Bei z = 0 ist M = 0. => Passt!
        KF_konv_ideal_etamotgen_Matrix(hilfszahl_M + 1 + z, s) = ...
            konv_ideal_etamot_aus_M_W( ... % mot! 
                M, ...
                W, ...
                Maschinenkonstanten_10, ...
                Betriebsgrenzen_4 ...
            );          
    end
end

%% Auff�llen des Generator-Bereichs (M < 0):
for s = 1:smax 
    W = (s -1)*W_Schrittweite; % Bei s = 1 ist W = 0. => Passt!
    % Im Idealbetrieb gibt es keine Grenzkurven!
    for z = 0:(-1):(-hilfszahl_M) % In dieser Reihenfolge!
        M = z*M_Schrittweite; % Bei z = 0 ist M = 0. => Passt!
        KF_konv_ideal_etamotgen_Matrix(hilfszahl_M + 1 + z, s) = ...
            konv_ideal_etagen_aus_M_W( ... % gen!
                M, ...
                W, ...
                Maschinenkonstanten_10, ...
                Betriebsgrenzen_4 ...
            );          
    end
end




clearvars smax zmax s z W M hilfszahl_M; % werden nicht mehr ben�tigt

