
%% Mit diesem Skript kann man den motorischen Beriech gut berechnen.
%Hier wird das eta Kennfeld f�r den motorischen Bereich auf klassiche Weise
%berechnet in dem die mechanische Leistung durch die elektrische Leistung
%dividiert wird. In der elektrischen Leistung sind bereits alle Verluste
%eingerechnet.


Mechanische_Leistung_Matrix=M_Vektor_motgen'*W_Vektor; 

Eta_Kennfeld=Mechanische_Leistung_Matrix./KF_konv_real_Pelmotgen_Matrix_1;

%Eta_Kennfeld_halb=Mechanische_Leistung_Matrix./KF_konv_real_Pelmotgen_Matrix_2; %Mit halben Eisenverlusten �ber dem Nennbereich