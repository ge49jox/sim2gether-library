%% Wichtige Hinweise:

% Dieses Skript schreibt folgende Matrix in den Workspace:
    % KF_opt_real_etamotgen_Matrix

% ACHTUNG: Dieses Skript ist sehr rechenintensiv!
    % Die Ausf�hrung ben�tigt ca. 10 Sekunden.

% Der Spaltenindex entspricht der Winkelgeschwindigkeit W.
% Der Zeilenindex entspricht dem Drehmoment M.
% In Matlab gebinnt die Indizierung mit 1 und nicht mit 0!

% bzgl. Schrittweite: siehe "Schrittweiten.m" im Ordner "Parameter".
% bzgl. _Vektor(_) : siehe "Vektoren.m" im Ordner "Vektoren".


%% Berechnung von KF_opt_real_etamotgen_Matrix:

smax = length(W_Vektor);        % gr��ter Spaltenindex
zmax = length(M_Vektor_motgen); % gr��ter Zeilenindex
hilfszahl_M = (zmax - 1)/2; % muss eine nat�rliche Zahl sein!

% M_Vektor_motgen k�nnte z. B. so aussehen:
    % [-40,-30,-20,-10,0,+10,+20,+30,+40]
    % => zmax = 9 => hilfszahl_M = 4
    % Der Wert M = 0 entspricht dem Index hilfszahl_M + 1!

KF_opt_real_etamotgen_Matrix_Nenn = NaN*ones(zmax,smax);
% initialisiert alle Eintr�ge mit NaN;


%% Auff�llen des Motor-Bereichs (M > 0):

for s = 1:smax 
    W = (s - 1)*W_Schrittweite; % Bei s = 1 ist W = 0. => Passt!
    M_motmax = GK_M_W_Matrix_4(1,s); % 1. Zeile steht f�r
        % Grenzkurve von M_mot!
    for z = 0 : floor(M_motmax/M_Schrittweite)
        % Es muss zur 0 hin gerundet werden, damit die Grenzkurve
        % sicher nicht �berschritten wird!
        M = z*M_Schrittweite; % Bei z = 0 ist M = 0. => Passt!
        KF_opt_real_etamotgen_Matrix_Nenn(hilfszahl_M + 1 + z, s) = ...
            opt_real_etamot_aus_M_W( ... % mot!
            M, ...
            W, ...
            Maschinenkonstanten_10, ...
            Betriebsgrenzen_4, ...
            Nennwerte_8 ...
        );          
    end
end


%% Auff�llen des Generator-Bereichs (M < 0):

for s = 1:smax 
    W = (s -1)*W_Schrittweite; % Bei s = 1 ist W = 0. => Passt!
    M_genmin = GK_M_W_Matrix_4(4,s); % 4. Zeile steht f�r
        % Grenzkurve von M_gen!
    for z = 0:(-1):ceil(M_genmin/M_Schrittweite) % In dieser Reihenfolge!
        % Es muss zur 0 hin gerundet werden, damit die Grenzkurve
        % sicher nicht unterschritten wird!
        M = z*M_Schrittweite; % Bei z = 0 ist M = 0. => Passt!
        KF_opt_real_etamotgen_Matrix_Nenn(hilfszahl_M + 1 + z, s) = ...
            opt_real_etagen_aus_M_W( ... % gen!
            M, ...
            W, ...
            Maschinenkonstanten_10, ...
            Betriebsgrenzen_4, ...
            Nennwerte_8 ...
        );           
    end
end


clearvars smax zmax s z W M hilfszahl_M; % werden nicht mehr ben�tigt

