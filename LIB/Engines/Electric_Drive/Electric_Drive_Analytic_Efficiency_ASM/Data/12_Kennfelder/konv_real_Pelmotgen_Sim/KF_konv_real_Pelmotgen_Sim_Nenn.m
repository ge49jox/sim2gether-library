% Dieses Skript schreibt folgende Matrix in den Workspace:
    % KF_konv_real_Pelmotgen_Sim_Matrix
   
% Diese Matrix wird sp�ter f�r die Simulation ben�tigt werden.
% Alle NaN-Werte im Generator-Bereich (M < 0) sind bereinigt worden.
% Details zur Funktion "no_NaN_in_gen_aus_Matrix(...)" siehe
% gleichnamiges File im Ordner "Kennfelder".
    
KF_konv_real_Pelmotgen_Sim_Matrix_Nenn = ...
    no_NaN_in_gen_aus_Matrix(KF_konv_real_Pelmotgen_Matrix_Nenn);



