function E = Engine_calc(EngineType, PeakPower_kW)
            % Engine(EngineType, PeakPower_kW, BrakeOption) constructor of
            % Engine class
            %   EngineType:     'Diesel'|'Gasoline'|'TurboGasoline'
            %   BrakeOption:    true|false
            
            E.PeakPower_kW = PeakPower_kW;
            
            E.EngineType = EngineType;
            
            
            switch EngineType
                case 'Diesel'
                    E.Fuel = 'Diesel';
                case 'Gasoline'
                    E.Fuel = 'Gasoline';
                case 'TurboGasoline'
                    E.Fuel = 'Gasoline';
            end
                       
          
            E = Update(E);
            E = plot_EngineMap_Efficiency(E);
            
        end
        
        function E = Update(E)
            
            msce = EngineScaling(E.EngineType);
            msce.Scale(E.PeakPower_kW);
            [ww, TT, mm_f_dot_kgps, w, T_max_curve, T_d, EffGrid, ConGrid] = msce.Export;
            
            E.Name = ['Engine_', E.EngineType] ;
            E.ww = ww;
            E.ww_trans= ww(:,1);
            E.TT = TT;
            E.TT_trans= transpose(TT(1,:));
            E.mm_f_dot = mm_f_dot_kgps; % mm_f_dot is in kg/s
            E.w = w;
            E.T_max = T_max_curve;
            E.T_drag = T_d;
            E.EffGrid = EffGrid;
            E.ConGrid = ConGrid;
            
            
            % Weight
            T_peak_Nm = max(T_max_curve(:));
            P_peak_kW = E.PeakPower_kW;
            % subcomponents
            m_eng = 2.752E-01 * T_peak_Nm + 5.065E01;
            l_avg_m = 4; % average length of a car for calculation of exhaust system weight
            switch E.EngineType
                case {'Gasoline', 'TurboGasoline'}
                    m_exh = 1.366E-1 * T_peak_Nm * l_avg_m + 2.958E00;
                    m_cool = 4.850E-02 * P_peak_kW + 2.904E00;
                case 'Diesel'
                    m_exh = 2.050E-02 * T_peak_Nm * l_avg_m + 1.771E01;
                    m_cool = 6.500E-02 * P_peak_kW + 4.221E00;
            end
            m_air = 2.720E-02 * T_peak_Nm + 2.527E-01;
            m_oil = 6.900E-03 * T_peak_Nm + 2.316E00;
            m_coollqu = 1.160E-02 * T_peak_Nm + 3.259E00;
            m_tot = m_eng + m_exh + m_cool + m_air + m_oil + m_coollqu;
            E.Weight = m_tot;
            
            
            % Inertia
            V_eng = E.Weight / 2700;
            J_eng = 0.5 * 0.25 * m_eng * (0.25 * V_eng/(4*pi))^(2/3);
            E.Inertia = J_eng; %kg*m^2r
            
            %% load properties
            LoadProperties;
            %E.LHV_Jpkg = Props.Fuels(strcmp({Props.Fuels.Type},E.Fuel)).LHV_Jpkg; % LHV in J/kg
            E.LHV_Jpkg = Props.FuelTable{E.Fuel,'LHV_Jpkg'};
            E.Density_kgpm3 = Props.FuelTable{E.Fuel,'Density_kgpm3'};
        end
        
        function [inf, MechBrakeTorque, out_mdot, out_dmdot, LHV] = Sim(E, in_w, in_dw, in_T)
            % [inf, MechBrakeTorque, out_mdot, out_dmdot, LHV] = Sim(E, in_w, in_dw, in_T)
            
            
            % additional torque from engine rotational inertia
            % in_T_I = in_dw .* E.Inertia + in_T;
            in_T_I = in_T;% - abs(interp1(E.w, E.T_drag, in_w,'linear', 0));
            
            % infeasible?
            % engine drag torque
            T_drag = interp1(E.w, E.T_drag, in_w); % engine min drag torque
            T_prop = interp1(E.w, E.T_max, in_w) - 4; % engine max propulsion torque
            % VariableBand = 5;
            
            infT_upper = (in_T_I > T_prop);
            infT_lower = (in_T_I < T_drag);
            
            infT = ( infT_upper | (~E.BrakeOption .* infT_lower) );
            
            % speed
            infw = (in_w<(min(E.w))) | (in_w>(max(E.w)));
            
            % overall
            inf = infT | infw;
            
            % consumption
            out_mdot = (in_T_I>=T_drag) .* interpn(E.ww, E.TT , E.mm_f_dot, in_w, in_T_I)+...
                (in_T_I<=T_drag) .* 0;
            % out_mdot = 0 .* in_T_I;
            out_mdot(isnan(out_mdot)) = 0; % prevents that output value is NaN, see Battery!
            
            %             PowerTemp = in_w .* in_T_I;
            %             out_mdot = (PowerTemp>0) + (PowerTemp<0) .* 0.1
            
            MechBrakeTorque = (in_T_I < T_drag) .* (in_T_I - T_drag);
            
            
            % Engine shutdown option
            EngineShutDown = isnan(in_w) & isnan(in_dw) & isnan(in_T);
            inf(EngineShutDown==true) = 0;
            MechBrakeTorque(EngineShutDown==true) = 0;
            out_mdot(EngineShutDown==true) = 0;
            
            
            % output
            Mat = ones(size(in_w));
            LHV = E.LHV_Jpkg .* Mat;
            out_dmdot = 0 .* Mat;
            
        end
        
        function plot_EngineMap_gps(E)
            
%             %% Colored respresentation
%             % plot fuel consumption g/s
%             hold on;
%             
%             % plot contour
%             mm_f_dot_gps = (E.mm_f_dot * 1000);
%             [temp, h] = contour(E.ww, E.TT, mm_f_dot_gps, [0:0.2:0.4, 0.5:0.5:20]);
%             set(h,'ShowText', 'on', 'TextStep', get(h, 'LevelStep'));
%             
%             % plot max and drag torque curves
%             E.plot_TMaxDragCurves
%             % plot power
%             E.plot_MaxPowerCurve;
%             
%             % Plot optimal efficiency
%             opt = E.calc_OptimalEfficiencyPerPowerLevel;
%             plot(opt.x_opt_radps, opt.y_opt_Nm, 'c-','LineWidth', 3);
%             
%             % make it look better & add legend
%             % legend('Fuel Consumption in g/s', 'T_{max}', 'T_{drag}', 'P_{max}');
%             xlabel('Rotation Speed [rad/s]');
%             ylabel('Torque in Nm/ Power in kW');
%             zlabel('Time');
%             % legend('Fuel Consumption in gps', 'T_{max}', 'T_{drag}', 'P_{max}', 'Opt. Operation Line');
%             % title('Engine Consumption Map');
%             grid on;
            
            %% Black and white representation
            % plot fuel consumption g/s
            hold on;
            
            % Extract and convert data
            mm_f_dot_gps = (E.mm_f_dot * 1000);
            ww = E.ww;
            TT = E.TT;
            
            % Reduce data points
            RedRate = 5; % "RedRate" is Reduction Rate, e.g. "2" only takes every second row and line in dataset
            [m, n] = size(mm_f_dot_gps);
            mm_f_dot_gps = mm_f_dot_gps(1:RedRate:m, 1:RedRate:n);
            ww = ww(1:RedRate:m, 1:RedRate:n);
            TT = TT(1:RedRate:m, 1:RedRate:n);
            
            % plot contour
            [C, h] = contour(ww, TT, mm_f_dot_gps, [0.5, 1:1:20], 'LineColor', [1 1 1]*0.5, 'LineWidth', 0.5);
            set(h,'ShowText', 'on', 'TextStep', get(h, 'LevelStep'));
            % set(h,'ShowText','on');
            colormap(gray);
            
            % Plot optimal efficiency
            % E.plot_OptimalEfficiencyCurve;
            
            % plot max and drag torque curves
            E.plot_TMaxDragCurves
            % plot power
            % E.plot_MaxPowerCurve;
            
            % make it look better & add legend
            % legend('Fuel Consumption in g/s', 'T_{max}', 'T_{drag}', 'P_{max}');
            % xlabel('Rotation Speed in rad/s');
            % ylabel('Torque in Nm, Power in kW');
            % zlabel('Time');
            % h = legend({'$\dot{m}_f$ in g/s', '$\dot{m}_{f,opt}$', '$T_{max}$', '$T_{drag}$', '$P_{max}$'});
            % set(h,'Interpreter','latex','Location','southeast')
            % title('Engine Consumption Map');
            % grid on;
            box on;
   
        end
        
        function plot_EngineMap_3d_gps(E)
            
            % Extract and convert data
            mm_f_dot_gps = (E.mm_f_dot * 1000);
            ww = E.ww;
            TT = E.TT;
            
            mesh(ww, TT, mm_f_dot_gps);
            
            
        end
        
        function E = plot_EngineMap_Efficiency(E)
            
           [eff_max,pos]=max(E.EffGrid.zgs_eff,[],2);
            M_be_min_unlim=E.EffGrid.ygs_Nm(1,pos(:));
            E.M_be_min=min(M_be_min_unlim,E.T_max);
            E.EffGrid.xgs_radps_vektor=E.EffGrid.xgs_radps(:,1);
            E.EffGrid.ygs_Nm_vektor=E.EffGrid.ygs_Nm(1,:);
        
            %Plot
            figure()
            hold on
            [temp, h] = contour(E.EffGrid.xgs_radps,E.EffGrid.ygs_Nm,E.EffGrid.zgs_eff, 'LineColor', 'k');
            plot(E.w,E.M_be_min);
            plot(E.w,E.T_max);
            set(h,'ShowText','on','TextStep',get(h,'LevelStep'));
            h = legend('Eff.', 'T_{Be min}', 'T_{max}');
           % set(h,'Interpreter','latex','Location','northwest');
            xlabel('Rotation Speed in rad/s');
            ylabel('Torque in Nm');
            
                        
        end
        
        function plot_EngineMap_FuelConsumption_gpkWh(E)
            
            %% Plot Engine Fuel Consumption Map
            % Power Matrix
            PP = E.ww .* E.TT; % W
            hold on;
            % plot max and drag torque curves
            E.plot_TMaxDragCurves
            % plot power
            E.plot_MaxPowerCurve;
            % plot power
            Power_W = E.T_max .* E.w;
            plot(E.w, Power_W/1000,'-g','LineWidth', 0.5);
            % Calculate consumption in gpkWh
            C_gpkWh = kg2g(E.mm_f_dot) ./ J2kWh(PP);
            % Consumption Lines
            [temp, h] = contour(E.ww, E.TT, C_gpkWh, [[600:-10:230],[230:-2:170]], 'LineWidth', 0.5); % rad/s
            set(h,'ShowText','on','TextStep',get(h,'LevelStep'));
            legend({'$T_{max}$', '$P_{max}$','$\dot{m}_f$ in g/kWh'}, 'Interpreter', 'latex');
            xlabel('Rotation Speed in rad/s');
            ylabel('Torque in Nm, Power in kW');
            % title('Engine Fuel Consumption Map');
            % grid on;
            box on;
            % plot(w,T_d,'-r','LineWidth',3);
            
        end
        
        function plot_TMaxDragCurves(E)
            % plot max torque curve
            plot(E.w, E.T_max, 'k-', 'LineWidth', 0.5);
            hold on;
            % plot engine drag curve
            plot(E.w, E.T_drag, 'k-', 'LineWidth', 0.5);
        end
        
        function plot_MaxPowerCurve(E)
            % plot power
            Power_W = E.T_max .* E.w;
            plot(E.w, Power_W/1000, 'k-.', 'LineWidth', 0.5);
        end
        
        function plot_MaxEffLinePerPower(E)
            
           fb = E.calc_OptimalEfficiencyPerPowerLevel;
           plot(fb.OutputPowerLevel_kW,fb.z_opt_eff, 'k-', 'LineWidth', 0.5);
           % grid on;
           
           xlabel('Mechanical Output Power in kW');
           ylabel('Efficiency of Power Conversion');
           
        end
        
        function plot_OptimalEfficiencyCurve(E)
           
            opt = E.calc_OptimalEfficiencyPerPowerLevel;
            plot(opt.x_opt_radps, opt.y_opt_Nm, 'k--', 'LineWidth', 0.5);
            
        end
        
        function plot_PowerHyperbola(E)
            
            % Extract and convert data
            ww = E.ww;
            TT = E.TT;
            
            % Power Matrix
            PP = ww .* TT / 1000; % kW
            
            [~, ~] = contour(ww, TT, PP, 0:10:max(PP(:)), 'ShowText', 'off', 'LineStyle', '-', 'LineWidth', 0.5, 'LineColor', [1 1 1]*0.5, 'DisplayName', 'Power Hyperbolas');
            
        end
        
        function opt = calc_OptimalEfficiencyPerPowerLevel(E)
            
            % Power Matrix
            PP = E.ww .* E.TT; % W
            PP_in = E.mm_f_dot .* E.LHV_Jpkg; % W
            ee = PP ./ PP_in;
            
            % Read data from class properties
            Grid.xgs_radps = E.ww;
            Grid.ygs_Nm = E.TT;
            Grid.zgs_eff = ee;
            MaxTrqLine.x_radps = E.w;
            MaxTrqLine.y_Nm = E.T_max;
            
            % Calculate max power curve
            Power_W = (MaxTrqLine.x_radps .* MaxTrqLine.y_Nm); %W
            PeakPower_kW = max(Power_W)/1000;
            PeakPower_HP = kW2hp(PeakPower_kW);
            
            % Calculate power hyperbolias
            PPower_kW = (Grid.xgs_radps .* Grid.ygs_Nm) / 1000;
            
            % Calculate contour lines for discrete power levels
            x_radps = squeeze(Grid.xgs_radps(:,1));
            y_Nm = squeeze(Grid.ygs_Nm(1,:))';
            ContourLines = contourcs(x_radps, y_Nm, PPower_kW', 0:0.5:(PeakPower_kW-2)); % "contourcs" is a third party function from matlab central
            
            % Find optimal efficiency
            for i = 1 : length(ContourLines)
                
                eff = interpn(Grid.xgs_radps, Grid.ygs_Nm, Grid.zgs_eff, ContourLines(i).X, ContourLines(i).Y, 'linear', NaN);
                
                [z_opt_eff(i), index_opt(i)] = nanmax(eff);
                
                % AllOptIndex = find(eff == z_opt_eff(i)); % calculates all optimal indices if along the constant power hyperbolia is a series of same best values
                
                x_opt_radps(i) = ContourLines(i).X(index_opt(i));
                y_opt_Nm(i) = ContourLines(i).Y(index_opt(i));
                OutputPowerLevel_kW(i) = ContourLines(i).Level;
                
            end
            
            % % Plot efficiency of power
            % mf;
            % plot(PowerLevel_kW, z_opt_eff);
            % xlabel('Power / kW');
            % ylabel('Optimal Efficiency / - ');
            
            % Sort matrix
            unsorted_M_be_min= [x_opt_radps;y_opt_Nm];
            unsorted_trans_M_be_min = transpose(unsorted_M_be_min);
            sorted_trans_M_be_min = sortrows(unsorted_trans_M_be_min,1);
            sorted_M_be_min = transpose(sorted_trans_M_be_min);
                       
            
            % Write output
            opt.x_opt_radps = x_opt_radps;
            E.x_opt_radps= sorted_M_be_min(1,:);
            opt.y_opt_Nm = y_opt_Nm;
            E.y_opt_Nm = sorted_M_be_min(2,:);
            opt.z_opt_eff = z_opt_eff;
            opt.OutputPowerLevel_kW = OutputPowerLevel_kW;
            opt.InputPowerLevel_kW = OutputPowerLevel_kW ./ z_opt_eff; % Input and output is here to understand as given by the real world causality and not by the (backward) simulation
            % sort out low values
            opt.x_opt_radps = opt.x_opt_radps(opt.y_opt_Nm>20);
            opt.y_opt_Nm = opt.y_opt_Nm(opt.y_opt_Nm>20);
            opt.z_opt_eff = opt.z_opt_eff(opt.y_opt_Nm>20);
            opt.OutputPowerLevel_kW = opt.OutputPowerLevel_kW(opt.y_opt_Nm>20);
            opt.InputPowerLevel_kW = opt.InputPowerLevel_kW(opt.y_opt_Nm>20);
            
            % figure;
            % plot(opt.InputPowerLevel_kW, opt.OutputPowerLevel_kW);
            % plot3(opt.InputPowerLevel_kW, opt.OutputPowerLevel_kW, 1000 * ones(length(opt.OutputPowerLevel_kW)), 'LineWidth', 5);
            
            
            % figure;
            % plot(OutputPowerLevel_kW, z_opt_eff);
            
        end
        
    
    


