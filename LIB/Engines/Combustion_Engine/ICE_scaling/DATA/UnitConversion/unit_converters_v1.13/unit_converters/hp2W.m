function [W] = hp2W(hp)
% Convert power from mechanical horsepower to watts.
% Chad A. Greene 2012
% W = hp*745.699871582;

% Change Tschochner 2014-08-06, above "mechanical horsepower", new below
% "matric horsepower" http://en.wikipedia.org/wiki/Horsepower
W = hp * 735.49875;
