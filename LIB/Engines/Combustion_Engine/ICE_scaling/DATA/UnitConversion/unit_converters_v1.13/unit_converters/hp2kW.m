function [kW] = hp2kW(hp)
% Convert power from mechanical horsepower to kilowatts.
% Chad A. Greene 2012
% kW = hp*0.745699872;

% Change Tschochner 2014-08-06, above "mechanical horsepower", new below
% "matric horsepower" http://en.wikipedia.org/wiki/Horsepower
kW = hp * 735.49875/1000;