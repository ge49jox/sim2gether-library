function [kWh] = J2kWh(J)
% Convert energy or work from joules to kilowatt-hours.
% Chad A. Greene 2012

% kWh = J*2.7777777778e-7;

% modified by Maximilian Karl Tschochner 2014-07-25
kWh = J/(1000*3600);