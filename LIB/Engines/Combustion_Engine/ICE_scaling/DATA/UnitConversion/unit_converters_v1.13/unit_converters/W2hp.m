function [hp] = W2hp(W)
% Convert power from watts to mechanical horsepower. 
% Chad A. Greene 2012
% hp = W*0.001341022;

% Change Tschochner 2014-08-06, above "mechanical horsepower", new below
% "matric horsepower" http://en.wikipedia.org/wiki/Horsepower
hp = W / 735.49875;