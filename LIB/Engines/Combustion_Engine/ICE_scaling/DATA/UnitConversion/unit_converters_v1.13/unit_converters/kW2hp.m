function [hp] = kW2hp(kW)
% Convert power from kilowatts to mechanical horsepower.
% Chad A. Greene 2012
% hp = kW*1.34102209;

% Change Tschochner 2014-08-06, above "mechanical horsepower", new below
% "matric horsepower" http://en.wikipedia.org/wiki/Horsepower
hp = kW / (735.49875/1000);
