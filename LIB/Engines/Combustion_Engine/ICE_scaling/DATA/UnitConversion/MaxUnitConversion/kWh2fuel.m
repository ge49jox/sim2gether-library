function [ OutputTable ] = kWh2fuel( kWh )
%UNTITLED15 Summary of this function goes here
%   Detailed explanation goes here

% import properties database
global Props
if isempty(Props)
    LoadProperties
end

n = length(Props.Fuels);

for i = 1 : 1 : n
    LHV_Jpkg(i) = Props.Fuels(i).LHV;
    LHV_kWhpkg(i) = J2kWh(LHV_Jpkg(i));
    Fuel_kg(i) = kWh / LHV_kWhpkg(i);
    if isempty(Props.Fuels(i).Density)
        Fuel_m3(i) = NaN;
    else
        Fuel_m3(i) = Fuel_kg(i) / Props.Fuels(i).Density;
    end
    Fuel_l(i) = m32l(Fuel_m3(i));
    FuelType{i} = Props.Fuels(i).Type;
end

Fuel_l = Fuel_l';

OutputTable = table(Fuel_l,'RowNames',FuelType);

end

