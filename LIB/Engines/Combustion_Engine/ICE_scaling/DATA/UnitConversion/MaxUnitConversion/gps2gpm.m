function [gpm] = gps2gpm(gps)
% Convert energy from grams per second to grams per minute.
% Maximilian Tschochner 2015

gpm = gps * 60;

end

