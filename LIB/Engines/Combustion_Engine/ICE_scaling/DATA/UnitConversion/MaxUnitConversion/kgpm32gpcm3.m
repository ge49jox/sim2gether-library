function [gpcm3] = kgpm32gpcm3(kgpm3)
% Convert energy from tons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

gpcm3 = kgpm3 * 1000 / 100^3;

end

