function [lb] = kg2lb(kg)
% Convert mass from from kg pound (lb).
% Maximilian Tschochner 2016

lb = kg / 0.45359237;

end

