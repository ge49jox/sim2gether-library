function [Mtoe] = PJ2Mtoe(PJ)
% Convert energy from petajoules to megatons of oil equivalent.
% Maximilian Tschochner 2015

Mtoe = PJ * 23885 / 1e6;

end


