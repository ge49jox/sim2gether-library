function [ kgpWs] = gpkWh2kgpWs( gpkWh )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

% conversion from grams per kilowatt-hours (g/kWh) to kilograms per
% watt-seconds (kg/Ws)

kgpWs = gpkWh/(1000*1000*3600);

end

