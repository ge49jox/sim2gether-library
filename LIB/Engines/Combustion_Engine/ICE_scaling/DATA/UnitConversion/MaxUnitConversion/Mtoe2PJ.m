function [PJ] = Mtoe2PJ(Mtoe)
% Convert energy from megatons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

PJ = Mtoe * 41.87;

end

