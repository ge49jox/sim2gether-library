function [gal] = Mbbl2gal(Mbbl)
% Convert volume one thousand oil barrels to US liquid gallons.
% Maximilian Tschochner 2015

gal = Mbbl * 31.5 * 10e3;

end

