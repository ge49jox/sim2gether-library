% electricity2CO2 class
%
% Description
%
% Methods::
%   -
%
% Static methods::
%   kWhp100km        description
%
% Properties (read/write)::
%   ExampleProperty           	description
%
% Examples::
%
% Description of the example
%   Example code
%
%
% Notes::
%   - 
%
% Reference::
%   -
%
% See also Example (write here classes, functions)


% Contact: MAXIMILIAN KARL TSCHOCHNER
% TUM CREATE LTD
% Research Project 9
% maximilian.tschochner@tum-create.edu.sg
% Author(s): Maximilian Karl Tschochner


% History:
% - 2014-05-14 created, Tschochner
% 
% ToDo:
% - 

classdef electricity2CO2
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    methods (Static)
        function OutputMatrix = kWhp100km(kWh)
            
            % import properties database
            global Props
            if isempty(Props)
                LoadProperties
            end
            
            % Display unit
            OutputMatrix{1,1} = 'COUNTRY/ REGION';
            OutputMatrix{1,2} = 'Gram CO2 per km';
            
            n = length(Props.Countries); %number of country/ region data sets
            
            for i = 1 : 1 : n
                % fprintf('%4.2f \t %s \n', (v * Props.Countries(i).gCO2pkWh/100), Props.Countries(i).Name)
                OutputMatrix{i+1,2} = num2str(kWh * Props.Countries(i).gCO2pkWh/100);
                OutputMatrix{i+1,1} = Props.Countries(i).Name;
            end
            
            OutputMatrix
                       
        end
    end
    
end