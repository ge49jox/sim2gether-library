function [gal] = MMbbl2gal(MMbbl)
% Convert volume one million oil barrels to US liquid gallons.
% Maximilian Tschochner 2015

gal = MMbbl * 31.5 * 10e6;

end

