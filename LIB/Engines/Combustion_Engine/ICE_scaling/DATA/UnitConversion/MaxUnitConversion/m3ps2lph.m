function [lph] = m3ps2lph(m3ps)
% Convert energy from tons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

lph = m3ps * (1000 * 3600);

end

