function struct2mfile( varstr, filename)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


fid = fopen([filename,'.m'],'w');

StructLayer = 1;

% while StructLayer <= 20
% if isstruct(var)
%     StructFieldnames(:,StructLayer) = fieldnames(var);
%     StructContentCell(:,StructLayer) = struct2cell(var);
%     
%     LayerEntries = length(StructFieldnames(:,StructLayer));
%     for Entry = 1 : 1 : LayerEntries
%         if isstruct(eval(['var.', StructFieldnames{Entry,StructLayer}]))
%             wait;
%         end
% end
% end


tempvar = evalin('base',varstr);

FieldNames = fieldnames(tempvar);

if isstruct(tempvar)
    % Initialize root field
    Level(1).FieldNames = cellstr(varstr);
    Level(1).NumberOfFields = 1;
    Level(1).CurrentField = 1; %current field in the root structure will always be 1
    % Number of struct levels is now at least two: root level and second
    % level
    NumberOfStructLevels = 2;
    % This means also the second level can be written in the structure
    % trace structure
    Level(2).FieldNames = fieldnames(tempvar);
    Level(2).NumberOfFields = length(Level(2).FieldNames);
    Level(2).CurrentField = 1;
else
    disp('This is not a struct!');
    return;
end

ThisLevel = 2;

while 1 % level loop start
    
    while 1 % field loop start
        
        RootCurrentField = Level(1).CurrentField;
        RootFieldName = Level(1).FieldNames{RootCurrentField,:};
        PathToField = [RootFieldName];
        
        for j = 2 : 1 : ThisLevel
            ThisLevelCurrentField = Level(j).CurrentField;
            CurrentFieldName = Level(j).FieldNames{ThisLevelCurrentField,:};
            PathToField = [PathToField, '.', CurrentFieldName];
        end
        
        tempvar = evalin('base',PathToField);
        
        if ~isstruct(tempvar)
            if isnumeric(tempvar)
                if ismatrix(tempvar)
                    n = length(tempvar);
                    tempstr = num2str(tempvar(1,1));
                    for i = 2 : 1 : n
                        tempstr = [tempstr, ';', num2str(tempvar(i,1))];
                    end
                    tempstr = ['[',tempstr,']'];
                end
            else
                tempstr = ['''',tempvar,''''];
            end
            fprintf(fid,'%s = %s;\n', PathToField, tempstr);
        else
            fprintf(fid,'%% %s\t STRUCT\n', PathToField);
        end
        
        %disp(PathToField)
        
        % jump out of the loop and go in the next level
        if isstruct(tempvar)
            break;
        end
        
        % if current field is not a struc but last field in substruct go to
        % previous level
        if (Level(ThisLevel).CurrentField) == (Level(ThisLevel).NumberOfFields)
            break;
        end
        
        Level(ThisLevel).CurrentField = Level(ThisLevel).CurrentField + 1;
           
    end % field loop end
    
    if isstruct(tempvar) % go to next level if current field is a struct
        ThisLevel = ThisLevel + 1;
        % record new layer in cell structure
        Level(ThisLevel).FieldNames = fieldnames(tempvar);
        Level(ThisLevel).NumberOfFields = length(Level(ThisLevel).FieldNames);
        Level(ThisLevel).CurrentField = 1;
    else %otherwise go to previous level
        while 1
            ThisLevel = ThisLevel - 1;
            % if previous level field is not last field go to next field in
            % same level
            if (Level(ThisLevel).CurrentField) < (Level(ThisLevel).NumberOfFields)
                Level(ThisLevel).CurrentField = Level(ThisLevel).CurrentField + 1;
                break;
            elseif ThisLevel == 2
                fclose(fid);
                return;
            end
        end
    end
 
end % level loop end



%%


%     function [level, SubNumber, Fieldnames] =  Ana(varstr, level, fid)
%         tempvar = evalin('base',varstr);
%         if isstruct(tempvar)
%             TempStructStr(level) = {varstr};
%             level = level + 1;
%             Fieldnames = fieldnames(tempvar);
%             SubNumber = length(Fieldnames);
%         else
%             fprintf(fid,'%s = %s;', varstr, num2str(tempvar));
%         end
%     end
        

%%



