function [kWh] = Ws2kWh(Ws)
% Convert energy from tons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

kWh = Ws / (1000 * 3600);

end

