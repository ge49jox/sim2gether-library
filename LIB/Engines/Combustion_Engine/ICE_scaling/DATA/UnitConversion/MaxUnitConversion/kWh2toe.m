function [ toe ] = kWh2toe( kWh )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% 1 Mtoe = 11630 GWh
% Source: https://www.iea.org/newsroomandevents/resources/conversiontables/#d.en.32153
% IEA Unit Online Unit Converter: https://www.iea.org/statistics/resources/unitconverter/
% 1e6 toe = 11630e6 kWh; 1 toe = 11630 kWh 

toe = kWh /  11630;

end

