function [l] = MMbbl2l(MMbbl)
% Convert volume one million oil barrels to liters.
% Maximilian Tschochner 2015

l = MMbbl * 119.2 * 10e6;

end

