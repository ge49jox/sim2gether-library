function [PJ] = ktoe2PJ(ktoe)
% Convert energy from kilotons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

PJ = ktoe * 1e3 / 23885;

end

