function [PJ] = toe2PJ(toe)
% Convert energy from tons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

PJ = toe / 23885;

end

