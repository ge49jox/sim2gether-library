function [ktoe] = PJ2ktoe(PJ)
% Convert energy from petajoules to kilotons of oil equivalent.
% Maximilian Tschochner 2015

ktoe = PJ * 23885 / 1e3;

end

