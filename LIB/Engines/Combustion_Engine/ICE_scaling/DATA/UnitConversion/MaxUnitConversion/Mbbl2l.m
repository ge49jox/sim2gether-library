function [l] = Mbbl2l(Mbbl)
% Convert volume one thousand oil barrels to liters.
% Maximilian Tschochner 2015

l = Mbbl * 119.2 * 10e3;

end

