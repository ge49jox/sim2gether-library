function [kg] = lb2kg(lb)
% Convert mass from pound (lb) to kg.
% Maximilian Tschochner 2016

kg = lb * 0.45359237;

end

