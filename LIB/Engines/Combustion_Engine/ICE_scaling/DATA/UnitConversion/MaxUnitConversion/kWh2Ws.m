function [Ws] = kWh2Ws(kWh)
% Convert energy from tons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

Ws = kWh * (1000 * 3600);

end

