function [kgpm3] = gpcm32kgpm3(gpcm3)
% Convert energy from tons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

kgpm3 = gpcm3 / 1000 * 100^3;

end

