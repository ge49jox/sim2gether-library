function [toe] = PJ2toe(PJ)
% Convert energy from petajoules to tons of oil equivalent.
% Maximilian Tschochner 2015

toe = PJ * 23885;

end

