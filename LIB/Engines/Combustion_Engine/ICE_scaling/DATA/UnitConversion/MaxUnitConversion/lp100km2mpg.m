function [ mpg ] = lp100km2mpg( lp100km )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% Vehicle Dynamics Toolbox
% Author: Maximilian Karl Tschochner
% Institution: TUM CREATE
% Date: 2014-03-06
% Changes:

% mpg (US)

% 1 mile = 1.609344 km
% 1 gallon us = 3.7854 liter

mpg = 1 / (lp100km * 1.609344 / 3.7854) * 100;

end

