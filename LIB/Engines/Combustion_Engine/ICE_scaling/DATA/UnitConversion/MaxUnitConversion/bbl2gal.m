function [gal] = bbl2gal(bbl)
% Convert volume oil barrels to US liquid gallons.
% Maximilian Tschochner 2015

gal = bbl * 31.5;

end

