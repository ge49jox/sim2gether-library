function [m3ps] = lph2m3ps(lph)
% Convert energy from tons of oil equivalent to petajoules.
% Maximilian Tschochner 2015

m3ps = lph / (1000 * 3600);

end

