function [l] = bbl2l(bbl)
% Convert volume oil barrels to liters.
% Maximilian Tschochner 2015

l = bbl * 119.2;

end

