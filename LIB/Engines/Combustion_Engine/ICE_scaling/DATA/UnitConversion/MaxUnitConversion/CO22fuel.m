function [ OutputTable ] = CO22fuel( CO2_kg )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% INPUTS
% CO2 in kg

% OUTPUTS
% fuel in l

% import properties database
global Props
if isempty(Props)
    LoadProperties
end

n = length(Props.Fuels);

for i = 1 : 1 : n
    Fuel_kg(i) = CO2_kg / Props.Fuels(i).CO2EmissionFactor;
    if isempty(Props.Fuels(i).Density)
        Fuel_m3(i) = NaN;
    else
        Fuel_m3(i) = Fuel_kg(i) / Props.Fuels(i).Density;
    end
    Fuel_l(i) = m32l(Fuel_m3(i));
    FuelType{i} = Props.Fuels(i).Type;
end

Fuel_l = Fuel_l';

OutputTable = table(Fuel_l,'RowNames',FuelType);

% OutputTable

end

