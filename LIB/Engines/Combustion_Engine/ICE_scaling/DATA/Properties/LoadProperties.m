% LoadProperties

% Extract value from table e.g.:
% Props.Fuels(strcmp({Props.Fuels.Type},'CNG')).Density
% If there are problem with the version before in Matlab Functions then
% this step by step coded version might be helful:
% temp = strcmp(({Props.Fuels.Type}),'CNG');
% temp = Props.Fuels(temp); %J/kg %LHV
% H_l = temp.Density;

% Extract all e.g. Type of fules from table:
% FuelTypes = {Props.Fuels.Type}

% Write to excel table:
% writetable(struct2table(Fuel),'MyTestExcelSheet.xls')

% Find length and entries of struct table
% length(Props.Country) %vertical length of table
% fieldnames(Props.Country) %horizontal field names
% length(fieldnames(Props.Country)) %number of horizontal fieldnames/
% horizontal length of table

% Every field in the table can be a matrix, e.g.
% Props.Components(2).Scaling.x = [2, 3, 4, 5];
% Props.Components(2).Scaling.y = [3, 3.5, 4, 4.5];
% Props.Components(2).Scaling.z = rand(4,4);

%% Fuels

% UNITS
% Props.Fuels(1).Type
% Props.Fuels(1).Density                   [kg/m^3]
% Props.Fuels(1).LHV                       [J/kg]
% Props.Fuels(1).CarbonRatio               [%m]
% Props.Fuels(1).CO2EmissionFactor         [kg/kg]
% Props.Fuels(1).RON                       [-]

% Gasoline [1] p.12
Props.Fuels(1).Type = 'Gasoline';
Props.Fuels(1).Density_kgpm3 = 745;
Props.Fuels(1).LHV_Jpkg = 43.2e6;
Props.Fuels(1).CarbonRatio_pcm = 86.4;
Props.Fuels(1).CO2EmissionFactor_kgpkg = 3.17;
Props.Fuels(1).RON = 95;

% Gasoline E10 [1] p.12
Props.Fuels(2).Type = 'GasolineE10';
Props.Fuels(2).Density_kgpm3 = 750;
Props.Fuels(2).LHV_Jpkg = 41.5e6;
Props.Fuels(2).CarbonRatio_pcm = 82.8;
Props.Fuels(2).CO2EmissionFactor_kgpkg = 3.04;
Props.Fuels(2).RON = 97;

% CNG [1] p.12
Props.Fuels(3).Type = 'CNG';
Props.Fuels(3).Density_kgpm3 = 0.79;
Props.Fuels(3).LHV_Jpkg = 45.1e6;
Props.Fuels(3).CarbonRatio_pcm = 69.2;
Props.Fuels(3).CO2EmissionFactor_kgpkg = 2.54;
Props.Fuels(3).RON = NaN;

% Diesel [1] p.12
Props.Fuels(4).Type = 'Diesel';
Props.Fuels(4).Density_kgpm3 = 832;
Props.Fuels(4).LHV_Jpkg = 43.1e6;
Props.Fuels(4).CarbonRatio_pcm = 86.1;
Props.Fuels(4).CO2EmissionFactor_kgpkg = 3.16;
Props.Fuels(4).RON = 51;

% Hydrogen (CGH2 & cCGH2) [1] p.12
Props.Fuels(5).Type = 'Hydrogen';
Props.Fuels(5).Density_kgpm3 = 0.084;
Props.Fuels(5).LHV_Jpkg = 120.1e6;
Props.Fuels(5).CarbonRatio_pcm = 0;
Props.Fuels(5).CO2EmissionFactor_kgpkg = 0;
Props.Fuels(5).RON = NaN;

% Coal ( = carbon) [2] p.6
Props.Fuels(6).Type = 'Coal';
Props.Fuels(6).Density_kgpm3 = NaN;
Props.Fuels(6).LHV_Jpkg = 34e6;
Props.Fuels(6).CarbonRatio_pcm = 1; % almost all is carbon
Props.Fuels(6).CO2EmissionFactor_kgpkg = 3.7;
Props.Fuels(6).RON = NaN;

% FAME ( = Fatty Acid Methyl Ester, "Biodiesel") [1] p.12
Props.Fuels(7).Type = 'FAME';
Props.Fuels(7).Density_kgpm3 = 890;
Props.Fuels(7).LHV_Jpkg = 34e6;
Props.Fuels(7).CarbonRatio_pcm = 77.3;
Props.Fuels(7).CO2EmissionFactor_kgpkg = 2.83;
Props.Fuels(7).RON = 56; % [-]

% DME ( = DiMethyl Ether) [1] p.12
Props.Fuels(8).Type = 'DME';
Props.Fuels(8).Density_kgpm3 = 670;
Props.Fuels(8).LHV_Jpkg = 28.4e6;
Props.Fuels(8).CarbonRatio_pcm = 52.2;
Props.Fuels(8).CO2EmissionFactor_kgpkg = 1.91;
Props.Fuels(8).RON = 55;

% Convert to table
TempFuelTable = struct2table(Props.Fuels);
% Convert "Type" variable to table row names
TempFuelTable.Properties.RowNames = TempFuelTable{:,'Type'}; % add Type entries as row names
TempFuelTable.Type = []; % delete Type variable
% Add to Props structure
Props.FuelTable = TempFuelTable;
clear TempFuelTable;
% Access values in table:
% % Load properties
% Props = load('Props');
% Props = Props.Props;
% LHV_Jpkg = Props.FuelTable{'Diesel', 'LHV_Jpkg'};
% Density_kgpm3 = Props.FuelTable{'Diesel', 'Density_kgpm3'};

%% Battery Cells & Supercap

% various sources, figures summerized and adapted:
% [2] p. 107, 109 
% [3] p. 26, 27, 28
% [4]
% [5] p. 168

% UNITS
% Props.BatteryCells(1).MinSpecEDensity       [Wh/kg]
% Props.BatteryCells(1).AvgSpecEDensity
% Props.BatteryCells(1).MaxSpecEDensity

% Props.BatteryCells(1).MinSpecPDensity       [W/kg]
% Props.BatteryCells(1).AvgSpecPDensity
% Props.BatteryCells(1).MaxSpecPDensity

% Props.BatteryCells(1).MinVolEDensity        [Wh/l]
% Props.BatteryCells(1).AvgVolEDensity
% Props.BatteryCells(1).MaxVolEDensity

% Props.BatteryCells(1).NomVoltage            [V]
% Props.BatteryCells(1).MaxLifeCycles         [-]
% Props.BatteryCells(1).MaxLifeAge            [a] (years)
% Props.BatteryCells(1).Eta_E                 [-]
% Props.BatteryCells(1).Cost                  [EUR/kWh]

% lithium-ion high energy
Props.BatteryCells(1).Type = 'Li-ion_HE';

Props.BatteryCells(1).MinSpecEDensity_Whpkg = 130;
Props.BatteryCells(1).AvgSpecEDensity_Whpkg = 150;
Props.BatteryCells(1).MaxSpecEDensity_Whpkg = 190;

Props.BatteryCells(1).MinVolEDensity_Whpl = 350;
Props.BatteryCells(1).AvgVolEDensity_Whpl = 350;
Props.BatteryCells(1).MaxVolEDensity_Whpl = 350;

Props.BatteryCells(1).MinSpecPDensity_Wpkg = 500;
Props.BatteryCells(1).AvgSpecPDensity_Wpkg = 600;
Props.BatteryCells(1).MaxSpecPDensity_Wpkg = 750;

Props.BatteryCells(1).NomVoltage_V = 3.6; %[2]  p.107
Props.BatteryCells(1).MaxLifeCycles = 1200; %[2]
Props.BatteryCells(1).MaxLifeAge = NaN;
Props.BatteryCells(1).Eta_E = 0.95; %[3]
Props.BatteryCells(1).AvgCost = NaN;

% lithium-ion high power
Props.BatteryCells(2).Type = 'Li-ion_HP';

Props.BatteryCells(2).MinSpecEDensity_Whpkg = 100;
Props.BatteryCells(2).AvgSpecEDensity_Whpkg = 100;
Props.BatteryCells(2).MaxSpecEDensity_Whpkg = 100;

Props.BatteryCells(2).MinVolEDensity_Whpl = 250;
Props.BatteryCells(2).AvgVolEDensity_Whpl = 250;
Props.BatteryCells(2).MaxVolEDensity_Whpl = 250;

Props.BatteryCells(2).MinSpecPDensity_Wpkg = 1300;
Props.BatteryCells(2).AvgSpecPDensity_Wpkg = 1800;
Props.BatteryCells(2).MaxSpecPDensity_Wpkg = 2000;

Props.BatteryCells(2).NomVoltage_V = 3.6; %[2]  p.107
Props.BatteryCells(2).MaxLifeCycles = 1200; %[2]
Props.BatteryCells(2).MaxLifeAge = NaN;
Props.BatteryCells(2).Eta_E = 0.95; %[3]
Props.BatteryCells(2).AvgCost = NaN;

% nickel-metal hydrid high energy
Props.BatteryCells(3).Type = 'Ni-MH_HE';

Props.BatteryCells(3).MinSpecEDensity_Whpkg = 65;
Props.BatteryCells(3).AvgSpecEDensity_Whpkg = 65;
Props.BatteryCells(3).MaxSpecEDensity_Whpkg = 65;

Props.BatteryCells(3).MinVolEDensity_Whpl = 150;
Props.BatteryCells(3).AvgVolEDensity_Whpl = 150;
Props.BatteryCells(3).MaxVolEDensity_Whpl = 150;

Props.BatteryCells(3).MinSpecPDensity_Wpkg = 200;
Props.BatteryCells(3).AvgSpecPDensity_Wpkg = 200;
Props.BatteryCells(3).MaxSpecPDensity_Wpkg = 200;

Props.BatteryCells(3).NomVoltage_V = 1.2; %[2]  p.107
Props.BatteryCells(3).MaxLifeCycles = 1500;  %[2]
Props.BatteryCells(3).MaxLifeAge = NaN;
Props.BatteryCells(3).Eta_E = 0.65; %[3]
Props.BatteryCells(3).AvgCost = NaN;

% nickel-metal hydrid high power
Props.BatteryCells(4).Type = 'Ni-MH_HP';
Props.BatteryCells(4).MinSpecEDensity_Whpkg = 55;
Props.BatteryCells(4).AvgSpecEDensity_Whpkg = 55;
Props.BatteryCells(4).MaxSpecEDensity_Whpkg = 55;

Props.BatteryCells(4).MinVolEDensity_Whpl = 110;
Props.BatteryCells(4).AvgVolEDensity_Whpl = 110;
Props.BatteryCells(4).MaxVolEDensity_Whpl = 110;

Props.BatteryCells(4).MinSpecPDensity_Wpkg = 1200;
Props.BatteryCells(4).AvgSpecPDensity_Wpkg = 1200;
Props.BatteryCells(4).MaxSpecPDensity_Wpkg = 1200;

Props.BatteryCells(4).NomVoltage_V = 1.2; %[2]  p.107
Props.BatteryCells(4).MaxLifeCycles = 1500;  %[2]
Props.BatteryCells(4).MaxLifeAge = NaN;
Props.BatteryCells(4).Eta_E = 0.65; %[3]
Props.BatteryCells(4).AvgCost = NaN;

% lead-acid
Props.BatteryCells(5).Type = 'Pb-acid';

Props.BatteryCells(5).MinSpecEDensity_Whpkg = 40;
Props.BatteryCells(5).AvgSpecEDensity_Whpkg = 40;
Props.BatteryCells(5).MaxSpecEDensity_Whpkg = 40;

Props.BatteryCells(5).MinVolEDensity_Whpl = 100;
Props.BatteryCells(5).AvgVolEDensity_Whpl = 100;
Props.BatteryCells(5).MaxVolEDensity_Whpl = 100;

Props.BatteryCells(5).MinSpecPDensity_Wpkg = 250;
Props.BatteryCells(5).AvgSpecPDensity_Wpkg = 250;
Props.BatteryCells(5).MaxSpecPDensity_Wpkg = 250;

Props.BatteryCells(5).NomVoltage_V = 2; %[2]  p.107
Props.BatteryCells(5).MaxLifeCycles = 600; %[2]
Props.BatteryCells(5).MaxLifeAge = NaN;
Props.BatteryCells(5).Eta_E = 0.8; %[3]
Props.BatteryCells(5).AvgCost = NaN;

% lithium-air
Props.BatteryCells(6).Type = 'Li-air';
Props.BatteryCells(6).MinSpecEDensity_Whpkg = NaN;
Props.BatteryCells(6).AvgSpecEDensity_Whpkg = 1000; %[2]  p.109
Props.BatteryCells(6).MaxSpecEDensity_Whpkg = NaN;

Props.BatteryCells(6).MinVolEDensity_Whpl = NaN;
Props.BatteryCells(6).AvgVolEDensity_Whpl = NaN; %[2]  p.109 % not available
Props.BatteryCells(6).MaxVolEDensity_Whpl = NaN;

Props.BatteryCells(6).MinSpecPDensity_Wpkg = NaN;
Props.BatteryCells(6).AvgSpecPDensity_Wpkg = NaN; %[2]  p.109 % not available
Props.BatteryCells(6).MaxSpecPDensity_Wpkg = NaN;

Props.BatteryCells(6).NomVoltage_V = 3.4; %[2]  p.107
Props.BatteryCells(6).MaxLifeCycles = NaN; %[2]  p.109 % not available
Props.BatteryCells(6).MaxLifeAge = NaN;
Props.BatteryCells(6).Eta_E = NaN;
Props.BatteryCells(6).AvgCost = NaN;

% lithium-metal-polymer
Props.BatteryCells(7).Type = 'Li-MP';
Props.BatteryCells(7).MinSpecEDensity_Whpkg = NaN;
Props.BatteryCells(7).AvgSpecEDensity_Whpkg = 120; %[2]  p.109
Props.BatteryCells(7).MaxSpecEDensity_Whpkg = NaN;

Props.BatteryCells(7).MinVolEDensity_Whpl = NaN;
Props.BatteryCells(7).AvgVolEDensity_Whpl = 140; %[2]  p.109
Props.BatteryCells(7).MaxVolEDensity_Whpl = NaN;

Props.BatteryCells(7).MinSpecPDensity_Wpkg = NaN;
Props.BatteryCells(7).AvgSpecPDensity_Wpkg = 320; %[2]  p.109
Props.BatteryCells(7).MaxSpecPDensity_Wpkg = NaN;

Props.BatteryCells(7).NomVoltage_V = 3.7; %[2]  p.107
Props.BatteryCells(7).MaxLifeCycles = NaN;
Props.BatteryCells(7).MaxLifeAge = NaN;
Props.BatteryCells(7).Eta_E = NaN;
Props.BatteryCells(7).AvgCost = NaN;

% zebra
Props.BatteryCells(8).Type = 'zebra';
Props.BatteryCells(8).MinSpecEDensity_Whpkg = NaN;
Props.BatteryCells(8).AvgSpecEDensity_Whpkg = 110; %[2]  p.109
Props.BatteryCells(8).MaxSpecEDensity_Whpkg = NaN;

Props.BatteryCells(8).MinVolEDensity_Whpl = NaN;
Props.BatteryCells(8).AvgVolEDensity_Whpl = 160; %[2]  p.109
Props.BatteryCells(8).MaxVolEDensity_Whpl = NaN;

Props.BatteryCells(8).MinSpecPDensity_Wpkg = NaN;
Props.BatteryCells(8).AvgSpecPDensity_Wpkg = 170; %[2]  p.109
Props.BatteryCells(8).MaxSpecPDensity_Wpkg = NaN;

Props.BatteryCells(8).NomVoltage_V = NaN;
Props.BatteryCells(8).MaxLifeCycles = 1000; %[2] p.109
Props.BatteryCells(8).MaxLifeAge = NaN;
Props.BatteryCells(8).Eta_E = NaN;
Props.BatteryCells(8).AvgCost = NaN;


% supercap
Props.BatteryCells(9).Type = 'supercap';
Props.BatteryCells(9).MinSpecEDensity_Whpkg = NaN;
Props.BatteryCells(9).AvgSpecEDensity_Whpkg = 5; %[5] p.169
Props.BatteryCells(9).MaxSpecEDensity_Whpkg = NaN;

Props.BatteryCells(9).MinVolEDensity_Whpl = NaN;
Props.BatteryCells(9).AvgVolEDensity_Whpl = NaN;
Props.BatteryCells(9).MaxVolEDensity_Whpl = NaN;

Props.BatteryCells(9).MinSpecPDensity_Wpkg = 1000; %[5] p.169
Props.BatteryCells(9).AvgSpecPDensity_Wpkg = 5500; % avg of min and max
Props.BatteryCells(9).MaxSpecPDensity_Wpkg = 10000; %[5] p.169

Props.BatteryCells(9).NomVoltage_V = NaN;
Props.BatteryCells(9).MaxLifeCycles = 500000; %[5] p.169
Props.BatteryCells(9).MaxLifeAge = 10; %[5] p.169
Props.BatteryCells(9).Eta_E = 0.95;  %[5] p.169
Props.BatteryCells(9).AvgCost = NaN;

% Lithium Ion Capacitor
Props.BatteryCells(10).Type = 'LIC';
Props.BatteryCells(10).MinSpecEDensity_Whpkg = NaN;
Props.BatteryCells(10).AvgSpecEDensity_Whpkg = 10; %[7]
Props.BatteryCells(10).MaxSpecEDensity_Whpkg = NaN;

Props.BatteryCells(10).MinVolEDensity_Whpl = NaN;
Props.BatteryCells(10).AvgVolEDensity_Whpl = 19; %[7]
Props.BatteryCells(10).MaxVolEDensity_Whpl = NaN;

Props.BatteryCells(10).MinSpecPDensity_Wpkg = NaN; 
Props.BatteryCells(10).AvgSpecPDensity_Wpkg = 14000; %[7]
Props.BatteryCells(10).MaxSpecPDensity_Wpkg = NaN;

Props.BatteryCells(10).NomVoltage_V = NaN;
Props.BatteryCells(10).MaxLifeCycles = NaN; 
Props.BatteryCells(10).MaxLifeAge = NaN; 
Props.BatteryCells(10).Eta_E = NaN; 
Props.BatteryCells(10).AvgCost = NaN;

% Convert to table
TempBatteryCellTable = struct2table(Props.BatteryCells);
% Convert "Type" variable to table row names
TempBatteryCellTable.Properties.RowNames = TempBatteryCellTable{:,'Type'}; % add Type entries as row names
TempBatteryCellTable.Type = []; % delete Type variable
% Add to Props structure
Props.BatteryCellTable = TempBatteryCellTable;
clear TempBatteryCellTable;
% Access values in table:
% % Load properties
% Props = load('Props');
% Props = Props.Props;
% LHV_Jpkg = Props.FuelTable{'Diesel', 'LHV_Jpkg'};
% Density_kgpm3 = Props.FuelTable{'Diesel', 'Density_kgpm3'};

%% Countries & Region Data

% UNITS
% Props.Countries(1).Name            
% Props.Countries(1).gCO2pkWh            [g/kWh]

% Singapore
Props.Countries(1).Name = 'Singapore';
Props.Countries(1).gCO2pkWh = 499; % [6] p.113

% Germany
Props.Countries(2).Name = 'Germany';
Props.Countries(2).gCO2pkWh = 461; % [6] p.113

% Peoples Republic of China
Props.Countries(3).Name = 'Peoples Republic of China';
Props.Countries(3).gCO2pkWh = 766; % [6] p.111

% United States
Props.Countries(4).Name = 'United States';
Props.Countries(4).gCO2pkWh = 522; % [6] p.111

% France
Props.Countries(5).Name = 'France';
Props.Countries(5).gCO2pkWh = 79; % [6] p.111

% Norway
Props.Countries(6).Name = 'Norway';
Props.Countries(6).gCO2pkWh = 17; % [6] p.111

% World
Props.Countries(7).Name = 'World';
Props.Countries(7).gCO2pkWh = 565; % [6] p.111

% Europe
Props.Countries(8).Name = 'Europe';
Props.Countries(8).gCO2pkWh = 231; % [6] p.111

% Africa
Props.Countries(9).Name = 'Africa';
Props.Countries(9).gCO2pkWh = 637; % [6] p.112

% Switzerland
Props.Countries(9).Name = 'Switzerland';
Props.Countries(9).gCO2pkWh = 27; % [6] p.112

% Asia
Props.Countries(10).Name = 'Asia';
Props.Countries(10).gCO2pkWh = 746; % [6] p.113


%% Drivetrain Components

% Permanent Synchronous Motor
Props.Components(1).Type = 'PSM';
Props.Components(1).SpecPDensity_kWpkg = 1.2; %kW/kg, including inverter and gear, Source: FTM from Benni
Props.Components(1).VolPDensity = NaN;
Props.Components(1).POverloadCapacity = 2; %Lienkamp 2014 Status Elektromobilitšt

% Asynchron Motor
Props.Components(2).Type = 'ASM';
Props.Components(2).SpecPDensity_kWpkg = 0.7; % kW/kg, including inverter and gear, Source: FTM from Benni
Props.Components(2).VolPDensity = NaN;
Props.Components(2).POverloadCapacity = 3.5; % Lienkamp 2014 Status Elektromobilitšt

% Kinetic Energy Recovery System (KERS)
Props.Components(3).Type = 'KERS';
Props.Components(3).SpecPDensity_kWpkg = NaN;
Props.Components(3).VolPDensity = NaN;
Props.Components(3).POverloadCapacity = NaN;

% Turbo Steamer for Waste Heat Recovery
Props.Components(3).Type = 'Turbo Steamer';
Props.Components(3).SpecPDensity_kWpkg = NaN;
Props.Components(3).VolPDensity = NaN;
Props.Components(3).POverloadCapacity = NaN;

%% Power Generation

Props.PowerGeneration(1).Type = '';


%% Costs in Different Countries

% EnergyCommodities = {'Electricity'; 'Gas'; 'Gasoline'; 'Diesel'};
% Singapore = [22.41 0.07; 17.62 0.07; NaN NaN; NaN NaN]; % Tarif from 01.07.2015, http://www.singaporepower.com.sg/irj/go/km/docs/wpccontent/Sites/SP%20Services/Site%20Content/Tariffs/documents/latest_press_release_doc.pdf
% Germany = [13.87 48; NaN NaN; NaN NaN; NaN NaN]; % avg. 2014, BDEW - 2014 - Strompreisanalyse 2014 Chartsatz 
% 
% Table2 = table(Singapore, Germany, 'RowNames',EnergyCommodities)

% table = readtable('CommodityCosts.xlsx')

%% Instanciate a Properties Container
PropertiesContainer = PropertiesContainer(Props);

%% Sources
% [1] European Comission, Concawe, EUCAR - 2013 - Tank-to-Wheels Report
% Version 4.0 JEC Well-to-Weels Analysis
% [2] Guzzella, Sciarretta - 2012 - Vehicle Propulsion Systems Introduction to Modeling and Optimization
% [3] Jossen, Weydanz - 2006 - Moderne Akkumulatoren richtig einsetzen
% [4] Panasonic - 2007 - Lithium Ion Batteries Technical Handbook
% [5] Braess, Seiffert - 2013 - Vieweg Handbuch Kraftfahrzeugtechnik
% [6] IEA Statistics - 2012 - CO2 Emissions from Fuel Combustion (numbers
% from 2010)
% [7] http://www.jsrmicro.com/index.php/EnergyAndEnvironment/LithiumIonCapacitor/FormFactorsPropertie/