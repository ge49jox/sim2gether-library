function [NewBuild] = CheckIfNewEngineMapisrequired (engine_type,peak_power)


persistent engine_type_Old;
persistent peak_power_Old;

EqualType = strcmp(engine_type_Old, engine_type);

if (peak_power_Old==peak_power) & (EqualType==1)
    NewBuild=0;
else
    NewBuild=1;
    
    engine_type_Old=engine_type;
    peak_power_Old=peak_power;
end



end