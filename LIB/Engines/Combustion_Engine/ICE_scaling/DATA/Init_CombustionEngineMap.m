function [Engine] = Init_CombustionEngineMap (engine_type,peak_power,BrakeOption)

%persistent matrix_eta_Old;
%persistent vektor_M_Old;
%persistent vektor_n_rad_Old;
persistent Engine_Old;

[NewBuild] = CheckIfNewEngineMapisrequired (engine_type,peak_power);

    if NewBuild==1
    Engine=Engine_calc(engine_type, peak_power);
    Engine_Old = Engine;
    else
     Engine = Engine_Old;
    end
end
