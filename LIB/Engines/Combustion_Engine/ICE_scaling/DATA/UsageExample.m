clear all
close all
clc

% Diesel Engine, 100 kW
MyDieselEngine = Engine('Diesel', 130, false);
% Plots
figure
MyDieselEngine.plot_EngineMap_Efficiency
figure
MyDieselEngine.plot_EngineMap_gps
% Simulation
[Possible, MechBrakeTorque, out_mdot, out_dmdot, LHV] = MyDieselEngine.Sim(250, 0, 200)
% List Engine class properties in Command Window
MyDieselEngine


% Same pattern for example for Gasoline Engine 130 kW
MyGasolineEngine = Engine('Gasoline', 130, false)
figure
MyGasolineEngine.plot_EngineMap_Efficiency
figure
MyGasolineEngine.plot_EngineMap_gps