classdef PropertiesContainer
    %PROPERTIESCONTAINER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Properties
    end
    
    methods
        
        function P = PropertiesContainer(Properties)
            
            P.Properties = Properties;
            
        end
        
        function plot_RagoneChartSpecific(P)
            
            % Extract data from properties
            BatteryCellTable = P.Properties.BatteryCellTable;
            
            CellTypes = BatteryCellTable.Properties.RowNames;
            NumberOfCellTypes = length(CellTypes);
            
            % Set figure properties
            hold on;
            grid on
            title('Ragone Plot', 'Interpreter', 'none');
            xlabel('Power Density in W/kg');
            ylabel('Energy Density in Wh/kg');
            
            for i = 1 : NumberOfCellTypes
                PDens_Wpkg = BatteryCellTable.AvgSpecPDensity_Wpkg(i);
                EDens_Whpkg = BatteryCellTable.AvgSpecEDensity_Whpkg(i);
                scatter( PDens_Wpkg, EDens_Whpkg);
                text( PDens_Wpkg + 5, EDens_Whpkg + 5, CellTypes{i}, 'Interpreter', 'none');
                
                MinPDens_Wpkg = BatteryCellTable.MinSpecPDensity_Wpkg(i);
                MinEDens_Whpkg = BatteryCellTable.MinSpecEDensity_Whpkg(i);
                MaxPDens_Wpkg = BatteryCellTable.MaxSpecPDensity_Wpkg(i);
                MaxEDens_Whpkg = BatteryCellTable.MaxSpecEDensity_Whpkg(i);
                
                if all(~isnan([MinPDens_Wpkg MinEDens_Whpkg MaxPDens_Wpkg MaxEDens_Whpkg]))
                    RangeWidth = MaxPDens_Wpkg - MinPDens_Wpkg;
                    RangeHeight = MaxEDens_Whpkg - MinEDens_Whpkg;
                    rectangle('Position',[MinPDens_Wpkg MinEDens_Whpkg RangeWidth RangeHeight],'Curvature',[1,1]);
                end
                
            end
            
        end
        
        function plot_CompareGasolineDiesel(P)
            
            % Extract data from properties
            FuelTable = P.Properties.FuelTable;
            
            FuelTypes = FuelTable.Properties.RowNames;
            
            CO2PerEnergy_kgpkWh = FuelTable.CO2EmissionFactor_kgpkg .* J2kWh(FuelTable.LHV_Jpkg);
            
            bar(CO2PerEnergy_kgpkWh);
            ax = gca;
            ax.XTickLabel = FuelTypes;
            
            ylabel('CO2 per Energy in kg/kWh'); 
            
        end
        
        %             function plot_RagoneChartVolumetric(P)
        %
        %                 % Extract data from properties
        %                 BatteryCellTable = P.Properties.BatteryCellTable;
        %
        %                 CellTypes = BatteryCellTable.Properties.RowNames;
        %                 NumberOfCellTypes = length(CellTypes);
        %
        %                 % Set figure properties
        %                 hold on;
        %                 title('Ragone Plot', 'Interpreter', 'none');
        %                 xlabel('Power Density in W/kg');
        %                 ylabel('Energy Density in Wh/kg');
        %
        %                 for i = 1 : NumberOfCellTypes
        %                     PDens_Wpkg = BatteryCellTable.AvgSpecPDensity_Wpkg(i);
        %                     EDens_Whpkg = BatteryCellTable.AvgSpecEDensity_Whpkg(i);
        %                     scatter( PDens_Wpkg, EDens_Whpkg);
        %
        %                     text( PDens_Wpkg + 5, EDens_Whpkg + 5, CellTypes{i}, 'Interpreter', 'none')
        %
        %                 end
        %             end
        
        
    end
    
end

