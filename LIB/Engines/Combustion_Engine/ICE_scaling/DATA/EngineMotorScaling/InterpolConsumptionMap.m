function [fitresult, gof] = InterpolConsumptionMap(x, y, z)
%CREATEFIT1(X_CON,Y_CON,Z_CON)
%  Create a fit.
%
%  Data for 'untitled fit 1' fit:
%      X Input : x_con
%      Y Input : y_con
%      Z Output: z_con
%  Output:
%      fitresult : a fit object representing the fit.
%      gof : structure with goodness-of fit info.
%
%  See also FIT, CFIT, SFIT.

%  Auto-generated by MATLAB on 02-Feb-2016 15:33:56


%% Fit: 'untitled fit 1'.
[xData, yData, zData] = prepareSurfaceData( x, y, z );

% Set up fittype and options.
ft = 'linearinterp';

% Fit model to data.
[fitresult, gof] = fit( [xData, yData], zData, ft, 'Normalize', 'on' );

% % Plot fit with data.
% figure( 'Name', 'untitled fit 1' );
% h = plot( fitresult, [xData, yData], zData );
% legend( h, 'untitled fit 1', 'z_con vs. x_con, y_con', 'Location', 'NorthEast' );
% % Label axes
% xlabel x_con
% ylabel y_con
% zlabel z_con
% grid on
% view( 49.5, 50.0 );


