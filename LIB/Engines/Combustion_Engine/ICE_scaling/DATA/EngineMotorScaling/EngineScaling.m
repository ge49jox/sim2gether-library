classdef EngineScaling < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        
        EffGrid % Efficiency Grid
        ConGrid % Consumption Grid
        MaxTrqLine
        DragTrqLine
        PeakPower_kW
        PeakPowerCurve_kW
        EngineType
        
        
    end
    
    methods
        
        function E = EngineScaling(EngineType)
            
            E.EngineType = EngineType;
            
            switch EngineType
                
                case 'Diesel'
                    
                    load('DieselEngine_Normalized_WithEdge.mat');
%                     load('DieselEngine_Normalized_WithEdge.mat');
                    % Efficiency
                    E.EffGrid = EffGrid;
                    E.MaxTrqLine = MaxTrqLine;
                    % Peak torque curve
                    E.PeakPowerCurve_kW = (E.MaxTrqLine.x_radps .* E.MaxTrqLine.y_Nm) / 1000;
                    E.PeakPower_kW = max(E.MaxTrqLine.x_radps .* E.MaxTrqLine.y_Nm) / 1000;
                    % Drag torque curve
                    % E.DragTrqLine.y_Nm = -0.035 .* max(E.MaxTrqLine.y_Nm) - (0.0022 .* E.EffGrid.xgs_radps(:,1)).^2 .* 0.1 .* max(E.MaxTrqLine.y_Nm); % EXPONENTIAL drag curve
                    E.DragTrqLine.y_Nm = -0.05 .* max(E.MaxTrqLine.y_Nm) - 0.08 .* E.EffGrid.xgs_radps(:,1); % LINEAR drag curve
                    E.DragTrqLine.x_radps = E.MaxTrqLine.x_radps;
                    
                case 'Gasoline'
                    
                    load('GasolineEngine_Normalized_WithEdge.mat');
                    % Efficiency
                    E.EffGrid = EffGrid;
                    E.MaxTrqLine = MaxTrqLine;
                    % Peak torque curve
                    E.PeakPowerCurve_kW = (E.MaxTrqLine.x_radps .* E.MaxTrqLine.y_Nm) / 1000;
                    E.PeakPower_kW = max(E.MaxTrqLine.x_radps .* E.MaxTrqLine.y_Nm) / 1000;
                    % Drag torque curve
                    % E.DragTrqLine.y_Nm = -0.15 .* max(E.MaxTrqLine.y_Nm) - (0.003 .* E.EffGrid.xgs_radps(:,1)).^2 .* 0.1 .* max(E.MaxTrqLine.y_Nm); % EXPONENTIAL drag curve
                    E.DragTrqLine.y_Nm = -0.12 .* max(E.MaxTrqLine.y_Nm) - 0.03 .* E.EffGrid.xgs_radps(:,1); % LINEAR drag curve
                    E.DragTrqLine.x_radps = E.MaxTrqLine.x_radps;
                    
                case 'TurboGasoline'
                    
                    load('TurboGasoline_Normalized_WithEdge.mat');
                    % Efficiency
                    E.EffGrid = EffGrid;
                    E.MaxTrqLine = MaxTrqLine;
                    % Peak torque curve
                    E.PeakPowerCurve_kW = (E.MaxTrqLine.x_radps .* E.MaxTrqLine.y_Nm) / 1000;
                    E.PeakPower_kW = max(E.MaxTrqLine.x_radps .* E.MaxTrqLine.y_Nm) / 1000;
                    % Drag torque curve
                    % E.DragTrqLine.y_Nm = -0.15 .* max(E.MaxTrqLine.y_Nm) - (0.003 .* E.EffGrid.xgs_radps(:,1)).^2 .* 0.1 .* max(E.MaxTrqLine.y_Nm); % EXPONENTIAL drag curve
                    E.DragTrqLine.y_Nm = -0.07 .* max(E.MaxTrqLine.y_Nm) - 0.04 .* E.EffGrid.xgs_radps(:,1); % LINEAR drag curve
                    E.DragTrqLine.x_radps = E.MaxTrqLine.x_radps;
                    
            end

        end
        
        function E = Scale(E, PeakPower_kWh )
            
            ScalingFactor = PeakPower_kWh / 100;
            
            E.PeakPowerCurve_kW = (E.MaxTrqLine.x_radps .* ScalingFactor .*  E.MaxTrqLine.y_Nm) / 1000;
            E.PeakPower_kW = max(E.MaxTrqLine.x_radps .* ScalingFactor .* E.MaxTrqLine.y_Nm) / 1000;
            E.MaxTrqLine.y_Nm = ScalingFactor .* E.MaxTrqLine.y_Nm;
            E.EffGrid.ygs_Nm = ScalingFactor .* E.EffGrid.ygs_Nm;
            E.DragTrqLine.y_Nm = ScalingFactor .* E.DragTrqLine.y_Nm;
            
        end
        
        function plot_Efficiency(E)
            
            %% Plot Engine Efficiency Map
            hold on;
            
            % Plot peak torque curve
            plot(E.MaxTrqLine.x_radps, E.MaxTrqLine.y_Nm, 'r-','LineWidth', 3);
            
            % Plot drag torque line
            plot(E.DragTrqLine.x_radps, E.DragTrqLine.y_Nm, 'r-','LineWidth', 3);
            
            % Plot peak power curve
            plot(E.MaxTrqLine.x_radps, E.PeakPowerCurve_kW, 'g-','LineWidth', 3);
            E.PeakPower_kW
            
            [temp, h] = contour(E.EffGrid.xgs_radps, E.EffGrid.ygs_Nm, E.EffGrid.zgs_eff, [0.1:0.01:1], 'b'); %, [0.41:0.01:1]]
            %             [temp, h] = contour(E.ww, E.TT, ee);
            set(h,'ShowText','on','TextStep',get(h,'LevelStep'));
            % legend('Engine Efficiency', 'T_{max}', 'T_{drag}', 'P_{max}');
            xlabel('Rotation Speed in rad/s');
            ylabel('Torque in Nm/ Power in kW');
            title('Engine Efficiency Map');
            grid on;
            
        end
        
        function calc_Consumption(E)
            
            PP_mech_W = E.EffGrid.xgs_radps .* E.EffGrid.ygs_Nm;

            PP_fuel_W = PP_mech_W ./ E.EffGrid.zgs_eff;
            
            Props = load('Props');
            Props = Props.Props;
            
            switch E.EngineType
                case 'Diesel'
                    fuel = 'Diesel';
                case {'Gasoline', 'TurboGasoline'}
                    fuel = 'Gasoline';
            end
            
            LHV_Jpkg = Props.FuelTable{fuel,'LHV_Jpkg'};
            Density_kgpm3 = Props.FuelTable{fuel,'Density_kgpm3'};
            
            PP_fuel_kgps = PP_fuel_W ./ LHV_Jpkg;
            PP_fuel_gps = kg2g(PP_fuel_kgps);
            
            % Write consumtion grid
            E.ConGrid.xgs_radps = E.EffGrid.xgs_radps;
            E.ConGrid.ygs_Nm = E.EffGrid.ygs_Nm;
            E.ConGrid.zgs_gps = PP_fuel_gps;
            
        end
        
        function calc_ConsumptionInterpolation(E)
           
            calc_Consumption(E);
            
            % Getting interpolation values from structure and convert to
            % vector
            % grid
            x_con = E.ConGrid.xgs_radps(:,1);
            y_con = E.ConGrid.ygs_Nm(:,1);
            z_con = E.ConGrid.zgs_gps(:,1);
            % min torque line
            x_con_drag = E.DragTrqLine.x_radps(:);
            y_con_drag = E.DragTrqLine.y_Nm(:);
            z_con_drag = zeros(size(x_con_drag));
            
            % Removing NaN values from grid
            x_con(isnan(z_con)) = [];
            y_con(isnan(z_con)) = [];
            z_con(isnan(z_con)) = [];
            
            % Compiling grid and drag torque
            x_con = [x_con; x_con_drag];
            y_con = [y_con; y_con_drag];
            z_con = [z_con; z_con_drag];
            
            [fitresult, gof] = InterpolConsumptionMap(x_con, y_con, z_con);
            
            % Determine max and min values for interpolation grid
            x_min = min(E.DragTrqLine.x_radps(:));
            x_max = max(E.ConGrid.xgs_radps(:));
            y_min = min(E.DragTrqLine.y_Nm(:));
            y_max = max(E.ConGrid.ygs_Nm(:));
            
            x_vec = linspace(x_min, x_max, 300);
            y_vec = linspace(y_min, y_max, 280);
            [xm, ym] = ndgrid(x_vec, y_vec);
            
            % Interpolate
            % Lower part
            xm_p_lower = xm( ym < min(E.ConGrid.ygs_Nm(:)) );
            ym_p_lower = ym( ym < min(E.ConGrid.ygs_Nm(:)) );
            zm_p_lower = fitresult(xm_p_lower,ym_p_lower);
            % Upper part
            xm_p_upper = xm( ym >= min(E.ConGrid.ygs_Nm(:)) );
            ym_p_upper = ym( ym >= min(E.ConGrid.ygs_Nm(:)) );
            zm_p_upper = interpn(E.ConGrid.xgs_radps, E.ConGrid.ygs_Nm, E.ConGrid.zgs_gps, xm_p_upper,ym_p_upper);
            % Compile two differently interpolated parts to one
            zm = [zm_p_lower; zm_p_upper];
            zm = reshape(zm,size(xm));
            
            % [temp, h] = contour(xm, ym, zm, [0:0.05:0.1, 0.1:0.1:0.5, 0:0.5:20], 'b'); %, [0.41:0.01:1]]
            
            % Write consumtion grid
            E.ConGrid.xgs_radps = xm;
            E.ConGrid.ygs_Nm = ym;
            E.ConGrid.zgs_gps = zm;
            
        end
        
        function plot_Consumption(E)
            
            calc_ConsumptionInterpolation(E);
            
            figure;
            hold on;
            
            % Plot peak torque curve
            plot(E.MaxTrqLine.x_radps, E.MaxTrqLine.y_Nm, 'r-','LineWidth', 3);
            
            % Plot drag torque line
            plot(E.DragTrqLine.x_radps, E.DragTrqLine.y_Nm, 'r-','LineWidth', 3);
            
            % Plot peak power curve
            plot(E.MaxTrqLine.x_radps, E.PeakPowerCurve_kW, 'g-','LineWidth', 3);
            E.PeakPower_kW
            
            [temp, h] = contour(E.ConGrid.xgs_radps, E.ConGrid.ygs_Nm, E.ConGrid.zgs_gps, [0:0.05:0.1, 0.1:0.1:0.5, 0:0.5:20], 'b'); %, [0.41:0.01:1]]
            %             [temp, h] = contour(E.ww, E.TT, ee);
            set(h,'ShowText','on','TextStep',get(h,'LevelStep'));
            % legend('Engine Efficiency', 'T_{max}', 'T_{drag}', 'P_{max}');
            xlabel('Rotation Speed in rad/s');
            ylabel('Torque in Nm/ Power in kW');
            title('Engine Efficiency Map');
            grid on;
                        
        end
        
        function [ww, TT, Consumption, w, T_max_curve, T_min_curve, EffGrid, ConGrid] = Export(E)
            
           E.calc_ConsumptionInterpolation;
           
           ww = E.ConGrid.xgs_radps;
           TT = E.ConGrid.ygs_Nm;
           Consumption = g2kg(E.ConGrid.zgs_gps);
           w = E.MaxTrqLine.x_radps;
           T_max_curve = E.MaxTrqLine.y_Nm;
           T_min_curve = E.DragTrqLine.y_Nm;
           EffGrid = E.EffGrid;
           ConGrid = E.ConGrid; 
        end 
        
    end
    
end

