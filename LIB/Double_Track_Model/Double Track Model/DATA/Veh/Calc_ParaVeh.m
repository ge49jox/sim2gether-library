 function [par_VEH]=Calc_ParaVeh(RohAir,VEH_m,VEH_xSP,VEH_ySP,VEH_zSP,VEH_m_usF,VEH_m_usR,VEH_g,VEH_Iwf,VEH_Iwr,VEH_ztSPf,VEH_ztSPr,VEH_cz,VEH_cx,VEH_cy,VEH_xDP,VEH_yDP,VEH_Ix,VEH_Iy,VEH_Iz,VEH_l,VEH_sF,VEH_sR,VEH_ssF,VEH_ssR,VEH_sdF,VEH_sdR,VEH_Afront,VEH_zRCf,VEH_zRCr,VEH_zPC,VEH_carbF,VEH_carbR,VEH_sarbF,VEH_sarbR,VEH_sw_kinematics,VEH_kin_lw_toe_f,VEH_kin_lw_camber_f,VEH_kin_camb_f,VEH_kin_toe_f,VEH_kin_camb_r,VEH_kin_toe_r,VEH_sw_compliance, VEH_elkin_toe_twistbeam, VEH_elkin_camb_twistbeam,VEH_elkin_y_toe_f,VEH_elkin_y_toe_r,VEH_elkin_x_toe_f,VEH_elkin_x_toe_r,VEH_elkin_y_camb_f,VEH_elkin_y_camb_r,VEH_elkin_x_camb_f,VEH_elkin_x_camb_r,VEH_ddF_reb,VEH_ddF_comp,VEH_ddR_reb,VEH_ddR_comp,VEH_cF_front,VEH_cF_rear)
par_VEH.RohAir=RohAir;
par_VEH.m=VEH_m;
par_VEH.xSP=VEH_xSP;
par_VEH.ySP=VEH_ySP;
par_VEH.zSP=VEH_zSP;
par_VEH.m_usF=VEH_m_usF;
par_VEH.m_usR=VEH_m_usR;
par_VEH.g=VEH_g;
par_VEH.Iwf=VEH_Iwf;
par_VEH.Iwr=VEH_Iwr;
par_VEH.ztSPf=VEH_ztSPf;
par_VEH.ztSPr=VEH_ztSPr;
par_VEH.cz=VEH_cz;
par_VEH.cx=VEH_cx;
par_VEH.cy=VEH_cy;
par_VEH.xDP=VEH_xDP;
par_VEH.yDP=VEH_yDP;
par_VEH.Ix=VEH_Ix;
par_VEH.Iy=VEH_Iy;
par_VEH.Iz=VEH_Iz;
par_VEH.l=VEH_l;
par_VEH.sF=VEH_sF;
par_VEH.sR=VEH_sR;
par_VEH.ssF=VEH_ssF;
par_VEH.ssR=VEH_ssR;
par_VEH.sdF=VEH_sdF;
par_VEH.sdR=VEH_sdR;
par_VEH.Afront=VEH_Afront;
par_VEH.zRCf=VEH_zRCf;
par_VEH.zRCr=VEH_zRCr;
par_VEH.zPC=VEH_zPC;
par_VEH.carbF=VEH_carbF;
par_VEH.carbR=VEH_carbR;
par_VEH.sarbF=VEH_sarbF;
par_VEH.sarbR=VEH_sarbR;
par_VEH.sw_kinematics=VEH_sw_kinematics;
par_VEH.kin_lw_toe_f=VEH_kin_lw_toe_f;
par_VEH.kin_lw_camber_f=VEH_kin_lw_camber_f;
par_VEH.kin_camb_f=VEH_kin_camb_f;
par_VEH.kin_toe_f=VEH_kin_toe_f;
par_VEH.kin_camb_r=VEH_kin_camb_r;
par_VEH.kin_toe_r=VEH_kin_toe_r;
par_VEH.sw_compliance=VEH_sw_compliance;
par_VEH.elkin_toe_twistbeam=VEH_elkin_toe_twistbeam;
par_VEH.elkin_camb_twistbeam=VEH_elkin_camb_twistbeam;
par_VEH.elkin_y_toe_f=VEH_elkin_y_toe_f;
par_VEH.elkin_y_toe_r=VEH_elkin_y_toe_r;
par_VEH.elkin_x_toe_f=VEH_elkin_x_toe_f;
par_VEH.elkin_x_toe_r=VEH_elkin_x_toe_r;
par_VEH.elkin_y_camb_f=VEH_elkin_y_camb_f;
par_VEH.elkin_y_camb_r=VEH_elkin_y_camb_r;
par_VEH.elkin_x_camb_f=VEH_elkin_x_camb_f;
par_VEH.elkin_x_camb_r=VEH_elkin_x_camb_r;
par_VEH.ddF_reb=VEH_ddF_reb;
par_VEH.ddF_comp=VEH_ddF_comp;
par_VEH.ddR_reb=VEH_ddR_reb;
par_VEH.ddR_comp=VEH_ddR_comp;
par_VEH.cF_front=VEH_cF_front;
par_VEH.cF_rear=VEH_cF_rear;




%% Berechnung von anderen Parameters
par_VEH.lF = par_VEH.xSP; %Braucht man ja dann eigentlich nicht?!
par_VEH.lR = par_VEH.l-par_VEH.xSP; 

par_VEH.lDP = -par_VEH.xDP+par_VEH.xSP; %Distance pressure point to CoM in x-direction SP-KOS
par_VEH.sDP = -par_VEH.yDP+par_VEH.ySP; %Distance pressure point to CoM in y-direction SP-KOS


%par_VEH.z_preload_F=-par_VEH.m*par_VEH.g*par_VEH.lR/par_VEH.l/par_VEH.csF/2;werden
%nicht mehr gebraucht
%par_VEH.z_preload_R=-par_VEH.m*par_VEH.g*par_VEH.lF/par_VEH.l/par_VEH.csR/2;

par_VEH.zRC = (par_VEH.zRCf-par_VEH.zRCr)/par_VEH.l*par_VEH.lR+par_VEH.zRCr;
% Is not used anymore
%par_VEH.zsSP = ((par_VEH.m*par_VEH.zSP)-(par_VEH.m_usF*par_VEH.ztSPf)-(par_VEH.m_usR*par_VEH.ztSPr))/par_VEH.mss; 



end