function [par_TIR, par_VEH] = Init_ZSM (PIOF, PPX1F, PPX2F ,PPX3F, PPX4F, PPY1F , PPY2F ,PPY3F ,PPY4F , QPZ1F, PIOR, PPX1R, PPX2R ,PPX3R, PPX4R, PPY1R , PPY2R ,PPY3R ,PPY4R , QPZ1R, SelectedTireFront, SelectedTireRear, RohAir,VEH_m,VEH_xSP,VEH_ySP,VEH_zSP,VEH_m_usF,VEH_m_usR,VEH_g,VEH_Iwf,VEH_Iwr,VEH_ztSPf,VEH_ztSPr,VEH_cz,VEH_cx,VEH_cy,VEH_xDP,VEH_yDP,VEH_Ix,VEH_Iy,VEH_Iz,VEH_l,VEH_sF,VEH_sR,VEH_ssF,VEH_ssR,VEH_sdF,VEH_sdR,VEH_Afront,VEH_zRCf,VEH_zRCr,VEH_zPC,VEH_carbF,VEH_carbR,VEH_sarbF,VEH_sarbR,VEH_sw_kinematics,VEH_kin_lw_toe_f,VEH_kin_lw_camber_f,VEH_kin_camb_f,VEH_kin_toe_f,VEH_kin_camb_r,VEH_kin_toe_r,VEH_sw_compliance, VEH_elkin_toe_twistbeam, VEH_elkin_camb_twistbeam,VEH_elkin_y_toe_f,VEH_elkin_y_toe_r,VEH_elkin_x_toe_f,VEH_elkin_x_toe_r,VEH_elkin_y_camb_f,VEH_elkin_y_camb_r,VEH_elkin_x_camb_f,VEH_elkin_x_camb_r,VEH_ddF_reb,VEH_ddF_comp,VEH_ddR_reb,VEH_ddR_comp,VEH_cF_front,VEH_cF_rear,LoadindexF,LoadindexR)


%% TIRE
[par_TIR]=GetTireParameters (SelectedTireFront, SelectedTireRear,LoadindexF,LoadindexR)
[par_TIR]=SetPressureParameters(PIOF, PPX1F, PPX2F ,PPX3F, PPX4F, PPY1F , PPY2F ,PPY3F ,PPY4F , QPZ1F, PIOR, PPX1R, PPX2R ,PPX3R, PPX4R, PPY1R , PPY2R ,PPY3R ,PPY4R , QPZ1R, par_TIR)

%% VEHICLE
[par_VEH]=Calc_ParaVeh(RohAir,VEH_m,VEH_xSP,VEH_ySP,VEH_zSP,VEH_m_usF,VEH_m_usR,VEH_g,VEH_Iwf,VEH_Iwr,VEH_ztSPf,VEH_ztSPr,VEH_cz,VEH_cx,VEH_cy,VEH_xDP,VEH_yDP,VEH_Ix,VEH_Iy,VEH_Iz,VEH_l,VEH_sF,VEH_sR,VEH_ssF,VEH_ssR,VEH_sdF,VEH_sdR,VEH_Afront,VEH_zRCf,VEH_zRCr,VEH_zPC,VEH_carbF,VEH_carbR,VEH_sarbF,VEH_sarbR,VEH_sw_kinematics,VEH_kin_lw_toe_f,VEH_kin_lw_camber_f,VEH_kin_camb_f,VEH_kin_toe_f,VEH_kin_camb_r,VEH_kin_toe_r,VEH_sw_compliance, VEH_elkin_toe_twistbeam, VEH_elkin_camb_twistbeam,VEH_elkin_y_toe_f,VEH_elkin_y_toe_r,VEH_elkin_x_toe_f,VEH_elkin_x_toe_r,VEH_elkin_y_camb_f,VEH_elkin_y_camb_r,VEH_elkin_x_camb_f,VEH_elkin_x_camb_r,VEH_ddF_reb,VEH_ddF_comp,VEH_ddR_reb,VEH_ddR_comp,VEH_cF_front,VEH_cF_rear)


end 