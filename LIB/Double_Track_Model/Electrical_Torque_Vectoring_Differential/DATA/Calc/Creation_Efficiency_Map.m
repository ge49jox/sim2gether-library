function [vektor_M_max, vektor_n_rad, matrix_eta, vektor_M] = Creation_Efficiency_Map(M_EM_max,M_EM_nenn,n_EM_max,n_EM_nenn, typ_EM, eta_mit_LE, U_Bat, cos_phi)

P_EM_nenn = M_EM_nenn * n_EM_nenn/60*2*pi;
P_EM_max = M_EM_max * n_EM_nenn/60*2*pi;

folder=strrep(which('Creation_Efficiency_Map'),'\Creation_Efficiency_Map.m','')
cd([folder '\CalculationEfficiencyMap'])

[vektor_eta, step_M, step_n, vektor_M_max, vektor_M, vektor_n, M_EM_max, n_EM_nenn, m_EM, J_EM] = Interpolieren(M_EM_nenn, n_EM_nenn, M_EM_max, n_EM_max, P_EM_nenn, typ_EM, [folder '\CalculationEfficiencyMap\']);


% Berechnung Gesamtwirkungsgrad E-M und LE
if eta_mit_LE == 1
    
    % Ausf�hren des Umrechnung.m Skripts
    [vektor_Pelv, vektor_Pelr, vektor_Irmsv, vektor_Irmsr, vektor_Urms] = Umrechnung(vektor_eta, M_EM_max, n_EM_nenn, U_Bat, cos_phi, step_n, step_M);

    % Ausf�hren des LE_Berechnung.m Skriptes
    [m_LE, eta_LE] = LE_Berechnung(P_EM_nenn, vektor_Pelv, vektor_Pelr, vektor_Irmsv, vektor_Irmsr, vektor_Urms, U_Bat, cos_phi);
    vektor_eta = vektor_eta .* eta_LE;
end

%Erstellen der eta Matrix
matrix_eta=zeros(201,201);
for idx=1:201
    matrix_eta(:,idx) = vektor_eta(((idx-1)*201+1):((idx-1)*201+201));
end

vektor_n_rad = (vektor_n /60 *2*pi);
%Keine 0 als Wikrungsgrad
matrix_eta(matrix_eta==0)= 0.001;

contourf(vektor_n,vektor_M,matrix_eta)
hold on
plot(vektor_n,vektor_M_max,'r', 'linewidth', 3)


end