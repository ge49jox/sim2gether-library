%% Leistungselektronik_ini

%% Parametrisierung
%  Daten des Leistungselektronik-Moduls SKiM406GD066HD
U_CEO = 0.9;               % [V]                                           Kollektor-Emitter-Spannung
r_CE  = 1.4 * 10^(-3);     % [Ohm]                                         Kollektor-Emitter-Widerstand
E_on_0  = 8 * 10^(-3);       % [J]                                           Einschaltverlustenergie 
E_off_0 = 25 * 10^(-3);      % [J]                                           Ausschaltverlustenergie 
I_ref = 400;               % [A]                                           Referenzstromstärke
U_ref = 300;               % [V]                                           Referenzspannung

U_D  = 1;                  % [V]                                           Diodenspannung
r_D  = 1.3 * 10^(-3);      % [Ohm]                                         Diodenwiderstand
E_rr_0 = 12 * 10^(-3);       % [J]                                           Sperrverzögerungsenergie

%  Schaltungsparameter
f_s = 8000;                % [Hz]                                          Schaltfrequenz