function [i,eta, m_eTV, Jred_ges, vektor_M_max, vektor_n_rad, matrix_eta, vektor_M] = Init_eTV (delta_M_max, MaxInputTrq, d_Rad, vzkonst, M_EM_max,M_EM_nenn,n_EM_max,n_EM_nenn, typ_EM, eta_mit_LE, U_Bat, cos_phi)


vzkonst.rho_st = 7900; %kg/m^3            %Wikipedia, deckt sich mit Tillinger
%vzkonst.rho_GG = 7200; %kg/m^3            %Tillinger, Wikipedia sagt 7250



[i, z, eta] = GeometrieWKG_eTV(delta_M_max, M_EM_max, d_Rad, vzkonst)


[m_eTV,Jred_ges] = MasseTraegheit_eTV(i, z, vzkonst, MaxInputTrq, delta_M_max)

[vektor_M_max, vektor_n_rad, matrix_eta, vektor_M] = Creation_Efficiency_Map(M_EM_max,M_EM_nenn,n_EM_max,n_EM_nenn, typ_EM, eta_mit_LE, U_Bat, cos_phi)



end