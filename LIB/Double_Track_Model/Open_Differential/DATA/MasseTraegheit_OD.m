 function [ m_OD, J_Komp, J_PKR,J_Sonnen,J_AW,Jx,Jy,Jz] = MasseTraegheit_OD(M_Diff_max, differential_struct)
%% Modell nach Fuchs f�r Abma�e

r_Eingangswelle = radius_Differentialeingangswelle(M_Diff_max); %[m]
[~, ~, r_korb_a, r_korb_i, r_planet,r_Sonne,r_AW,l_Korb] = differential_calculation(M_Diff_max, differential_struct, r_Eingangswelle); %Ma�e in [m]

%% Massenberechnung der Komponenten Planeten, Sonnen, Ausgleichswellen und K�rbe �ber (abgespeicherte) Regressionen, weil besser als Modell
load('Massenkoeffizienten_OD_planet.mat');
load('Massenkoeffizienten_OD_sun.mat');
load('Massenkoeffizienten_OD_shaft.mat');
load('Massenkoeffizienten_OD_case.mat');
%load('./mat-Files/Regressionen/Massenkoeffizienten_OD_Komp.mat');
m_Planeten = p_planet(1)*M_Diff_max + p_planet(2);
m_Sonnen = p_sun(1)*M_Diff_max + p_sun(2);
m_Ausgleichswelle = p_shaft(1)*M_Diff_max + p_shaft(2);
m_Korb = p_case(1)*M_Diff_max + p_case(2);
m_OD = m_Planeten + m_Sonnen + m_Ausgleichswelle + m_Korb;
%m_OD = p_Komp(1)*M_Diff_max + p_Komp(2);

%% Tr�gheiten f�r Konsistenz mit diesen Massen errechnen, alle in [kg m^2]
%Tr�gheit Korbkomplex
J_Komp = (m_Korb + m_Planeten + m_Ausgleichswelle)*(r_korb_a^2+r_korb_i^2)/2;
%Tr�gheit Planetenkegelr�der mit allerdings errechneten Radien; K�rper =
%Zylinder nicht hohl, homogen
J_PKR = 0.5*m_Planeten*r_planet^2;
%Tr�gheiten Sonnen
J_Sonnen = 0.5*m_Sonnen*r_Sonne^2;
%Tr�gheit AW (Ausgleichswelle)
J_AW =  0.5*m_Ausgleichswelle*r_AW^2;
%% Gesamte Tr�gheit unm Hauptachsen (ersetzt TraegheitAchsen_OD.m)
%Modell Hohlzylinder -> y-Achse ist achsparallel
%Tr�gheit Hohlzylinder um Querachse: m*(1/12)*(3(r_a^2+r_i^2)+h^2)
Jy = J_Komp;
Jx = m_OD*(1/12)*(3*(r_korb_a^2+r_korb_i^2)+l_Korb^2);
Jz = Jx;
end

