function [Eff_Load,m_OD, J_Komp, J_PKR,J_Sonnen,J_AW,Jx,Jy,Jz]=OD_Init (M_Diff_max, differential_struct)
load ('Open_Differential\DATA\OD_Eff_Load.mat')


[m_OD, J_Komp, J_PKR,J_Sonnen,J_AW,Jx,Jy,Jz] = MasseTraegheit_OD(M_Diff_max, differential_struct)
end