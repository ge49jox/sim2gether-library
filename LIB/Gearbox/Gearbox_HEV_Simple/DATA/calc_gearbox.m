function [gears, ratios, trq_eff, inertia] = calc_gearbox(z,i1,i2,i3,i4,i5,i6,i7,i8,i9,eta1,eta2,eta3,eta4,eta5,eta6,eta7,eta8,eta9,inertia1,inertia2,inertia3,inertia4,inertia5,inertia6,inertia7,inertia8,inertia9)

%function returns vectors of ratio, efficiency and inertia according to the
%entries in the mask

gears=(1:1:z);

for n=1:z
    ratio=['i',num2str(n)];
    ratios(n)=eval(ratio);  
end

for n=1:z
    trq_eff_temp=['eta',num2str(n)];
    trq_eff(n)=eval(trq_eff_temp);  
end

for n=1:z
    inertia_temp=['inertia',num2str(n)];
    inertia(n)=eval(inertia_temp);  
end

