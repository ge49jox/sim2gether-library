function [d1,d2,d3,d4,b_1_2,b_3_4,z1,z3,m] = festigkeit_G1_S2(M_EM_max, i_Stufe_1, i_Stufe_2, n_getr, vzkonst)
%Programm zur Zahnraddimensionierung bei einem Getriebe mit einem Gang und
%zwei Stufen

%% Vorgaben
%K-Faktor%
K=vzkonst.K;
%E-Modul%
E=vzkonst.E;
%Winkel%
alpha_n=vzkonst.alpha;  %gleich alpha_n
beta=vzkonst.beta;
%Sicherheit% 
S_H_min=vzkonst.S_H_min;
S_F_min=vzkonst.S_F_min;

%% Berechnung
%Startmodul%
m=3;
while 1
    %Verzahnungsdaten%
    [d1,d3]=Verzahnungsdaten_2STG(M_EM_max,m,i_Stufe_1);
    %Festigkeitsrechnung%
    %Zahnflankentragf�higkeit_1_2%
    [b1,d1]=Zahnflankentragfaehigkeit_2(i_Stufe_1,S_H_min,d1,M_EM_max,n_getr,K,E,m,alpha_n,beta);
    %Zahnfu�tragf�higkeit_1_2%
    [b2,d1]=Zahnfusstragfaehigkeit_2(i_Stufe_1,S_F_min,d1,M_EM_max,n_getr,K,m,alpha_n,beta);
    %Maximum der Breiten_1_2%
    b_1_2=max(b1,b2);
    M_EM_max_neu=i_Stufe_1*M_EM_max;
    %Zahnflankentragf�higkeit_3_4%
    [b1,d3]=Zahnflankentragfaehigkeit_2(i_Stufe_2,S_H_min,d3,M_EM_max_neu,n_getr,K,E,m,alpha_n,beta);
    %Zahnfu�tragf�higkeit_3_4%
    [b2,d3]=Zahnfusstragfaehigkeit_2(i_Stufe_2,S_F_min,d3,M_EM_max_neu,n_getr,K,m,alpha_n,beta);
    %Maximum der Breiten_3_4%
    b_3_4=max(b1,b2);
    %Modulbedingung%        %Kirchner S.186: b/m = 7...12, also ok
    b=max(b_1_2,b_3_4);
    if m>=b/12 
    break;
    end
    m=m+0.5;
end

%Restliche Zahnraddimensionen
d2 = d1*i_Stufe_1;
d4 = d3*i_Stufe_2;
z1 = d1/m;
z3 = d3/m;

end
