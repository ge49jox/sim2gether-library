function[d1,d3]=Verzahnungsdaten_2STG(M_EM_max,m,iG1)
%überschlägige Auslegung der Welle und Mindestgröße des daruf sitzenden Ritzels
    
    if M_EM_max<=13 
        tau_t_zul=10;
    elseif 13<M_EM_max&&M_EM_max<=201
        tau_t_zul=16;
    elseif 201<M_EM_max&&M_EM_max<=1229
        tau_t_zul=25;
    elseif 1229<M_EM_max&&M_EM_max<=6289
        tau_t_zul=32;
    else
        tau_t_zul=40;
    end
    
    d1min=1.72*(((M_EM_max)*1000)/tau_t_zul)^(1/3);     %Mindestdurchmesser der Welle, Til S.8 aus NWH S.266
    d1=d1min+3*m;                                       %Überschlägig aus NWH S.266
    d3min=1.72*(((M_EM_max*iG1)*1000)/tau_t_zul)^(1/3);
    d3=d3min+3*m;
    
end