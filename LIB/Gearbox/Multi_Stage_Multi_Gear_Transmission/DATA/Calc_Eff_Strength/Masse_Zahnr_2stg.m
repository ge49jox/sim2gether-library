function [m] = Masse_Zahnr_2stg(d1,d2,d3,d4,b12,b34)

%Masse vierer Zahnr�der
rho_st = 7.9/1000000; %[kg/mm^3]
m = 0.25*pi*(d1^2*b12+d2^2*b12+d3^2*b34+d4^2*b34)*rho_st; %[kg]

end