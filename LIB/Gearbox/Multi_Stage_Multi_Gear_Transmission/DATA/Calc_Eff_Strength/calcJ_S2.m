function J_red = calcJ_S2(b12,b34,d1,d3,i_Stufe_1,i_Stufe_2)

d4=i_Stufe_2*d3;
d2=i_Stufe_1*d1;
rho_St=7.9/1000000; %[kg/mm^3]
J21=(pi*b12*rho_St)/32*((d1/1000)^4) * ((i_Stufe_1*i_Stufe_2)^2); %Rad 1 ist EM-seitig und Rad 4 ist Rad-seitig; somit dreht Ritzel 4 mit Raddrehzahl, Ritzel 3 mit i_2, Ritzel 2 ist fest verbunden mit 3 und hat damit selbe Drehzahl und Ritzel 4 ist nochmal schneller um i_1
J22=(pi*b12*rho_St)/32*((d2/1000)^4) * (i_Stufe_2^2);
J23=(pi*b34*rho_St)/32*((d3/1000)^4) * (i_Stufe_2^2);
J24=(pi*b34*rho_St)/32*((d4/1000)^4);
J_red = (J21+J22+J23+J24)*1000000;    %[kg*m^2]

end