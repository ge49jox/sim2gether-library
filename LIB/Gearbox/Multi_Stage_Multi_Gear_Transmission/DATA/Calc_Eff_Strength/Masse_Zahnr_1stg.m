function [m] = Masse_Zahnr_1stg(d1,b,d2)

%Masse zweier Zahnr�der
rho_st = 7.9/1000000; %[kg/mm^3], 7.9 ist kg/dm^3
m = 0.25*pi*b*(d1^2+d2^2)*rho_st; %[kg]

end