%Zahnfu�tragf�higkeit%
function[b2,d1]=Zahnfusstragfaehigkeit_1(uebersetzung,S_F_min,d1,M_EM_max,n_getr,K,m,alpha_n,beta)
    
    sigma_F_lim=451;
    sigma_FP=sigma_F_lim/S_F_min;
    
    b=(2000*M_EM_max/(K*(d1^2)))*((uebersetzung+1)/uebersetzung);

    K_A=1;
    K_1=8.5;
    K_2=1;
    K_3=0.0087;

    while 1 
        z1=d1/m;
        d2=d1*uebersetzung;
        z2=d2/m;
        da1=d1+2*m;
        da2=d2+2*m;
        alpha_t=atan((tan(alpha_n))/(cos(beta)));
        db1=d1*cos(alpha_t);
        db2=d2*cos(alpha_t);
        a=(d1+d2)/2;
        alpha_w=acos((((z1+z2)*m)/(2*a))*(cos(alpha_n)));
        epsilon_alpha=(1/(m*pi*(cos(alpha_n))))*((((0.5*da1)^2-(0.5*db1)^2)^(1/2))+(((0.5*da2)^2-(0.5*db2)^2)^(1/2))-(a*sin(alpha_w)));
        Y_Fa=z1/(0.47*z1-2.3);
        Y_Sa=1+0.2*log(z1-2);
        epsilon_alpha_n=epsilon_alpha/(1-(sin(beta)^2)*(cos(alpha_n)^2));
        Y_epsilon=0.25+0.75/epsilon_alpha_n;
        v=pi*(n_getr/60)*(d1/1000);
        F_t=2*M_EM_max/d1;
        h=2.25*m;

        epsilon_beta=(b*sin(beta))/(m*pi);
        Y_beta=1-epsilon_beta*beta/120*(180/pi);
        K_V=1+((K_1*K_2/(K_A*F_t/b))+K_3)*(z1*v/100)*sqrt(uebersetzung^2/(1+uebersetzung^2));
        K_H_beta=1.15+0.18*((b/d1)^2)+0.3*(10^-3)*b;
        N=1/(1+h/b+(h/b)^2);
        K_F_beta=K_H_beta^N;
        zeta=F_t/b*K_A;
        
        if zeta>100
            K_F_alpha=1;
        else
            K_F_alpha=max(1.4,epsilon_alpha_n);
        end
        
        sigma_F=F_t/(b*m)*Y_Fa*Y_Sa*Y_beta*Y_epsilon*K_A*K_V*K_F_beta*K_F_alpha;
        sigma_F(isnan(sigma_F)==1)=0;
        
        if sigma_F<=sigma_FP; 
          break; 
        end
        
        b=b+1;
       
        while b>1.1*d1;
            d1=d1+1;
        end
    end
    b2=b;
end