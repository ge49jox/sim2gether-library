%Zahnflankentragf�higkeit%
function[b1,d1]=Zahnflankentragfaehigkeit_2(uebersetzung,S_H_min,d1,M_EM_max,n_getr,K,E,m,alpha_n,beta)
    
    sigma_H_lim=1599;
    sigma_HP=sigma_H_lim/S_H_min;

    b=(2000*M_EM_max/(K*(d1^2)))*((uebersetzung+1)/uebersetzung);   %Startwert nach NWH S.266

    K_A=1;
    K_1=8.5;
    K_2=1;
    K_3=0.0087;
    Z_E=sqrt(0.175*E);
    Z_beta=sqrt(cos(beta));

    while 1
        z1=d1/m;        %Ritzelz�hnezahl, krummer Wert
        d2=d1*uebersetzung;
        z2=d2/m;
        da1=d1+2*m;
        da2=d2+2*m;
        alpha_t=atan((tan(alpha_n))/(cos(beta)));
        db1=d1*cos(alpha_t);
        db2=d2*cos(alpha_t);
        a=(d1+d2)/2;
        alpha_w=acos((((z1+z2)*m)/(2*a))*(cos(alpha_n)));
        epsilon_alpha=(1/(m*pi*(cos(alpha_n))))*((((0.5*da1)^2-(0.5*db1)^2)^(1/2))+(((0.5*da2)^2-(0.5*db2)^2)^(1/2))-(a*sin(alpha_w)));
        beta_b=acos(sqrt(1-(sin(beta)*cos(alpha_n))^2));
        m_t=m/cos(beta);
        alpha_wt=acos((z1+z2)*m_t*cos(alpha_t)/(2*a));
        Z_H=sqrt((2*cos(beta_b)*cos(alpha_w))/((cos(alpha_t)^2)*sin(alpha_wt)));
        v=pi*(n_getr/60)*(d1/1000);
        F_t=2*M_EM_max/d1;

        K_V=1+((K_1*K_2/(K_A*F_t/b))+K_3)*(z1*v/100)*sqrt(uebersetzung^2/(1+uebersetzung^2));
        K_H_beta=1.15+0.18*((b/d1)^2)+0.3*(10^-3)*b;
        zeta=F_t/b*K_A;
        
        if zeta>=100
            K_H_alpha=1;
        else
            K_H_alpha=max(1.4,(epsilon_alpha/(cos(beta)^2)));
        end
        
        epsilon_beta=(b*sin(beta))/(m*pi);
        
        if epsilon_beta<1
           Z_epsilon=sqrt(((4-epsilon_alpha)/3)*(1-epsilon_beta)+(epsilon_beta/epsilon_alpha));
           M1=(tan(alpha_wt))/(sqrt((sqrt((da1/db1)^2-1)-(2*pi/z1))*(sqrt((da2/db2)^2-1)-((epsilon_alpha-1)*2*pi/z2))));
           Z_B=M1-epsilon_beta*(M1-1);
           Z_B=max(1,Z_B);
        else
           Z_epsilon=sqrt(1/epsilon_beta);
           Z_B=1;
        end
        
        sigma_H=sqrt((2*M_EM_max*(uebersetzung+1))*K_A*K_V*K_H_beta*K_H_alpha/(d1^2*b*uebersetzung))*Z_H*Z_E*Z_epsilon*Z_beta*Z_B;
        sigma_H(isnan(sigma_H)==1)=0;
        
        if sigma_H<=sigma_HP; 
          break; 
        end
        
        b=b+1;
        
        while b>1.1*d1;
            d1=d1+1;
        end
        
    end
    
    b1=b;
    
end