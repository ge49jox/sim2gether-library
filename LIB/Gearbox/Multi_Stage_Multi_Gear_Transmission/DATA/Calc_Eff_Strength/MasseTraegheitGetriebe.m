 function [m_GTR, J_red_GTR, wkg_gears,d_Rad,b_Rad] = MasseTraegheitGetriebe(i_Gaenge, n_Gaenge, M_EM_max, n_nenn_GTR, vzkonst)
%% �BERSICHT

%Erweiterung der Pesce-Getriebeberechnung um eine Wirkungsgradberechnung
%und mehr M�glichkeiten, verschiedene Getriebe abzubilden: Mehrg�ngig mit
%bis zu vier G�ngen und gemeinsamer letzter Stufe

%Berechnung der Breite und Durchmesser der verwendeten Zahnr�der f�r das
%zweistufige Getriebe, um daraus das Tr�gheitsmoment und die Masse bestimmen zu k�nnen

%% Vorgaben
%maximale �bersetzung mit einer Stufe
imax = vzkonst.imax_Stufe;
%% Berechnung der Hv und der Dimensionen, der Einzelmassen und -tr�gheiten

if n_Gaenge == 1
    if i_Gaenge(1) <= imax       %bis imax einstufig
        [d1,d2,b,z1,m] = festigkeit_G1_S1(M_EM_max,i_Gaenge(1),n_nenn_GTR,vzkonst);
        B(1,1) = d1;
        B(1,2) = d2;
        d_Rad = d2;
        b_Rad = b;
        Hv.G1 = calchv(i_Gaenge(1), z1, vzkonst);
        %Zahnradmasse
        m_ZR = Masse_Zahnr_1stg(d1,b,d2);
        %Geh�usemasse
        [V_i_1,V_a_1]= Volumen_Geh_1stg(b,d1,i_Gaenge(1),m);
        m_GH = Masse_Geh_1stg(V_i_1,V_a_1);
        %Tr�gheit
        J_red = calcJ_S1(b,d1,i_Gaenge(1));
        %save('.\mat-Files\Getriebedaten\Daten_1S1G','B');
    else                        %zweistufig
        i_Stufe_1 = 0.8*i_Gaenge(1)^(2/3);  %nach NWH S.266
        i_Stufe_2 = i_Gaenge(1)/i_Stufe_1;
        A(1,1) = i_Stufe_1;
        A(1,2) = i_Stufe_2;
        [d1,d2,d3,d4,b12,b34,z1,z3,m] = festigkeit_G1_S2(M_EM_max, i_Stufe_1, i_Stufe_2,n_nenn_GTR,vzkonst);
        B(1,1) = d1;
        B(1,2) = d2;
        B(1,3) = d3;
        B(1,4) = d4;
        d_Rad = d4;
        B(1,5) = b12;
        B(1,6) = b34;
        b_Rad = b34;
        i_Stufen = [i_Stufe_1, i_Stufe_2];
        z_Ritzel = [z1, z3];
        Hv.G1 = calchv(i_Stufen, z_Ritzel, vzkonst);    %das gibt gleich nen 1x2-Vektor
        %Zahnradmasse
        m_ZR = Masse_Zahnr_2stg(d1,d2,d3,d4,b12,b34);
        C(1,1) = m_ZR;
        %Geh�usemasse
        [V_i_2,V_a_2]=Volumen_Geh_2stg(b12,b34,d1,d3,i_Stufe_1,i_Stufe_2,m);
        m_GH = Masse_Geh_2stg(V_i_2,V_a_2);
        C(1,2) = m_GH;
        %Tr�gheit
        J_red = calcJ_S2(b12,b34,d1,d3,i_Stufe_1,i_Stufe_2);
        %save('.\mat-Files\Getriebedaten\Daten_2S1G','A','B','C');
    end
else
    if max(i_Gaenge) <= imax     %einstufig
        B = zeros(n_Gaenge,2);
        for j = 1:n_Gaenge
            [d1(j),d2(j),b(j),z1(j),m(j)] = festigkeit_G1_S1(M_EM_max,i_Gaenge(j),n_nenn_GTR,vzkonst);
            B(j,1) = d1(j);
            B(j,2) = d2(j);
            d_Rad = d2(1);
            b_Rad = b(1);
            Hv_Gang = calchv(i_Gaenge(j), z1(j), vzkonst);
            eval(['Hv.G' num2str(j) '=Hv_Gang;'])
            %Zahnradmasse pro Gang
            m_ZR_S1(j) = Masse_Zahnr_1stg(d1(j),b(j),d2(j));
            %Tr�gheit pro Gang: Tr�gheit ist gangabh�ngig, nur der
            %eingelegte Gang dreht mit, also keine Summierung wie in
            %fr�heren Versionen; stattdessen Vektor
            J_red_S1(j) = calcJ_S1(b(j),d1(j),i_Gaenge(j));
        end
        %Geh�usemasse
        %relevante Stufen: erste und letzte, plus Breiten b jedes Ganges
        [V_i_1,V_a_1]= Volumen_Geh_1stg_xG(b,d1,i_Gaenge,m,n_Gaenge);
        m_GH = Masse_Geh_1stg(V_i_1,V_a_1);
        m_ZR = sum(m_ZR_S1);
        J_red = J_red_S1; %ist Vektor
        %save('.\mat-Files\Getriebedaten\Daten_1S2G','B');
    else                                                                        %zweistufig
        %Problem: zweite Stufe muss gleich sein
         i_Stufe_1 = 0.8*i_Gaenge(1)^(2/3);  %die ist variabel
         i_Stufe_2_fix = i_Gaenge(1)/i_Stufe_1;  %die bleibt jetzt
         A = zeros(n_Gaenge,2); %Speichermatrix f�r die �bersetzungen zum anschauen nachher
         B = zeros(n_Gaenge,6);%SpM f�r die Durchmesser aller vier ZR und der zwei Stufenbreiten jedes Gangs
         C = zeros(n_Gaenge,3);%SpM mit den Massen der ersten Stufen in der ersten Spalte, der zweiten Stufe in der zweiten und dem Geh. in der 3.
        for j = 1:n_Gaenge
            i_Stufe_1 = i_Gaenge(j)/i_Stufe_2_fix; %die ist variabel
            i_Stufe_2 = i_Stufe_2_fix;
            A(j,1) = i_Stufe_1;
            A(j,2) = i_Stufe_2;
            [d1(j),d2(j),d3,d4,b12(j),b34,z1,z3,m] = festigkeit_G1_S2(M_EM_max, i_Stufe_1, i_Stufe_2,n_nenn_GTR,vzkonst);
            %d3, d4 bei Gang 2 und aufw�rts mit den Werten von Gang 1
            %�berschreiben
            if j >= 2
                d3 = B(1,3); d4 = B(1,4); b34 = B(1,6);
            end
            B(j,1) = d1(j);
            B(j,2) = d2(j);
            B(j,3) = d3;
            B(j,4) = d4;
            d_Rad = d4;
            B(j,5) = b12(j);
            B(j,6) = b34;
            b_Rad = b34;
            Hv_Stufe_1 = calchv(i_Stufe_1, z1, vzkonst);                         %Hv f�r die beiden Stufen
            Hv_Stufe_2 = calchv(i_Stufe_2, z3, vzkonst); 
            eval(['Hv.G' num2str(j) '=[Hv_Stufe_1, Hv_Stufe_2];']);             %Zuweisung zur Gangnummer
            %Masse der Zahnr�der pro Gang
            m_ZR_S1(j) = Masse_Zahnr_1stg(d1(j),b12(j),d2(j));  %Vektor mit den Massen der ersten Stufen jedes Ganges
            m_ZR_S2 = Masse_Zahnr_1stg(d3,b34,d4);     %kein Vektor; wird zwar jedes Mal neu berechnet, aber die �bergabewerte sind nur von i_Stufe_2 abh�ngig und deshalb konstant
            C(j,1) = m_ZR_S1(j);
            C(1,2) = m_ZR_S2;
            %Tr�gheiten der Zahnr�der pro Gang
            J_halbred_S1(j) = calcJ_S1(b12(j),d1(j),i_Stufe_1); %auf die Drehzahl des Rades 2 reduzierte Tr�gheit der ersten Stufen
            J_red_S1(j) = i_Stufe_2^2.*J_halbred_S1(j);      %auf die Abtriebsdrehzahl reduzierte Tr�gheiten der ersten Stufen, Vektor
            J_red_S2 = calcJ_S1(b34,d3,i_Stufe_2);          %auf die Abtriebsdrehzahl reduzierte Tr�gheit der zweiten Stufe, Skalar
        end
        %Geh�usemasse
        i_Stufe_1 = 0.8*i_Gaenge(1)^(2/3);  %i_Stufe_1 wieder zur�ck auf den ersten Gang setzen
        [V_i_2,V_a_2]=Volumen_Geh_2stg_xG(b12,b34,d1,d3,i_Stufe_1,i_Stufe_2,m,n_Gaenge);
        m_GH = Masse_Geh_2stg(V_i_2,V_a_2);
        C2(1,3) = m_GH;
        m_ZR = sum(m_ZR_S1)+m_ZR_S2; %Skalar
        J_red = J_red_S1 + J_red_S2;%Vektor
        %save('.\mat-Files\Getriebedaten\Daten_2S2G','A','B','C');
    end   
end

%% WKG berechnen und ausgeben
mu = vzkonst.mu;
wkg_gears = calcwkg(Hv, mu);
%mitlaufende G�nge ber�cksichtigen -> jeder maximale Gangwirkungsgrad wird
%um n_mitlaufendeG�nge*eta_leer verschlechtert
eta_leer = 0.99;    %Pesce/Literatur
wkg_gears = wkg_gears.*eta_leer^(n_Gaenge-1);
%Kein Getriebe vorhanden? Dann:
if all(i_Gaenge == 1)
    wkg_gears = 1;
end

%% Berechnung der Getriebemasse
m_GTR = m_ZR + m_GH;
%  if all(i_Gaenge == 1)
%      m_GTR = 0;
%  end

%% Berechnung des auf die Raddrehzahl reduzierten Tr�gheitsmoments
J_red_GTR = J_red;
if all(i_Gaenge == 1)
    J_red_GTR = 0;
end

end

 

