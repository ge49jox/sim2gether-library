function [d1,d2,b,z1,modul] = festigkeit_G1_S1(M_EM_max, i_Stufe, n_nenn, vzkonst)
%Programm zur Zahnraddimensionierung bei einem Getriebe mit einem Gang und
%einer Stufe

%% Vorgaben
%K-Faktor%
K=vzkonst.K;
%E-Modul%
E=vzkonst.E;
%Winkel%
alpha_n=vzkonst.alpha;  %gleich alpha_n
beta=vzkonst.beta;
%Sicherheit% 
S_H_min=vzkonst.S_H_min;
S_F_min=vzkonst.S_F_min;
%% Berechnung
%Startmodul%
m=3;
while 1 
    %Verzahnungsdaten%
    d1=Verzahnungsdaten_1STG(M_EM_max,m);
    %Festigkeitsrechnung%
    %Zahnflankentragf�higkeit%
    [b1,d1]=Zahnflankentragfaehigkeit_1(i_Stufe, S_H_min, d1, M_EM_max, n_nenn, K, E, m, alpha_n, beta);
    %Zahnfu�tragf�higkeit%
    [b2,d1]=Zahnfusstragfaehigkeit_1(i_Stufe, S_F_min, d1, M_EM_max, n_nenn, K, m, alpha_n, beta);
    %Maximum der Breiten%
    b=max(b1,b2);
    %Modulbedingung%
    if m>=b/12 
    break;
    end
    m=m+0.5;
end      
modul = m;
%Durchmesser Rad (=2)
d2 = d1*i_Stufe;
%Ritzelz�hnezahl (krummer Wert)
z1 = d1/m;

end