function[V_i_2,V_a_2]=Volumen_Geh_2stg_xG(b12,b34,d1,d3,i_Stufen_1,i_Stufe_2,m, n_Gaenge)

%% Variablenerkl�rung
%b12 Vektor, der alle Breiten der jeweils ersten Stufen der G�nge enth�lt; b34
%Skalar mit der Breite der universellen zweiten Stufe; d1 Vektor, der die Durchmesser aller
%Ritzel der G�nge enth�lt, d3 Skalar mit dem Durchmesser des Ritzels der universellen zweiten Stufe;
%i_Stufe_1 und i_Stufe_1_end �bersetzungen der ersten Stufen des ersten und letzten Ganges, m ist der Modul, der hier eine kleine Schwachstelle ist,
%da er dem Modul des dritten Ganges entspricht, wobei eigentlich f�r jede
%der ersten Stufen ein anderer Modul geschrieben werden sollte. Die zweite
%Stufe muss sich dann entweder den ersten oder den letzten Modul nehmen.
%Das vernachl�ssige ich jetzt, da es in den 10mm Luft untergeht.
%% Rauspicken der Gr��en der ersten und der letzten Stufe
d1 = d1(1);
%d1end = d1(end);
i_Stufe_1 = i_Stufen_1(1);
%i_Stufe_1_end = i_Stufen_1(end);

%% Berechnung
%relevante Durchmesser f�r die Randkreise des Getriebes: Rad der ersten Stufe des ersten
%Ganges (d2) und Raddurchmesser der univ. zweiten Stufe, d4 genannt;
d2=i_Stufe_1*d1;
%d2end = iGend*d1end;
d4 = d3*i_Stufe_2;

l=0.5*sqrt((d3+d4)^2-(d4-d2)^2);
gamma=asin((d4-d2)/(d3+d4));
haP = m;
d2_h=d2+2*haP+10;
d4_h=d4+2*haP+10;
nspace = n_Gaenge-1;             %Anzahl an L�cken zwischen den Zahnr�dern
b_h=sum(b12)+b34+nspace*30+3*10;     %30 statt 10mm wegen Schaltelementen, 10 zwischen den ersten und der zweiten Stufe (Quelle: Schaeffler-Synchronisierung)
L=0.5*(d3+d4+d2_h+d4_h);
x_w=0.007*L+4;

if x_w<8;
    x_w=8;
elseif x_w>50;
    x_w=50;
end

d2_s=d2_h+2*x_w;
d4_s=d4_h+2*x_w;
b_s=b_h+2*x_w;
V_i_2=(0.5*l*(d2_h+d4_h)+(pi-2*gamma)*(d2_h/2)^2+(pi+2*gamma)*(d4_h/2)^2)*b_h*10^-6;
V_a_2=(0.5*l*(d2_s+d4_s)+(pi-2*gamma)*(d2_s/2)^2+(pi+2*gamma)*(d4_s/2)^2)*b_s*10^-6;

end