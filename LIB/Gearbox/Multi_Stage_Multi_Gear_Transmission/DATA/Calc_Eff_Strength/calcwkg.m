function [wkg_gears] = calcwkg(hv_gears, mu )
%wkg_gears ist ein Skalar pro Gang

%convert to cell to be able to index properly
hvgears = struct2cell(hv_gears);    
num_gears = numel(hvgears);           
num_stages = numel(hvgears{1});
wkg_stufen = zeros(num_gears, num_stages);
wkg_gears = zeros(num_gears);

for i = 1:num_gears
    for j = 1:num_stages
        wkg_stufen(i,j) = 1-mu*hvgears{i}(j);  %hvgears{Gang}(Stufe)
    end
    wkg_gears = prod(wkg_stufen, 2);
end

end