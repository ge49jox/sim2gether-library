 function[V_i_1,V_a_1]=Volumen_Geh_1stg_xG(b,d1,i_Gaenge,m, n_Gaenge)

%% Variablenerkl�rung
%b/d1 Breiten/Durchmesser der Stufen des ersten und des letzten Ganges,  iG1 und iGend
%deren �bersetzungen, m1 und mend deren Moduln
%% Rauspicken der Gr��en der ersten und der letzten Stufe
d1 = d1(1);
%d1end = d1(end);
iG1 = i_Gaenge(1);
%iGend = i_Gaenge(end);
m1 = m(1);
%mend = m(end);

%% Berechnung bei verschiedenen Ritzelgr��en
% 
% %relevante Durchmesser f�r die Randkreise des Getriebes: Rad des ersten
% %Ganges (d2) und Ritzel (nicht Rad!) des letzten Ganges, also d1end
% %d1 ist immer Ritzel, d2 immer Rad
% d2=iG1*d1
% d2end = iGend*d1end
% 
% l=0.5*sqrt((d2end+d1end)^2-(d1end-d2)^2)
% gamma=asin((d1end-d2)/(d2end+d1end));
% haP1=m1;
% haPend = mend;
% d2_h=d2+2*haP1+10;
% d1end_h=d1end+2*haPend+10;
% nspace = n_Gaenge-1;             %Anzahl an L�cken zwischen den Zahnr�dern
% b_h=sum(b)+nspace*15+2*10;     %15 statt 10mm wegen Schaltelementen
% L=0.5*(d2end+d1end+d2_h+d1end_h);
% x_w=0.007*L+4;
% 
% if x_w<8;
%     x_w=8;
% elseif x_w>50;
%     x_w=50;
% end
% 
% d2_s=d2_h+2*x_w;
% d1end_s=d1end_h+2*x_w;
% b_s=b_h+2*x_w;
% V_i_2=(0.5*l*(d2_h+d1end_h)+(pi-2*gamma)*(d2_h/2)^2+(pi+2*gamma)*(d1end_h/2)^2)*b_h*10^-6;
% V_a_2=(0.5*l*(d2_s+d1end_s)+(pi-2*gamma)*(d2_s/2)^2+(pi+2*gamma)*(d1end_s/2)^2)*b_s*10^-6;

%% Berechnung wie einstufig (Orientieren am ersten Gang)
d2=d1*iG1;
l=0.5*sqrt((d1+d2)^2-(d2-d1)^2);
gamma=asin((d2-d1)/(d1+d2));
haP=m1;
d1_h=d1+2*haP+10;
d2_h=d2+2*haP+10;
nspace = n_Gaenge-1;             %Anzahl an L�cken zwischen den Zahnr�dern
b_h=sum(b)+nspace*30+2*10;     %30 statt 10mm wegen Schaltelementen (Quelle: Schaeffler-Synchronisierung)
L=0.5*(d1+d2+d1_h+d2_h);
x_w=0.007*L+4;

if x_w<8;
    x_w=8;
elseif x_w>50;
    x_w=50;
end

d1_s=d1_h+2*x_w;
d2_s=d2_h+2*x_w;
b_s=b_h+2*x_w;
V_i_1=(0.5*l*(d1_h+d2_h)+(pi-2*gamma)*(d1_h/2)^2+(pi+2*gamma)*(d2_h/2)^2)*b_h*10^-6;
V_a_1=(0.5*l*(d1_s+d2_s)+(pi-2*gamma)*(d1_s/2)^2+(pi+2*gamma)*(d2_s/2)^2)*b_s*10^-6;

end