function[V_i_1,V_a_1]=Volumen_Geh_1stg(b,d1,uebersetzung,m)

d2=d1*uebersetzung;
l=0.5*sqrt((d1+d2)^2-(d2-d1)^2);
gamma=asin((d2-d1)/(d1+d2));
haP=m;
d1_h=d1+2*haP+10;
d2_h=d2+2*haP+10;
b_h=b+2*10;
L=0.5*(d1+d2+d1_h+d2_h);
x_w=0.007*L+4;

if x_w<8;
    x_w=8;
elseif x_w>50;
    x_w=50;
end

d1_s=d1_h+2*x_w;
d2_s=d2_h+2*x_w;
b_s=b_h+2*x_w;
V_i_1=(0.5*l*(d1_h+d2_h)+(pi-2*gamma)*(d1_h/2)^2+(pi+2*gamma)*(d2_h/2)^2)*b_h*10^-6;%[dm^3]
V_a_1=(0.5*l*(d1_s+d2_s)+(pi-2*gamma)*(d1_s/2)^2+(pi+2*gamma)*(d2_s/2)^2)*b_s*10^-6;

end