function J_red = calcJ_S1(b,d1,i)

d2=d1*i;
rho_St=7.9/1000000; %[kg/mm^3]
J11=(pi*b*rho_St)/32*((d1/1000)^4) * (i^2);      % Rad 1 ist Antrieb von E-Maschine und Rad 2 ist Abtrieb
J12=(pi*b*rho_St)/32*((d2/1000)^4);
J_red=(J11+J12)*1000000;  %[kg*m^2]

end