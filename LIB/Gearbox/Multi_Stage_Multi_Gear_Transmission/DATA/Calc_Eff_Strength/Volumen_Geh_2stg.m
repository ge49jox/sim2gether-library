 function[V_i_2,V_a_2]=Volumen_Geh_2stg(b_1_2,b_3_4,d1,d3,iG1,iG2,m)

d4=iG2*d3;
d2=iG1*d1;
l=0.5*sqrt((d3+d4)^2-(d4-d2)^2);
gamma=asin((d4-d2)/(d3+d4));
haP=m;
d2_h=d2+2*haP+10;
d4_h=d4+2*haP+10;
b_h=b_1_2+b_3_4+3*10;
L=0.5*(d3+d4+d2_h+d4_h);
x_w=0.007*L+4;

if x_w<8;
    x_w=8;
elseif x_w>50;
    x_w=50;
end

d2_s=d2_h+2*x_w;
d4_s=d4_h+2*x_w;
b_s=b_h+2*x_w;
V_i_2=(0.5*l*(d2_h+d4_h)+(pi-2*gamma)*(d2_h/2)^2+(pi+2*gamma)*(d4_h/2)^2)*b_h*10^-6;
V_a_2=(0.5*l*(d2_s+d4_s)+(pi-2*gamma)*(d2_s/2)^2+(pi+2*gamma)*(d4_s/2)^2)*b_s*10^-6;

end