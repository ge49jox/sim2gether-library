function [Eff_Load, m_GTR, J_red_GTR, MaxEffGears,d_Rad,b_Rad]=MSMGT_Init (iGears, Max_M_Torque, n_nominal_Trans, vzkonst)

%Load Eff-Table
DATA=load ('Multi_Stage_Multi_Gear_Transmission\DATA\MSMGT_Eff_Load.mat')
Eff_Load=DATA.Eff_Load

%Amount of gears
nGears=length(iGears)

%Calculation
[m_GTR, J_red_GTR, MaxEffGears,d_Rad,b_Rad] = MasseTraegheitGetriebe(iGears, nGears, Max_M_Torque, n_nominal_Trans, vzkonst)

end
