$---------------------------------------------------------------------MDI_HEADER
[MDI_HEADER]
FILE_TYPE                =  'tir'
FILE_VERSION             =  2
FILE_FORMAT              =  'ASCII'
(COMMENTS)
{comment_string}
'Tire                       115 / 70 R 16 PROXIMA PX2'
'Manufacturer               MICHELIN'
'Nom. Section width (m)     0.115'
'Nom. aspect ratio (-)      70'
'Infl. pressure (Pa)        300000'
'Rim radius (m)             0.2032'
'Measurement ID'            J5J33100  NEUF'
'Test speed (m/s)           22.2'
'Road surface               Safety walk'
'Road condition             Dry'
'MICHELIN property              15-juil.-2010'
'Matricule                  E190373C4019'
'Tire ref XY                ALPIN2A_BRAKE_DRY_V80_2B4'
'TED Xpur                =  5.6844'
'TEG Xpur                =  5.6868'
'TED Xcouple             =  6.4003'
'TEG Xcouple             =  6.4008'
'TED Ypur                =  0.11'
'TEG Ypur                =  0.11'
'TED Ycouple             =  10.3338'
'TEG Ycouple             =  10.3302'
'TED Mzpur               =  2.21'
'TEG Mzpur               =  2.12'
'TED Mzcouple            =  88.6074'
'TEG Mzcouple            =  88.074'
'TED Mx                  =  0.14'
'TEG Mx                  =  0.03'
'TED My                  =  73.4821'
'TEG My                  =  73.5127'
$--------------------------------------------------------------------------units
[UNITS]
LENGTH                   =  'meter'
FORCE                    =  'newton'
ANGLE                    =  'radians'
MASS                     =  'kg'
TIME                     =  'second'
$--------------------------------------------------------------------------model
[MODEL]
PROPERTY_FILE_FORMAT     =  'MF_05'
USE_MODE                 =  14               $typarr(  1)        $Tyre use switch
FITTYP                   =  5                $typarr(  2)        $Magic Formula Version number
MFSAFE1                  =  -528             $typarr(  3)
MFSAFE2                  =  0                $typarr(  4)
MFSAFE3                  =  0                $typarr(  5)
VXLOW                    =  1                $typarr(  29)
LONGVL                   =  22.2             $typarr(  6)        $Measurement speed
TYRESIDE 		 = 'LEFT' 	     			 $Mounted side of tyre
$----------------------------------------------------------------------dimension
[DIMENSION]
UNLOADED_RADIUS          =  0.284            $typarr(  7)        $Free tyre radius
WIDTH                    =  0.115            $typarr(  8)        $Nominal section width of the tyre
RIM_RADIUS               =  0.2032           $typarr(  9)        $Nominal rim radius
RIM_WIDTH                =  0.0762           $typarr(  10)       $Rim width
ASPECT_RATIO             =  0.70          	$Nominal aspect ratio
$----------------------------------------------------------------------shape
[SHAPE]
{radial width}
1.0                         0
1.0                         0.2
1.0                         0.4
1.0                         0.5
1.0                         0.6
1.0                         0.7
1.0                         0.8
1.0                         0.85
1.0                         0.9
0.9                         1
$-----------------------------------------------------------------------vertical
[VERTICAL]
VERTICAL_STIFFNESS       =  170385.439       $typarr(  15)       $Tyre vertical stiffness
VERTICAL_DAMPING         =  500              $typarr(  16)       $Tyre vertical damping
BREFF                    =  3.4594           $typarr(  11)       $Low load stiffness e.r.r.
DREFF                    =  0.59354          $typarr(  12)       $Peak value of e.r.r.
FREFF                    =  0.084353         $typarr(  13)       $High load stiffness e.r.r.
FNOMIN                   =  1282.8           $typarr(  14)       $Nominal wheel load
$----------------------------------------------------------------long_slip_range
[LONG_SLIP_RANGE]
KPUMIN                   =  -0.35333         $typarr(  23)       $Minimum valid wheel slip
KPUMAX                   =  0.35333          $typarr(  24)       $Maximum valid wheel slip
$---------------------------------------------------------------slip_angle_range
[SLIP_ANGLE_RANGE]
ALPMIN                   =  -0.16            $typarr(  25)       $Minimum valid slip angle
ALPMAX                   =  0.16             $typarr(  26)       $Maximum valid slip angle
$---------------------------------------------------------inclination_slip_range
[INCLINATION_ANGLE_RANGE]
CAMMIN                   =  -0.09            $typarr(  27)       $Minimum valid camber angle
CAMMAX                   =  0.09             $typarr(  28)       $Maximum valid camber angle
$-----------------------------------------------------------vertical_force_range
[VERTICAL_FORCE_RANGE]
FZMIN                    =  349.34           $typarr(  21)       $Minimum allowed wheel load
FZMAX                    =  5000          	 $typarr(  22)       $Maximum allowed wheel load
$------------------------------------------------------------------------scaling
[SCALING_COEFFICIENTS]
LFZO                     =  1                $typarr(  31)       $Scale factor of nominal (rated) load
LCX                      =  1                $typarr(  32)       $Scale factor of Fx shape factor
LMUX                     =  1                $typarr(  33)       $Scale factor of Fx peak friction coefficient
LEX                      =  1                $typarr(  34)       $Scale factor of Fx curvature factor
LKX                      =  1                $typarr(  35)       $Scale factor of Fx slip stiffness
LHX                      =  1                $typarr(  36)       $Scale factor of Fx horizontal shift
LVX                      =  1                $typarr(  37)       $Scale factor of Fx vertical shift
LGAX                     =  1                $typarr(  58)       $Scale factor of camber for Fx
LCY                      =  1                $typarr(  38)       $Scale factor of Fy shape factor
LMUY                     =  1                $typarr(  39)       $Scale factor of Fy peak friction coefficient
LEY                      =  1.2                $typarr(  40)       $Scale factor of Fy curvature factor
LKY                      =  1                $typarr(  41)       $Scale factor of Fy cornering stiffness
LHY                      =  1                $typarr(  42)       $Scale factor of Fy horizontal shift
LVY                      =  1                $typarr(  43)       $Scale factor of Fy vertical shift
LGAY                     =  0.8                $typarr(  44)       $Scale factor of camber for Fy
LTR                      =  1                $typarr(  45)       $Scale factor of Peak of pneumatic trail
LRES                     =  1                $typarr(  46)       $Scale factor for offset of residual torque
LGAZ                     =  1                $typarr(  47)       $Scale factor of camber for Mz
LXAL                     =  1                $typarr(  48)       $Scale factor of alpha influence on Fx
LYKA                     =  0.58                $typarr(  49)       $Scale factor of alpha influence on Fx
LVYKA                    =  1                $typarr(  50)       $Scale factor of kappa induced Fy
LS                       =  1                $typarr(  51)       $Scale factor of Moment arm of Fx
LSGKP                    =  1                $typarr(  52)       $Scale factor of Relaxation length of Fx
LSGAL                    =  0.8                $typarr(  53)       $Scale factor of Relaxation length of Fy
LGYR                     =  1                $typarr(  54)       $Scale factor of gyroscopic torque
LMX                      =  1                $typarr(  55)       $Scale factor of overturning couple
LVMX                     =  1                $typarr(  57)       $Scale factor of Mx vertical shift
LMY                      =  1                $typarr(  56)       $Scale factor of rolling resistance torque
$-------------------------------------------------------------------longitudinal
[LONGITUDINAL_COEFFICIENTS]
PCX1                     =  1.95296          $typarr(  61)       $Shape factor Cfx for longitudinal force
PDX1                     =  -1.160516699     $typarr(  62)       $Longitudinal friction Mux at Fznom
PDX2                     =  0.129500759      $typarr(  63)       $Variation of friction Mux with load
PDX3                     =  0                $typarr(  60)       $Variation of friction Mux with camber
PEX1                     =  1.535528352      $typarr(  64)       $Longitudinal curvature Efx at Fznom
PEX2                     =  -0.064364432     $typarr(  65)       $Variation of curvature Efx with load
PEX3                     =  0.185397216      $typarr(  66)       $Variation of curvature Efx with load squared
PEX4                     =  -0.128881        $typarr(  67)       $Factor in curvature Efx while driving
PKX1                     =  8.96885895       $typarr(  68)       $Longitudinal slip stiffness Kfx/Fz at Fznom
PKX2                     =  4.578002952      $typarr(  69)       $Variation of slip stiffness Kfx/Fz with load
PKX3                     =  -0.048649845     $typarr(  70)       $Exponent in slip stiffness Kfx/Fz with load
PHX1                     =  0                $typarr(  71)       $Horizontal shift Shx at Fznom
PHX2                     =  0                $typarr(  72)       $Variation of shift Shx with load
PVX1                     =  0                $typarr(  73)       $Vertical shift Svx/Fz at Fznom
PVX2                     =  0                $typarr(  74)       $Variation of shift Svx/Fz with load
RBX1                     =  10.439           $typarr(  75)       $Slope factor for combined slip Fx reduction
RBX2                     =  9.30659          $typarr(  76)       $Variation of slope Fx reduction with kappa
RCX1                     =  1.27399          $typarr(  77)       $Shape factor for combined slip Fx reduction
REX1                     =  0                $typarr(  82)       $Curvature factor of combined Fx
REX2                     =  0                $typarr(  83)       $Curvature factor of combined Fx with load
RHX1                     =  0.00384835       $typarr(  78)       $Shift factor for combined slip Fx reduction
PTX1                     =  0.158509052      $typarr(  79)       $Relaxation length SigKap0/Fz at Fznom
PTX2                     =  0.080908275      $typarr(  80)       $Variation of SigKap0/Fz with load
PTX3                     =  0.048649845      $typarr(  81)       $Variation of SigKap0/Fz with exponent of load
$--------------------------------------------------------------------overturning
[OVERTURNING_COEFFICIENTS]
QSX1                     =  0.035745         $typarr(  86)
QSX2                     =  0.36694          $typarr(  87)
QSX3                     =  0.014612         $typarr(  88)
$------------------------------------------------------------------------lateral
[LATERAL_COEFFICIENTS]
PCY1                     =  1.1445           $typarr(  91)       $Shape factor Cfy for lateral forces
PDY1                     =  1.0054           $typarr(  92)       $Lateral friction Muy
PDY2                     =  -0.11454         $typarr(  93)       $Variation of friction Muy with load
PDY3                     =  0.1              $typarr(  94)       $Variation of friction Muy with squared camber
PEY1                     =  -0.07491         $typarr(  95)       $Lateral curvature Efy at Fznom
PEY2                     =  -0.92608         $typarr(  96)       $Variation of curvature Efy with load
PEY3                     =  0.36608          $typarr(  97)       $Zero order camber dependency of curvature Efy
PEY4                     =  4.5655           $typarr(  98)       $Variation of curvature Efy with camber
PKY1                     =  -27.1958         $typarr(  99)       $Maximum value of stiffness Kfy/Fznom
PKY2                     =  3.3814           $typarr(  100)      $Load at which Kfy reaches maximum value
PKY3                     =  1.3455           $typarr(  101)      $Variation of Kfy/Fznom with camber
PHY1                     =  0.00039294       $typarr(  102)      $Horizontal shift Shy at Fznom
PHY2                     =  -0.00069671      $typarr(  103)      $Variation of shift Shy with load
PHY3                     =  0.017505         $typarr(  104)      $Variation of shift Shy with camber
PVY1                     =  -0.033325        $typarr(  105)      $Vertical shift in Svy/Fz at Fznom
PVY2                     =  0.0058766        $typarr(  106)      $Variation of shift Svy/Fz with load
PVY3                     =  -0.37452         $typarr(  107)      $Variation of shift Svy/Fz with camber
PVY4                     =  0.028616         $typarr(  108)      $Variation of shift Svy/Fz with camber and load
RBY1                     =  20.1404          $typarr(  109)      $Slope factor for combined Fy reduction
RBY2                     =  -15.421          $typarr(  110)      $Variation of slope Fy reduction with alpha
RBY3                     =  -0.00279094      $typarr(  111)      $Shift term for alpha in slope Fy reduction
RCY1                     =  0.936548         $typarr(  112)      $Shape factor for combined Fy reduction
REY1                     =  0                $typarr(  122)      $Curvature factor of combined Fy
REY2                     =  0                $typarr(  123)      $Curvature factor of combined Fy with load
RHY1                     =  -1.46599E-09     $typarr(  113)      $Shift factor for combined Fy reduction
RHY2                     =  0                $typarr(  124)      $Shift factor for combined Fy reduction with load
RVY1                     =  22.9926          $typarr(  114)      $Kappa induced side force Svyk/Muy*Fz at Fznom
RVY2                     =  0.003500113      $typarr(  115)      $Variation of Svyk/Muy*Fz with load
RVY3                     =  0.228037         $typarr(  116)      $Variation of Svyk/Muy*Fz with camber
RVY4                     =  0.0396081        $typarr(  117)      $Variation of Svyk/Muy*Fz with alpha
RVY5                     =  0                $typarr(  118)      $Variation of Svyk/Muy*Fz with kappa
RVY6                     =  0                $typarr(  119)      $Variation of Svyk/Muy*Fz with atan(kappa)
PTY1                     =  3.2403           $typarr(  120)      $Peak value of relaxation length SigAlp0/R0
PTY2                     =  3.3814           $typarr(  121)      $Value of Fz/Fznom where SigAlp0 is extreme
$-------------------------------------------------------------rolling
[ROLLING_COEFFICIENTS]
QSY1                     =  0.0049             $typarr(  126)      $Rolling resistance torque coefficient
QSY2                     =  0                $typarr(  127)      $Rolling resistance torque depending on Fx
QSY3                     =  0.0016                $typarr(  128)      $Rolling resistance torque depending on speed
QSY4                     =  0                $typarr(  129)      $Rolling resistance torque depending on speed ^4
$-----------------------------------------------------------------------aligning
[ALIGNING_COEFFICIENTS]
QBZ1                     =  11.2064          $typarr(  131)      $Trail slope factor for trail Bpt at Fznom
QBZ2                     =  -1.3236          $typarr(  132)      $Variation of slope Bpt with load
QBZ3                     =  0.32639          $typarr(  133)      $Variation of slope Bpt with load squared
QBZ4                     =  -1.3721          $typarr(  134)      $Variation of slope Bpt with camber
QBZ5                     =  -1.4457          $typarr(  135)      $Variation of slope Bpt with absolute camber
QBZ9                     =  -3.7604          $typarr(  136)      $Slope factor Br of residual torque Mzr
QBZ10                    =  0                $typarr(  130)      $Slope factor Br of residual torque Mzr
QCZ1                     =  1.4674           $typarr(  137)      $Shape factor Cpt for pneumatic trail
QDZ1                     =  0.065283         $typarr(  138)      $Peak trail Dpt"
QDZ2                     =  -0.0055688       $typarr(  139)      $Variation of peak Dpt" with load
QDZ3                     =  -3.0732          $typarr(  140)      $Variation of peak Dpt" with camber
QDZ4                     =  -89.6177         $typarr(  141)      $Variation of peak Dpt" with camber squared
QDZ6                     =  -0.0081428       $typarr(  142)      $Peak residual torque Dmr"
QDZ7                     =  0.0055423        $typarr(  143)      $Variation of peak factor Dmr" with load
QDZ8                     =  -0.06489         $typarr(  144)      $Variation of peak factor Dmr" with camber
QDZ9                     =  0.014601         $typarr(  145)      $Variation of peak factor Dmr" with camber and load
QEZ1                     =  -0.018905        $typarr(  146)      $Trail curvature Ept at Fznom
QEZ2                     =  0.060454         $typarr(  147)      $Variation of curvature Ept with load
QEZ3                     =  -0.043326        $typarr(  148)      $Variation of curvature Ept with load squared
QEZ4                     =  18.8335          $typarr(  149)      $Variation of curvature Ept with sign of Alpha-t
QEZ5                     =  -115.1275        $typarr(  150)      $Variation of Ept with camber and sign Alpha-t
QHZ1                     =  0.014714         $typarr(  151)      $Trail horizontal shift Sht at Fznom
QHZ2                     =  -0.0091913       $typarr(  152)      $Variation of shift Sht with load
QHZ3                     =  0.43332          $typarr(  153)      $Variation of shift Sht with camber
QHZ4                     =  -0.56384         $typarr(  154)      $Variation of shift Sht with camber and load
SSZ1                     =  -9.6645E-11      $typarr(  155)      $Nominal value of s/R0: effect of Fx on Mz
SSZ2                     =  -4.64513E-10     $typarr(  156)      $Variation of distance s/R0 with Fy/Fznom
SSZ3                     =  7.08811E-09      $typarr(  157)      $Variation of distance s/R0 with camber
SSZ4                     =  6.42009E-09      $typarr(  158)      $Variation of distance s/R0 with load and camber
QTZ1                     =  0                $typarr(  159)      $Gyration torque constant
MBELT                    =  0                $typarr(  160)      $Belt mass of the wheel
