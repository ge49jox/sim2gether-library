%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Parameter Zelle (Panasonic NCR 18650PF)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [HV_Pack_RC2] = InitParameterPanasonicNCR18650PF()

%% load cell parameters
load ('Battery_Specs_Panasonic_NCR18650PF.mat');

%% assign cell parameters
% cell capacity
HV_Pack_RC2.CellPara.CapacityCell.v = Battery_Specs.ECMPara.CA;
%HV_Pack_RC2.CellPara.Capacity.vUnit = 'Ah';
%HV_Pack_RC2.CellPara.Capacity.vComment = 'Capacity of the Panasonic NCR18650PF cell';

%ECM
HV_Pack_RC2.CellPara.Capacity.x = Battery_Specs.ECMPara.Dyn.EIS.SOCs;
%HV_Pack_RC2.CellPara.Capacity.xUnit = '0_1';
HV_Pack_RC2.CellPara.CapacityC1.v = Battery_Specs.ECMPara.Dyn.EIS.C1;
%HV_Pack_RC2.CellPara.CapacityC1.vUnit = 'Ah';
HV_Pack_RC2.CellPara.CapacityC2.v = Battery_Specs.ECMPara.Dyn.EIS.C2;
%HV_Pack_RC2.CellPara.CapacityC2.vUnit = 'Ah';
HV_Pack_RC2.CellPara.CapacityR1.v = Battery_Specs.ECMPara.Dyn.EIS.R1;
%HV_Pack_RC2.CellPara.CapacityR1.vUnit = 'Ohm';
HV_Pack_RC2.CellPara.CapacityR2.v = Battery_Specs.ECMPara.Dyn.EIS.R2;
%HV_Pack_RC2.CellPara.CapacityR2.vUnit = 'Ohm';
HV_Pack_RC2.CellPara.CapacityRohm.v = Battery_Specs.ECMPara.Dyn.EIS.Rohm;
%HV_Pack_RC2.CellPara.CapacityRohm.vUnit = 'Ohm';

%OCV
HV_Pack_RC2.CellPara.OpenCircuitVoltageMap.v = Battery_Specs.ECMPara.OCV.ConstantC.U_OCV_C25;
%HV_Pack_RC2.CellPara.OpenCircuitVoltageMap.vUnit = 'V';
%HV_Pack_RC2.CellPara.OpenCircuitVoltageMap.vComment = 'for 298.15 K';
HV_Pack_RC2.CellPara.OpenCircuitVoltageMap.x = Battery_Specs.ECMPara.OCV.ConstantC.SOC;
end