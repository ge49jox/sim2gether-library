%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Parameter Zelle (Sanyo UR 18650E)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [HighVoltageBattery.CellPara.Capacity.v,HighVoltageBattery.CellPara.OpenCircuitVoltageMap.x,HighVoltageBattery.CellPara.OpenCircuitVoltageMap.v,]=InitParameterSanyoUR18650E ()

% Laden der Zellparameter fuer das Ersatzschaltkreismodell und der Alterung der Sanyo UR 18650E Zelle
load ('Battery_Cell_1\DATA\Battery_Specs_Sanyo_UR18650E.mat');

%HighVoltageBattery.CellPara.Capacity.Comment = 'Capacity of the simulated cell, NOT scaled to fit the simulated car!';
%HighVoltageBattery.CellPara.Capacity.vUnit = 'Ah';
HighVoltageBattery.CellPara.Capacity.v = Battery_Specs.NominalCapacity_Ah;

% HighVoltageBattery.CellPara.RatedVoltage.Comment = 'Rated battery voltage';
% HighVoltageBattery.CellPara.RatedVoltage.vUnit = 'V';
% HighVoltageBattery.CellPara.RatedVoltage.v = Battery_Specs.NominalVoltage_V;

%HighVoltageBattery.CellPara.OpenCircuitVoltageMap.Comment = 'Open circuit voltage of battery characteristic'; 
%HighVoltageBattery.CellPara.OpenCircuitVoltageMap.xComment = 'State of charge';
%HighVoltageBattery.CellPara.OpenCircuitVoltageMap.xUnit = '0_1';
%HighVoltageBattery.CellPara.OpenCircuitVoltageMap.vComment = 'Voltage';
%HighVoltageBattery.CellPara.OpenCircuitVoltageMap.vUnit = 'V';
HighVoltageBattery.CellPara.OpenCircuitVoltageMap.x = Battery_Specs.ECM.OCV.SOC;
HighVoltageBattery.CellPara.OpenCircuitVoltageMap.v = Battery_Specs.ECM.OCV.U;

%HighVoltageBattery.CellPara.ResistMap.Comment = 'Look-Up Table fuer Innenwiderstand der Zelle, eigene Vermessung';
%HighVoltageBattery.CellPara.ResistMap.xComment = 'State of charge';
%HighVoltageBattery.CellPara.ResistMap.xUnit = '0_1';
%HighVoltageBattery.CellPara.ResistMap.yComment = 'C-Rate';
%HighVoltageBattery.CellPara.ResistMap.yUnit = '1/h';
%HighVoltageBattery.CellPara.ResistMap.zComment = 'Temperatur';
%HighVoltageBattery.CellPara.ResistMap.zUnit = 'K';
%HighVoltageBattery.CellPara.ResistMap.vComment = 'Innenwiderstand';
%HighVoltageBattery.CellPara.ResistMap.vUnit = 'Ohm';
HighVoltageBattery.CellPara.ResistMap.x = Battery_Specs.ECM.SOC;
HighVoltageBattery.CellPara.ResistMap.y = Battery_Specs.ECM.C_rate;
HighVoltageBattery.CellPara.ResistMap.z = Battery_Specs.ECM.T;
HighVoltageBattery.CellPara.ResistMap_R_i_20ms.v = Battery_Specs.ECM.R_i_20ms;
HighVoltageBattery.CellPara.ResistMap_R_i_1s.v = Battery_Specs.ECM.R_i_1s;

%HighVoltageBattery.CellPara.CellMass.Comment = 'Masse der Zelle, eigene Vermessung';
%HighVoltageBattery.CellPara.CellMass.vUnit = 'kg';
%HighVoltageBattery.CellPara.CellMass.v = Battery_Specs.CellMass_Kg;

%HighVoltageBattery.CellPara.SpecificHeatCapacityCell.Comment  = 'Spezifische Waermekapazitaet der Sanyo-Zelle, eigene Vermessung';
%HighVoltageBattery.CellPara.SpecificHeatCapacityCell.vUnit = '[J/kgK]';
%HighVoltageBattery.CellPara.SpecificHeatCapacityCell.v = Battery_Specs.HeatCap_J_kgK;

end
