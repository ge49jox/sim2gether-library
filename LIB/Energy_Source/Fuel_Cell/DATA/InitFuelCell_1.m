function [eff, eta, Load, relStromdichte, Spannung]= InitFuelCell_1()


if strcmp(get_param(gcb,'ArtBSZ'),'Hydrogen')==1
    load('Fuel_Cell\DATA\Kennlinien_H2FC.mat')

elseif strcmp(get_param(gcb,'ArtBSZ'),'Direct menthanol')==1
    load('Fuel_Cell\DATA\Kennlinien_DMFC.mat')
    
else
    eff=0;
    eta=0;
    Load=0;
    relStromdichte=0;
    Spannung=0;
end

end