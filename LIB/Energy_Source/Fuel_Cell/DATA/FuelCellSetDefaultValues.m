function []= FuelCellSetDefaultValues()

if strcmp(get_param(gcb,'ArtBSZ'),'Hydrogen')==1
    %[eff, eta, Load, relStromdichte, Spannung]= InitFuelCell_1 (ArtBSZ)   
    set_param(gcb,'maxInhalt','4');
    set_param(gcb,'Flaeche','182');
    set_param(gcb,'maxStromdichte','0.5');
    set_param(gcb,'Nennspannung','0.687');
    set_param(gcb,'Nseriell','80');
    set_param(gcb,'etaNenn','0.417');
    set_param(gcb,'HuH2','119970000');
end
if strcmp(get_param(gcb,'ArtBSZ'),'Direct menthanol')==1
    %[eff, eta, Load, relStromdichte, Spannung]= InitFuelCell_1 (ArtBSZ)
    set_param(gcb,'maxInhalt','3.7');
    set_param(gcb,'Flaeche','190');
    set_param(gcb,'maxStromdichte','0.4');
    set_param(gcb,'Nennspannung','0.358');
    set_param(gcb,'Nseriell','150');
    set_param(gcb,'etaNenn','0.198');
    set_param(gcb,'HuH2','19900000');
end

end