function [P_Loss,M_Vektor_motgen,W_Vektor,Full_Load_Line_M,Full_Load_Line_W] = GetWSParaForMCU (current_path_mcu,select_method)

%% Add path to current directory
addpath(genpath('Motor_Control_Unit'));

%% Get custom input of the edit fields from the MCU mask-workspace
WS_Variables_MCU = get_param(current_path_mcu,'MaskWSVariables'); 
    
    for i_mcu= 1:length(WS_Variables_MCU)
        name = {WS_Variables_MCU(i_mcu).Name};
        value = WS_Variables_MCU(i_mcu).Value;
        struct_mcu.(name{1}) = value;
    end

%% Case P_EL_Map    
if strcmp(select_method,'P_EL_Map')
    
adress_block = struct_mcu.adress_block;
ref_full_load_m = struct_mcu.ref_full_load_m;
ref_full_load_w = struct_mcu.ref_full_load_w;
ref_m = struct_mcu.ref_m;
ref_om = struct_mcu.ref_om;
p_el = struct_mcu.p_el;

try % Get custom E-Motor mask-workspace variables
    name_model = bdroot;
    WS_Variables_Motor = get_param([name_model adress_block],'MaskWSVariables');
    
    for i_ews= 1:length(WS_Variables_Motor)
        name = {WS_Variables_Motor(i_ews).Name};
        value = WS_Variables_Motor(i_ews).Value;
        struct_motor.(name{1}) = value;
    end
    
%% Assign Data for Lookup-Tables
    
    OM_M_Pel = struct_motor.(p_el); % mapped Pel data
    W_Vektor = struct_motor.(ref_om); % Reference Omega-Vector for P-Loss Motor Mode
    M_Vektor_motgen = struct_motor.(ref_m); % Reference Torque-Vector for P-Loss Motor Mode
    OM_M_P_Mech = W_Vektor'.*M_Vektor_motgen; % calculate Pmech
    Full_Load_Line_W = struct_motor.(ref_full_load_w); % Reference Omega-Vector for Full Load Line
    Full_Load_Line_M = struct_motor.(ref_full_load_m); % Full Load Line Vector
    P_Loss = OM_M_Pel-OM_M_P_Mech; %calculate Plosses
    
catch
    warndlg('Error: Could not import Motor Data. All values will be set to 0')
    W_Vektor =0; % Reference Omega-Vector for P-Loss Motor Mode
    M_Vektor_motgen = 0; % Reference Torque-Vector for P-Loss Motor Mode
    Full_Load_Line_W = 0; % Reference Omega-Vector for Full Load Line
    Full_Load_Line_M = 0; % Full Load Line Vector
    P_Loss = 0; % assign Plosses
end
    
end

%% Case P_Loss 
if strcmp(select_method,'P_Loss')
adress_block = struct_mcu.adress_block;
ref_full_load_m = struct_mcu.ref_full_load_m;
ref_full_load_w = struct_mcu.ref_full_load_w;
ref_m = struct_mcu.ref_m;
ref_om = struct_mcu.ref_om;
p_losses = struct_mcu.p_losses;


try % Get custom E-Motor mask-workspace variables
    name_model = bdroot;
    WS_Variables_Motor = get_param([name_model adress_block],'MaskWSVariables');
    
    for i_ews= 1:length(WS_Variables_Motor)
        name = {WS_Variables_Motor(i_ews).Name};
        value = WS_Variables_Motor(i_ews).Value;
        struct_motor.(name{1}) = value;
    end
    
%% Assign Data for Lookup-Tables
    
    W_Vektor = struct_motor.(ref_om); % Reference Omega-Vector for P-Loss Motor Mode
    M_Vektor_motgen = struct_motor.(ref_m); % Reference Torque-Vector for P-Loss Motor Mode
    Full_Load_Line_W = struct_motor.(ref_full_load_w); % Reference Omega-Vector for Full Load Line
    Full_Load_Line_M = struct_motor.(ref_full_load_m); % Full Load Line Vector
    P_Loss = struct_motor.(p_losses); % assign Plosses
catch
    warndlg('Error: Could not import Motor Data. All values will be set to 0')
    W_Vektor =0; % Reference Omega-Vector for P-Loss Motor Mode
    M_Vektor_motgen = 0; % Reference Torque-Vector for P-Loss Motor Mode
    Full_Load_Line_W = 0; % Reference Omega-Vector for Full Load Line
    Full_Load_Line_M = 0; % Full Load Line Vector
    P_Loss = 0; % assign Plosses
end
    
end
end