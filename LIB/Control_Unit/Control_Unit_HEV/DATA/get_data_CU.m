function [ Dyn, Drive, Bat, DaE, CU, init, DP] = get_data_CU()
% function first runs "get_all_variables" to get all variables from masks that are defined in 
% the function. Then runs simulation_backwards to get the gear vector for the simulation. 
% If strategies are chosen that need the results of dynamic programming, 
% hev_main_sim will be called.

%% get variables for simulation
[Dyn, Drive, Bat, DaE, CU]=get_all_variables();
[RotSpd, Forces, Torques, Consumptions, Efficiency, gears_user, gearsCE, gearsEE, time, VehSpd, VehAcc]=simulation_backwards(Dyn, Drive, DaE);

init.RotSpd=RotSpd;
init.Forces=Forces;
init.Torques=Torques;
init.Consumptions=Consumptions;
init.Efficiency=Efficiency;
init.gears_user=gears_user;
init.gearsCE=gearsCE;
init.gearsEE=gearsEE;
init.time=time;
init.VehSpd=VehSpd;
init.VehAcc=VehAcc;

%% check if DP is needed. If not, variables that are needed in the control unit get entries to enable simulation

[ NewDP ] = checkifDPisrequired( CU, DaE, Bat, Drive, Dyn );
persistent DP_Old;

if NewDP ==1 && (strcmp(CU.strategy_string,'DP-S')==1 || strcmp(CU.strategy_string,'RB-A')==1)    
    [par, res, prb, fuel_l_100km, fuel_l_100km_CE, plane, av_U_VM, speed_border, P_border, av_P_border] =hev_main_sim(Dyn, Drive, Bat, DaE, CU, init);
    DP.par = par;
    DP.res = res;
    DP.prb = prb;
    DP.fuel_l_100km = fuel_l_100km;
    DP.fuel_l_100km_CE = fuel_l_100km_CE;
    DP.plane = plane;
    DP.av_U_VM = av_U_VM;
    DP.speed_border = speed_border;
    DP.P_border = P_border;
    DP.av_P_border = av_P_border;
    
    DP_Old = DP;
elseif NewDP ==0 && (strcmp(CU.strategy_string,'DP-S')==1 || strcmp(CU.strategy_string,'RB-A')==1)
    DP = DP_Old;
else 
    DP.plane.xg = [0 1; 0 1];
    DP.plane.yg = [0 0; 1 1];
    DP.plane.zg = [0 0; 0 0];
    DP.speed_border = [0 1];
    DP.P_border = [0 0];
    DP.res.U = ones(1,length(time));
end
end

