function [ NewDP ] = checkifDPisrequired( CU, DaE, Bat, Drive, Dyn )
%function checks if model parameter have changed and RB-A or DP-S strategy
%is chosen

persistent CU_Old;
persistent DaE_Old;
persistent Bat_Old;
persistent Drive_Old;
persistent Dyn_Old;


if isequaln(CU_Old,CU) && isequaln(DaE_Old,DaE) && isequaln(Bat_Old,Bat) && isequaln(Drive_Old,Drive) && isequaln(Dyn_Old,Dyn) && (strcmp(CU.strategy_string,'DP-S')==1 || strcmp(CU.strategy_string,'RB-A')==1) 
    NewDP=0;
else
    NewDP=1;
    
CU_Old = CU;
DaE_Old = DaE;
Bat_Old = Bat;
Drive_Old = Drive;
Dyn_Old = Dyn;
  
end

end

