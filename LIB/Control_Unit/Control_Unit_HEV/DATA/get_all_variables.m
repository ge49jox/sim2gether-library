function [Dyn, Drive, Bat, DaE, CU]=get_all_variables()
%function gets all parameters of the simulink model and save them in
%structs

name_model = bdroot;

%parameter control unit

adress_mask = '/Control Unit/Control Unit HEV';
CU=get_workspace_parameter(name_model, adress_mask);

%%%EXAMPLE%%%
%parameter dynamics

 adress_mask = '/Dynamics/Acceleration Calculation';
 Dyn.AC=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Dynamics/Slope Resistance';
 Dyn.SR=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Dynamics/Air Resistance';
 Dyn.AR=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Dynamics/Roll Resistance';
 Dyn.RR=get_workspace_parameter(name_model, adress_mask);

%parameter drivetrain

 adress_mask = '/Drivetrain/Wheel System Const R Const Coeff';
 Drive.WS=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Drivetrain/Axle Drive';
 Drive.AD=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Drivetrain/Gearbox HEV';
 Drive.GB=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Drivetrain/Clutch Simple';
 Drive.C=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Drivetrain/Electric Drive Analytic Efficiency PSM';
 Drive.EE=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Drivetrain/ICE scaling simple';
 Drive.ICE=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Drivetrain/Auxiliary Power';
 Drive.AUX=get_workspace_parameter(name_model, adress_mask);

%parameter battery

 adress_mask = '/Battery/HV Pack Easy';
 Bat.BAT_BEV=get_workspace_parameter(name_model, adress_mask);
% 
 adress_mask = '/Battery/HV Pack Easy/Battery Sanyo UR18650E';
 Bat.BAT_BEV.Sanyo=get_workspace_parameter(name_model, adress_mask);
% 
 adress_mask = '/Battery/Battery HEV';
 Bat.BAT_HEV=get_workspace_parameter(name_model, adress_mask);


%parameter driver and environment

 adress_mask = '/Driver and Environment/Driving Cycle';
 DaE.DC=get_workspace_parameter(name_model, adress_mask);
 adress_mask = '/Driver and Environment/Constant Environment';
 DaE.CE=get_workspace_parameter(name_model, adress_mask);


end

