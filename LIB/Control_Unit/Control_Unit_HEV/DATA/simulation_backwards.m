function [RotSpd, Forces, Torques, Consumptions, Efficiency, gears_user, gearsCE, gearsEE, time, VehSpd, VehAcc]=simulation_backwards(Dyn, Drive, DaE)


VehSpd= DaE.DC.init.speed;
VehAcc= gradient(VehSpd);
time= DaE.DC.init.time;
time_steps= length(VehSpd);
z_gears= length(Drive.GB.gears);
n_maxCE=max(Drive.ICE.init.ww_trans);
n_minCE=min(Drive.ICE.init.ww_trans);
n_maxEE=0.95*max(Drive.EE.OM_Vektor);
n_max_user=300;
n_min_user=150;
diff_Eff_CE=0.06; %minimal difference of efficiency before changing gear in %/100
diff_Eff_EE=0.03; 


%% Forces, torques and rotation speeds wheel

RotSpd.WheelsRotSpd= VehSpd/Dyn.AC.WheelRadius;

Inertia_EE= Drive.EE.J.*Drive.GB.ratios./Drive.AD.ratio;
Inertia_ICE= Drive.ICE.init.Inertia*Drive.GB.ratios./Drive.AD.ratio;

Forces.F_long= VehAcc*(Dyn.AC.VEH_Mass+(Drive.WS.J_FourWheels+Inertia_EE+Inertia_ICE)./Dyn.AC.WheelRadius^2);
Forces.F_air=  0.5*(VehSpd-DaE.CE.Long_Wind_Speed).^2*Dyn.AR.A_st*Dyn.AR.c_w*Dyn.AR.Roh_Air;
Forces.F_roll= Drive.WS.Wheel_Roll_Resistance*cos(DaE.CE.Ground_Slope)*Dyn.AC.VEH_Mass*Dyn.RR.Gravity;
Forces.F_slope= sin(DaE.CE.Ground_Slope)*Dyn.AC.VEH_Mass*Dyn.RR.Gravity;

Forces.F_wheel= Forces.F_long + Forces.F_roll + Forces.F_air + Forces.F_slope;
%sets force to zero while standstill
VehSpd_matrix=VehSpd.*ones(size(Forces.F_wheel));
Forces.F_wheel(VehSpd_matrix==0)=0;

Torques.wheel= Forces.F_wheel*Dyn.AC.WheelRadius; %axle drive output

% torque and rotation speed gearbox exit
RotSpd.gearbox_exit= RotSpd.WheelsRotSpd*Drive.AD.ratio;

Torques.gearbox_exit_acc=Torques.wheel./Drive.AD.ratio./Drive.AD.trq_eff;
Torques.gearbox_exit_brake=Torques.wheel./Drive.AD.ratio.*Drive.AD.trq_eff;
Torques.gearbox_exit= Torques.gearbox_exit_acc.*(Torques.wheel>0) + Torques.gearbox_exit_brake.*(Torques.wheel<0);

  
% torque and rotation speed gearbox entrance

RotSpd.gearbox_entrance=RotSpd.gearbox_exit*Drive.GB.ratios;        
   
Torques.gearbox_entrance_acc=Torques.gearbox_exit./Drive.GB.ratios./Drive.GB.trq_eff;
Torques.gearbox_entrance_brake=Torques.wheel./Drive.GB.ratios.*Drive.GB.trq_eff;
Torques.gearbox_entrance= Torques.gearbox_entrance_acc.*(Torques.wheel>0) + Torques.gearbox_entrance_brake.*(Torques.wheel<0);  

%% Power, consumption CE (CE only)

Consumptions.CE_kg_per_s=interpn(Drive.ICE.init.ww_trans,Drive.ICE.init.TT_trans,Drive.ICE.init.mm_f_dot,RotSpd.gearbox_entrance,Torques.gearbox_entrance);   
Consumptions.CE_W= Consumptions.CE_kg_per_s*Drive.ICE.init.LHV_Jpkg;
Efficiency.CE= (Torques.gearbox_entrance>0).* RotSpd.gearbox_entrance.*Torques.gearbox_entrance./Consumptions.CE_W;



%% Power, consumption EE only

Consumptions.EE_W=interp2(Drive.EE.M_Vektor_lang,Drive.EE.OM_Vektor,Drive.EE.om_M_Pel_addiert,Torques.gearbox_entrance,RotSpd.gearbox_entrance);   
Efficiency.EE= RotSpd.gearbox_entrance.*Torques.gearbox_entrance./Consumptions.EE_W;

%% get gears over cycle

%gears with highest efficiency in case of EE only with check for max torques
%of motor

gearsEE= zeros(length(VehSpd),1);
gear_temp=1;
for i= 1:length(VehSpd)
    if  1<gear_temp && gear_temp<Drive.GB.z
        if  Efficiency.EE(i,gear_temp)<Efficiency.EE(i,gear_temp+1)*(1-diff_Eff_EE) || RotSpd.gearbox_entrance(i,gear_temp)>n_maxEE
            gear_temp=gear_temp+1;
            gearsEE(i)=gear_temp;
        elseif Efficiency.EE(i,gear_temp)<Efficiency.EE(i,gear_temp-1)*(1-diff_Eff_EE)
            gear_temp=gear_temp-1;
            gearsEE(i)=gear_temp;
        else
        gearsEE(i)=gear_temp;
        end
    elseif gear_temp == 1 &&  Efficiency.EE(i,gear_temp)<Efficiency.EE(i,gear_temp+1)*(1-diff_Eff_EE)|| RotSpd.gearbox_entrance(i,gear_temp)>n_maxEE
            gear_temp=gear_temp+1;
            gearsEE(i)=gear_temp;
    elseif gear_temp == Drive.GB.z &&  Efficiency.EE(i,gear_temp)<Efficiency.EE(i,gear_temp-1)*(1-diff_Eff_EE)
            gear_temp=gear_temp-1;
            gearsEE(i)=gear_temp;
    else
        gearsEE(i)=gear_temp;
    end
    torque_demand= Torques.gearbox_entrance(i,gear_temp);
    Tm_max = interp1(Drive.EE.init.om_min,Drive.EE.init.m_min,RotSpd.gearbox_entrance(i,gear_temp),'linear*','extrap');

    while Torques.gearbox_entrance(i,gearsEE(i))>Tm_max && gearsEE(i)>1
        gearsEE(i)=gearsEE(i)-1;
    end
end

%gears with minimal MassFlow in case of CE only
MassFlow =Consumptions.CE_kg_per_s;
MassFlow(RotSpd.gearbox_entrance<n_minCE) = NaN;
gearsCE= zeros(length(VehSpd),1);
gear_temp=1;
for i= 1:length(VehSpd)
    if  1<gear_temp && gear_temp<Drive.GB.z
        if  MassFlow(i,gear_temp)>MassFlow(i,gear_temp+1)/(1-diff_Eff_CE) || RotSpd.gearbox_entrance(i,gear_temp)>n_maxCE
            gear_temp=gear_temp+1;
            gearsCE(i)=gear_temp;
        elseif MassFlow(i,gear_temp)>MassFlow(i,gear_temp-1)/(1-diff_Eff_CE)|| RotSpd.gearbox_entrance(i,gear_temp)<n_minCE
            gear_temp=gear_temp-1;
            gearsCE(i)=gear_temp;
        else
        gearsCE(i)=gear_temp;
        end
    elseif (gear_temp == 1 &&  (MassFlow(i,gear_temp)>MassFlow(i,gear_temp+1)/(1-diff_Eff_CE))|| RotSpd.gearbox_entrance(i,gear_temp)>n_maxCE)
            gear_temp=gear_temp+1;
            gearsCE(i)=gear_temp;
    elseif gear_temp == Drive.GB.z &&  (MassFlow(i,gear_temp)>MassFlow(i,gear_temp-1)/(1-diff_Eff_CE)|| RotSpd.gearbox_entrance(i,gear_temp)<n_minCE)
            gear_temp=gear_temp-1;
            gearsCE(i)=gear_temp;
        else
        gearsCE(i)=gear_temp;
    end
    torque_demand= Torques.gearbox_entrance(i,gear_temp);
    Te_max = interp1(Drive.ICE.init.w,Drive.ICE.init.T_max,RotSpd.gearbox_entrance(i,gear_temp));
    Te_max(isnan(Te_max)==1)=0;
    while Torques.gearbox_entrance(i,gearsCE(i))>Te_max && gearsCE(i)>1 && RotSpd.gearbox_entrance(i,gearsCE(i)-1)>n_minCE 
        gearsCE(i)=gearsCE(i)-1;
    end

end

%gears in range of highest efficiency potential
gear_temp=1;
gears_user=zeros(length(VehSpd),1);
for i= 1:length(VehSpd)
    if RotSpd.gearbox_entrance(i,gear_temp)>n_max_user && gear_temp< Drive.GB.z
        gear_temp=gear_temp+1;
        gears_user(i)=gear_temp;
    elseif RotSpd.gearbox_entrance(i,gear_temp)<n_min_user && gear_temp>1
        gear_temp=gear_temp-1;
        gears_user(i)=gear_temp;
    else
        gears_user(i)=gear_temp;
    end
  
    
end
%% Consumption of fuel mass for CE only (without electric motor)
consum=zeros(length(VehSpd),1);
for i= 1:length(VehSpd)
    consum(i)=Consumptions.CE_kg_per_s(i,gearsCE(i));
end
consum(isnan(consum))=0;
consum(Torques.wheel(:,1)<0)=0;
Consumptions.onlyCE=cumsum(consum);

Torques.onlyCE=zeros(length(VehSpd),1);
for i= 1:length(VehSpd)
    Torques.onlyCE(i)=Torques.gearbox_entrance(i,gearsCE(i));
end
Torques.onlyCE(Torques.wheel<0)=0;

RotSpd.Engine=zeros(length(VehSpd),1);
for i= 1:length(VehSpd)
    RotSpd.Engine(i)=RotSpd.gearbox_entrance(i,gearsCE(i));
end
end