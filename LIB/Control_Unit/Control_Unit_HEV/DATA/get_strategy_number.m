function [ strategy ] = get_strategy_number( strategy_string )
%function gets strategy string and returns the strategy number for the
%control unit chart

% 1: DP-S
% 2: RB-A
% 3: RB-U
% 4: RB-SOC
% 5: ECMS
% 6: EE only
% 7: CE only

switch strategy_string
                case 'DP-S'
                    strategy = 1;
                case 'RB-A'
                    strategy = 2;
                case 'RB-U'
                    strategy = 3;
                case 'RB-SOC'
                    strategy = 4;
                case 'ECMS'
                    strategy = 5;
                case 'EE only'
                    strategy = 6;
                case 'CE only'
                    strategy = 7;
end

