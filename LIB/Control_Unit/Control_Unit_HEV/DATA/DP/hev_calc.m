function [X C I out] = hev(inp,par)
%function [X C I out] = hev(inp,par)
%HEV Computes the resulting state-of-charge based on current state-
%   of-charge, inputs and drive cycle demand.
%   
%   [X C I out] = HEV(INP,PAR)
%
%   INP   = input structure
%   PAR   = user defined parameters
%
%   X     = resulting state-of-charge
%   C     = cost matrix
%   I     = infeasible matrix
%   out   = user defined output signals

%% VEHICLE
time=inp.W{5};

% Wheel speed (rad/s)
wv  = inp.W{1} ./ par.WheelRadius;
% Wheel acceleration (rad/s^2)
dwv = inp.W{2} ./ par.WheelRadius;
% Wheel torque (Nm)
Inertia_EE= par.Drive.EE.J.*par.gear_ratios./par.axle_ratio;
Inertia_ICE= par.Drive.ICE.init.Inertia*par.gear_ratios./par.axle_ratio;
VEH_Mass_tot=par.VEH_Mass+(par.Drive.WS.J_FourWheels+Inertia_EE+Inertia_ICE)./par.WheelRadius^2;
VEH_Mass_tot_EE= par.VEH_Mass+(par.Drive.WS.J_FourWheels+Inertia_EE)./par.WheelRadius^2;
F_long= inp.W{2} .*VEH_Mass_tot;
F_long_EE= inp.W{2} .*VEH_Mass_tot_EE;
F_air=  0.5*(inp.W{1}-par.Long_Wind_Speed).^2*par.A_st*par.c_w*par.Roh_Air;
F_roll= par.Wheel_Roll_Resistance*cos(par.Ground_Slope)*par.VEH_Mass*par.Gravity;
F_slope= sin(par.Ground_Slope)*par.VEH_Mass*par.Gravity;

F_wheel= (inp.W{1}>0).*(F_long + F_roll + F_air + F_slope);
F_wheel_EE= (inp.W{1}>0).*(F_long_EE + F_roll + F_air + F_slope);

Tv= F_wheel.*par.WheelRadius;
Tv_EE= F_wheel_EE.*par.WheelRadius;

% TRANSMISSION
% Crankshaft speed (rad/s)
wg_all  = (inp.W{3}>0) .* par.gear_ratios.*par.axle_ratio .* wv;
% Crankshaft acceleration (rad/s^2)
dwg = (inp.W{3}>0) .* par.gear_ratios(inp.W{3} + (inp.W{3}==0)).*par.axle_ratio .* dwv;
% Crankshaft torque (Nm)
Tg_all  = (inp.W{3}>0) .* (Tv>0)  .* Tv ./ par.gear_ratios./par.axle_ratio./ (par.trq_eff_axledrive .* par.trq_eff_gearbox)...
    + (inp.W{3}>0) .* (Tv<=0) .* Tv ./ par.gear_ratios./par.axle_ratio .* (par.trq_eff_axledrive .* par.trq_eff_gearbox);
Tg_all_EE  = (inp.W{3}>0) .* (Tv_EE>0)  .* Tv_EE ./ par.gear_ratios./par.axle_ratio./ (par.trq_eff_axledrive .* par.trq_eff_gearbox)...
    + (inp.W{3}>0) .* (Tv_EE<=0) .* Tv_EE ./ par.gear_ratios./par.axle_ratio .* (par.trq_eff_axledrive .* par.trq_eff_gearbox);


wg  = wg_all((inp.W{3} + (inp.W{3}==0)));
Tg  = Tg_all((inp.W{3} + (inp.W{3}==0)));
Tg_EE = Tg_all_EE((inp.W{3} + (inp.W{3}==0)));


%% TORQUE SPLIT

% Engine drag torque (Nm)
%Te0  = (-1)*interp1(par.we,par.T_drag,min(max(wg,par.we(1)),par.we(end)));
Te0 = 0;
% Electric motor drag torque (Nm)
Tm0  = dwg * 0;
% Total required torque (Nm)
Ttot = (wg>0) .*(Te0.*(inp.U{1}~=1) + Tm0 + Tg.*(inp.U{1}~=1) + Tg_EE.*(inp.U{1}==1)); %different torque for EE only beacause inertia of ICE is not relevant
% Torque provided by engine
Te  = (wg>0) .* (Ttot>0)  .* (1-inp.U{1}).*Ttot;
%Te  = (wg>130).*(Ttot>0)  .* (1-inp.U{1}).*Ttot;
Tb  = (Ttot<=0) .* (1-inp.U{1}).*Ttot;
% Torque provided by electric motor
Tm  = (wg>0) .*    inp.U{1} .*       Ttot;
%Tm  = inp.U{1} .* Ttot + (wg<130).*(Ttot>0)  .* (1-inp.U{1}).*Ttot;
inps = (Te>0).*(wg<100) + (Ttot<0).*(Tm>0);
U=inp.U{1};

%% ENGINE
%   
% fuel mass flow (either through efficiency or directly if available)

%e_th_w = interp1(par.w_eff,par.eta_eff,wg,'linear*','extrap');
%e_th = interp1(par.T_eff,e_th_w,Te,'linear*','extrap'); %use of interpn causes problems due to change of input size
e_th = interp2(par.T_eff,par.w_eff,par.eta_eff,Te,wg.*ones(size(Te))) + (wg==0);
e_th(isnan(e_th))=0.01;
m_dot_fuel = Te.*wg./e_th./par.LHV_Jpkg;
m_dot_fuel = interp2(par.T_mm,par.w_mm,par.mm_f_dot,Te,wg.*ones(size(Te)));
m_dot_fuel(isnan(m_dot_fuel))=1000;
m_dot_fuel= (Te>0).*m_dot_fuel;
 
% Maximum engine torque
Te_max = interp1(par.we,par.Te_max,wg);
Te_max(isnan(Te_max))=1;
% Fuel power consumption
Pe = m_dot_fuel .* par.LHV_Jpkg;
% Calculate infeasible
ine = (Te > Te_max);

 
%% E-MOTOR
wg=wg.*ones(size(Tm));

%efficient gear for electric motor only
if  par.eff_gear_choice==1 && inp.W{5}~=0
    Tg_e= Tg_all_EE(inp.W{4});
    wg_e=wg_all(inp.W{4}); 
    wg(inp.U{1}==1)=wg_e;
    Tm(inp.U{1}==1)=Tg_e+ Tm0;
    gear_mod=inp.W{3};
    if length(inp.U{1})==1 && inp.U{1}==1
        gear_mod=inp.W{4};
    end
else
    gear_mod=inp.W{3}; %can't be empty
end


% Calculate electric power consumption
Pm=interp2(par.T_m,par.wm,par.Pm,Tm,wg);
Eff_EE=Tm.*wg./Pm;
Eff_EE(Eff_EE==0)=0.1;
Pm=Tm.*wg./Eff_EE.*(Tg>0) + Tm.*wg.*Eff_EE.*(Tg<0);


% MOTOR
% motor maximum torque 
Tmmax   = 0.715*interp1(par.wm_lim,par.Tm_max,wg,'linear*','extrap'); 
% motor minimum torque 
Tmmin   = 0.96*interp1(par.wm_lim,par.Tm_min,wg,'linear*','extrap');
Tmmin   = (Te>0).*Tmmin + (Te<=0).*Tmmin.*par.CU.RekuFaktor;
% Summarize infeasible
inm = (Tm<0)  .* (Tm < Tmmin) + (Tm>=0) .* (Tm > Tmmax);


%% BATTERY

if par.battery == 'ETH'
    % state-of-charge list
    soc_list = [0 0.2 0.4 0.6 0.8 1];
    % discharging resistance (indexed by state-of-charge list)
    R_dis    = [1.75    0.60    0.40    0.30    0.30    0.30]; % ohm
    % charging resistance (indexed by state-of-charge list)
    R_chg    = [0.35    0.50    0.85    1.00    2.00    5.00]; % ohm
    % open circuit voltage (indexed by state-of-charge list)
    V_oc     = [230     240     245     250     255     257]; % volt

    % Battery efficiency
    % columbic efficiency (0.9 when charging)
    e = (Pm>0) + (Pm<=0) .* 0.9;
    % Battery internal resistance
    r = (Pm>0)  .* interp1(soc_list, R_dis, inp.X{1},'linear*','extrap')...
      + (Pm<=0) .* interp1(soc_list, R_chg, inp.X{1},'linear*','extrap');

    % Battery current limitations
    %   battery capacity            = 6 Ah 
    %   maximum discharging current = 100A
    %   maximum charging current    = 125A
    im = (Pm>0) .* 100 + (Pm<=0) .* 175;
    % Battery voltage
    v = interp1(soc_list, V_oc, inp.X{1},'linear*','extrap');
    % Battery current
    
    Ib  =   e .* (v-sqrt(v.^2 - 4.*r.*Pm))./(2.*r) + par.P_aux./v;
    % New battery state of charge
    X{1}  = - Ib / (1.1 * 3600) + inp.X{1};
    % Battery power consumption
    Pb =   Ib .* v;
    % Update infeasible 
    inb = (v.^2 < 4.*r.*Pm) + (abs(Ib)>im);
    
elseif par.battery == 'FTM'
    
    % Battery current
    OCV_cell = interp1(par.SOC_list_V,par.V_over_SOC,inp.X{1},'linear*','extrap');
    I_bat =  (-1).* Pm./(OCV_cell.*par.Cells_serial); %negative power causes charging (positive) in battery
    I_cell= I_bat./par.Cells_parallel;
    C_rate = I_cell./par.CellCapacity;

    % Battery internal resistance
    resistance= interp3(par.C_rate_list_R,par.SOC_list_R,par.CellTemp_list,par.ResistanceMap,C_rate,inp.X{1},par.CellTemp*ones(size(C_rate)));
    resistance(isnan(resistance)) = 0.045;

    % Battery current limitations
    %   maximum discharging current = 100A
    %   maximum charging current    = 125A
    im = (Pm>0) .* 100 + (Pm<=0) .* 125;
    % Battery voltage
    V_drop = I_cell.*resistance;
    V_cell = OCV_cell+V_drop;
    V_bat  = V_cell.*par.Cells_serial;
    % Battery current
    Ib  =   Pm./V_bat;
    % New battery state of charge
    X{1}  = I_cell ./ (par.CellCapacity * 3600) + inp.X{1};

    % Battery power consumption
    Pb =   Ib .* V_bat;
    % Update infeasible 
    inb =  (abs(Ib)>im);

else
    disp('choose battery model')
end


%% Set new state of charge to real values
X{1} = (conj(X{1})+X{1})/2;
Pb   = (conj(Pb)+Pb)/2;
Ib   = (conj(Ib)+Ib)/2;

%% COST
% Summarize infeasible matrix
I = (inps+inb+ine+inm~=0);
% Calculate cost matrix (fuel mass flow)
C{1}  = m_dot_fuel;


%% SIGNALS
%   store relevant signals in out
out.time = time;
out.Te = Te;
out.gear= inp.W{3};
out.gear_mod= gear_mod;
out.Eff_EE = Eff_EE;
out.e_th = e_th;
out.Tmotor = Tm;
out.Tb = Tb;
out.Tg = Tg;
out.Ttot = Ttot;
out.Tv = Tv(gear_mod + (gear_mod==0));
out.wg = wg;
out.wv = wv;
out.Ib = Ib;
out.Pb = Pb;
out.Pm = Pm;
out.Tema = Te_max;
out.m_dot_fuel = m_dot_fuel;
out.U=U;
out.F_long= F_long(gear_mod + (gear_mod==0));
out.F_air=  F_air;
out.F_roll= F_roll;
out.F_slope= F_slope;
out.F_wheel=F_wheel(gear_mod + (gear_mod==0));
out.VEH_Mass_tot=VEH_Mass_tot(gear_mod + (gear_mod==0));
out.VEH_Mass_tot_EE=VEH_Mass_tot_EE(gear_mod + (gear_mod==0));
out.v=v;




% REVISION HISTORY
% =========================================================================
% DATE      WHO                 WHAT
% -------------------------------------------------------------------------
% 





