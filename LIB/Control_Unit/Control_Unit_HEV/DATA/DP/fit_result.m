function [plane, av_P_border, av_U_VM, P_border, speed_border]=fit_result(res, init, CU)
%function approximates the plane and line for RB-A operational strategy

%% calculate power
P_demand=transpose(res.F_wheel).*init.VehSpd;
%% seperate EE only
U_EM_EE_only=res.U;
U_EM_red=res.U;
U_EM_EE_only(res.U~=1)=NaN;
U_EM_red(res.U==1)=NaN;
U_EM_red(P_demand<=0)=NaN; %Bremsvorgänge und Stand aussortieren
U_EM_red(0<res.U)=NaN; %LPD aussortieren
U_VM_EE_only=1-U_EM_EE_only;
U_VM_red=1-U_EM_red;
%% get average of P_ratio_red
av_U_VM=U_VM_red;
av_U_VM(isnan(av_U_VM)==1)=[]; %cut out NaN
av_U_VM=mean(av_U_VM); % get average

%% get border line

speed=init.VehSpd;
P=P_demand;

speed(isnan(U_VM_red)==1)=[];
P(isnan(U_VM_red)==1)=[];

[speed, idx] = sort(speed);
P=P(idx);

%Schrittweite
s=CU.step_v;
n=ceil(max(speed)/s);
index=1;
for k=1:n
    
    upper_limit=k*s;
    lower_limit=(k-1)*s;
    speed_temp=speed;
    speed_temp(speed > upper_limit)=[];
    speed_temp(speed < lower_limit)=[];
    P_temp=P;
    P_temp(speed > upper_limit)=[];
    P_temp(speed < lower_limit)=[];
    if isempty(speed_temp)==0
        [P_min,ind]=min(P_temp);
        border_line_P(index)=P_min;
        border_line_speed(index)=speed_temp(ind);
        index=index+1;
    end
end
av_P_border=mean(border_line_P);
p_line = polyfit(border_line_speed,border_line_P,CU.degree_line);
speed_border=0:0.1:max(speed);
P_border=polyval(p_line,speed_border);



%% fit plane
x = init.VehSpd;
y = P_demand/1000;
z = U_VM_red;
%z = P_engine_red;
p_plane = polyfitn([x,y],z,CU.degree_plane);

if exist('sympoly') == 2
  polyn2sympoly(p_plane);
end
if exist('sym') == 2
  polyn2sym(p_plane);
end

[plane.xg,plane.yg]=meshgrid(0:1:max(init.VehSpd)*1.3,min(P_border)/1000:1:max(P_demand/1000)*1.1);
plane.zg = polyvaln(p_plane,[plane.xg(:),plane.yg(:)]);
plane.zg=reshape(plane.zg,size(plane.xg));
plane.zg(plane.zg>2)=2;

%% Evaluate on a grid and plot:
if CU.show_plots == 1
    figure()
    hold on
    view(3);
    surf(plane.xg,plane.yg,plane.zg)
    %colorbar
    plot3(x,y,z,'r*')
    xlabel('{\it v} in m/s')
    ylabel('{\it P_{gefordert}} in kW')
    zlabel('{\it u_{VM}} in -')
    grid on
    zlim([0 max(z)])
    %legend('Ausgleichsebene','u_{VM}')
    %hcb=colorbar
    %colorTitleHandle = get(hcb,'Title');
    %titleString = '{\it u_{VM Ebene}} in -';
    %set(colorTitleHandle ,'String',titleString);
    hold off
    
    figure
    hold on
    view(3);
    plot3(init.VehSpd,P_demand/1000,U_VM_EE_only,'*','Color',[0.44 0.44 0.44])
    plot3(init.VehSpd,P_demand/1000,U_VM_red,'*','Color',[0 0.447 0.741])
    plot(speed_border,P_border/1000,'red','LineWidth',2)
    xlabel('{\it v} in m/s')
    ylabel('{\it P_{gefordert}} in kW')
    zlabel('{\it u_{VM}} in -')
    grid on
end
end
