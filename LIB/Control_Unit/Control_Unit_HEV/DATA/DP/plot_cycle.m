function []= plot_cycle(res, init)
figure()
fig1= subplot(3,3,3);
grid on
hold on
plot(res.Te)
plot(init.Torques.onlyCE,'green')
title('T Combustion Engine')
legend('Torque engine DP','Torque engine CE only')
ylabel('torque [Nm]')

fig2= subplot(3,3,2);
grid on
hold on
plot(res.Tmotor)
title('T Electric Motor')
ylabel('torque [Nm]')

fig3= subplot(3,3,1);
grid on
hold on
plot(init.VehSpd)
title('Speed')
ylabel('Speed [m/s]')

fig4= subplot(3,3,4);
grid on
hold on
plot(res.wg)
title('rot speed')
ylabel('rotation speed [rad/s]')

fig5= subplot(3,3,5);
grid on
hold on
plot(res.Ib)
title('battery current')
ylabel('current [A]')


fig6= subplot(3,3,6);
grid on
hold on
plot(res.Pb/1000)
title('battery power')
ylabel('power [kW]')

fig7= subplot(3,3,7);
grid on
hold on
plot(res.gear,'blue')
plot(res.gear_mod,'red')
legend('gear original','gear mod')
title('gears')
ylabel('[-]')
ylim([0 max(res.gear)+2])

fig8= subplot(3,3,8);
grid on
hold on
plot(res.X{1, 1})
title('SOC')
ylabel('[-]')


fig9= subplot(3,3,9);
grid on
hold on
plot(cumsum(res.m_dot_fuel))% only valid for step size 1s
plot(init.Consumptions.onlyCE,'green')
legend('consumption DP','consumption CE only')
title('cost in fuel mass')
ylabel('mass [kg]')

linkaxes([fig1,fig2,fig3,fig4,fig5,fig6,fig7,fig8,fig9],'x')
fig1.XLim = [0, length(init.VehSpd)+10];
end