function [fuel_l_100km, fuel_l_100km_CE]=get_consumption(res, init, Drive)
%calculate fuel consumption of HEV in DP compared and consumption of a normal vehicle with ICE 
fuel_mass=trapz(res.m_dot_fuel); %in kg
fuel_mass_CE=init.Consumptions.onlyCE(end); %in kg
distance = trapz(init.VehSpd);
fuel_m3= fuel_mass/Drive.ICE.init.Density_kgpm3;
fuel_l= fuel_m3*1000;
fuel_l_100km=fuel_l/distance*100*1000;

fuel_m3_CE= fuel_mass_CE/Drive.ICE.init.Density_kgpm3;
fuel_l_CE= fuel_m3_CE*1000;
fuel_l_100km_CE=fuel_l_CE/distance*100*1000;

end