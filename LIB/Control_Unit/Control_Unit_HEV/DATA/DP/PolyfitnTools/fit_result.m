x = VehSpd;
y = P_demand;
z = P_ratio;
p = polyfitn([x,y],z,3);

if exist('sympoly') == 2
  polyn2sympoly(p)
end
if exist('sym') == 2
  polyn2sym(p)
end

% Evaluate on a grid and plot:
[xg,yg]=meshgrid(0:1:40,0:1000:45000);
zg = polyvaln(p,[xg(:),yg(:)]);
surf(xg,yg,reshape(zg,size(xg)))
hold on
plot3(x,y,z,'ro')
hold off