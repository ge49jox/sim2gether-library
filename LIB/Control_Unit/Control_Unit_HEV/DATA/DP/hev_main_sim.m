function [par, res, prb, fuel_l_100km, fuel_l_100km_CE, plane, av_U_VM, speed_border, P_border, av_P_border] =...
    hev_main_sim(Dyn, Drive, Bat, DaE, CU, init)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% settings for optimization
% create grid
clear grd
grd.Nx{1}    = CU.Nx; 
grd.Xn{1}.hi = CU.X_hi; 
grd.Xn{1}.lo = CU.X_lo;

grd.Nu{1}    = CU.Nu; %number has to end with 1 to ensure list of input U contains U=0
grd.Un{1}.hi = CU.Un_hi; 
grd.Un{1}.lo = CU.Un_lo;	% Att: Lower bound may vary with engine size.

% set initial state
grd.X0{1} = Bat.BAT_HEV.init_SOC;

% final state constraints
grd.XN{1}.hi = CU.XN_hi;
grd.XN{1}.lo = CU.XN_lo;

% define problem
clear prb
prb.W{1} = init.VehSpd;
prb.W{2} = init.VehAcc; 
prb.W{3} = init.gearsCE;
prb.W{4} = init.gearsEE;
prb.W{5} = init.time;
prb.Ts = 1;
prb.N  = (length(init.VehSpd)-1)*1/prb.Ts + 1;


% set parameters (therefore no need to change them in hev.m)
par.WheelRadius         = Dyn.AC.WheelRadius;
par.VEH_Mass            = Dyn.AC.VEH_Mass;
par.Long_Wind_Speed     = DaE.CE.Long_Wind_Speed;
par.A_st                = Dyn.AR.A_st;
par.c_w                 = Dyn.AR.c_w;
par.Roh_Air             = Dyn.AR.Roh_Air;
par.Wheel_Roll_Resistance= Drive.WS.Wheel_Roll_Resistance;
par.Ground_Slope        = DaE.CE.Ground_Slope;
par.Gravity             = Dyn.RR.Gravity;
par.gear_ratios         = Drive.GB.ratios;
par.axle_ratio          = Drive.AD.ratio;
par.trq_eff_axledrive   = Drive.AD.trq_eff;
par.trq_eff_gearbox     = Drive.GB.trq_eff;
par.we                  = Drive.ICE.init.w;              %w engine
par.T_drag              = Drive.ICE.init.T_drag;
par.T_mm                = Drive.ICE.init.TT_trans;
par.w_mm                = Drive.ICE.init.ww_trans;
par.mm_f_dot            = Drive.ICE.init.mm_f_dot;
par.LHV_Jpkg            = Drive.ICE.init.LHV_Jpkg;
par.Te_max              = Drive.ICE.init.T_max;
par.T_eff               = Drive.ICE.init.EffGrid.ygs_Nm_vektor;
par.w_eff               = Drive.ICE.init.EffGrid.xgs_radps_vektor;
par.eta_eff             = Drive.ICE.init.EffGrid.zgs_eff;
par.wm                  = Drive.EE.OM_Vektor;
par.T_m                 = Drive.EE.M_Vektor_lang;
par.wm_lim              = Drive.EE.init.omega_Grenz;
par.Tm_max              = Drive.EE.init.m_Grenz;
par.Tm_min              = Drive.EE.init.m_Grenz_neg;
par.Pm                  = Drive.EE.om_M_Pel_addiert;
par.P_aux               = Drive.AUX.AuxiliaryPower;  

par.BEV_V_over_SOC          = Bat.BAT_BEV.Sanyo.HighVoltageBattery.CellPara.OpenCircuitVoltageMap.v;
par.BEV_SOC_list_V          = Bat.BAT_BEV.Sanyo.HighVoltageBattery.CellPara.OpenCircuitVoltageMap.x;
par.BEV_SOC_list_R          = Bat.BAT_BEV.Sanyo.HighVoltageBattery.CellPara.ResistMap.x;
par.BEV_C_rate_list_R       = Bat.BAT_BEV.Sanyo.HighVoltageBattery.CellPara.ResistMap.y;
par.BEV_CellTemp_list       = Bat.BAT_BEV.Sanyo.HighVoltageBattery.CellPara.ResistMap.z;  
par.BEV_CellTemp            = Bat.BAT_BEV.CellTemp;
par.BEV_ResistanceMap       = Bat.BAT_BEV.Sanyo.ResistanceMap;
par.BEV_Cells_serial        = Bat.BAT_BEV.Cells_Serial;
par.BEV_Cells_parallel      = Bat.BAT_BEV.Cells_Parallel;
par.BEV_CellCapacity        = Bat.BAT_BEV.Sanyo.HighVoltageBattery.CellPara.Capacity.v;

par.HEV_R_chg               = Bat.BAT_HEV.R_chg;
par.HEV_R_dis               = Bat.BAT_HEV.R_dis;
par.HEV_V_oc                = Bat.BAT_HEV.V_oc;
par.HEV_capacity            = Bat.BAT_HEV.capacity;
par.HEV_colombic_eff_charge = Bat.BAT_HEV.columbic_eff_charge;
par.HEV_colombic_eff_discharge = Bat.BAT_HEV.columbic_eff_discharge;
par.HEV_init_SOC            = Bat.BAT_HEV.init_SOC;
par.HEV_max_I_charge        = Bat.BAT_HEV.max_I_charge;
par.HEV_max_I_discharge     = Bat.BAT_HEV.max_I_discharge;
par.HEV_soc_list            = Bat.BAT_HEV.soc_list;

par.Bat=Bat;
par.Drive=Drive;
par.Dyn=Dyn;
par.CU=CU;
par.DaE=DaE;

% set options
par.eff_gear_choice = CU.eff_gear_choice;
% par.battery = 'FTM';
par.battery = 'ETH';
%no need to change any of the following
options = dpm();
options.MyInf = 1000;
options.BoundaryMethod = 'Line'; % also possible: 'none' or 'LevelSet';
if strcmp(options.BoundaryMethod,'Line') 
    %these options are only needed if 'Line' is used
    options.Iter = 5;
    options.Tol = 1e-8;
    options.FixedGrid = 0;
    options.Waitbar = 'on';
end
[res dyn] = dpm(@hev_calc,par,grd,prb,options);

%% call skripts for plots and calculations
[fuel_l_100km, fuel_l_100km_CE]=get_consumption(res, init, Drive);
if CU.show_plots == 1
plot_cycle(res, init);
engine_plot(Drive, res, par);
end
[plane, av_P_border, av_U_VM, P_border, speed_border]=fit_result(res, init, CU);

end

