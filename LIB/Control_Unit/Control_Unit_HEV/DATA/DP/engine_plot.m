function []=engine_plot(Drive, res, par)
%%CE
figure()
hold on
grid on
xlabel('{\it \Omega} in rad/s');
ylabel('{\it M} in Nm');
contourf(Drive.ICE.init.EffGrid.xgs_radps,Drive.ICE.init.EffGrid.ygs_Nm,Drive.ICE.init.EffGrid.zgs_eff,'ShowText','on');
plot(Drive.ICE.init.w,Drive.ICE.init.M_be_min,'blue', 'linewidth', 2);
plot(Drive.ICE.init.w,Drive.ICE.init.T_max,'black', 'linewidth', 2);

driveCE=res.Te;
driveCE(res.U~=0)=NaN;
driveLPI=res.Te;
driveLPI(res.U>=0)=NaN;
driveLPD=res.Te;
driveLPD(res.U<=0)=NaN;
plot(res.wg,driveCE,'bo');
plot(res.wg,driveLPI,'go');
plot(res.wg,driveLPD,'yo');
legend('Effizienz','M_{Be min}','M_{max}','nur VM','Lastpunktanhebung','Lastpunktabsenkung')
xlim([min(par.we) max(par.we)])
ylim([min(par.T_eff) max(par.T_eff)])

clearvars driveCE driveLPI driveLPD 
%% EE(Horlbeck)
gcf = figure( ...
    'PaperUnits','centimeters', ...
    'Units','centimeters', ...
    'Position',[ ...
        Drive.EE.init.Plot_figure_x, ...
        Drive.EE.init.Plot_figure_y, ...
        Drive.EE.init.Plot_figure_Breite, ...
        Drive.EE.init.Plot_figure_Hoehe ...
    ], ...
    'Resize',Drive.EE.init.Plot_Resize_String ...
);

gca = axes( ...
    'XLim',[0, 1250], ...
    'YLim',[0, Drive.EE.init.Plot_y_max] ...
);


hold on;
grid on;
set(gca,'layer',Drive.EE.init.Plot_Layer_String);
box on;

%title('Wirkungsgrad-Kennfeld');
xlabel('{\it \Omega} in rad/s');
ylabel('{\it M} in Nm');




[C,h] = contourf(Drive.EE.init.OM, Drive.EE.init.M, ...
    Drive.EE.init.om_M_eta,Drive.EE.init.Plot_Contour_Levels_eta);
% "(1/1000)", damit P_el in kW angezeigt wird.
drive=res.Tmotor;
drive(drive<=0)=NaN;
drive_onlyEE=drive;
drive_onlyEE(res.U~=1)=NaN;
lpd=drive;
lpd(res.U==1)=NaN;
lpd(res.U<=0)=NaN;

brake=res.Tmotor;
brake(brake>=0)=NaN;
recu=brake;
recu(res.U<0)=NaN;
lpi=brake;
lpi(res.U>=0)=NaN;

d=plot(res.wg,drive_onlyEE,'r+');               
r=plot(res.wg,-recu,'go');
li=plot(res.wg,-lpi,'^','Color',[0 0.447 0.741]);
ld=plot(res.wg,lpd,'v','Color',[0.44 0.44 0.44]);
clabel(C,h,Drive.EE.init.Plot_Label_Vektor_eta);
%clabel(C,'manual'); % manuelles Setzen von Zahlenwerten;
% clabel(C,h,'manual'); % manuelles Setzen von Zahlenwerten; zus�tzlich
    % werden die Labels rotiert.


h2= plot(Drive.EE.init.om,Drive.EE.init.m, 'red', 'linewidth', 2);
h4=plot(Drive.EE.init.om_min,Drive.EE.init.m_min, 'black', 'linewidth', 2);




% Fahrzyklus-Kurve:
%h1 = plot( ...
%        Simulation_Ergebnis.signals.values(:,1), ... % W
%        Simulation_Ergebnis.signals.values(:,2), ... % M
%        'ro', ...
%        'LineWidth',Plot_Fahrzyklus_LineWidth ...
%);

hcb = colorbar(Drive.EE.init.Plot_Colorbar_Position_String);
colorTitleHandle = get(hcb,'Title');
titleString = ('Wirkungsgrad \eta');
set(colorTitleHandle ,'String',titleString);



%colormap jet

legend([h2 h4 d r li ld],{'motorische �berlast-Grenzkurve','motorische Grenzkurve','elektrisches Fahren','Rekuperation','Lastpunktanhebung','Lastpunktabsenkung'});
             
end