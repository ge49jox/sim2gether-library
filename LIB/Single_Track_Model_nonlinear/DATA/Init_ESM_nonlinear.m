function [par_TIR, par_VEH] = Init_ESM_nonlinear (PIOF, PPX1F, PPX2F ,PPX3F, PPX4F, PPY1F , PPY2F ,PPY3F ,PPY4F , QPZ1F, PIOR, PPX1R, PPX2R ,PPX3R, PPX4R, PPY1R , PPY2R ,PPY3R ,PPY4R , QPZ1R, SelectedTireFront, SelectedTireRear,RohAir,VEH_m,VEH_xSP,VEH_g,VEH_Iwf,VEH_Iwr,VEH_cz,VEH_cx,VEH_cy,VEH_xDP,VEH_yDP,VEH_Iz,VEH_l,VEH_Afront,LoadindexF,LoadindexR)



%% TIRE
[par_TIR]=GetTireParameters (SelectedTireFront, SelectedTireRear,LoadindexF,LoadindexR)
[par_TIR]=SetPressureParameters(PIOF, PPX1F, PPX2F ,PPX3F, PPX4F, PPY1F , PPY2F ,PPY3F ,PPY4F , QPZ1F, PIOR, PPX1R, PPX2R ,PPX3R, PPX4R, PPY1R , PPY2R ,PPY3R ,PPY4R , QPZ1R, par_TIR)

%% VEHICLE
[par_VEH]=Calc_ParaVeh_ESM_nonlinear(RohAir,VEH_m,VEH_xSP,VEH_g,VEH_Iwf,VEH_Iwr,VEH_cz,VEH_cx,VEH_cy,VEH_xDP,VEH_yDP,VEH_Iz,VEH_l,VEH_Afront)


end 