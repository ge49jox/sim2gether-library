function [breakLinkAction, links] = checkBlockLinks(modelName, blockNames)
    % Initialization
    links = {};
    breakLinkAction = false;
    % Check if block is in active link
    links = find_system(modelName, 'LookUnderMasks','all', 'FollowLinks','on', 'LinkStatus','resolved');
    % Ask to break links
    for iBlock=1:length(blockNames)
        if strcmp(modelName, blockNames{iBlock}), continue, end
        linkStatus = get_param(blockNames{iBlock},'LinkStatus');
        if strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved')
            button = questdlg(sprintf('Minimum one block is located in an active library link.\n\nDisable links for model?'),...
                              'Active Links','Yes','No','No');
            switch button
                case 'Yes'
                    breakLinkAction = true;
                    break
                case 'No'
                    breakLinkAction = false;
                    break
                case ''
                    return
            end
        end
    end    
end

