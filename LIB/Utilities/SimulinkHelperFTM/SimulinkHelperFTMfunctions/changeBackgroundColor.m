function [fail, breakLinkAction] = changeBackgroundColor(system, blockType, backgroundColor, breakLinkAction)
    %% Initilization
    if nargin<4 || isempty(breakLinkAction), breakLinkAction = false; end
    fail = false;
    blockNames = {};
    
    %% Search for Blocks
    blocksFound = find_system(system, 'LookUnderMasks','all', 'FollowLinks','on', 'BlockType',blockType);
    if ~isempty(blocksFound)
        for iBlock=1:length(blocksFound)
            blockNames{length(blockNames)+1} = blocksFound{iBlock};
        end
    end
    if isempty(blockNames), return, end
    
    %% Check for links
    [breakLinkAction, links] = checkBlockLinks(system, blockNames);
    
    %% Change color
    for iBlock=1:length(blockNames)

        % Link status
        linkStatus = get_param(blockNames{iBlock},'LinkStatus');
        linkFound  = (strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved'));

        % Path
        path = get_param(blockNames{iBlock}, 'Parent');

        if ~linkFound || breakLinkAction
            % Find link and disable
            if linkFound && breakLinkAction
                for iLink=1:length(links)
                    if strfind(path,links{iLink})
                        set_param(links{iLink},'LinkStatus','inactive');
                    end
                end
            end
            % Set Color
            set_param(getfullname(blockNames{iBlock}),'BackgroundColor',backgroundColor);
        end
    
end

