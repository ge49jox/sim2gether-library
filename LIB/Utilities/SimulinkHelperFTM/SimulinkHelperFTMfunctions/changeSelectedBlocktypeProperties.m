function [] = changeSelectedBlocktypeProperties(system)
    %% Find all blocks
    blockNames = find_system(system,'LookUnderMasks','all', 'FollowLinks','on', 'Selected','on', 'Type','Block');
    if isempty(blockNames)  
        changeBlocktypeProperties(system);
        return
        waitfor(errordlg(sprintf('No selected Blocks.\n\nOperation was canceled'),...
                                 'Error!','modal'))
        return
    end
    
    %% Remove parent
    remove = [];
    for iBlock=1:length(blockNames)
        checkParent = ~cellfun(@isempty, regexp(blockNames,sprintf('^%s$',get_param(blockNames{iBlock},'Parent'))));
        if any(checkParent)
            remove = [remove, find(checkParent==1)];
        end
    end
    if ~isempty(remove)
        remove = sort(unique(remove));
        blockNames{remove} = [];
        blockNames = blockNames(~cellfun(@isempty, blockNames));
    end
    
    %% Check if blocks are in active link
    [breakLinkAction, links] = checkBlockLinks(system, blockNames);
    
    %% Find same properties
    % all properties
    for iBlock=1:length(blockNames)
        simu = get_param(blockNames(iBlock),'ObjectParameters');
        simu = fieldnames(simu{1});
        for iProperty=1:length(simu)
            properties{iBlock}{iProperty} = simu{iProperty,1};
            propertiesString{iBlock}{iProperty}=sprintf('%s',simu{iProperty,1});
        end    
    end   
    % Same properties
    same = ones(size(properties{1,1}));
    for iProperty=1:length(properties{1})
        for jProperty = 2:length(properties)
          same(iProperty) = same(iProperty) & any(~cellfun(@isempty, regexp(properties{1,jProperty}, sprintf('^%s$',properties{1}{iProperty}))));
        end
    end
    [sameProperties, ind] = sort(properties{1,1}(find(same==1)));
    samePropertiesString = propertiesString{1,1}(find(same==1)); 
    samePropertiesString = samePropertiesString(ind);
    
    %% Change Property
    finished_1 = 0;
    while finished_1 == 0
        % Choose property
        choice = listdlg('Name','Auswahl...','PromptString','Choose property to change...',...
                 'SelectionMode','single','ListSize',[300 300],...
                 'ListString',samePropertiesString);
        if isempty(choice),return,end
        
        % Change
        finished_2 = 0;
        while finished_2 == 0
            prop = inputdlg(sprintf('New value for property "%s":',sameProperties{choice}),'Choose Property...',[1 100],{'User input'});
            if isempty(prop), finished = 1;return, end

            try
                for iBlock=1:length(blockNames)

                    % Link status
                    linkStatus = get_param(blockNames{iBlock},'LinkStatus');
                    linkFound  = (strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved'));

                    % Path
                    path = get_param(blockNames{iBlock}, 'Parent');

                    if ~linkFound || breakLinkAction
                        % Find Link and disable
                        if linkFound && breakLinkAction
                            for iLink=1:length(links)
                                if strfind(path,links{iLink})
                                    set_param(links{iLink},'LinkStatus','inactive');
                                end
                            end
                        end
                        if any(~cellfun(@isempty, regexp(simu, sprintf('^%s$',sameProperties{choice}))))
                            set_param(getfullname(blockNames{iBlock}),sameProperties{choice},sprintf('%s',prop{1}));
                        end
                    end
                end
            finished_2 = 1;
            catch   
                button = questdlg(sprintf('While changing property "%s" to "%s" an error occured.\n\nTry again?',sameProperties{choice},prop{1}),...
                                  'Try again','Yes','No','No');
                switch button
                    case 'Yes'
                        finished_2 = 0;
                    case 'No'
                        finished_2 = 1;
                    otherwise
                        finished_2 = 1;
                end
            end
        end
        % Next property
        button = questdlg('Change another property?', 'Go on...','Yes','No','No');
        switch button
            case 'Yes'
                finished_1 = 0;
            case 'No'
                finished_1 = 1;
            otherwise
                finished_1 = 1;
        end
    end
end