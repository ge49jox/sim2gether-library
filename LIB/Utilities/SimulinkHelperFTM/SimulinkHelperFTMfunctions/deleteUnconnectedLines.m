%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Developed by Per-Anders Ekstrm, 2003-2006 Facilia AB.           %
%       Customized by Konstantin Riedl                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function deleteUnconnectedLines(system,enableAddterms)
    %% Find Lines
    lines = find_system(gcs, 'LookUnderMasks','all', 'FollowLinks','on', 'FindAll', 'on', 'Type', 'line');
    if isempty(lines), return, end
    
    %% Check if Line is in active Link
    [breakLinkAction, links] = checkBlockLinks(system, []);
    if ~isempty(links)
        for iLines=1:length(lines)
            parent = get_param(lines(iLines),'Parent');
            if strfind(parent,'/')
                linkStatus = get_param(parent,'LinkStatus');
            else
                linkStatus = '';
            end
            if strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved')
                button = questdlg(sprintf('Found active library links.\n\nDisable links if necessaryfor model?'),...
                                  'Active Links','Yes','No','No');
                switch button
                    case 'Yes'
                        breakLinkAction = true;
                        break
                    case 'No'
                        breakLinkAction = false;
                        break
                    case ''
                        return
                end
            end
        end
    end
    
    %% Delete open lines
    for iLine=1:length(lines)
        % Linie l�schen
        if ishandle(lines(iLine))
            deleteRecursive(lines(iLine), links, breakLinkAction)
        end
    end
    
    %% Add terminators
    if enableAddterms
        addtermsCustomized(system,breakLinkAction,links)
    end
end


function deleteRecursive(line, links, breakLinkAction)
%DELETE_RECURSIVE( LINE )  Delete line if:
%   1) do not have any source-block
%   2) do not have any line-children AND no destination-block
%   otherwise go recursively through all eventual line-children
% Link status
if ishandle(line)
    parent = get_param(line,'Parent');
    if strfind(parent,'/')
        linkStatus = get_param(parent,'LinkStatus');
    else
        linkStatus = '';
    end
    linkFound  = (strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved'));

    % Pfad
    path = get_param(line, 'Parent'); 

    if ~linkFound || breakLinkAction
        % Find link and disable
        if linkFound && breakLinkAction
            for iLink=1:length(links)
                if strfind(path,links{iLink}) && (get( line, 'SrcPortHandle' ) < 0 || (isempty(get(line, 'LineChildren')) && get( line, 'DstPortHandle' ) < 0))
                    set_param(links{iLink},'LinkStatus','inactive');
                end
            end
        end
        if get( line, 'SrcPortHandle' ) < 0
            delete_line( line ) ;
            return
        end
        LineChildren = get( line, 'LineChildren' ) ;
        if isempty( LineChildren )
            if get( line, 'DstPortHandle' ) < 0
                delete_line( line ) ;
            end
        else
            for i=1:length( LineChildren )
                deleteRecursive( LineChildren( i ), links, breakLinkAction)
            end
        end
    end
end
end
