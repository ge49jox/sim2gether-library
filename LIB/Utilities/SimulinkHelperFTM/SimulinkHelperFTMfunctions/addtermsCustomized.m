%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%                  Matlab's addterms adjusted                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function addtermsCustomized(system,breakLinkAction,links)
    %% Check if active link is in system
    if ~isempty(get_param(system,'Parent'))
        system = getfullname(system);                                       %Pfad und name des Systems ermitteln
        %system = [get_param(system,'Parent'),'/',get_param(system,'Name')]; %Dieser Befehl hat Probleme, wenn sich'/' im Namen befindet
    end
    if isempty(breakLinkAction)
        [breakLinkAction, links] = checkBlockLinks(system, find_system(system));
    end
    blockHandles = find_system(get_param(system,'Handle'), 'LookUnderMasks','all', 'FollowLinks','on', 'SearchDepth','1', 'Type','block');
    blockHandles = blockHandles(blockHandles~=get_param(system,'Handle'));
    lines = get_param(get_param(system,'Handle'),'Lines');

    %% Check block connections
    for iBlock=1:size(blockHandles,1)
        % Link status
        parent = get_param(blockHandles(iBlock),'Parent');
        if strfind(parent,'/')
            linkStatus = get_param(parent,'LinkStatus');
        else
            linkStatus = '';
        end
        linkFound  = (strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved'));

        % Path
        path = get_param(blockHandles(iBlock), 'Parent'); 

        % Search through Port Handles
        portHandles = get_param(blockHandles(iBlock),'PortHandles');
        % Treat all enable + trigger + Action input ports the same they will be checked
        % against the dst of the line
        inputPortHandles   = portHandles.Inport;
        triggerPortHandles = portHandles.Trigger;
        enablePortHandles  = portHandles.Enable;
        actionPortHandles  = portHandles.Ifaction;
        inputPortHandles   = [inputPortHandles , triggerPortHandles, enablePortHandles, actionPortHandles];
        numInputs=size(inputPortHandles, 2);
        % Here deal with output port which includes state ports
        statePortHandles = portHandles.State;
        outputPortHandles = portHandles.Outport;
        outputPortHandles = [outputPortHandles, statePortHandles];
        numOutputs=size(outputPortHandles, 2);

        for port=1:numInputs
            if ~BlockInputIsConnected(blockHandles(iBlock), inputPortHandles(port), lines)
                 % find links and disable
                 if ~linkFound || breakLinkAction
                     if linkFound && breakLinkAction
                        for j=1:length(links)
                            if strfind(path,links{j})
                                set_param(links{j},'LinkStatus','inactive');
                            end
                        end
                     end
                     AddGroundToInputPort(blockHandles(iBlock), inputPortHandles(port));
                 end
            end
        end

        for port=1:numOutputs
            if ~BlockOutputIsConnected(blockHandles(iBlock), outputPortHandles(port), lines)
                % find links and disable
                if ~linkFound || breakLinkAction
                     if linkFound && breakLinkAction
                        for j=1:length(links)
                            if strfind(path,links{j})
                                set_param(links{j},'LinkStatus','inactive');
                            end
                        end
                     end
                    AddTerminatorToOutputPort(blockHandles(iBlock), outputPortHandles(port));
                end
            end
        end
        
        % Repeat if subsystem  
        if strcmp(get_param(blockHandles(iBlock),'BlockType'),'SubSystem'),
        	addtermsCustomized(blockHandles(iBlock),breakLinkAction,links);
        end

    end
end

% end addterms

%
%=============================================================================
% BlockInputIsConnected
% Test that a specific block input port is connected.  The array of lines
% for the system that the block lives in is required.
%=============================================================================
%
function connected=BlockInputIsConnected(block,blockPortHandle,lines)

connected = ~isempty(get_param(blockPortHandle,'siggenportname'));
if connected,
  return;
end

for i=1:size(lines,1),
  lineInfo = lines(i).Handle;
  line = get_param(blockPortHandle,'Line');
  %
  % the line is not a branch parent if the Branch field is empty,
  % in that case, all that needs to be done is to test for the
  % destination block/port of this line to be connected to the
  % specified block.
  %
  if isempty(lines(i).Branch),

      if ((~isempty (lines(i).DstBlock)) && (lines(i).DstBlock==block) &&  ...
        lineInfo == line)
      connected = 1;
    end
  else
    connected=BlockInputIsConnected(block, blockPortHandle,lines(i).Branch);
  end

  if connected,
    break;
  end
end
end

% end BlockInputIsConnected

%
%=============================================================================
% AddGroundToInputPort
% Connect a Ground block to a specific input port on a block.
%=============================================================================
%
function AddGroundToInputPort(block, portHandle)

%
% need the parent system
%
sys = get_param(block,'Parent');

%
% need the port position to place the Ground block
%
%portPos=get_param(block,'InputPorts');
portPos = get_param(portHandle,'Position');

%
% the Ground position will start as [0 0 10 10] and be modifed
% down below
%
position=[0 0 10 10];

%
% Determine if the port is on the left/right side or if it is on the 
% top/bottom of the block
%
Port = GetPortLocation(block, portPos, 'input');

%
% need the block orientation to help in placing the Ground
%
DefOrient = get_param(block,'Orientation');

switch get_param(block,'Orientation')

  case 'left',
    if Port.Down
      DefOrient = 'up';
    end
    position(1) = position(1) + portPos(1) + 10 - 15*Port.Down;
    position(2) = position(2) + portPos(2) -  5 + 15*Port.Down;
    position(3) = position(3) + portPos(1) + 10 - 15*Port.Down;
    position(4) = position(4) + portPos(2) -  5 + 15*Port.Down;

  case 'right',
    if Port.Top
      DefOrient = 'down';
    end
    position(1) = position(1) + portPos(1) - 20 + 15*Port.Top;
    position(2) = position(2) + portPos(2) -  5 - 15*Port.Top;
    position(3) = position(3) + portPos(1) - 20 + 15*Port.Top;
    position(4) = position(4) + portPos(2) -  5 - 15*Port.Top;

  case 'up',
    if Port.Left
      DefOrient = 'right';
    end
    position(1) = position(1) + portPos(1) -  5 - 15*Port.Left;
    position(2) = position(2) + portPos(2) + 10 - 15*Port.Left;
    position(3) = position(3) + portPos(1) -  5 - 15*Port.Left;
    position(4) = position(4) + portPos(2) + 10 - 15*Port.Left;

  case 'down'
    if Port.Right
      DefOrient = 'left';
    end
    position(1) = position(1) + portPos(1) -  5 + 15*Port.Right;
    position(2) = position(2) + portPos(2) - 20 + 15*Port.Right;
    position(3) = position(3) + portPos(1) -  5 + 15*Port.Right;
    position(4) = position(4) + portPos(2) - 20 + 15*Port.Right;

end



numGrounds=size(find_system(sys,'SearchDepth',1,'LookUnderMasks','all', 'FollowLinks','on','BlockType','Ground'),1);
blocknumber = numGrounds + 1;
ground = sprintf('Ground_%d', blocknumber);

while ~isempty(find_system(sys,'SearchDepth',1,'LookUnderMasks','all', 'FollowLinks','on','Name',ground))
    % there is a name clash
    % keep on incrementing the number until there is no name clash
    blocknumber = blocknumber + 1;
    ground = sprintf('Ground_%d', blocknumber);
end

add_block('built-in/Ground',[sys '/' ground],...
          'Position',position,...
          'ShowName','off', ...
          'Orientation', DefOrient);

%GroundPortPos = get_param([sys '/' ground],'OutputPorts'); obsolete usage of OutputPorts
GroundPortHandles = get_param([sys '/' ground],'PortHandles');
GroundPortPos = get_param(GroundPortHandles.Outport,'Position');

add_line(sys,[GroundPortPos;portPos]);
end
% end AddGroundToInputPort

%
%=============================================================================
% BlockOutputIsConnected
% Test that a specific block input port is connected.  The array of lines
% for the system that the block lives in is required.
%=============================================================================
%
function connected=BlockOutputIsConnected(block,blockPortHandle,lines)
connected=0;
for i=1:size(lines,1),
   lineInfo = lines(i).Handle;
   line = get_param(blockPortHandle,'Line');

  %
  % the line is not a branch parent if the Branch field is empty,
  % in that case, all that needs to be done is to test for the
  % destination block/port of this line to be connected to the
  % specified block.
  %
  if ((~isempty (lines(i).SrcBlock)) && (lines(i).SrcBlock==block) &&...
    isequal(line, lineInfo))
    connected = 1;
  end
end
end
% end BlockOutputIsConnected

%
%=============================================================================
% AddTerminatorToOutputPort
% Connect a Terminator block to a specific output port on a block.
%=============================================================================
%
function AddTerminatorToOutputPort(block, portHandle)

%
% need the parent system
%
sys = get_param(block,'Parent');

%
% need the port position to place the Ground block
%
%portPos=get_param(block,'OutputPorts');
portPos = get_param(portHandle, 'Position');

%
% Determine if the port is on the left/right side or if it is on the 
% top/bottom of the block
%
Port = GetPortLocation(block, portPos, 'output');

%
% the Ground position will start as [0 0 10 10] and be modifed
% down below
%
position=[0 0 10 10];

%
% need the block orientation to help in placing the Terminator
%
DefOrient = get_param(block, 'Orientation');
switch get_param(block,'Orientation')

  case 'left',
    if Port.Down
      DefOrient = 'down';
    end
    position(1) = position(1) + portPos(1) - 20 + 15*Port.Down;
    position(2) = position(2) + portPos(2) -  5 + 15*Port.Down;
    position(3) = position(3) + portPos(1) - 20 + 15*Port.Down;
    position(4) = position(4) + portPos(2) -  5 + 15*Port.Down;

  case 'right',
    if Port.Top
      DefOrient = 'up';
    end
    position(1) = position(1) + portPos(1) + 10 - 15*Port.Top;
    position(2) = position(2) + portPos(2) -  5 - 15*Port.Top;
    position(3) = position(3) + portPos(1) + 10 - 15*Port.Top;
    position(4) = position(4) + portPos(2) -  5 - 15*Port.Top;

  case 'up',
    if Port.Left
      DefOrient = 'left';
    end
    position(1) = position(1) + portPos(1) -  5 - 15*Port.Left;
    position(2) = position(2) + portPos(2) - 20 + 15*Port.Left;
    position(3) = position(3) + portPos(1) -  5 - 15*Port.Left;
    position(4) = position(4) + portPos(2) - 20 + 15*Port.Left;

  case 'down'
    if Port.Right
      DefOrient = 'right';
    end
    position(1) = position(1) + portPos(1) -  5 + 15*Port.Right;
    position(2) = position(2) + portPos(2) + 10 - 15*Port.Right;
    position(3) = position(3) + portPos(1) -  5 + 15*Port.Right;
    position(4) = position(4) + portPos(2) + 10 - 15*Port.Right;

end

numTerms=size(find_system(sys,'SearchDepth',1,'LookUnderMasks','all', 'FollowLinks','on','BlockType','Terminator'),1);
blocknumber = numTerms+1;
term=sprintf('Terminator_%d', blocknumber);

while ~isempty(find_system(sys,'SearchDepth',1,'LookUnderMasks','all', 'FollowLinks','on','Name',term))
    % there is a name clash
    % keep on incrementing the number until there is no name clash
    blocknumber = blocknumber + 1;
    term=sprintf('Terminator_%d', blocknumber);
end

add_block('built-in/Terminator',[sys '/' term],...
          'Position',position,...
          'ShowName','off',...
          'Orientation', DefOrient);

%TermPortPos = get_param([sys '/' term],'InputPorts'); obsolete usage of InputPorts
TermPortHandles = get_param([sys '/' term],'PortHandles');
TermPortPos = get_param(TermPortHandles.Inport,'Position');

add_line(sys,[portPos;TermPortPos]);
end
% end AddTerminatorToOutputPort

function Port = GetPortLocation(block, portPos, side)
%
% Determine the Orientation of the Port w.r.t the block itself
%
BlockPos = get_param(block, 'Position');

if strcmp(side, 'input')
  offset   = 5;
else
  offset = -5;
  BlockPos = BlockPos([3 4 1 2]);
end

if (BlockPos(1) == (portPos(1) + offset))
  Port.Top = 0;
else
  Port.Top = 1;
end
if (BlockPos(2) == (portPos(2) + offset))
  Port.Right = 0;
else
  Port.Right = 1;
end
if (BlockPos(3) == (portPos(1) - offset))
  Port.Down = 0;
else
  Port.Down = 1;
end
if (BlockPos(4) == (portPos(2) - offset))
  Port.Left = 0;
else
  Port.Left = 1;
end
end

