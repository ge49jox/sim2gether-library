function []=addMuxAndScope(system)    
    %% Find selected lines
    lines = find_system(system, 'LookUnderMasks','all', 'FollowLinks','on', 'FindAll','on', 'Type','line', 'Selected','on');%find_system(gcs, 'FollowLinks', 'on', 'FindAll', 'on', 'Type', 'line', 'Selected', 'on');
    if isempty(lines)
        waitfor(errordlg(sprintf('No signals were selected.\n\nOperation was canceled!'),...
                                 'Error!','modal'))
        return
    end
    
    %% Check if all lines are in the same subsystem
    if length(lines)>1 && length(unique(get_param(lines, 'Parent')))~=1
        waitfor(errordlg(sprintf('Selected lines are located in different Subsystems.\n\nOperation was canceled!'),...
                                 'Error!','modal'))
        return
    end
    
    %% Check if Lines are in active Link
    [breakLinkAction, links] = checkBlockLinks(system, []);
    if ~isempty(links)
        for iLink=1:length(lines)
            parent = get_param(lines(iLink),'Parent');
            if strfind(parent,'/')
                linkStatus = get_param(parent,'LinkStatus');
            else
                linkStatus = '';
            end
            if strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved')
                button = questdlg(sprintf('Minimum one block is located in an active library link.\n\nDisable links for model?'),...
                                  'Active Links','Yes','No','No');
                switch button
                    case 'Yes'
                        breakLinkAction = true;
                        break
                    case 'No'
                        breakLinkAction = false;
                        break
                    case ''
                        return
                end
            end
        end
    end
    
    %% Add Scope
    % Link status
    if all(ishandle(lines))
        parent = get_param(lines(1),'Parent');
        if strfind(parent,'/')
            linkStatus = get_param(parent,'LinkStatus');
        else
            linkStatus = '';
        end
        linkFound  = (strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved'));

        % Pfad
        path = get_param(lines(1), 'Parent'); 

        if ~linkFound || breakLinkAction
            % Find link and disable
            if linkFound && breakLinkAction
                for iLink=1:length(links)
                    if strfind(path,links{iLink})
                        set_param(links{iLink},'LinkStatus','inactive');
                    end
                end
            end
            % Add scope
            addScope(lines, path);
        end
    end
end

function addScope(lines, path)
    % Simulink Library 
    simulink('open')
    % Scope Parameters
    blockName = 'Scope';
    blockType = 'Scope';
    Library    = 'simulink/Sinks/Scope';
    nameScope = findBlockName(path,blockName,blockType);
    
    % Scope (one singnal)
    if length(lines) == 1

        % Add Scope
        add_block(Library, [path,'/',nameScope]);
        pos = get_param(lines, 'Points');
        pos = [floor(pos(1,1)),floor(pos(1,2))]+[15 -66];
        set_param([path,'/',nameScope],'Position',[pos(1) pos(2) pos(1)+30 pos(2)+32],'LimitDataPoints','off');

        % Linien hinzufügen
        toScope  = get_param([path,'/',nameScope], 'PortHandles');
        fromLine = get_param(lines,'SrcPortHandle');
        add_line(path, fromLine, toScope.Inport);

    % Mux/Scope-Combination (multiple Signals)
    elseif length(lines)>=1

        % Get Positions
        pos = get_param(lines(1), 'Points');
        posScope = [floor(pos(1,1)),floor(pos(1,2))]+[30*length(lines) -66];

        % Scope hinzufügen und Parameter setzen
        add_block(Library, [path,'/',nameScope]);

        set_param([path,'/',nameScope],'Position',[posScope(1) posScope(2) posScope(1)+30 posScope(2)+32],...
                  'LimitDataPoints','off',...
                  'Selected','on');

        % add Mux
        nameMux = findBlockName(path,'Mux','Mux');


        % Mux hinzufügen und Parameter setzen
        Library    = 'simulink/Signal Routing/Mux';
        posMux = [floor(pos(1,1)),floor(pos(1,2))]+[30*(length(lines)-1) -69];
        add_block(Library, [path,'/',nameMux]);
        numberLines = length(unique(cell2mat(get_param(lines,'SrcPortHandle'))));
        set_param([path,'/',nameMux],'Position',[posMux(1) posMux(2) posMux(1)+5 posMux(2)+38],...
                  'Inputs',sprintf('%d',numberLines),...
                  'Selected','on');

        % add Line from Mux to Scope
        hHandle  = get_param([path,'/',nameMux], 'PortHandles');
        fromMux  = hHandle.Outport;
        toScope  = get_param([path,'/',nameScope], 'PortHandles');
        add_line(path, fromMux, toScope.Inport);

        % Linien hinzufügen - von den Signalen zum Mux
        fromLine = unique(cell2mat(get_param(lines,'SrcPortHandle')));
        toMux    = hHandle.Inport;

        for iLine = 1:numberLines
            hHandle = add_line(path, fromLine(iLine),toMux(iLine),'autorouting','on');
            pos = get_param(hHandle,'Points');
            posL = [pos(4,1)-10*(length(lines)+1-iLine),pos(1,2);...
                    pos(4,1)-10*(length(lines)+1-iLine),pos(2,2);...
                    pos(4,1)-10*(length(lines)+1-iLine),pos(3,2);...
                    pos(4,1),pos(4,2)];
            set_param(hHandle, 'Points', posL);
        end
    end
end
function [blockNameNew] = findBlockName(path,blockName,blockType)
    % Initialization
    blockNameNew = [blockName,'1'];
    % find new Scope name
    numScope = 1;
    while true   
        blockNameNew = sprintf('%s%d',blockName,numScope);
        if isempty(find_system(path, 'BlockType',blockType,'Name',blockNameNew))
            break
        end
        numScope = numScope + 1;
    end 
end
