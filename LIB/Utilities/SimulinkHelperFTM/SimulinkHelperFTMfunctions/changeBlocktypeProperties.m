function [] = changeBlocktypeProperties(system)
    %% Find all Blocks
    allBlocks = find_system(system, 'LookUnderMasks','all', 'FollowLinks','on', 'Type','block');
    [blockType,indexBlock] = unique(get_param(allBlocks,'BlockType'));
    if isempty(blockType)
        waitfor(errordlg(sprintf('No simulink blocks were found.\n\nOperation canceled!'),...
                                 'Error!','modal'))
        return
    end
    
    %% Select Block
    choice = listdlg('Name','Select...','PromptString','Select block-type',...
                     'SelectionMode','single','ListSize',[220 300],...
                     'ListString',blockType);
    if isempty(choice),return,end
    properties = get_param(allBlocks(indexBlock(choice)),'ObjectParameters');
    properties = sort(fieldnames(properties{1}));
    if isempty(properties)
        waitfor(errordlg(sprintf('The selected blcok has no properties to change.\n\nOperation canceled!'),...
                                 'Error!','modal'))
        return
    end
    blockNames = regexp(get_param(allBlocks,'BlockType'),sprintf('^%s$',blockType{choice}));
    blockNames = allBlocks(~cellfun('isempty', blockNames));
    
    %% Check if Line is in active Link
    [breakLinkAction, links] = checkBlockLinks(system, blockNames);
    
    %% Change Property
    finished_1 = 0;
    while finished_1 == 0
        % Choose property
        choice = listdlg('Name','Auswahl...','PromptString','Choose property to change...',...
                 'SelectionMode','single','ListSize',[300 300],...
                 'ListString',properties);
        if isempty(choice),return,end
        
        % Change
        finished_2 = 0;
        while finished_2 == 0
            prop = inputdlg(sprintf('New value for property "%s":',properties{choice}),'Choose Property...',[1 100],{'User input'});
            if isempty(prop), finished = 1;return, end

            try
                for iBlock=1:length(blockNames)

                    % Link status
                    linkStatus = get_param(blockNames{iBlock},'LinkStatus');
                    linkFound  = (strcmp(linkStatus,'implicit') || strcmp(linkStatus, 'resolved'));

                    % Path
                    path = get_param(blockNames{iBlock}, 'Parent');

                    if ~linkFound || breakLinkAction
                        % Find Link and disable
                        if linkFound && breakLinkAction
                            for iLink=1:length(links)
                                if strfind(path,links{iLink})
                                    set_param(links{iLink},'LinkStatus','inactive');
                                end
                            end
                        end
                        set_param(getfullname(blockNames{iBlock}), properties{choice}, sprintf('%s',prop{1}));
                    end
                end
            finished_2 = 1;
            catch
                button = questdlg(sprintf('While changing property "%s" to "%s" an error occured.\n\nTry again?',properties{choice},prop{1}),...
                                  'Try again','Yes','No','No');
                switch button
                    case 'Yes'
                        finished_2 = 0;
                    case 'No'
                        finished_2 = 1;
                    otherwise
                        finished_2 = 1;
                end
            end
        end
        % Next property
        button = questdlg('Change another property?', 'Go on...','Yes','No','No');
        switch button
            case 'Yes'
                finished_1 = 0;
            case 'No'
                finished_1 = 1;
            otherwise
                finished_1 = 1;
        end
    end

