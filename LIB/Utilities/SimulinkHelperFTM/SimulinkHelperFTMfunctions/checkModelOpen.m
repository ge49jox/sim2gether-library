function [modelName, fail] = checkModelOpen(system)
    % Initialization
    fail = false;
    % Find model
    modelName = '';
    if isempty(system)
        modelName  = regexp(gcs,'/','split');
        modelName  = modelName{1}; 
    else
        modelName = system;
    end
    if isempty(modelName)
        fail = true;
        waitfor(errordlg(sprintf('No Simulink-Modell was selected.\n\nOperation was canceled!'),...
                                 'Error!','modal'))
        return
    end
    % Check if model is open
    openStatus = 0;
    openSys = regexp(find_system('FollowLinks','on', 'shown','on'),'/','split');
    for i=1:length(openSys)
        if strcmp(openSys{i}{1},modelName)
            openStatus = 1;
        end
    end
    if openStatus == 0;
        button = questdlg(sprintf('The choosen model isn''t open.\n\nOpen model?'),...
                                 'Model open','Yes','No','No');
        switch button
            case 'Yes'
                open(modelName);
            case 'No'
                fail = true;
                return
            case ''
                fail = true;
                return
        end
    end
end

