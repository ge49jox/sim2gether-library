%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%           Autor: Konstantin Riedl                                     %
%                  riedl@ftm.mw.tum.de                                  %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function varargout = SimulinkHelperFTM(varargin)
% GUI_SIMULINK_HELPERS MATLAB code for SimulinkHelperFTM.fig
%      GUI_SIMULINK_HELPERS, by itself, creates a new GUI_SIMULINK_HELPERS or raises the existing
%      singleton*.
%
%      H = GUI_SIMULINK_HELPERS returns the handle to a new GUI_SIMULINK_HELPERS or the handle to
%      the existing singleton*.
%
%      GUI_SIMULINK_HELPERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_SIMULINK_HELPERS.M with the given input arguments.
%
%      GUI_SIMULINK_HELPERS('Property','Value',...) creates a new GUI_SIMULINK_HELPERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SimulinkHelperFTM_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SimulinkHelperFTM_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SimulinkHelperFTM

% Last Modified by GUIDE v2.5 06-Aug-2018 09:17:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SimulinkHelperFTM_OpeningFcn, ...
                   'gui_OutputFcn',  @SimulinkHelperFTM_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end
% End initialization code - DO NOT EDIT


%% Initilization
function SimulinkHelperFTM_OpeningFcn(hObject, eventdata, handles, varargin)
    % Add Path to folder
    addpath(genpath(fileparts(mfilename('fullpath'))));
    
    % find open Systems
    openSys = regexp(find_system('shown','on'),'/','split');
    namesSys = {};
    for i=1:length(openSys)
        if ~any(cell2mat(regexp(namesSys,sprintf('^%s$',openSys{i}{1}))))
            namesSys{length(namesSys)+1} = openSys{i}{1};
        end
    end
    
    % update handles
    if ~isempty(openSys)
        set(handles.pumFindSystem, 'String',namesSys );
        set(handles.btnDeleteUnconnectedLines,  'Enable', 'on');
        set(handles.btnAddTerminatorsAndGrounds,          'Enable', 'on');
        set(handles.btnCleanUp,                 'Enable', 'on');
        set(handles.btnSetProperties,           'Enable', 'on');
        set(handles.btnAddScope,                'Enable', 'on');
        set(handles.btnBackgroundColors,          'Enable', 'on');
    else
        set(handles.pumFindSystem, 'String', 'No open systems were found!');
        set(handles.btnDeleteUnconnectedLines,  'Enable', 'off');
        set(handles.btnAddTerminatorsAndGrounds,          'Enable', 'off');
        set(handles.btnCleanUp,                 'Enable', 'off');
        set(handles.btnSetProperties,           'Enable', 'off');
        set(handles.btnAddScope,                'Enable', 'off');
        set(handles.btnBackgroundColors,          'Enable', 'off');
    end
    
    handles.colors.inPort  = 'Green';
    handles.colors.outPort = 'Red';
    handles.colors.lookUp  = 'LightBlue';
    handles.colors.goTo    = 'Yellow';
    handles.colors.from    = 'Orange';
    
    
    % Choose default command line output for SimulinkHelperFTM
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);
end
function varargout = SimulinkHelperFTM_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;
end

%% Find system
function btnFindSystem_Callback(hObject, eventdata, handles)
    % Start progressbar
    [progressWindow] = openProgressWindow('Searching open systems...');   
    % Find open systems
    openSys = regexp(find_system('shown','on'),'/','split');
    namesSys = {};
    for i=1:length(openSys)
        if ~any(cell2mat(regexp(namesSys,sprintf('^%s$',openSys{i}{1}))))
            namesSys{length(namesSys)+1} = openSys{i}{1};
        end
    end
    
    % Update handles
    if ~isempty(openSys)
        set(handles.pumFindSystem, 'String',namesSys );
        set(handles.btnDeleteUnconnectedLines,  'Enable', 'on');
        set(handles.btnAddTerminatorsAndGrounds,          'Enable', 'on');
        set(handles.btnCleanUp,                 'Enable', 'on');
        set(handles.btnSetProperties,           'Enable', 'on');
        set(handles.btnAddScope,                'Enable', 'on');
        set(handles.btnBackgroundColors,          'Enable', 'on');
    else
        set(handles.pumFindSystem, 'String', 'No open systems were found!');
        set(handles.btnColorInOutPort,           'Enable', 'off');
        set(handles.btnLabelAll,                'Enable', 'off');
        set(handles.btnLabelAllDelete,          'Enable', 'off');
        set(handles.btnDeleteUnconnectedLines,  'Enable', 'off');
        set(handles.btnAddTerminatorsAndGrounds,          'Enable', 'off');
        set(handles.btnCleanUp,                 'Enable', 'off');
        set(handles.btnSetProperties,           'Enable', 'off');
        set(handles.btnAddScope,                'Enable', 'off');
        set(handles.btnBackgroundColors,          'Enable', 'off');
    end
    % Close progressbar
    close(progressWindow);      
end
function pumFindSystem_Callback(hObject, eventdata, handles)
end
function pumFindSystem_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end


%% Clean up
function btnAddTerminatorsAndGrounds_Callback(hObject, eventdata, handles)
    % Start progressbar
    [progressWindow] = openProgressWindow('Changes in progress...');
    % Get Model
    [systemName] = getSelcetedSystem(handles);
    [modelName, fail] = checkModelOpen(systemName);
    if fail, return, end
    
    addtermsCustomized(modelName,[],[]);
    % Close progressbar
    close(progressWindow);    
end
function btnCleanUp_Callback(hObject, eventdata, handles)
    % Start progressbar
    [progressWindow] = openProgressWindow('Changes in progress...');
    % Get Model
    [systemName] = getSelcetedSystem(handles);
    [modelName, fail] = checkModelOpen(systemName);
    if fail, return, end
    
    deleteUnconnectedLines(modelName,1);
    % Close progressbar
    close(progressWindow);    
end
function btnDeleteUnconnectedLines_Callback(hObject, eventdata, handles)
    % Start progressbar
    [progressWindow] = openProgressWindow('Changes in progress...');
    % Get Model
    [systemName] = getSelcetedSystem(handles);
    [modelName, fail] = checkModelOpen(systemName);
    if fail, return, end
    
    deleteUnconnectedLines(modelName,0);
    % Close progressbar
    close(progressWindow);    
end

%% Extras
function btnSetProperties_Callback(hObject, eventdata, handles)
    % Start progressbar
    [progressWindow] = openProgressWindow('Changes in progress...');
    % Get Model
    [systemName] = getSelcetedSystem(handles);
    [modelName, fail] = checkModelOpen(systemName);
    if fail, return, end
    
    changeSelectedBlocktypeProperties(modelName);
    % Close progressbar
    close(progressWindow);
end
function btnBackgroundColors_Callback(hObject, eventdata, handles)
    % Start progressbar
    [progressWindow] = openProgressWindow('Changes in progress...');
    % Get Model
    [systemName] = getSelcetedSystem(handles);
    [modelName, fail] = checkModelOpen(systemName);
    if fail, return, end
    
    % Change Color
    [fail, breakLinkAction] = changeBackgroundColor(modelName, 'Inport', handles.colors.inPort, []);
    [fail, breakLinkAction] = changeBackgroundColor(modelName, 'Outport', handles.colors.outPort, []);
    [fail, breakLinkAction] = changeBackgroundColor(modelName, 'Lookup_n-D', handles.colors.lookUp, []);
    [fail, breakLinkAction] = changeBackgroundColor(modelName, 'LookupNDDirect', handles.colors.lookUp, []);
    [fail, breakLinkAction] = changeBackgroundColor(modelName, 'Interpolation_n-D', handles.colors.lookUp, []);
    [fail, breakLinkAction] = changeBackgroundColor(modelName, 'PreLookup', handles.colors.lookUp, []);
    [fail, breakLinkAction] = changeBackgroundColor(modelName, 'Goto', handles.colors.goTo, []);
    [fail, breakLinkAction] = changeBackgroundColor(modelName, 'From', handles.colors.from, []);
    % Close progressbar
    close(progressWindow);
end
function btnAddScope_Callback(hObject, eventdata, handles)
    % Start progressbar
    [progressWindow] = openProgressWindow('Changes in progress...');
    % Get Model
    [systemName] = getSelcetedSystem(handles);
    [modelName, fail] = checkModelOpen(systemName);
    if fail, return, end
    
    addMuxAndScope(modelName);
    % Close progressbar
    close(progressWindow);
end

%% Helper Function    
function [systemName] = getSelcetedSystem(handles)
    % Initialization
    systemName = '';
    % Get Data from Handles
    systemNames    = get(handles.pumFindSystem, 'String');
    systemSelected = get(handles.pumFindSystem, 'Value');
    systemName = systemNames{systemSelected};
end
function [progressWindow] = openProgressWindow(text)
    progressWindow = waitbar(0.5,text);
end
