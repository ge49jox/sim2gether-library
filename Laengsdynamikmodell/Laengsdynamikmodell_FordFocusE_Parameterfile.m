%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameter File for Laengsdynamikmodell_FordFocusE		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Allgemeine Parameter
MaxMotTrq = 250;
WheelRadius=0.3284;
VEH_Mass=1700;
CellTemp=298.15;
AuxiliaryPower=400;
J_FourWheels=4.96;
i_Gear=7.82;
initSOC=0.9;

%% Laengsdynamikmodell_FordFocusE/Battery/HV Pack Simple

	%Number Cells Parallel [-]
	set_param('Laengsdynamikmodell_FordFocusE/Battery/HV Pack Simple','Cells_Parallel','37');

	%Number Cells Serial [-]
	set_param('Laengsdynamikmodell_FordFocusE/Battery/HV Pack Simple','Cells_Serial','86');

	%Initial SOC [-]
	set_param('Laengsdynamikmodell_FordFocusE/Battery/HV Pack Simple','InitSOC','initSOC');

	%Choice of Resistance Map
	set_param('Laengsdynamikmodell_FordFocusE/Battery/HV Pack Simple','ResistMap','R_i_20ms');


%% Laengsdynamikmodell_FordFocusE/Control Unit/Crontrol Unit Simple

	%Brake Distribution Front
	set_param('Laengsdynamikmodell_FordFocusE/Control Unit/Crontrol Unit Simple','Brake_Distribution_Front','0.7');

	%Maximal Braking Torque
	set_param('Laengsdynamikmodell_FordFocusE/Control Unit/Crontrol Unit Simple','MaxBrkTrq','1800');

	%Maximal Motor Torque
	set_param('Laengsdynamikmodell_FordFocusE/Control Unit/Crontrol Unit Simple','MaxMotorTrq','MaxMotTrq');


%% Laengsdynamikmodell_FordFocusE/Data Import/Longitudinal Driving Cycles

	%Choose Driving Cycle
	set_param('Laengsdynamikmodell_FordFocusE/Data Import/Longitudinal Driving Cycles','dcname','EUROPE_NEDC');

%% Laengsdynamikmodell_FordFocusE/Driver and Environment/Constant Environment

	%Ground Slope [rad]
	set_param('Laengsdynamikmodell_FordFocusE/Driver and Environment/Constant Environment','Ground_Slope','0');

	%Longitudinal Wind Velocity [m/s]
	set_param('Laengsdynamikmodell_FordFocusE/Driver and Environment/Constant Environment','Long_Wind_Speed','0');


%% Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency

	%Rated Power [kW]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','P_n','71.3333');

	%Rated Speed [1/min]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','n_n','4087');

	%Maximal Speed [1/min]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','n_max','8700');

	%Rated Voltage [V]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','U_n','320');

	%Overload Factor [-]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','Uberlastfaktor','1.5');

	%cphi
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','cphi','0.9');

	%Amount of Phases [-]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','m_ph','3');

	%Amount of Data points per axis in Diagram
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','tics','80');

	%Circuit
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','Schaltung','star');

	%Cooling
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','Kuehlung','liquid');

	%Material of Stator
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','Material_Stator','copper');

	%Enable Current Limitation [-]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Electric Drive Analytic Efficiency','LimitationON','on');

%% Laengsdynamikmodell_FordFocusE/Drivetrain/Gear Stage

	%Efficiency [-]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Gear Stage','eta','0.96');

	%Gear Ratio [-]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Gear Stage','iGear','i_Gear');

	%Reduced Moment of Inertia [kg m^2]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Gear Stage','JredGetr','0.194');


%% Laengsdynamikmodell_FordFocusE/Drivetrain/Wheel System Const R Const Coeff

	%Wheel Radius [m]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Wheel System Const R Const Coeff','Wheel_Radius','WheelRadius');

	%Roll Resistance Coefficient [ ]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Wheel System Const R Const Coeff','Wheel_Roll_Resistance','0.009562');

	%Dynamic Roll Resistance Coefficient [s/m]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Wheel System Const R Const Coeff','Wheel_Roll_Resistance_Dyn','0.000305');

	%Inertia Moment of four Wheels [kg m^2]
	set_param('Laengsdynamikmodell_FordFocusE/Drivetrain/Wheel System Const R Const Coeff','J_FourWheels','4.96');


%% Laengsdynamikmodell_FordFocusE/Dynamics/Acceleration Calculation

	%Initial Speed [m/s]
	set_param('Laengsdynamikmodell_FordFocusE/Dynamics/Acceleration Calculation','InitialCondition','0');


%% Laengsdynamikmodell_FordFocusE/Dynamics/Air Resistance

	%Front Surface [m^2]
	set_param('Laengsdynamikmodell_FordFocusE/Dynamics/Air Resistance','A_st','2.26');

	%Density Air [kg/m^3]
	set_param('Laengsdynamikmodell_FordFocusE/Dynamics/Air Resistance','Roh_Air','1.19');

	%c_w-Factor [-]
	set_param('Laengsdynamikmodell_FordFocusE/Dynamics/Air Resistance','c_w','0.2599');


%% Laengsdynamikmodell_FordFocusE/Dynamics/Roll Resistance

	%Gravity [N/kg]
	set_param('Laengsdynamikmodell_FordFocusE/Dynamics/Roll Resistance','Gravity','9.81');


%% Laengsdynamikmodell_FordFocusE/Dynamics/Slope Resistance

	%Gravity [N/kg]
	set_param('Laengsdynamikmodell_FordFocusE/Dynamics/Slope Resistance','Gravity','9.81');

