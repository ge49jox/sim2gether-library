%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameter File for Laengsdynamikmodell_MercedesB_EDrive		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% General Parameters

% Vehicle
Veh.Mass = 1926; % [kg] Argonne
Veh.Payload = 0; % [kg] 
Veh.FrontSurface = 	2.41; % [m^2] http://www.automobile-catalog.com/auta_details1.php
Veh.c_w = 0.28; % [-] http://www.automobile-catalog.com/auta_details1.php

% Battery 360V, 28kWh, NCR18650PF
Battery.CellsParallel = 31; % [-] roughly calculated from batterycapacity
Battery.CellsSerial = 86; % [-] roughly calculated from batterycapacity
Battery.initSOC = 1; % [-]

% PSM
EM.RatedPower  = 132; % [kW] http://www.automobile-catalog.com/auta_details1.php
EM.RatedSpeed  = 3800; % [1/min] http://www.automobile-catalog.com/auta_details1.php
EM.MaxSpeed = 14500; % [1/min] http://www.automobile-catalog.com/auta_details1.php
EM.RatedVoltage = 360; % [V]
EM.MaxMotTrq = 340;

% Gear
GearSt.i_Gear = 9.73; % [-] http://www.automobile-catalog.com/auta_details1.php

% Wheel System
WhSys.WheelRadius = 0.3285; % [m] http://www.automobile-catalog.com/auta_details1.php
WhSys.RollResist = 0.009562; % [-]
WhSys.dynRollResist = 0.000305; % [-]
WhSys.WheelBase = 2.699; 
WhSys.WBtoFront = WhSys.WheelBase*55/100;
WhSys.WBtoRear = WhSys.WheelBase*45/100;


% Environment
Envir.rho_L = 1.2041; % [kg/m^3]
Envir.gravity = 9.81; % [m/s^2]
Envir.CellTemp = 308.15; % [K]
Envir.TempGearbox = 293.15; % [K]
Envir.InsideTemp = 293.15; %[K]
Envir.CarBodyHeatTransf = 60; % [W/K]
