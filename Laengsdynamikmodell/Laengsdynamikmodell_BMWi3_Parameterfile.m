%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameter File for Laengsdynamikmodell_BMWi3		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load('41_Bmwi3_2014TargetVelo.mat');

%% General Parameters

% Vehicle
Veh.Mass = 1433.3; % [kg] Argonne
Veh.Payload = 0; % [kg] incl. weight of all passengers
Veh.FrontSurface = 2.38; % [m^2] BMW
Veh.c_w = 0.29; % [-] BMW
Veh.BrakeLights = 63; % [W] 

% Battery V=355.2, E=18.1kWh useable
Battery.CellsParallel = 29; % [-] roughly calculated from batterycapacity
Battery.CellsSerial = 85; % [-] roughly calculated from batterycapacity
Battery.initSOC = 0.918; % [-]

% PSM
EM.RatedPower = 125; % [kW] BMW
EM.RatedSpeed = 4800; % [1/min] BMW
EM.MaxSpeed = 11000; % [1/min] guessed from peak power 125kW
EM.RatedVoltage = 353; % [V] BMW
EM.MaxMotTrq = 250; %[Nm] BMW

% Gear
GearSt.i_Gear = 9.665; % [-] BMW

% Wheel System
WhSys.WheelRadius = 0.35; % [m] BMW
WhSys.RollResist = 0.009562; % [-]
WhSys.dynRollResist = 0.000305; % [-]
WhSys.WheelBase = 2.570; % [m] BMW
WhSys.WBtoFront = WhSys.WheelBase*0.5;
WhSys.WBtoRear  =  WhSys.WheelBase*0.5;

% Environment
Envir.rho_L = 1.2041; % [kg/m^3]
Envir.gravity = 9.81; % [m/s^2]
Envir.CellTemp = 308.15; % [K]
Envir.TempGearbox = 293.15; % [K]
Envir.InsideTemp = 293.15; %[K]
Envir.CarBodyHeatTransf = 60; % [W/K]
