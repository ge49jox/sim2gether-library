%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameterfile for Laengsdynamikmodell_BMWi3		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% General Parameters
% Vehicle
Veh.Mass = 1927.8; % [kg] Argonne
Veh.Payload = 0; % [kg] incl. weight of all passengers
Veh.FrontSurface = 2.34; % [m^2] autocatalog
Veh.c_w = 0.32; %[-] autocatalog

% Motor
Mot.PeakPower = 300*0.7355; % [kw] Argonne
Mot.MaxMotTrq = 264*1.355818; % [Nm] Argonne

% Gearstage
GearSt.NomimalSpeed = 4200; % [1/min] Argonne
GearSt.i_Gear = [4.71 3.14 2.10 1.67 1.29 1.00 0.84 0.67]*2.65; % [-] Argonne

WhSys.WheelRadius = 72.7/100/2; % [m] autocatalog
WhSys.Wheelbase = 3.052; % [m] autocatalog
WhSys.WBtoFront = WhSys.Wheelbase/2 ; % [m] autocatalog
WhSys.WBtoRear = WhSys.Wheelbase/2 ; % [m] autocatalog

% Environment
Envir.rho_L = 1.2041; % [kg/m^3]
Envir.gravity = 9.81; % [m/s^2]
Envir.CellTemp = 308.15; % [K]
Envir.TempGearbox = 293.15; % [K]
Envir.InsideTemp = 293.15; %[K]
Envir.CarBodyHeatTransf = 60; % [W/K]
    