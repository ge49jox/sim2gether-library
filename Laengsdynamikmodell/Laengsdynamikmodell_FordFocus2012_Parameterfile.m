%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameterfile for Laengsdynamikmodell_BMWi3		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% General Parameters
% Vehicle
Veh.Mass = 1474.2; % [kg] Argonne
Veh.Payload = 0; % [kg] incl. weight of all passengers
Veh.FrontSurface = 2.22; % [m^2] autocatalog
Veh.c_w = 0.31; %[-] autocatalog

% Powertrain
Mot.PeakPower =  117.68; % [kw] Argonne
Mot.MaxMotTrq = 197.94; % [Nm] Argonne

GearSt.NomimalSpeed = 4450; % [1/min] Argonne
GearSt.i_Gear = [3.917*3.850 2.429*3.850 1.436*4.278 1.021*4.278 0.867*3.850 0.702*3.850]; % [-] Argonne

WhSys.WheelRadius = 0.3215; % [m] autocatalog
WhSys.Wheelbase = 2.649 ; % [m] autocatalog
WhSys.WBtoFront = 1.555  ; % [m] autocatalog
WhSys.WBtoRear = 1.534 ; % [m] autocatalog

% Environment
Envir.rho_L = 1.2041; % [kg/m^3]
Envir.gravity = 9.81; % [m/s^2]
Envir.CellTemp = 308.15; % [K]
Envir.TempGearbox = 293.15; % [K]
Envir.InsideTemp = 293.15; %[K]
Envir.CarBodyHeatTransf = 60; % [W/K]
    