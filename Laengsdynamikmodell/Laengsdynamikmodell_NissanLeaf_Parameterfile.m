%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameter File for Laengsdynamikmodell_Nissan Leaf		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% General Parameters

% Vehicle
Veh.Mass = 1700; % [kg] Argonne
Veh.Payload = 0; % [kg]
Veh.FrontSurface = 	2.28; % [m^2]   http://www.automobile-catalog.com/auta_details1.php
Veh.c_w = 0.28; % [-]               http://www.automobile-catalog.com/auta_details1.php
Veh.BrakeLights = 63; % [W]

% Battery 24 kWh, 345 V,
Battery.CellsParallel = 40; % [-] Datenblatt analysieren
Battery.CellsSerial = 83; % [-] Datenblatt analysieren
Battery.initSOC = 0.971; % [-]

% Annahme PSM, nur Tesla setzt auf ASM, normale eher auf PSM
EM.RatedPower = 80; % [kW] Nissen Leaf Fahrzeugschein
EM.RatedSpeed = 2750; % [1/min]    Nissan
EM.MaxSpeed = 9800; % [1/min]      Nissan
EM.RatedVoltage = 345; % [V]       Nissan
EM.MaxMotTrq = 280;

% Gear
GearSt.i_Gear = 7.9377; % [-]            Nissan

% Wheel System
WhSys.WheelRadius = 0.316; % [m] Nissan
WhSys.RollResist = 0.009562; % [-]
WhSys.dynRollResist = 0.000305; % [-]
WhSys.WheelBase = 2700;
WhSys.WBtoFront = WhSys.WheelBase*0.55;
WhSys.WBtoRear = WhSys.WheelBase*0.45;

% Environment
Envir.rho_L = 1.2041; % [kg/m^3]
Envir.gravity = 9.81; % [m/s^2]
Envir.CellTemp = 308.15; % [K]
Envir.TempGearbox = 293.15; % [K]
Envir.InsideTemp = 293.15; %[K]
Envir.CarBodyHeatTransf = 60; % [W/K]
