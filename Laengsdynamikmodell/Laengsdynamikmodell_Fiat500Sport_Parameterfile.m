%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%		Parameterfile for Laengsdynamikmodell_BMWi3		%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% General Parameters
% Vehicle
Veh.Mass = 1247.4; % [kg] Argonne
Veh.Payload = 0; % [kg] incl. weight of all passengers
Veh.FrontSurface = 2.05; % [m^2] autocatalog
Veh.c_w = 0.362;

% Powertrain
Mot.PeakPower = 75; % [kw] Argonne
Mot.MaxMotTrq = 133; % [Nm] Argonne

% Gearstage
GearSt.NomimalSpeed = 4000; % [1/min] Argonne
GearSt.i_Gear = [4.04 2.37 1.55 1.15 0.85 0.67]*4.1; % [-] Argonne

WhSys.WheelRadius = 19.5/100/2; % [m] autocatalog
WhSys.Wheelbase = 2.300; % [m] autocatalog
WhSys.WBtoFront = 1.407; % [m] autocatalog
WhSys.WBtoRear = 1.397; % [m] autocatalog

% Environment
Envir.rho_L = 1.2041; % [kg/m^3]
Envir.gravity = 9.81; % [m/s^2]
Envir.CellTemp = 308.15; % [K]
Envir.TempGearbox = 293.15; % [K]
Envir.InsideTemp = 293.15; %[K]
Envir.CarBodyHeatTransf = 60; % [W/K]
    